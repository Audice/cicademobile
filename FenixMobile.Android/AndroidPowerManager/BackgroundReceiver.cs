﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;

namespace FenixMobile.Droid.AndroidPowerManager
{
    [BroadcastReceiver(Enabled = true)]
    public class BackgroundReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.SetAutoCancel(true)
                .SetDefaults((int)NotificationDefaults.All)
                .SetSmallIcon(Resource.Drawable.icon)
                .SetContentTitle("Alarm actived!")
                .SetContentText("this is my alarm")
                .SetContentInfo("Штащ");

            //NotificationManager manager = (NotificationManager)context.GetSystemService(Context.NotificationService);
            //manager.Notify(1, builder.Build());
        }
    }
}