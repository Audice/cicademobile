﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace FenixMobile.Droid.ForegroundIntent
{
    [Service]
    public class HeadService : Service
    {
        static readonly string TAG = typeof(HeadService).FullName;

        UtcTimestamper timestamper;
        bool isStarted;
        Handler handler;
        Action runnable;
        bool isVisualizationPulse;

        object locker = new object();

        public override void OnCreate()
        {
            base.OnCreate();
            timestamper = new UtcTimestamper();
            handler = new Handler();

            //ActivityManager am = (ActivityManager)this.GetSystemService(Context.ActivityService);
            //IList<ActivityManager.RunningTaskInfo> taskInfo = am.GetRunningTasks(1);
            //ComponentName componentInfo = taskInfo[0].TopActivity;
            //string componentname = componentInfo.PackageName;


            // This Action is only for demonstration purposes.
            runnable = new Action(() =>
            {
                if (timestamper == null)
                {
                }
                else
                {
                    string msg = timestamper.GetFormattedTimestamp();
                    Intent i = new Intent(Constants.NOTIFICATION_BROADCAST_ACTION);
                    i.PutExtra(Constants.BROADCAST_MESSAGE_KEY, msg);
                    Android.Support.V4.Content.LocalBroadcastManager.GetInstance(this).SendBroadcast(i);
                    handler.PostDelayed(runnable, Constants.DELAY_BETWEEN_LOG_MESSAGES);
                }
            });
        }


        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            if (intent.Action.Equals(Constants.ACTION_START_SERVICE))
            {
                if (isStarted)
                {
                    //??? Что делать если сервис уже запущени? /// Пока не придумал, будем убивать приложение в системе
                }
                else
                {
                    RegisterForegroundService("Сервис мониторинга запущен", "Приложение готово к работе");
                    handler.PostDelayed(runnable, Constants.DELAY_BETWEEN_LOG_MESSAGES);
                    isStarted = true;
                }
            }
            else if (intent.Action.Equals(Constants.ACTION_STOP_SERVICE))
            {
                Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
                //MainActivity.CurrentMainActivity.Destroyer();
            }
            else if (intent.Action.Equals(Constants.ACTION_RESTART_TIMER))
            {
                timestamper.Restart();
            }

            // This tells Android not to restart the service if it is killed to reclaim resources.
            return StartCommandResult.Sticky;
        }


        public override IBinder OnBind(Intent intent)
        {
            // Return null because this is a pure started service. A hybrid service would return a binder that would
            // allow access to the GetFormattedStamp() method.
            return null;
        }


        public override void OnDestroy()
        {

            // Stop the handler.
            handler.RemoveCallbacks(runnable);

            // Remove the notification from the status bar.
            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Cancel(Constants.SERVICE_RUNNING_NOTIFICATION_ID);

            timestamper = null;
            isStarted = false;
            base.OnDestroy();
        }

        /// <summary>
        /// This method will return a formatted timestamp to the client.
        /// </summary>
        /// <returns>A string that details what time the service started and how long it has been running.</returns>
        string GetFormattedTimestamp()
        {
            return timestamper?.GetFormattedTimestamp();
        }

        void RegisterForegroundService(string LocalContentText, string DiagnosisString)
        {

            var channelId = "";
            if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.O)
            {
                channelId = createNotificationChannel();
                var notification = new Notification.Builder(this, channelId)
                    .SetContentTitle(LocalContentText)
                    .SetContentText(DiagnosisString)
                    .SetSmallIcon(Resource.Drawable.Fenix)
                    .SetContentIntent(BuildIntentToShowMainActivity())
                    .SetOngoing(true)
                    .AddAction(BuildRestartTimerAction())
                    .AddAction(BuildStopServiceAction())
                    .Build();


                // Enlist this instance of the service as a foreground service
                StartForeground(Constants.SERVICE_RUNNING_NOTIFICATION_ID, notification);
            }
            else
            {
                // If earlier version channel ID is not used
                // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
                channelId = "";
                var notification = new Notification.Builder(this)
                    .SetContentTitle(LocalContentText)
                    .SetContentText(DiagnosisString)
                    .SetSmallIcon(Resource.Drawable.Fenix)
                    .SetContentIntent(BuildIntentToShowMainActivity())
                    .SetOngoing(true)
                    .AddAction(BuildRestartTimerAction())
                    .AddAction(BuildStopServiceAction())
                    .Build();

                // Enlist this instance of the service as a foreground service
                StartForeground(Constants.SERVICE_RUNNING_NOTIFICATION_ID, notification);
            }
        }


        //@RequiresApi(Build.VERSION_CODES.O)
        private string createNotificationChannel()
        {
            var channelId = "my_service";
            var channelName = "My Background Service";
            var chan = new NotificationChannel(channelId,
                channelName, Android.App.NotificationImportance.Max);
            chan.LightColor = Color.Blue;
            chan.LockscreenVisibility = NotificationVisibility.Private;
            var service = GetSystemService(Context.NotificationService) as NotificationManager;
            service.CreateNotificationChannel(chan);
            return channelId;
        }


        /// <summary>
        /// Builds a PendingIntent that will display the main activity of the app. This is used when the 
        /// user taps on the notification; it will take them to the main activity of the app.
        /// </summary>
        /// <returns>The content intent.</returns>
        PendingIntent BuildIntentToShowMainActivity()
        {
            //ActivityManager am = (ActivityManager)this.GetSystemService(Context.ActivityService);
            //IList<ActivityManager.RunningTaskInfo> taskInfo = am.GetRunningTasks(1);
            //ComponentName componentInfo = taskInfo[0].TopActivity;
            //string componentname = componentInfo.PackageName;

            /*
            Intent intent = new Intent(context, MainActivity.class);

intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
		| Intent.FLAG_ACTIVITY_SINGLE_TOP);
            */

            var notificationIntent = new Intent(this, typeof(MainActivity));
            notificationIntent.SetAction(Constants.ACTION_MAIN_ACTIVITY);
            notificationIntent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop); // .SingleTop | ActivityFlags.ClearTask);
            notificationIntent.PutExtra(Constants.SERVICE_STARTED_KEY, true);

            var pendingIntent = PendingIntent.GetActivity(this, 0, notificationIntent, PendingIntentFlags.CancelCurrent);
            return pendingIntent;
        }

        private void NotificationUpdate(string LocalContentText, string DiagnosisString)
        {
            try
            {
                var channelId = "";
                Notification notification;
                if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.O)
                {
                    channelId = createNotificationChannel();
                    notification = new Notification.Builder(this, channelId)
                        .SetContentTitle(LocalContentText)
                        .SetContentText(DiagnosisString)
                        .SetSmallIcon(Resource.Drawable.Fenix)
                        .SetContentIntent(BuildIntentToShowMainActivity())
                        .SetOngoing(true)
                        .AddAction(BuildRestartTimerAction())
                        .AddAction(BuildStopServiceAction())
                        .Build();
                }
                else
                {
                    channelId = "";
                    notification = new Notification.Builder(this)
                        .SetContentTitle(LocalContentText)
                        .SetContentText(DiagnosisString)
                        .SetSmallIcon(Resource.Drawable.Fenix)
                        .SetContentIntent(BuildIntentToShowMainActivity())
                        .SetOngoing(true)
                        .AddAction(BuildRestartTimerAction())
                        .AddAction(BuildStopServiceAction())
                        .Build();
                }

                NotificationManager notificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
                notificationManager.Notify(Constants.SERVICE_RUNNING_NOTIFICATION_ID, notification);
            }
            catch (Exception ex)
            {
                string exeptionMessage = ex.Message;
            }
        }

        /// <summary>
        /// Builds a Notification.Action that will instruct the service to restart the timer.
        /// </summary>
        /// <returns>The restart timer action.</returns>
        Notification.Action BuildRestartTimerAction()
        {
            
            var restartTimerIntent = new Intent(this, GetType());
            restartTimerIntent.SetAction(Constants.ACTION_RESTART_TIMER);
            var restartTimerPendingIntent = PendingIntent.GetService(this, 0, restartTimerIntent, 0);
            Notification.Action.Builder builder = null;
            

            try
            {
                builder = new Notification.Action.Builder(Resource.Drawable.Fenix,
                                  "Запуск пульсометрии",
                                  restartTimerPendingIntent);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }


            return builder.Build();
        }

        /// <summary>
        /// Builds the Notification.Action that will allow the user to stop the service via the
        /// notification in the status bar
        /// </summary>
        /// <returns>The stop service action.</returns>
        Notification.Action BuildStopServiceAction()
        {
            var stopServiceIntent = new Intent(this, GetType());
            stopServiceIntent.SetAction(Constants.ACTION_STOP_SERVICE);
            var stopServicePendingIntent = PendingIntent.GetService(this, 0, stopServiceIntent, 0);

            var builder = new Notification.Action.Builder(Android.Resource.Drawable.IcMediaPause,
                                                          "Приостановить пульсометрию",
                                                          stopServicePendingIntent);
            return builder.Build();

        }
    }
}