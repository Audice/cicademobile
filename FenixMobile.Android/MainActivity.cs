﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Net;
using Android.Content;
using FenixMobile.Droid.ForegroundIntent;
using Xamarin.Forms;
using FenixMobile.Droid.AndroidPowerManager;

namespace FenixMobile.Droid
{
    [Activity(Label = "FenixMobile", Theme = "@style/MyTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        //События запуска и остановки Foreground-сервиса
        public Intent startServiceIntent;
        public static Intent stopServiceIntent;
        public static MainActivity CurrentMainActivity = null;
        public static App CurrentApp = null;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            ServicePointManager.ServerCertificateValidationCallback += (o, cert, chain, errors) => true;

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            App.ScreenWidth = (int)(Resources.DisplayMetrics.WidthPixels / Resources.DisplayMetrics.Density); // real pixels
            App.ScreenHeight = (int)(Resources.DisplayMetrics.HeightPixels / Resources.DisplayMetrics.Density); // real pixels
            App.RealScreenHeight = (int)Resources.DisplayMetrics.HeightPixels;
            App.RealScreenWidth = (int)Resources.DisplayMetrics.WidthPixels;

            base.OnCreate(savedInstanceState);


            //Инициализация сервиса
            startServiceIntent = new Intent(this, typeof(HeadService));
            startServiceIntent.SetAction(Constants.ACTION_START_SERVICE);

            stopServiceIntent = new Intent(this, typeof(HeadService));
            stopServiceIntent.SetAction(Constants.ACTION_STOP_SERVICE);

            //StopService(startServiceIntent);

            //Огорничение на запуск сервиса на версиях Android Lollipop и младше
            if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Lollipop)
            {
                //Запуск Foreground-сервиса
                StartService(startServiceIntent);
            }


            Rg.Plugins.Popup.Popup.Init(this, savedInstanceState);
            FFImageLoading.Forms.Platform.CachedImageRenderer.Init(false);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

            CurrentMainActivity = this;
            if (CurrentApp == null)
            {
                CurrentApp = new App();
            }

            //StartAlarm();
            //ShutUpAndTakeMyBattery();

            LoadApplication(CurrentApp);
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        private void StartAlarm()
        {
            AlarmManager manager = (AlarmManager)GetSystemService(Context.AlarmService);
            Intent alarmIntent = new Intent(this, typeof(BroadcastToastReceiver));
            PendingIntent pendingIntent = PendingIntent.GetBroadcast(this, 0, alarmIntent, 0);

            manager.SetRepeating(AlarmType.RtcWakeup, SystemClock.ElapsedRealtime() + 3000, 60 * 1000, pendingIntent); //
            //manager.SetExactAndAllowWhileIdle(AlarmType.RtcWakeup, 60 * 1000, pendingIntent); //
        }
        /*
        protected override void OnDestroy()
        {
            int s = 0;
            //Уничтожает бытиё. Пока не придумал иного - поэтому будет так
            //StopService(startServiceIntent);
            //Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
            //base.OnDestroy();
        }
        */

        public void Destroyer()
        {
            //this.OnDestroy();
        }
    }
}