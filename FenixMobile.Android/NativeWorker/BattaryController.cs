﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using FenixMobile.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(FenixMobile.Droid.NativeWorker.BattaryController))]
namespace FenixMobile.Droid.NativeWorker
{
    class BattaryController : IBattaryController
    {
        public bool GetIgnoringBatteryOptimizationsPolicy()
        {
            var pm = (PowerManager)Android.App.Application.Context.GetSystemService(Context.PowerService);
            bool result = pm.IsIgnoringBatteryOptimizations("com.companyname.FenixMobile");
            return result;
        }

        public void SetIgnoringBatteryOptimizationsPolicy()
        {
            Intent intent = new Intent();
            intent.SetAction(Android.Provider.Settings.ActionIgnoreBatteryOptimizationSettings);
            Forms.Context.StartActivity(intent);
        }
    }
}