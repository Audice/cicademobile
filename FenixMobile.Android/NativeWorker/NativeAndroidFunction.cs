﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using FenixMobile.View.Interface;
using Xamarin.Forms;

[assembly: Dependency(typeof(FenixMobile.Droid.NativeWorker.NativeAndroidFunction))]
namespace FenixMobile.Droid.NativeWorker
{
    class NativeAndroidFunction : ISystemFunction
    {
        public void CloseApplication()
        {
            MainActivity.CurrentMainActivity.Destroyer();
        }
    }
}