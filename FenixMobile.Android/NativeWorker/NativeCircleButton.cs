﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using FenixMobile.Droid.NativeWorker;
using FenixMobile.View.CustomControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics.Drawables;
using Android.Graphics;
using Android.Util;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using System.ComponentModel;

[assembly: ExportRenderer(typeof(CircleButton), typeof(NativeCircleButton))]
namespace FenixMobile.Droid.NativeWorker
{
    public class NativeCircleButton : ViewRenderer<CircleButton, Android.Widget.Button>
    {
        private Android.Widget.Button viewButton;
        protected override void OnElementChanged(ElementChangedEventArgs<CircleButton> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                // Cоздаем и настраиваем элемент
                viewButton = new Android.Widget.Button(Context);
                viewButton.Click += Button_Click;
                SetNativeControl(viewButton);

                // установка свойств
                if (e.NewElement != null)
                {
                    SetBackgroundColor();
                    SetText();
                }
            }
        }

        private void Button_Click(object sender, EventArgs e)
        {
            if (Element != null)
                Element.FireClick(System.EventArgs.Empty);
        }

        // изменения свойства
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == CircleButton.TextProperty.PropertyName)
            {
                SetText();
            }
            if (e.PropertyName == CircleButton.BackgroundColProperty.PropertyName)
            {
                SetCustomColor();
            }

            if (e.PropertyName == CircleButton.CustomHeightProperty.PropertyName)
            {
                Control.SetHeight((int)Element.HeightRequest);
            }

            if (e.PropertyName == CircleButton.CustomWidthProperty.PropertyName)
            {
                Control.SetTextSize(ComplexUnitType.Dip, (float)(Element.Width / 3));
                SetBackgroundColor();
            }
        }

        private void SetBackgroundColor()
        {
            Xamarin.Forms.Color tmpColor = Element.CustomBackgroundColor.AddLuminosity(0.7);
            int startColor = Xamarin.Forms.Color.White.ToAndroid();//tmpColor.ToAndroid();
            int endColor = Element.CustomBackgroundColor.ToAndroid();
            GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LeftRight, new int[] { startColor, endColor });
            gradientDrawable.SetShape(ShapeType.Oval);
            gradientDrawable.SetGradientRadius((float)Element.Width * 2);
            gradientDrawable.SetGradientType(GradientType.RadialGradient);
            gradientDrawable.SetUseLevel(false);
            gradientDrawable.SetGradientCenter(Element.GradientCenter.Item1, Element.GradientCenter.Item2);
            //gradientDrawable.SetGradientCenter(1f, 1f); // Сделать зависимым
            Control.SetBackgroundDrawable(gradientDrawable);
        }

        private void SetCustomColor()
        {
            int startColor = Xamarin.Forms.Color.White.ToAndroid();//tmpColor.ToAndroid();
            int endColor = Element.CustomBackgroundColor.ToAndroid();
            GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LeftRight, new int[] { startColor, endColor });
            gradientDrawable.SetShape(ShapeType.Oval);
            gradientDrawable.SetGradientRadius((float)Element.Width * 2);
            gradientDrawable.SetGradientType(GradientType.RadialGradient);
            gradientDrawable.SetUseLevel(false);
            gradientDrawable.SetGradientCenter(Element.GradientCenter.Item1, Element.GradientCenter.Item2);
            Control.SetBackgroundDrawable(gradientDrawable);
        }

        private void SetText()
        {
            Control.SetTextColor(Xamarin.Forms.Color.Beige.ToAndroid());
            Typeface font = Typeface.Default;
            Control.SetTypeface(font, TypefaceStyle.Bold);
            Control.Gravity = GravityFlags.Center;
            Control.SetTextSize(ComplexUnitType.Dip, (float)(Element.Width / 3));
            float r = Control.TextSize;
            string realText = "";
            string tmpText = Element.Text;
            if (Element.Text.Length > 3) realText = tmpText.Remove(3, tmpText.Length - 3);

            Control.Text = realText;
        }
    }
}