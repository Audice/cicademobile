﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Xamarin.Forms;
using FenixMobile.View.Interface;
using Android.OS;

[assembly: Dependency(typeof(FenixMobile.Droid.NativeWorker.NativeFileWorker))]
namespace FenixMobile.Droid.NativeWorker
{
    public class NativeFileWorker : IFileWorker
    {
        private readonly string ServerDir = "ServerFiles";
        public bool ExistsFile(string filename)
        {
            string filepath = GetFilePath(filename);
            return File.Exists(filepath);
        }
        public int CreateFile(string filename)
        {
            string filepath = GetFilePath(filename);
            try
            {
                using (FileStream fs = new FileStream(filepath, FileMode.Create, FileAccess.Write))
                {
                }
                if (this.ExistsFile(filename))
                    return 0;
            }
            catch (Exception ex)
            {
                string rxceptionMessage = ex.Message;
            }
            return -1;
        }
        public List<short> GetFile(string filename)
        {
            List<short> resultData = new List<short>();
            try
            {
                string filepath = GetFilePath(filename);
                //Проверка на наличие такого файла
                if (!this.ExistsFile(filename)) return null;

                using (FileStream fs = new FileStream(filepath, FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader br = new BinaryReader(fs))
                    {
                        while (br.BaseStream.Position != br.BaseStream.Length)
                            resultData.Add(br.ReadInt16());
                    }
                }
                return resultData;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;
                resultData = null;
            }
            return resultData;
        }

        public List<short> GetDataFromSpecificByte(string filename, long position)
        {
            if (position <= 0) return null;
            string filepath = GetFilePath(filename);
            //Проверка на наличие такого файла
            if (!this.ExistsFile(filename)) return null;
            if (position % 2 != 0) return null;
            List<short> resultData = new List<short>();

            try
            {
                using (FileStream fs = new FileStream(filepath, FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader br = new BinaryReader(fs))
                    {
                        if (position >= br.BaseStream.Length)
                        {
                            resultData = null;
                        }
                        else
                        {
                            br.BaseStream.Position = position;

                            while (br.BaseStream.Position != br.BaseStream.Length)
                                resultData.Add(br.ReadInt16());
                        }
                    }
                }
                return resultData;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;
                resultData = null;
            }
            return resultData;
        }

        



        
        public void DeleteFile(string filename)
        {
            if (!this.ExistsFile(filename)) return;
            File.Delete(GetFilePath(filename));
        }
        // Строим путь к локальному хранилищу
        public string GetFilePath(string filename)
        {
            return Path.Combine(GetDocsPath(), filename);
        }
        // получаем путь к папке Personal
        string GetDocsPath()
        {
            return System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
        }

        public bool CreateServerFile(string filename)
        {
            string filepath = GetServerFilePath(filename);
            try
            {
                if (File.Exists(filepath)) return true;
                using (FileStream fs = new FileStream(filepath, FileMode.Create, FileAccess.Write))
                {
                }
                if (File.Exists(filepath))
                    return true;
            }
            catch (Exception ex)
            {
                string rxceptionMessage = ex.Message;
            }
            return false;
        }

        public bool AppendServerFile(string filename, short[] data)
        {
            try
            {
                string filepath = GetServerFilePath(filename);

                using (FileStream fs = new FileStream(filepath, FileMode.Append, FileAccess.Write))
                {
                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {
                        foreach (short value in data)
                        {
                            bw.Write(value);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;
            }
            return false;
        }

        public List<short> GetServerFile(string filename)
        {
            List<short> resultData = new List<short>();
            try
            {
                string filepath = GetServerFilePath(filename);
                //Проверка на наличие такого файла
                if (!File.Exists(filepath)) return null;

                using (FileStream fs = new FileStream(filepath, FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader br = new BinaryReader(fs))
                    {
                        while (br.BaseStream.Position != br.BaseStream.Length)
                            resultData.Add(br.ReadInt16());
                    }
                }
                return resultData;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;
                resultData = null;
            }
            return resultData;
        }

        public void DeleteServerFile(string filename)
        {
            string filepath = GetServerFilePath(filename);
            if (File.Exists(filepath)) return;
            File.Delete(filepath);
        }

        public string GetServerFilePath(string filename)
        {
            return Path.Combine(GetServerPath(), filename);
        }
        // получаем путь к папке Personal/Server ... Я не помню как...
        string GetServerPath()
        {
            string ServerFolderPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal) + "/" + this.ServerDir;
            if (!Directory.Exists(ServerFolderPath))
            {
                Directory.CreateDirectory(ServerFolderPath);
            }
            return ServerFolderPath;
        }

        public List<short> GetPartServerFile(string filename, long startByte, ref long count)
        {
            string filepath = GetServerFilePath(filename);
            //Проверка на наличие такого файла
            if (!File.Exists(filepath)) return null;
            if (startByte % 2 != 0) return null;
            List<short> resultData = new List<short>();

            try
            {
                using (FileStream fs = new FileStream(filepath, FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader br = new BinaryReader(fs))
                    {
                        if (startByte >= br.BaseStream.Length)
                        {
                            resultData = null;
                        }
                        else
                        {
                            br.BaseStream.Position = startByte;
                            long rightBorder = startByte + count;
                            while (br.BaseStream.Position != br.BaseStream.Length && br.BaseStream.Position < rightBorder)
                                resultData.Add(br.ReadInt16());
                            count = br.BaseStream.Position;
                        }
                    }
                }
                return resultData;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;
                resultData = null;
            }
            return resultData;
        }

        public long GetBytesCount(string filename)
        {
            string filepath = GetServerFilePath(filename);
            //Проверка на наличие такого файла
            if (!File.Exists(filepath)) return -1;
            long results = -1;

            try
            {
                System.IO.FileInfo file = new System.IO.FileInfo(filepath);
                results = file.Length;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;
                results = -1;
            }
            return results;
        }

        public bool ServerFileExist(string filename)
        {
            string filepath = GetServerFilePath(filename);
            if (File.Exists(filepath)) return true;
            else return false;
        }



        private object sync = new object();

        public string GetLogs()
        {
            //Имя файла LogFile.txt
            string LogFileName = "LogFile.txt";
            string filepath = GetFilePath(LogFileName);
            bool exists = File.Exists(filepath);

            if (!exists) return null;

            using (StreamReader reader = File.OpenText(filepath))
            {
                return reader.ReadToEnd();
            }
        }

        public async Task AddNewLogInfo(string LogString)
        {
            //Имя файла LogFile.txt
            string LogFileName = "LogFile.txt";
            string filepath = GetFilePath(LogFileName);

            lock (sync)
            {
                using (StreamWriter writer = File.AppendText(filepath))
                {
                    writer.Write(LogString);
                }
            }

            await Task.Delay(1);
        }

        public void DeleteLogs()
        {
            //Имя файла LogFile.txt
            string LogFileName = "LogFile.txt";
            string filepath = GetFilePath(LogFileName);
            if (!File.Exists(filepath)) return;
            File.Delete(filepath);
        }

        public bool WriteByteData(string filename, byte[] data)
        {
            try
            {
                string filepath = GetFilePath(filename);
                using (FileStream fs = new FileStream(filepath, FileMode.Create, FileAccess.Write))
                {
                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {
                        foreach (byte value in data)
                        {
                            bw.Write(value);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;
            }
            return false;
        }

        public int AppendFile(string filename, short[] data)
        {
            try
            {
                string filepath = GetFilePath(filename);

                using (FileStream fs = new FileStream(filepath, FileMode.Append, FileAccess.Write))
                {
                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {
                        foreach (short value in data)
                        {
                            bw.Write(value);
                        }
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;
            }
            return -1;
        }


        public bool CreateFile(string filename, List<short> data)
        {
            if (data == null) 
                return false;
            try
            {
                string filepath = GetFilePath(filename);
                using (FileStream fs = new FileStream(filepath, FileMode.Append, FileAccess.Write))
                {
                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {
                        foreach (short value in data)
                            bw.Write(value);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;
            }
            return false;
        }
    }
}