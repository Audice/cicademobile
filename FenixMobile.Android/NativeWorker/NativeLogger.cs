﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace FenixMobile.Droid.NativeWorker
{
    public class NativeLogger
    {
        private static object sync = new object();
        private string LogFilename = "LogFile.txt";

        // Строим путь к локальному хранилищу
        public string GetFilePath(string filename)
        {
            return Path.Combine(GetDocsPath(), filename);
        }
        // получаем путь к папке Personal
        string GetDocsPath()
        {
            return System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
        }

        public Task DeleteLogsAsync()
        {
            string filepath = GetFilePath(LogFilename);
            if (!File.Exists(filepath)) return Task.FromResult(true);
            File.Delete(filepath);
            return Task.FromResult(true);
        }

        public string GetLogs()
        {
            string filepath = GetFilePath(LogFilename);
            bool exists = File.Exists(filepath);

            if (!exists) return null;

            using (StreamReader reader = File.OpenText(filepath))
            {
                return reader.ReadToEnd();
            }
        }

        public async Task AddNewLogInfo(string LogString)
        {
            //Имя файла LogFile.txt
            string LogFileName = "LogFile.txt";
            string filepath = GetFilePath(LogFileName);

            lock (sync)
            {
                using (StreamWriter writer = File.AppendText(filepath))
                {
                    writer.Write(LogString);
                }
            }
        }
    }
}