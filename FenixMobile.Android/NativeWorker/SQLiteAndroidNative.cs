﻿using Android.OS;
using FenixMobile.Droid.NativeWorker;
using FenixMobile.Interfaces;
using System.IO;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLiteAndroidNative))]
namespace FenixMobile.Droid.NativeWorker
{
    public class SQLiteAndroidNative : ISQLite
    {
        public SQLiteAndroidNative() { }
        public string GetDatabasePath(string sqliteFilename)
        {
            string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var path = Path.Combine(documentsPath, sqliteFilename);
            return path;
        }
    }
}