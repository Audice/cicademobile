﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FenixMobile.Droid.NativeWorker
{
    public delegate void VerticalSeekBarStartTrackingTouchEventHandler(object sender, SeekBar.StartTrackingTouchEventArgs args);
    public delegate void VerticalSeekBarStopTrackingTouchEventHandler(object sender, SeekBar.StopTrackingTouchEventArgs args);

    public class VerticalSlider : SeekBar
    {
        #region ctor

        protected VerticalSlider(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer)
        { }

        public VerticalSlider(Context context)
            : base(context)
        { }

        public VerticalSlider(Context context, IAttributeSet attrs)
            : base(context, attrs)
        { }

        public VerticalSlider(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        { }

        #endregion

        #region fields

        private int _min;
        private int _max;

        #endregion

        #region properties

        public override int Min
        {
            get { return _min; }
            set
            {
                if (Min > Progress)
                    Progress = Min;
                _min = value;
                OnSizeChanged(Width, Height, 0, 0);
            }
        }

        public override int Progress
        {
            get
            {
                return base.Progress <= Min ? Min : base.Progress;
            }
            set
            {
                if (value <= Min)
                    base.Progress = Min;
                else if (value >= Max)
                    base.Progress = Max;
                else
                    base.Progress = value;

                OnSizeChanged(Width, Height, 0, 0);
            }
        }

        #endregion

        #region events

        public new event VerticalSeekBarStartTrackingTouchEventHandler StartTrackingTouch;
        public new event VerticalSeekBarStopTrackingTouchEventHandler StopTrackingTouch;

        #endregion

        public override void Draw(Android.Graphics.Canvas canvas)
        {
            canvas.Rotate(-90);
            canvas.Translate(-Height, 0);
            base.OnDraw(canvas);
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {
            base.OnMeasure(heightMeasureSpec, widthMeasureSpec);
            SetMeasuredDimension(MeasuredHeight, MeasuredWidth);
        }

        protected override void OnSizeChanged(int w, int h, int oldw, int oldh)
        {
            base.OnSizeChanged(h, w, oldh, oldw);
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            if (!Enabled)
                return false;

            switch (e.Action)
            {
                case MotionEventActions.Down:
                    if (null != StartTrackingTouch)
                        StartTrackingTouch(this, new StartTrackingTouchEventArgs(this));
                    Selected = true;
                    Pressed = true;
                    Progress = Max - (int)(Max * e.GetY() / Height);
                    System.Diagnostics.Debug.WriteLine(">  Down: " + Progress);
                    break;
                case MotionEventActions.Move:
                    Progress = Max - (int)(Max * e.GetY() / Height);
                    System.Diagnostics.Debug.WriteLine(">  Move: " + Progress);
                    break;
                case MotionEventActions.Up:
                    if (null != StopTrackingTouch)
                        StopTrackingTouch(this, new StopTrackingTouchEventArgs(this));
                    Selected = false;
                    Pressed = false;
                    Progress = Max - (int)(Max * e.GetY() / Height);
                    System.Diagnostics.Debug.WriteLine(">  Up: " + Progress);
                    break;
                case MotionEventActions.Cancel:
                    Selected = false;
                    Pressed = false;
                    System.Diagnostics.Debug.WriteLine(">  Cancel: " + Progress);
                    break;
            }

            return true;
        }
    }
}