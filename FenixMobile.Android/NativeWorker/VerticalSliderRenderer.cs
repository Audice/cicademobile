﻿using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using FenixMobile.Droid.NativeWorker;
using FenixMobile.View.CustomControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(XamarinVerticalSlider), typeof(VerticalSliderRenderer))]
namespace FenixMobile.Droid.NativeWorker
{
    [Obsolete]
    class VerticalSliderRenderer : ViewRenderer<XamarinVerticalSlider, VerticalSlider>
    {
        VerticalSlider UIVerticalSlider;

        protected override void OnElementChanged(ElementChangedEventArgs<XamarinVerticalSlider> _E)
        {
            base.OnElementChanged(_E);

            if (this.Control != null && Element != null)
            {
                System.Diagnostics.Debug.WriteLine(">  Control Found: " + Element.Value + "   " + Control.Progress);
            }

            if (this.Control == null)
            {
                // Instantiate the native control and assign it to the Control property with
                // the SetNativeControl method
                UIVerticalSlider = new VerticalSlider(this.Context);
                this.SetNativeControl(UIVerticalSlider);
                this.Control.ProgressChanged += ElementOnPropertyChanged;
            }

            if (_E.OldElement != null)
            {
                // Unsubscribe from event handlers and cleanup any resources
                System.Diagnostics.Debug.WriteLine(">  OldElement: " + Element.Value);
                _E.OldElement.PropertyChanged -= ElementOnPropertyChanged;
            }

            if (_E.NewElement != null)
            {
                // Привязка многоуровнего стиля градиента
                GradientDrawable gdd = new GradientDrawable();
                gdd.SetColor(_E.NewElement.SliderColor.ToAndroid());
                gdd.SetCornerRadius(_E.NewElement.SliderRadius);
                gdd.SetSize((int)(_E.NewElement.SliderHeight), (int)(_E.NewElement.SliderWidht));
                Control.SetProgressDrawableTiled(gdd);
                Control.SplitTrack = false;

                // Привязка стиля курсора
                GradientDrawable gd = new GradientDrawable();
                gd.SetColor(_E.NewElement.ThumbColor.ToAndroid());
                gd.SetShape(ShapeType.Rectangle);
                gd.SetSize((int)(_E.NewElement.ThumbHeight), (int)(_E.NewElement.ThumbWidht));
                gd.SetCornerRadius(_E.NewElement.ThumbRadius);
                Control.SetThumb(gd);

                Control.Progress = _E.NewElement.Value;
                System.Diagnostics.Debug.WriteLine(">  New Element: " + Element.Value);
                _E.NewElement.PropertyChanged += ElementOnPropertyChanged;
            }
        }

        void ElementOnPropertyChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            if (Element != null && Control != null)
                Element.Value = Control.Progress;
        }

        void ElementOnPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Value")
                if (Element != null && Control != null)
                    Control.Progress = Element.Value;
            
        }
    }
}