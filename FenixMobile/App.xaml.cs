﻿using FenixMobile.Model.DatabasePart;
using FenixMobile.Model.SettingsPart;
using FenixMobile.View;
using FenixMobile.View.HeadMasterMenu;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile
{
    public partial class App : Application
    {
        public static int ScreenWidth;
        public static int ScreenHeight;
        DatabaseWorker DatabaseWorker;
        public static int RealScreenWidth;
        public static int RealScreenHeight;

        public App()
        {
            InitializeComponent();

            DatabaseWorker = DatabaseWorker.getInstance();

            string loginLastUser = SettingsModel.GetLastUserLogin();

            if (loginLastUser != null)
            {
                StartAction(loginLastUser);
            }
            else
            {
                MainPage = new NavigationPage(new Login());
            }
        }

        void StartAction(string usersLogin)
        {
            //Запускаем поиск login в базе данных
            if (this.DatabaseWorker.CheckUser(usersLogin))
            {
                //Пользователь есть - работаем с ним
                if (this.DatabaseWorker.CheckSessionByLogin(usersLogin))
                {
                    //Если и сессия есть, то переходим дальше, устанавливая текущего юзера
                    this.DatabaseWorker.SetCurrentUser(usersLogin);
                    MainPage = new NavigationPage(new NotCoolMenu());
                }
                else
                {
                    //Нет токена сессии, заставляем пройти аутентификацию
                    //Заполняем поля login и password
                    MainPage = new NavigationPage(new Login());
                }
            }
            else
            {
                MainPage = new NavigationPage(new Login());
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
