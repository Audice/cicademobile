﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Enumeration
{
   public enum DataTransferMode
    {
        Current,
        Archive
    }
    

    public enum HeartRateVariabilityIndex
    {
        Good,
        NotVeryGood,
        Warning,
        Danger,
        Critical
    }
}
