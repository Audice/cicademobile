﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Interfaces
{
    public interface IBattaryController
    {
        bool GetIgnoringBatteryOptimizationsPolicy();
        void SetIgnoringBatteryOptimizationsPolicy();
    }
}
