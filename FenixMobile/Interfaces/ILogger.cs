﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FenixMobile.Interfaces
{
    public interface ILogger
    {
        Task AddNewLogInfo(string LogString);
        string GetLogs();
        Task DeleteLogsAsync();
    }
}
