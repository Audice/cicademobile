﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Interfaces
{
    public interface ISQLite
    {
        string GetDatabasePath(string filename);
    }

    public enum ECGreceiveMode
    {
        ShortECG = 10,
        MidECG = 300,
        HolterECG = 1000
    }
}
