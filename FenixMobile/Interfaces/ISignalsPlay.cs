﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Interfaces
{
    public interface ISignalsPlay
    {
        void PlayBadPulse();
        void Play();
        void Pause();
        Action OnFinishedPlaying { get; set; }
    }
}
