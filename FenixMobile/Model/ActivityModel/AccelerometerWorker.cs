﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Essentials;

namespace FenixMobile.Model.ActivityModel
{
    /// <summary>
    /// Перечисление видов активности: Rest - нет активности, LowActivity - слабая активность, ModerateActivity - нормальная активность, 
    /// GreatActivity - высокая активность, Tension - очень высокая активность
    /// </summary>
    public enum ActivityMode { Rest = 0, LowActivity = 1, ModerateActivity = 2, GreatActivity = 3, Tension = 4 };
    public class AccelerometerWorker : IDisposable
    {
        /// <summary>
        /// Скорость, с которой программа обращается к аппаратному акселерометру
        /// </summary>
        private readonly SensorSpeed Speed = SensorSpeed.UI;
        /// <summary>
        /// Список углов, расчитанных в рамках пришедшей порции показаний акселерометра
        /// </summary>
        private List<double> CosAngles = null;
        /// <summary>
        /// Предыдущее значение датчика акселерометра по оси X
        /// </summary>
        private float OldX = 0;
        /// <summary>
        /// Предыдущее значение датчика акселерометра по оси Y
        /// </summary>
        private float OldY = 0;
        /// <summary>
        /// Предыдущее значение датчика акселерометра по оси Z
        /// </summary>
        private float OldZ = 0;
        /// <summary>
        /// Флаг инициализации стартовых значений датчика
        /// </summary>
        private bool isInit = false;
        /// <summary>
        /// Переменная, хранящая текущее время для обновления показаний активности
        /// </summary>
        private DateTime CurrentTime = DateTime.Now;
        /// <summary>
        /// Количество секунд для накопления показаний акселерометра
        /// </summary>
        private readonly byte CountSeconds = 1;
        /// <summary>
        /// Текущая активность пользователя
        /// </summary>
        ActivityMode ActivityMode;
        /// <summary>
        /// Режим, устанавливающий скорость опроса акселерометра
        /// </summary>
        private readonly SensorSpeed AccelerometerSpeed = SensorSpeed.UI;
        /// <summary>
        /// Публичное событие изменения состояния акселерометра
        /// </summary>
        public EventHandler<AccelerometerEventArgs> AccelerometerValueChange;



        public AccelerometerWorker()
        {
            this.CosAngles = new List<double>();
            this.ActivityMode = ActivityMode.Rest;
            //Подписывание на обновление информации с кселерометра
            Accelerometer.ReadingChanged += Accelerometer_ReadingChanged;
        }


        /// <summary>
        /// Обрабатка новых данных от акселерометра
        /// </summary>
        /// <param name="sender">Объект-отправитель</param>
        /// <param name="e">Пакет с данными акселерометра</param>
        void Accelerometer_ReadingChanged(object sender, AccelerometerChangedEventArgs e)
        {
            var data = e.Reading;
            AddAngles(data.Acceleration.X, data.Acceleration.Y, data.Acceleration.Z);
            byte curAngle = 0;
            if (isFindActivity(out curAngle))
            {
                this.ActivityMode = ActivityAssessment(curAngle);
                this.OnAccelerometerValueChange(new AccelerometerEventArgs()
                {
                    ActivityMode = this.ActivityMode,
                    Angle = curAngle,
                    TimeBetweenUpdates = this.CountSeconds
                });
            }
        }

        /// <summary>
        /// Добавление нового значения косинуса угла изменения двух векторов
        /// </summary>
        /// <param name="newX">Новое значение по X</param>
        /// <param name="newY">Новое значение по Y</param>
        /// <param name="newZ">Новое значение по Z</param>
        void AddAngles(float newX, float newY, float newZ)
        {
            if (isInit)
            {
                //Поиск косинуса угла между векторами
                double cosAngleVector = (double)(OldX * newX + OldY * newY + OldZ * newZ)
                    / (Math.Sqrt(OldX * OldX + OldY * OldY + OldZ * OldZ) * Math.Sqrt(newX * newX + newY * newY + newZ * newZ));
                //Расчёт угла между векторами: от 0 до Pi
                //byte curAngle = (byte)(180.0 * (Math.Acos(cosAngleVector) / Math.PI));
                CosAngles.Add(cosAngleVector);
                //Обнавление старых велечин на новые
                UdateOldCoord(newX, newY, newZ);
            }
            else
            {
                //Инициализация стартовых значений
                UdateOldCoord(newX, newY, newZ);
                this.isInit = true;
            }
        }
        /// <summary>
        /// Замена предыдущих значений акселерометра по осям на новые
        /// </summary>
        /// <param name="newX">Новое значение по X</param>
        /// <param name="newY">Новое значение по Y</param>
        /// <param name="newZ">Новое значение по Z</param>
        void UdateOldCoord(float newX, float newY, float newZ)
        {
            OldX = newX;
            OldY = newY;
            OldZ = newZ;
        }
        /// <summary>
        /// Поиск активности и наибольшего угла отклонения в рамках данного измерения
        /// </summary>
        /// <param name="angle">Максимальный угол отклонения</param>
        /// <returns>Есть ли активность или нет</returns>
        bool isFindActivity(out byte angle)
        {
            DateTime timeNow = DateTime.Now;
            TimeSpan secondsSpan = timeNow - CurrentTime;
            angle = 0;

            if (secondsSpan.TotalSeconds > this.CountSeconds)
            {
                this.CurrentTime = timeNow;
                List<double> tmp = new List<double>();
                //Фильтрация массива углов
                for (int i = 1; i < this.CosAngles.Count - 1; i++)
                {
                    tmp.Add((this.CosAngles[i - 1] + this.CosAngles[i] + this.CosAngles[i + 1]) / 3);
                }
                if (tmp.Count == 0)
                    return false;

                byte baseAngle = (byte)(180.0 * (Math.Acos(tmp.Min()) / Math.PI));
                byte maxAngle = (byte)(180.0 * (Math.Acos(tmp.Max()) / Math.PI));

                if (Math.Abs(maxAngle - baseAngle) > byte.MaxValue)
                    angle = byte.MaxValue;
                else
                    angle = (byte)(Math.Abs(maxAngle - baseAngle));

                tmp.Clear();
                this.CosAngles.Clear();
                return true;
            }
            return false;
        }


        /// <summary>
        /// Оценка активности пользователя по углу отклонения
        /// </summary>
        /// <param name="maxAngle">Максимальный угол отклонения</param>
        /// <returns>Результат оценки активности</returns>
        ActivityMode ActivityAssessment(byte maxAngle)
        {
            if (maxAngle < 10) return ActivityMode.Rest;
            if (maxAngle >= 10 && maxAngle < 30) return ActivityMode.LowActivity;
            if (maxAngle >= 30 && maxAngle < 60) return ActivityMode.ModerateActivity;
            if (maxAngle >= 60 && maxAngle < 90) return ActivityMode.GreatActivity;
            return ActivityMode.Tension;
        }
        /// <summary>
        /// Метод активации\деактивации акселерометра
        /// </summary>
        public void ToggleAccelerometer()
        {
            try
            {
                if (Accelerometer.IsMonitoring)
                    Accelerometer.Stop();
                else
                    Accelerometer.Start(AccelerometerSpeed);
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Feature not supported on device
            }
            catch (Exception ex)
            {
                // Other error has occurred.
            }
        }
        /// <summary>
        /// Метод активации оповещения о новом изменении состояния акселерометра
        /// </summary>
        /// <param name="e"></param>
        private void OnAccelerometerValueChange(AccelerometerEventArgs e)
        {
            EventHandler<AccelerometerEventArgs> handler = AccelerometerValueChange;
            if (handler != null)
                handler(this, e);
        }

        public void Dispose()
        {
            if (Accelerometer.IsMonitoring)
                Accelerometer.Stop();
            Accelerometer.ReadingChanged -= Accelerometer_ReadingChanged;
        }
    }

    /// <summary>
    /// Класс-контейнер для данных акселерометра
    /// </summary>
    public class AccelerometerEventArgs
    {
        /// <summary>
        /// Активность пользователя
        /// </summary>
        public ActivityMode ActivityMode { get; set; }
        /// <summary>
        /// Соответствующий активности угол
        /// </summary>
        public byte Angle { get; set; }
        /// <summary>
        /// Период обновления данных
        /// </summary>
        public byte TimeBetweenUpdates { get; set; }
    }


    /*
        // Пример использования 
        AccelerometerWorker accelerometer;
        public TestPage()
        {
            accelerometer = new AccelerometerWorker();
            accelerometer.AccelerometerValueChange += UpdateData;
            accelerometer.ToggleAccelerometer();
        }

        void UpdateData(object sender, AccelerometerEventArgs e)
        {
            string info = "Угол: " + e.Angle.ToString() + Environment.NewLine + "Состояние: " + e.ActivityMode.ToString();
        }
     */
}
