﻿using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.BatteryPart
{
    public class BatteryHandler
    {
        /// <summary>
        /// Максимальное значение заряда аккумулятора кардиографа
        /// </summary>
        private readonly ushort MaxChargeLevel = 4110;
        /// <summary>
        /// Минимльное значение заряда аккумулятора кардиографа
        /// </summary>
        private readonly ushort MinChargeLevel = 3200;
        /// <summary>
        /// Текущее значение тока на аккумуляторе
        /// </summary>
        private ushort AmperageValue = 0;
        /// <summary>
        /// Заряд аккамуоятора на текущий момент
        /// </summary>
        public byte ChargePercent
        {
            get; private set;
        } = 0;


        public event EventHandler<BatteryEventArgs> BattaryStateChangeEvent;
        protected virtual void OnBatteryStateChange(BatteryEventArgs e)
        {
            EventHandler<BatteryEventArgs> handler = BattaryStateChangeEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public BatteryHandler()
        {

        }

        public void ParseBatteryInfo(List<byte> batteryInfo)
        {
            string convertedBattState = System.Text.Encoding.Default.GetString(batteryInfo.ToArray());
            ushort BattState = 0;
            if (ushort.TryParse(convertedBattState, out BattState))
            {
                if (this.AmperageValue == 0 || this.AmperageValue != BattState)
                {
                    this.AmperageValue = BattState;
                    UpdateBattaryState(CalcChargesPercent(this.AmperageValue));
                }
            }
        }
        private byte CalcChargesPercent(ushort batteryState)
        {
            if (batteryState >= MaxChargeLevel)
                return 100;
            if (batteryState <= MinChargeLevel)
                return 0;
            else
                return (byte)(100 * ((double)(batteryState - MinChargeLevel) / (MaxChargeLevel - MinChargeLevel)));
        }

        private void UpdateBattaryState(byte batteryState)
        {
            this.ChargePercent = batteryState;
            BatteryEventArgs args = new BatteryEventArgs();
            args.ChargeLevel = batteryState;
            this.OnBatteryStateChange(args);
        }
    }
}
