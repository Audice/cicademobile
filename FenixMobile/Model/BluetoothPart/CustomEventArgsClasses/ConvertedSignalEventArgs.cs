﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.BluetoothPart.CustomEventArgsClasses
{
    public class ConvertedSignalEventArgs: EventArgs
    {
        public uint PackageNumber { get; set; }
        public List<short[]> Signals { get; set; }
    }
}
