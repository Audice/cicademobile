﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.BluetoothPart.CustomEventArgsClasses
{
    public class DeltsDataEventArgs : EventArgs
    {
        public uint FileNumber { get; set; }
        public List<short[]> DeltasData { get; set; }
        public DateTime? FilesCreateTime { get; set; }
        public byte ChannelsNum { get; set; }
        public ushort UMV { get; set; }
        public short SamplePerSecond { get; set; }
        public int FileDirectory { get; set; }
        public int NumDirectory { get; set; }

    }
}
