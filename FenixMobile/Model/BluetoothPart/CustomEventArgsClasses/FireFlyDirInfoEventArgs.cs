﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.BluetoothPart.CustomEventArgsClasses
{
    public class FireFlyDirInfoEventArgs : EventArgs
    {
        public FireFlyDirInfo FireFlyDirInfo { get; set; }
    }
}
