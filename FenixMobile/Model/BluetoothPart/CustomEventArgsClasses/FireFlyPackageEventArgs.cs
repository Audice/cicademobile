﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.BluetoothPart.CustomEventArgsClasses
{
    public class FireFlyPackageEventArgs : EventArgs
    {
        public FireFlyPackage Package { get; set; }
    }
}
