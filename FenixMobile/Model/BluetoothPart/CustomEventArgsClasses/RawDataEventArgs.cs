﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.BluetoothPart.CustomEventArgsClasses
{
    public class RawDataEventArgs:EventArgs
    {
        public DateTime? FilesCreateTime { get; set; } = null;
        public uint PackageNumber { get; set; }
        public byte[,] RawData { get; set; } //Первый элемент - сигнал, второй - отведение
        public ushort UMV { get; set; }
        public short SamplePerSecond { get; set; }
        public byte ChannelsNum { get; set; }
        public int FileDirectory { get; set; }
    }
}
