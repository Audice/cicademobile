﻿using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using FenixMobile.Model.BluetoothPart.PackageControl;
using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.BluetoothPart.DataConvert
{
    /// <summary>
    /// Получение данных и преобразование их к объекивным данным
    /// </summary>
    public class DataConverter : IDisposable
    {
        SignalHandler Signal;
        EventHandler<RawDataEventArgs> RawDataReadyHandler;
        IPackageSequenceControler PackageSequenceControler;

        public DataConverter(IPackageSequenceControler packageSequenceControler, uint channelsNumber)
        {
            Signal = new SignalHandler(channelsNumber);
            this.PackageSequenceControler = packageSequenceControler;
            this.PackageSequenceControler.PartRawDataReadyEvent += RawDataReceived;
        }

        public event EventHandler<ConvertedSignalEventArgs> SignalsConvertedEvent;

        public void Dispose()
        {
            if (this.PackageSequenceControler != null)
                this.PackageSequenceControler.PartRawDataReadyEvent -= RawDataReceived;
        }

        protected virtual void OnSignalsConverted(ConvertedSignalEventArgs e)
        {
            EventHandler<ConvertedSignalEventArgs> handler = SignalsConvertedEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<DeltsDataEventArgs> DeltsDataReadyEvent;
        protected virtual void OnDeltasReady(DeltsDataEventArgs e)
        {
            EventHandler<DeltsDataEventArgs> handler = DeltsDataReadyEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }


        private void RawDataReceived(object sender, RawDataEventArgs e)
        {
            var data = e.RawData;
            if (this.Signal != null && data != null)
            {
                this.Signal.SetUMV(e.UMV);

                List<short[]> deltaSignals = this.Signal.SignalsSplitting(data);
                if (deltaSignals != null)
                {
                    List<short[]> signals = this.Signal.GetProcessedSignal(deltaSignals);
                    signals = this.Signal.LeadsCalculate(signals);
                    deltaSignals = this.Signal.GetDeltasSignals(signals);

                    //Инициируем событие появления нового пакета дельт
                    DeltsDataEventArgs args = new DeltsDataEventArgs()
                    {
                        DeltasData = deltaSignals,
                        FileNumber = e.PackageNumber,
                        FilesCreateTime = e.FilesCreateTime,
                        ChannelsNum = e.ChannelsNum,
                        FileDirectory = e.FileDirectory,
                        SamplePerSecond = e.SamplePerSecond,
                        UMV = e.UMV
                    };
                    //Возможно, что при FileTime != null нужно будет всё сбросить... В частности начальное положение интеграторов
                    OnDeltasReady(args);

                    if (signals != null)
                    {
                        ConvertedSignalEventArgs argsConverted = new ConvertedSignalEventArgs();
                        argsConverted.Signals = signals;
                        argsConverted.PackageNumber = e.PackageNumber;
                        OnSignalsConverted(argsConverted);
                    }
                }
            }
        }
    }
}
