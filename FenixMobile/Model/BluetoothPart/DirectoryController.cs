﻿using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using FenixMobile.Model.DatabasePart;
using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.BluetoothPart
{
    /// <summary>
    /// Объект, контроллирующий текущее состояние директории на кардиографе и самую свежую директорию
    /// </summary>
    public class DirectoryController : IDisposable
    {
        /// <summary>
        /// Текущая директория кардиографа
        /// </summary>
        FireFlyDirInfo CurrentDirectoryInfo { get; set; } = null;
        /// <summary>
        /// Информация о последней свежей директории кардиографа
        /// </summary>
        FireFlyDirInfo RecentDirectoryInfo { get; set; } = null;

        /// <summary>
        /// Ссылка а объект-генератор пакета DirInfo
        /// </summary>
        RawDataWorker RawDataWorker { get; set; }
        public DirectoryController(RawDataWorker rawDataWorker)
        {
            this.RawDataWorker = rawDataWorker;
            this.RawDataWorker.FireFlyDirInfoEvent += ReceivedDirInfo;
        }

        /// <summary>
        /// Метод, реагирующий на получение текущего состояния директорий и обновляющий его локально
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReceivedDirInfo(object sender, FireFlyDirInfoEventArgs e)
        {
            FireFlyDirInfo dirinfo = e.FireFlyDirInfo;
            this.SetDirInfo(dirinfo);
        }

        private void SetDirInfo(FireFlyDirInfo dirinfo)
        {
            if (dirinfo.CurrentDirectory == dirinfo.NumDirectories)
            {
                //Если номер текущей директории совпадает с числом директорий, значит мы в текущей директории
                this.RecentDirectoryInfo = dirinfo;
            }
            else
            {
                //Текущая директория не является свежей, а является поддиректорией. В этом случае поле FilesNum является не актуальным 
                this.CurrentDirectoryInfo = dirinfo;
            }
        }

        /// <summary>
        /// Метод определяет количество файлов, ориентируясь на разницу между текущей директорией и требуемой директорией
        /// TODO: обновить директорию перед закрытием??? возможно стоит брать информацию о последней записи?
        /// </summary>
        public void RegisteringUnreadFiles(int recordID = -1)
        {
            if (this.RecentDirectoryInfo != null)
            {
                RecordTable goalRecord = DatabaseWorker.getInstance().GetRecordByID(recordID);

                short numUnreadRecord = (short)(this.RecentDirectoryInfo.NumDirectories - goalRecord.CatalogNumber);
                if (numUnreadRecord > 0)
                {
                    //Если записей много - 
                    for (int i = 0; i < numUnreadRecord; i++)
                    {
                        short fileNum = -1;
                        if (this.RecentDirectoryInfo.NumDirectories == goalRecord.CatalogNumber + 1 + i)
                            fileNum = this.RecentDirectoryInfo.FilesNumber;
                        RecordTable record = new RecordTable()
                        {
                            CatalogNumber = goalRecord.CatalogNumber + 1 + i, //Получаем из DirInfo
                            UserID = DatabaseWorker.getInstance().CurrentUser.ID,
                            PatientID = DatabaseWorker.getInstance().CurrentPatient.ID,
                            FileName = "", //Имя фала необходимо для идентификации частей его
                            FileID = -1, //Появляется при устойчивом интернет соединении и создание файла
                            IsFileEnd = false,
                            LastFileNum = fileNum,
                            MAC = DatabaseWorker.getInstance().CurrentUser.MACS
                        };
                        DatabaseWorker.getInstance().CreateRecord(record);
                    }
                    return;
                }
                if (numUnreadRecord == 0 && recordID >= 0)
                {
                    //Значит мы уже в текущем файле - просто сохраним посленый известный номер файла
                    if (goalRecord.LastBluetoothFileNumber < this.RecentDirectoryInfo.FilesNumber)
                    {
                        DatabaseWorker.getInstance().UpdateRecordsLastFileNum(recordID, this.RecentDirectoryInfo.FilesNumber);
                    }
                    else
                    {
                        DatabaseWorker.getInstance().UpdateRecordsLastFileNum(recordID, (short)(goalRecord.LastBluetoothFileNumber));
                    }
                }
            }
        }

        public void Dispose()
        {
            this.RawDataWorker.FireFlyDirInfoEvent -= ReceivedDirInfo;
        }
    }
}
