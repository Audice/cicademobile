﻿using FenixMobile.Model.BatteryPart;
using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using FenixMobile.Model.DatabasePart;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace FenixMobile.Model.BluetoothPart
{
    /// <summary>
    /// Объект, описывающий мобильный кардиограф
    /// </summary>
    public class FireFly
    {
        private static FireFly BluetoothWorker;
        /// <summary>
        /// Объект обеспечения коннекта с кардиографом
        /// </summary>
        public BluetoothConnectServicer BluetoothConnectController
        {
            get; private set;
        }

        private RawDataWorker RawDataWorker
        {
            get; set;
        }

        public string DevicesName
        {
            get; private set;
        } = "FireFly";

        public int CharageLevel
        {
            get; private set;
        } = -1;

        private FireFlyDirInfo FireFlyInfo
        {
            get; set;
        } = null;




        public event EventHandler<FireFlyDirInfoEventArgs> FireFlyInfoUpdate;
        protected virtual void OnFireFlyInfoUpdate(FireFlyDirInfoEventArgs e)
        {
            EventHandler<FireFlyDirInfoEventArgs> handler = FireFlyInfoUpdate;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<EventArgs> NoDataCameEvent;
        protected virtual void NoDataCame(EventArgs e)
        {
            EventHandler<EventArgs> handler = NoDataCameEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<FireFlyPackageEventArgs> PackageCameEvent;
        protected virtual void OnPackageCame(FireFlyPackageEventArgs e)
        {
            EventHandler<FireFlyPackageEventArgs> handler = PackageCameEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }


        public FireFlyDirInfo GetFireFlyDirInfo()
        {
            return FireFlyInfo;
        }

        public BatteryHandler BatteryHandler
        {
            get; private set;
        } = null;

        /// <summary>
        /// Объект контроля состояния директории
        /// </summary>
        public DirectoryController DirectoryController
        {
            get; private set;
        } = null;


        protected FireFly()
        {
            
            //После коннекта запускается процедура автоматического подключения bluetooth устройств. 
            //Подключение циклическое... То есть, цикл поиска и подключения устройств максимально не зависим от пользователяы
            DatabaseWorker databaseWorker = DatabaseWorker.getInstance();
            if (databaseWorker != null && databaseWorker.CurrentUser != null && databaseWorker.CurrentUser.MACS != null 
                && databaseWorker.CurrentUser.MACS.Length == 17)
            {
                BluetoothConnectController = new BluetoothConnectServicer("FireFly", databaseWorker.CurrentUser.MACS);
            }
            else
            {
                BluetoothConnectController = new BluetoothConnectServicer("FireFly");
                
            }
            this.BatteryHandler = new BatteryHandler();
            RawDataWorker = new RawDataWorker(this.BluetoothConnectController, this.BatteryHandler);

            this.DirectoryController = new DirectoryController(this.RawDataWorker);

            this.BatteryHandler.BattaryStateChangeEvent += BattStateChangeAction;
            RawDataWorker.FireFlyDirInfoEvent += DirInfoChangeAction;
            RawDataWorker.NoDataEvent += NoDataHandler;
            RawDataWorker.FireFlyPackageReadyEvent += PackageCameHandler;
        }

        private void PackageCameHandler(object sender, FireFlyPackageEventArgs e)
        {
            this.OnPackageCame(new FireFlyPackageEventArgs() { Package = e.Package });
        }

        private void NoDataHandler(object sender, EventArgs e)
        {
            this.NoDataCame(EventArgs.Empty);
        }

        private void BattStateChangeAction(object sender, BatteryEventArgs e)
        {
            this.CharageLevel = e.ChargeLevel;
        }

        private void DirInfoChangeAction(object sender, FireFlyDirInfoEventArgs e)
        {
            //Проброс события изменения DirInfo
            this.FireFlyInfo = e.FireFlyDirInfo;
            this.OnFireFlyInfoUpdate(new FireFlyDirInfoEventArgs() { FireFlyDirInfo = this.FireFlyInfo });
        }

        public void SetNewDevice(string newMAC)
        {
            if (this.BluetoothConnectController != null)
            {
                //Удаляем информацию о директориях
                this.FireFlyInfo = null;
                this.OnFireFlyInfoUpdate(new FireFlyDirInfoEventArgs() { FireFlyDirInfo = this.FireFlyInfo });
                this.BluetoothConnectController.UpdateDevicesMAC(newMAC);
            }
        }

        public static FireFly getInstance()
        {
            if (BluetoothWorker == null)
            {
                BluetoothWorker = new FireFly();
            }
            return BluetoothWorker;
        }

        public void SetActualDirectory(short currentCatalog = -1)
        {
            if (currentCatalog < 0)
                //Мы уверены, что информация о дирректории обновлена
                this.BluetoothConnectController.SetDirectory(this.FireFlyInfo.NumDirectories);
            else
                this.BluetoothConnectController.SetDirectory(currentCatalog);
        }

        public List<string> GetDevicesList()
        {
            List<string> resultsString = new List<string>();
            if (DependencyService.Get<IBluetoothController>().CheckBluetoothModule())
            {
                var devicesList = DependencyService.Get<IBluetoothController>().GetListDevices(this.DevicesName);
                for (int i=0; i < devicesList.Count; i++)
                {
                    resultsString.Add(devicesList[i].Item2);
                }
            }
            return resultsString;
        }




    }
}
