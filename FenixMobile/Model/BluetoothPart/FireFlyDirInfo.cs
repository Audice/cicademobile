﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.BluetoothPart
{
    public class FireFlyDirInfo
    {
        //Список полей сообщения
        /// <summary>
        /// Поле сообщения SD Present
        /// </summary>
        readonly byte[] MessageSD_Present = new byte[] { 83, 68, 58, 32 };
        /// <summary>
        /// Поле сообщения Current DIR
        /// </summary>
        readonly byte[] MessageCurrent_Dir = new byte[] { 68, 105, 114, 58, 32 };
        /// <summary>
        /// Поле сообщения Current File
        /// </summary>
        readonly byte[] MessageCurrent_File = new byte[] { 70, 105, 108, 101, 58, 32 };
        /// <summary>
        /// Поле сообщения File Length
        /// </summary>
        readonly byte[] MessageFile_Length = new byte[] { 76, 101, 110, 103, 116, 104, 58, 32 };
        /// <summary>
        /// Поле сообщения File seamples per second 
        /// </summary>
        readonly byte[] MessageFile_SPS = new byte[] { 83, 80, 83, 58, 32 };
        /// <summary>
        /// Поле сообщения File channels
        /// </summary>
        readonly byte[] MessageFile_Channels = new byte[] { 67, 104, 97, 110, 110, 101, 108, 115, 58, 32 };
        /// <summary>
        /// Поле сообщения File channels
        /// </summary>
        readonly byte[] MessageFile_Time = new byte[] { 84, 105, 109, 101, 58, 32 };
        /// <summary>
        /// Маска времени
        /// </summary>
        readonly string TimesMask = "dd.MM.yy HH:mm:ss";
        /// <summary>
        /// Шаблон конца строки
        /// </summary>
        readonly byte[] EndStrokeTemplate = new byte[] { 13, 10 };

        //Базовые поля класса
        /// <summary>
        /// Флаг наличия дополнительной флэш памяти
        /// </summary>
        public bool IsSD_Memory
        {
            get; private set;
        } = false;
        /// <summary>
        /// Номер текущего каталога
        /// </summary>
        public short CurrentDirectory
        {
            get; set;
        } = -1;
        /// <summary>
        /// Номер каталога, созданного при текущем включении
        /// </summary>
        public short NumDirectories
        {
            get; set;
        } = -1;
        /// <summary>
        /// Номер текущего файла
        /// </summary>
        public short FilesNumber
        {
            get; set;
        } = -1;
        /// <summary>
        /// Длина файла
        /// </summary>
        public short FilesLength
        {
            get; private set;
        } = -1;
        /// <summary>
        /// Частота семплов в файле
        /// </summary>
        public short FilesSPS
        {
            get; private set;
        } = -1;
        /// <summary>
        /// Число каналов
        /// </summary>
        public short FilesChannels
        {
            get; private set;
        } = -1;

        public DateTime? DirTime
        {
            get; private set;
        } = null;

        public FireFlyDirInfo(byte[] rawDirInfo)
        {
            ParseInfo(rawDirInfo);
        }

        private void ParseInfo(byte[] rawDirInfo)
        {
            try
            {
                this.IsSD_Memory = SearchTemplate(rawDirInfo, this.MessageSD_Present) == 1 ? true : false;
                SearchCatalogs_FilesNumber(rawDirInfo);
                this.FilesLength = SearchTemplate(rawDirInfo, this.MessageFile_Length);
                this.FilesChannels = SearchTemplate(rawDirInfo, this.MessageFile_Channels);
                this.FilesSPS = SearchTemplate(rawDirInfo, this.MessageFile_SPS);
                this.FilesNumber = SearchTemplate(rawDirInfo, this.MessageCurrent_File);
                this.DirTime = SearcheTime(rawDirInfo);
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
        }

        private DateTime? SearcheTime(byte[] rawData)
        {
            if (rawData == null) return null;
            int rightBorder = rawData.Length - this.MessageFile_Time.Length;
            int startIndex = -1;
            for (int i = 0; i < rightBorder; i++)
            {
                bool isTemplateFind = true;
                for (int j = 0; j < this.MessageFile_Time.Length; j++)
                {
                    if (rawData[i + j] != this.MessageFile_Time[j])
                    {
                        isTemplateFind = false;
                    }
                }
                if (isTemplateFind)
                {
                    startIndex = i;
                    break;
                }
            }
            if (startIndex >= 0)
            {
                startIndex += this.MessageFile_Time.Length;
                int finishIndex = startIndex + TimesMask.Length;
                if (finishIndex < rawData.Length)
                {
                    byte[] byteDate = new byte[TimesMask.Length];
                    for (int i = 0; i < byteDate.Length; i++)
                    {
                        byteDate[i] = rawData[i + startIndex];
                    }
                    string strCurDate = Encoding.ASCII.GetString(byteDate);
                    DateTime curDate = DateTime.ParseExact(strCurDate, this.TimesMask,
                                       System.Globalization.CultureInfo.InvariantCulture);
                    return curDate;
                }
            }
            return null;
        }

        private void SearchCatalogs_FilesNumber(byte[] rawData)
        {
            if (rawData == null) return;
            int rightBorder = rawData.Length - this.MessageCurrent_Dir.Length;
            int startIndex = -1;

            for (int i = 0; i < rightBorder; i++)
            {
                bool isTemplateFind = true;
                for (int j = 0; j < this.MessageCurrent_Dir.Length; j++)
                {
                    if (rawData[i + j] != this.MessageCurrent_Dir[j])
                    {
                        isTemplateFind = false;
                    }
                }
                if (isTemplateFind)
                {
                    startIndex = i;
                    break;
                }
            }
            if (startIndex >= 0)
            {
                int slashIndex = -1;
                int finishIndex = -1;
                startIndex += this.MessageCurrent_Dir.Length;
                //Поиск конца шаблонной строки
                for (int i = startIndex; i < rightBorder - 1; i++)
                {
                    if (rawData[i] == EndStrokeTemplate[0] && rawData[i + 1] == EndStrokeTemplate[1])
                    {
                        finishIndex = i;
                        break;
                    }
                    if (rawData[i] == 47)
                    {
                        slashIndex = i;
                    }
                }
                if (finishIndex > 0 && slashIndex > 0 && slashIndex < finishIndex)
                {
                    byte[] valueCatalogArray = new byte[slashIndex - startIndex];
                    Array.Copy(rawData, startIndex, valueCatalogArray, 0, valueCatalogArray.Length);
                    byte[] valueRecArray = new byte[finishIndex - slashIndex - 1];
                    Array.Copy(rawData, slashIndex + 1, valueRecArray, 0, valueRecArray.Length);

                    string strCatalogValue = Encoding.ASCII.GetString(valueCatalogArray);
                    string strRecValue = Encoding.ASCII.GetString(valueRecArray);

                    short catalogValue = -1; short recValue = -1;

                    if (short.TryParse(strCatalogValue, out catalogValue) && short.TryParse(strRecValue, out recValue))
                    {
                        this.CurrentDirectory = catalogValue; this.NumDirectories = recValue;
                        return;
                    }
                }
            }
            this.CurrentDirectory = -1; this.NumDirectories = -1;
        }

        private short SearchTemplate(byte[] rawData, byte[] template)
        {
            short result = -100;
            if (rawData == null) return result;            
            int rightBorder = rawData.Length - template.Length;
            int startIndex = -1;
            
            for (int i=0; i < rightBorder; i++)
            {
                bool isTemplateFind = true;
                for (int j = 0; j < template.Length; j++)
                {
                    if (rawData[i + j] != template[j])
                    {
                        isTemplateFind = false;
                    }
                }
                if (isTemplateFind)
                {
                    startIndex = i;
                    break;
                }
            }
            if (startIndex >= 0)
            {
                int finishIndex = -1;
                startIndex += template.Length;
                //Поиск конца шаблонной строки
                for (int i = startIndex; i < rawData.Length - 1; i++)
                {
                    if (rawData[i] == EndStrokeTemplate[0] && rawData[i + 1] == EndStrokeTemplate[1])
                    {
                        finishIndex = i;
                        break;
                    }
                }
                if (finishIndex > 0)
                {
                    byte[] valueArray = new byte[finishIndex - startIndex];
                    Array.Copy(rawData, startIndex, valueArray, 0, valueArray.Length);
                    string strValue = Encoding.ASCII.GetString(valueArray);
                    short.TryParse(strValue, out result);
                }
            }
            return result;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj is FireFlyDirInfo)
            {
                FireFlyDirInfo localInfo = obj as FireFlyDirInfo;
                if (this.CurrentDirectory == localInfo.CurrentDirectory && this.DirTime == localInfo.DirTime
                    && this.FilesChannels == localInfo.FilesChannels && this.FilesLength == localInfo.FilesLength
                    && this.FilesNumber == localInfo.FilesNumber && this.FilesSPS == localInfo.FilesSPS
                    && this.IsSD_Memory == localInfo.IsSD_Memory && this.NumDirectories == localInfo.NumDirectories)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

    }
}
