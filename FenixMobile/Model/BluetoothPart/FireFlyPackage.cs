﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.BluetoothPart
{
    public class FireFlyPackage
    {
        public readonly byte HeaderSize = 11;
        public Int16 PackageLength { get; private set; }
        public Int16 SamplingFrequency { get; private set; }
        public Byte NumOfBit { get; private set; }
        public Int16 UMV { get; private set; }
        public Byte NumOfChenal { get; private set; }
        public uint MeasurementNumber { get; private set; }
        public byte[,] PackageData { get; private set; }


        public FireFlyPackage(Int16 _PackageLength, Int16 _SamplingFrequency, Byte _NumOfBit, Int16 _UMV, Byte _NumOfChenal, uint _MeasurementNumber, byte[] data)
        {
            this.PackageLength = _PackageLength;
            this.SamplingFrequency = _SamplingFrequency;
            this.NumOfBit = _NumOfBit;
            this.UMV = _UMV;
            this.NumOfChenal = _NumOfChenal;
            this.MeasurementNumber = _MeasurementNumber;
            int countWire = this.NumOfChenal;
            int countMeasurements = (this.PackageLength - HeaderSize) / this.NumOfChenal; // / (this.NumOfBit / 8); - так как в пакете храним только массив byte, то отказываемся от выделения днных
            if (countWire > 0 && countMeasurements > 0)
                this.PackageData = new byte[countMeasurements, countWire];
            else
                this.PackageData = null;

            SetPackageData(data);
        }

        public ushort GetDataSize()
        {
            return (ushort)(this.PackageLength - this.HeaderSize);
        }

        private void SetPackageData(byte[] SignalsArray)
        {
            if (this.NumOfChenal > 0)
            {
                if (PackageData == null || (this.PackageLength - HeaderSize) != SignalsArray.Length)
                {
                    this.PackageData = new byte[(SignalsArray.Length) / this.NumOfChenal, this.NumOfChenal];
                }
                int recordBytes = this.NumOfBit / 8; //Количество байт в единице ЭКГ
                uint arraySwitcher = 0;
                int PackageDataCounter = 0;

                for (int i = 0; i < SignalsArray.Length; i++)
                {
                    if (i != 0 && i % recordBytes == 0)
                    {
                        arraySwitcher = (arraySwitcher + 1) % this.NumOfChenal;
                        PackageDataCounter = (i / (recordBytes * this.NumOfChenal)) * recordBytes;
                    }
                    else
                    {
                        if (i != 0)
                        {
                            PackageDataCounter++;
                        }
                    }
                    this.PackageData[PackageDataCounter, arraySwitcher] = SignalsArray[i];
                }
            }
            else
            {
                PackageData = null;
            }
        }
    }
}
