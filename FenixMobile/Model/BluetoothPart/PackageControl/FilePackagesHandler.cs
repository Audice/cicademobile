﻿using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FenixMobile.Model.BluetoothPart.PackageControl
{
    /// <summary>
    /// Класс обеспечивает контроль получения данных и подготовку пакета для класса, формирующего сигнал
    /// Содержит очередь из пакетов, полученных от кардиографа.
    /// Маркером корректности пакета служит получение файлов, количество которых указано в FileCapacity
    /// </summary>
    public class FilePackagesHandler
    {
        private uint? PrevPackageNumber { get; set; } = null;
        /// <summary>
        /// Номер текущей директории
        /// </summary>
        private int DirectoryNum { get; set; }
        /// <summary>
        /// Флаг, характеризующий состояние инициализации обработчика пакетов
        /// </summary>
        public bool PackagesHandlerInit { get; private set; } = false;
        private short? SamplePerSecond
        {
            get; set;
        } = null;
        /// <summary>
        /// Число каналов ЭКГ
        /// </summary>
        public byte? ChannelsNum { 
            get; 
            private set; 
        }

        private List<uint> PackageNumberMap = null;

        /// <summary>
        /// Количество пакетов, которое содержит текущий файл
        /// </summary>
        private short? FileCapacity
        {
            get; set;
        } = null;
        /// <summary>
        /// Список пакетов, принятых от кардиографа, в одном файле
        /// </summary>
        private List<FireFlyPackage> FilesPackages = null;
        /// <summary>
        /// Список пакетов, подготовленные для отправки формирователю сигнала
        /// </summary>
        private List<FireFlyPackage> Packages = null;        
        /// <summary>
        /// Номер файла, для сохранения последовательности данных
        /// </summary>
        private short FileNumber
        {
            get; set;
        } = 0;
        
        
        /// <summary>
        /// Корректирующий коэфициент для напряжения. Настраивается отдельно
        /// </summary>
        private short? FilesUMV
        {
            get; set;
        } = null;
        /// <summary>
        /// Время создания директории. Служит для организации времени создания файла на сервисе.
        /// Если он не равен null, значит пора создать новый файл
        /// </summary>
        public DateTime? DirTime { 
            get; set; } = null;

        public FilePackagesHandler()
        {
            this.PackagesHandlerInit = false;
            this.Packages = new List<FireFlyPackage>();
            this.FilesPackages = new List<FireFlyPackage>();
        }

        /// <summary>
        /// Метод установки парамметров обрабатываемых файлов.
        /// Может изменяться при смене директории
        /// </summary>
        /// <param name="dirInfo">Информация о директории</param>
        /// <returns>Были ли установлены парамметры файлов: true - установлены, false - нет</returns>
        public bool SetTransmissionParameters(FireFlyDirInfo dirInfo, DateTime? recordTime = null)
        {
            //Сбросим информацию о вольтаже 
            this.FilesUMV = null;
            //Сбрасываем информацию о номере
            this.FileNumber = 0;
            this.DirTime = recordTime == null ? dirInfo.DirTime : recordTime;
            if (this.DirTime == null) return false;
            this.FileCapacity = dirInfo.FilesLength;
            if (this.FileCapacity < 1) return false;   
            this.ChannelsNum = (byte)(dirInfo.FilesChannels);
            //TODO: при появлении большего числа каналов, изменить флаг
            if (this.ChannelsNum < 1 || this.ChannelsNum > 2) return false; 
            this.SamplePerSecond = dirInfo.FilesSPS;
            if (this.SamplePerSecond < 0) return false;
            this.PrevPackageNumber = null;
            this.DirectoryNum = dirInfo.CurrentDirectory;
            this.PackagesHandlerInit = true;
            return true;
        }

        /// <summary>
        /// Метод вставки пакета в список данных
        /// </summary>
        /// <param name="package">Пакет с данными, содержащий данные и информацию о сигнале</param>
        public void InsertPackages(FireFlyPackage package)
        {
            if (package == null) return; //Если пакет битый - отклоняем добавление
            if (package.UMV <= 0) return; //Если коэфициент напрежения плохой - то отклоняем такой пакет
            this.FilesUMV = package.UMV; //Обновляем состояние напряжения для данного пакета ???
            int insertIndex = this.FilesPackages.FindIndex(x => x.MeasurementNumber + 1 == package.MeasurementNumber);
            if (insertIndex == -1 || insertIndex == (this.FilesPackages.Count - 1))
                this.FilesPackages.Add(package);
            else
                this.FilesPackages.Insert(insertIndex + 1, package);
        }
        /// <summary>
        /// Формирование данных для сигнала
        /// </summary>
        /// <returns>Пакет данных, либо NULL если возникли проблемы с формированием</returns>
        public RawDataEventArgs GetFilePackages()
        {
            List<FireFlyPackage> readyFile = BuildPackagesData();
            if (readyFile != null)
            {
                RawDataEventArgs args = new RawDataEventArgs();
                args.PackageNumber = (uint)(this.FileNumber);
                args.UMV = (ushort)(this.FilesUMV);
                args.FilesCreateTime = this.DirTime;
                args.RawData = GetFilesData(readyFile);
                args.ChannelsNum = this.ChannelsNum.Value;
                args.SamplePerSecond = this.SamplePerSecond.Value;
                args.FileDirectory = this.DirectoryNum;
                this.FileNumber++;
                //DirTime нужен лишь один раз, далее его можно занулить
                this.DirTime = null;
                return args;
            }
            return null;
        }
        /// <summary>
        /// Получение хвоста данных
        /// </summary>
        /// <returns>Пакет, содержащий последнии данные</returns>
        public RawDataEventArgs GetFileTail()
        {
            RawDataEventArgs args = null;
            if (IsBuildDataTail())
            {
                args = new RawDataEventArgs();
                args.PackageNumber = (uint)(this.FileNumber);
                args.UMV = (ushort)(this.FilesUMV);
                args.FilesCreateTime = null;
                args.RawData = GetFilesData(this.Packages);
                args.ChannelsNum = this.ChannelsNum.Value;
                args.SamplePerSecond = this.SamplePerSecond.Value;
                args.FileDirectory = this.DirectoryNum;
            }
            //Сбрасываем текущее состояние обработчика пакетов
            this.ResetHandler();
            return args;
        }

        private void ResetHandler()
        {
            this.PackagesHandlerInit = false;
            this.Packages.Clear();
            this.CleanUsedLists();
            this.FileCapacity = null;
            this.ChannelsNum = null;
            this.SamplePerSecond = null;
            this.DirTime = null;
        }

        /// <summary>
        /// Метод, организовывающий нормальную структуру оставшихся пакетов
        /// </summary>
        /// <returns>Результат реструктуризации оставшихся пакетов</returns>
        private bool IsBuildDataTail()
        {
            //Если данных нетто возвращаем ничего
            if (this.Packages == null) return false;
            if (this.Packages.Count <= 0) return false;
            FireFlyPackage referencePackage = this.Packages[0];
            for (int i = 0; i < this.Packages.Count - 1; i++)
            {
                //Если встречена ситуация, когда следующий пакет отличается от предыдущего больше чем на единицу,
                //Значит там пропущен пакет. 
                if ((this.Packages[i].MeasurementNumber + 1) != this.Packages[i + 1].MeasurementNumber)
                {
                    //Замастим пустое место сгенерированным пакетом
                    FireFlyPackage generatedPackage = new FireFlyPackage(referencePackage.PackageLength, referencePackage.SamplingFrequency,
                                                        referencePackage.NumOfBit, referencePackage.UMV, this.ChannelsNum.Value,
                                                        (uint)(this.Packages[i].MeasurementNumber + 1),
                                                        FakeDataBuilder(referencePackage.GetDataSize(), referencePackage.NumOfChenal));
                    //Вставляем сгенерированный пакет в общий список пакетов
                    this.Packages.Insert(i + 1, generatedPackage);
                }
            }
            return true;
        }
        private List<FireFlyPackage> BuildPackagesData()
        {
            if (this.Packages == null || this.FileCapacity.Value <= 0) return null;
            //Если в пакете данных содержится больше или столько же пакетов, сколько определено вместимостью - начинаем сбор данных
            if (this.Packages.Count >= this.FileCapacity.Value)
            {
                //Запоминаем эталонные пакет
                FireFlyPackage referencePackage = this.Packages[0];
                //Зная старт пакетов файла накопим FileCapacity пакетов
                List<FireFlyPackage> localPackagesList = new List<FireFlyPackage>();
                int i = 0;
                //Если нет информации о номере предыдущего пакета, нужно его установить и добавить исследованный пакет в список
                if (this.PrevPackageNumber == null)
                {
                    this.PrevPackageNumber = referencePackage.MeasurementNumber;
                    i = 1;
                }
                    


                for (; i < this.FileCapacity.Value; i++)
                {
                    //Если встречена ситуация, когда следующий пакет отличается от предыдущего больше чем на единицу,
                    //Значит там пропущен пакет. 
                    if ((this.PrevPackageNumber + 1) != this.Packages[i].MeasurementNumber)
                    {
                        //Замастим пустое место сгенерированным пакетом
                        FireFlyPackage generatedPackage = new FireFlyPackage(referencePackage.PackageLength, referencePackage.SamplingFrequency,
                                                            referencePackage.NumOfBit, referencePackage.UMV, this.ChannelsNum.Value,
                                                            (uint)(this.PrevPackageNumber + 1),
                                                            FakeDataBuilder(referencePackage.GetDataSize(), referencePackage.NumOfChenal));
                        //Вставляем сгенерированный пакет в общий список пакетов
                        this.Packages.Insert(i, generatedPackage);
                    }
                    this.PrevPackageNumber++;
                }
                //Теперь подготовим файл с FileCapacity пакетами
                for (i = 0; i < this.FileCapacity.Value; i++)
                    localPackagesList.Add(this.Packages[i]);
                //Удалим ненужные пакеты
                this.Packages.RemoveRange(0, this.FileCapacity.Value);
                return localPackagesList;
            }
            //Если пакетов недостаточно
            return null;
        }

        /// <summary>
        /// Проверка валидности пакетов, полученных в файле. 
        /// В частности проверяется количество пакетов в файле: должно быть равно FileCapacity
        /// TODO: бывают косяки!!!
        /// </summary>
        /// <returns>Валидный файл - true, не валидный - false</returns>
        public bool CheckFilesValidity()
        {
            if (this.FilesPackages != null && this.FilesPackages.Count == this.FileCapacity)
            {
                //Полученный файл имеет число пакетов, указанное в this.FileCapacity
                //Данные в порядке. Можно добавлять их в общий список пакетов и чистить карту и хранилище
                this.Packages.AddRange(this.FilesPackages);
                this.CleanUsedLists();
                return true;
            }
            else
            {
                if (this.FilesPackages == null) 
                    return false; //Приходит не валидный файл, продолжим просить один и тот же файл

                if (this.PackageNumberMap == null)
                {
                    //Формируем карту пакетов
                    List<uint> localPackageNumberMap = new List<uint>();
                    for (int i=0; i < this.FilesPackages.Count; i++)
                        localPackageNumberMap.Add(this.FilesPackages[i].MeasurementNumber);
                    this.PackageNumberMap = localPackageNumberMap;
                }
                else
                {
                    //Если карта пакетов сформирована, нужно сравнить пришедшие пакеты 
                    List<uint> localPackageNumberMap = new List<uint>();
                    for (int i = 0; i < this.FilesPackages.Count; i++)
                        localPackageNumberMap.Add(this.FilesPackages[i].MeasurementNumber);

                    if (localPackageNumberMap.Count != this.PackageNumberMap.Count)
                    {
                        //Не равны длины двух пакетов, то вераятно были потери при передаче
                        //Очищаем всё
                        if (this.PackageNumberMap != null)
                        {
                            this.PackageNumberMap.Clear();
                            this.PackageNumberMap = null;
                        }
                        //И просим о повторной отправке текущего пакета
                        return false;
                    }
                    bool equalsMap = true;
                    for (int i=0; i < localPackageNumberMap.Count; i++)
                        if (localPackageNumberMap[i] != this.PackageNumberMap[i])
                        {
                            equalsMap = false;
                            break;
                        }

                    if (equalsMap)
                    {
                        //Похоже, что есть потеря пакета на самом кардиографе
                        //Добавляем пакет в таком виде к общему массиву данных
                        this.Packages.AddRange(this.FilesPackages);
                        this.CleanUsedLists();
                        return true;
                    }
                    else
                    {
                        //TODO: можно ввести счётчик неудачных попыток получения файла...
                        //Если карты не равны, значит вероятны проблемы соединения. Очищаем старые данные
                        this.CleanUsedLists();
                        return false;
                    }
                }
                this.FilesPackages.Clear();
            }
            return false;
        }

        void CleanUsedLists()
        {
            this.FilesPackages.Clear();
            if (this.PackageNumberMap != null)
            {
                this.PackageNumberMap.Clear();
                this.PackageNumberMap = null;
            }
        }
        /// <summary>
        /// Построение фейкового пакета для замены пропусков
        /// </summary>
        /// <param name="dataSize">Размер данных, который необходимо сгенерировать</param>
        /// <param name="numLeads">Число каналов</param>
        /// <returns>Массив byte, представляющие сигнал</returns>
        private byte[] FakeDataBuilder(ushort dataSize, byte numLeads)
        {
            byte[] fakeSignal = new byte[dataSize];
            dataSize = (ushort)(dataSize / 2);
            int extremController = 0;
            int dataLimits = 0;
            List<short> fakeSignalList = new List<short>();
            short someSignal = 0;
            for (int i=0; i < dataSize - numLeads; i += numLeads)
            {
                if (extremController % 3 == 0)
                    someSignal = 10000;
                if (extremController % 2 == 0)
                    someSignal = -10000;

                for (int j = 0; j < numLeads; j++)
                {
                    fakeSignalList.Add(someSignal);
                }
                dataLimits += someSignal;
                extremController++;
            }
            someSignal = (short)(0-dataLimits);
            //TODO
            for (int i = dataSize - numLeads; i < dataSize; i ++)
                fakeSignalList.Add(someSignal);

            int bytearrayCounter = 0;
            for (int i=0; i < fakeSignalList.Count; i++)
            {
                if (bytearrayCounter >= fakeSignal.Length) break;
                byte[] bytes = BitConverter.GetBytes(fakeSignalList[i]);
                fakeSignal[bytearrayCounter] = bytes[0];
                fakeSignal[bytearrayCounter + 1] = bytes[1];
                bytearrayCounter += 2;
            }
            return fakeSignal;
        }

        public void ClearPackagesList()
        {
            this.FilesPackages.Clear();
        }

        byte[,] GetFilesData(List<FireFlyPackage> filesPackages)
        {
            List<byte>[] listLeads = new List<byte>[this.ChannelsNum.Value];
            for (int i = 0; i < listLeads.Length; i++)
                listLeads[i] = new List<byte>();
            for (int i = 0; i < filesPackages.Count; i++)
            {
                for (int pac_i = 0; pac_i < filesPackages[i].PackageData.GetLength(0); pac_i++)
                {
                    for (int pac_j = 0; pac_j < filesPackages[i].PackageData.GetLength(1); pac_j++)
                    {
                        listLeads[pac_j].Add(filesPackages[i].PackageData[pac_i, pac_j]);
                    }
                }
            }
            byte[,] filesData = new byte[listLeads[0].Count, this.ChannelsNum.Value];
            for (int pac_i = 0; pac_i < filesData.GetLength(0); pac_i++)
            {
                for (int pac_j = 0; pac_j < filesData.GetLength(1); pac_j++)
                {
                    filesData[pac_i, pac_j] = listLeads[pac_j][pac_i];
                }
            }
            return filesData;
        }

    }
}
