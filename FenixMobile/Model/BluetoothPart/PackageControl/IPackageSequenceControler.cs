﻿using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.BluetoothPart.PackageControl
{
    public interface IPackageSequenceControler
    {
        EventHandler<RawDataEventArgs> PartRawDataReadyEvent { get; set; }
    }
}
