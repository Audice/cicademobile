﻿using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FenixMobile.Model.BluetoothPart.PackageControl
{
    public class OldPackageSequenceControler : IDisposable, IPackageSequenceControler
    {
        private readonly ushort FileSize = 125;
        /// <summary>
        /// Актуальная информация о кардиографе
        /// </summary>
        private FireFlyDirInfo DirInfo { get; set; }

        /// <summary>
        /// Номер текущего файла, полученного от кардиографа
        /// </summary>
        public uint CurrentFileNumber
        {
            get; private set;
        } = 0;

        private uint currentPackageNumber = 0;
        /// <summary>
        /// Номер последнего пакета, полученного от кардиографа
        /// </summary>
        public uint CurrentPackageNumber
        {
            get { return currentPackageNumber; }
            private set
            {
                currentPackageNumber = value;
            }
        }

        private bool isCheckAvailability = false;
        /// <summary>
        /// Контроль завершения процесса считывания старых данных
        /// </summary>
        public bool IsCheckAvailability
        {
            get { return isCheckAvailability; }
            private set
            {
                isCheckAvailability = value;
                if (!value)
                    ProcessStopEvent?.Invoke(this, EventArgs.Empty); //Продумать Dispose
            }
        }
        public EventHandler<EventArgs> ProcessStopEvent;

        /// <summary>
        /// Ссылка на кардиограф
        /// </summary>
        FireFly Device { get; set; }

        /// <summary>
        /// Ссылка на RawDataWorker
        /// </summary>
        RawDataWorker LocalRawDataWorker;

        /// <summary>
        /// Событие, возникающее при появлении нового валидного пакета данных
        /// </summary>
        public EventHandler<RawDataEventArgs> PartRawDataReadyEvent { get; set; }
        protected virtual void OnPartRawDataReady(RawDataEventArgs e)
        {
            EventHandler<RawDataEventArgs> handler = PartRawDataReadyEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        bool IsCurrentCatalogMode { get; set; } = false;

        public OldPackageSequenceControler(RawDataWorker rawDataWorker, FireFly device, uint lastBtPackageNum, bool isCurrentCatalog = false)
        {
            this.IsCurrentCatalogMode = isCurrentCatalog;
            this.CurrentPackageNumber = lastBtPackageNum;
            this.Device = device;
            this.LocalRawDataWorker = rawDataWorker;

            this.LocalRawDataWorker.FireFlyPackageReadyEvent += OldFileBatchProcessing;

            if (!this.IsCurrentCatalogMode)
            {
                this.LocalRawDataWorker.NoDataEvent += FlashOutOfData;
            }
            

            this.LocalRawDataWorker.FireFlyDirInfoEvent += FireFlyDirInfoUpdate;

            this.IsCheckAvailability = true;
        }

        public void RunSDsLoadProces()
        {
            //Точка входа для запуска опрашивающей таски одна. Циклическая таска работает, пока процесс не будет остановлен
            FileAvailabilityCheckTask().ContinueWith(RunnerAvailabilityTask);
        }

        Task FileAvailabilityCheckTask()
        {
            return Task.Factory.StartNew(
                () =>
                {
                    if (this.IsCheckAvailability)
                    {
                        Task.Delay(1000).GetAwaiter().GetResult();
                        if (this.Device.BluetoothConnectController.IsBluetoothConnect)
                        {
                            this.Device.BluetoothConnectController.GetDirInfo();
                        }
                        Task.Delay(15000).GetAwaiter().GetResult(); //Чаще чем 4 раза в минуту просить нет смысла
                    }
                });
        }
        void RunnerAvailabilityTask(Task t)
        {
            if (this.IsCheckAvailability)
            {
                FileAvailabilityCheckTask().ContinueWith(RunnerAvailabilityTask);
            }
        }

        /// <summary>
        /// Метод, реагирующий на окончание данных в текущей директории кардиографа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void FlashOutOfData(object sender, EventArgs e)
        {
            //Пришло NO DATA от кардиографа, что сведетельствует об конце пакетов в текущей директории
            this.IsCheckAvailability = false;
        }

        /// <summary>
        /// Метод ручной остановки обмена данными с кардиографом
        /// </summary>
        public void AbortFileTransfer()
        {
            this.IsCheckAvailability = false;
        }

        /// <summary>
        /// При работе со старыми файлами, метод служит флагом наличия интернет соединения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void FireFlyDirInfoUpdate(object sender, EventArgs e)
        {
            FireFlyDirInfo info = (e as FireFlyDirInfoEventArgs).FireFlyDirInfo;
            if (this.IsCheckAvailability)
            {
                GetFilePart();
            }
        }

        void GetFilePart()
        {
            uint currentFileNumber = (this.CurrentPackageNumber + 1) / this.FileSize;
            // Отправка запроса на получение данных
            if (this.IsCheckAvailability && this.Device.BluetoothConnectController.IsBluetoothConnect)
            {
                this.Device.BluetoothConnectController.GetConcretFile((int)currentFileNumber);
            }
        }

        void OldFileBatchProcessing(object sender, EventArgs e)
        {
            FireFlyPackage package = (e as FireFlyPackageEventArgs).Package;
            if (this.CurrentPackageNumber == 0) this.CurrentPackageNumber = package.MeasurementNumber;
            else
            {
                uint packageNumberInterest = this.CurrentPackageNumber + 1;
                if (packageNumberInterest == package.MeasurementNumber)
                {
                    //При соблюдении последовательности формируется пакет данных и прокидывается дальше
                    RawDataEventArgs args = new RawDataEventArgs();
                    args.PackageNumber = package.MeasurementNumber;
                    args.RawData = package.PackageData;
                    this.OnPartRawDataReady(args);
                    this.CurrentPackageNumber = package.MeasurementNumber;
                }
            }
        }


        public void Dispose()
        {
            this.IsCheckAvailability = false;
            this.LocalRawDataWorker.FireFlyPackageReadyEvent -= OldFileBatchProcessing;
            if (!this.IsCurrentCatalogMode)
            {
                this.LocalRawDataWorker.NoDataEvent -= FlashOutOfData;
            }

            this.LocalRawDataWorker.FireFlyDirInfoEvent -= FireFlyDirInfoUpdate;
        }
    }
}
