﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.BluetoothPart.PackageControl.PackageControllerState.Actions
{
    public class ChangeDirAction : IActionKind
    {
        public ushort TimerRange { get; set; } = 45000;

        public void SendCommand(PackageBehaviorController behController)
        {
            throw new NotImplementedException();
        }

        public void TimeAction(object behController)
        {
            PackageBehaviorController currentBehavior = behController as PackageBehaviorController;
            FireFly.getInstance().BluetoothConnectController.SetDirectory(currentBehavior.RequiredDirInfo.CurrentDirectory);
        }
    }
}
