﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.BluetoothPart.PackageControl.PackageControllerState.Actions
{
    public class CheckCatalogAction : IActionKind
    {
        public ushort TimerRange { get; set; } = 45000;

        public void SendCommand(PackageBehaviorController behController)
        {
            try
            {
                FireFly.getInstance().BluetoothConnectController.GetDirInfo();
            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка в состоянии проверки каталога ардиографа, функции SendCommand. " + ex.Message + Environment.NewLine + ex.StackTrace);
            }
        }

        public void TimeAction(object behController)
        {
            PackageBehaviorController currentBehavior = behController as PackageBehaviorController;

            FireFly.getInstance().BluetoothConnectController.GetDirInfo();
        }
    }
}
