﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.BluetoothPart.PackageControl.PackageControllerState.Actions
{
    public class PacketHandlingAction : IActionKind
    {
        public ushort TimerRange { get; set; } = 30000;

        public void SendCommand(PackageBehaviorController behController)
        {
            throw new NotImplementedException();
        }

        public void TimeAction(object behController)
        {
            PackageBehaviorController currentBehavior = behController as PackageBehaviorController;
            currentBehavior.PacketProcessingTimeOut(EventArgs.Empty); //Не очень красиво...
        }
    }
}
