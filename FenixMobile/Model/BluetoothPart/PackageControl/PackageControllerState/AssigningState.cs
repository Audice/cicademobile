﻿using FenixMobile.Enumeration;
using FenixMobile.Interfaces;
using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using FenixMobile.Model.BluetoothPart.PackageControl.PackageControllerState.Actions;
using FenixMobile.Model.DatabasePart;
using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.BluetoothPart.PackageControl.PackageControllerState
{
    public class AssigningState : IDisposable
    {
        /// <summary>
        /// Объект-обработчик последовательности пакетов
        /// </summary>
        FilePackagesHandler FilePackagesHandler = null;
        /// <summary>
        /// Время простоя между командами DIR ?
        /// </summary>
        private readonly int Downtime = 30000;

        /// <summary>
        /// Локальное состояние каталога
        /// </summary>
        private FireFlyDirInfo LocalFireFlyInfo = null;
        /// <summary>
        /// Текущее состояние каталога
        /// </summary>
        private FireFlyDirInfo CurrentFireFlyInfo = null;

        private PackageBehaviorController BehaviorController { get; set; } = null;


        private bool IsBasementStateInit { get; set; } = false;

        DataTransferMode TransferMode { get; set; }

        /// <summary>
        /// Конструктор обработчика пакетов. 
        /// Алгоритм:
        /// 1. Состояние инициализации. 
        /// </summary>
        /// <param name="specificInfo">Данные о текущей директории, если равен null - начинаем новое исследование</param>
        public AssigningState(FireFlyDirInfo basementDirInfo, DataTransferMode transferMode)
        {
            //Установка текущего режима работы
            this.TransferMode = transferMode;
            this.FilePackagesHandler = new FilePackagesHandler();
            this.LocalFireFlyInfo = basementDirInfo;
            this.BehaviorController = new PackageBehaviorController();
            this.BehaviorController.RequiredDirInfo = this.LocalFireFlyInfo;
            this.BehaviorController.PacketProcessingTimeOutEvent += PacketProcessingTimeOut;
            FireFly.getInstance().FireFlyInfoUpdate += FireFlyInitialization;
            //FireFly.getInstance().FireFlyInfoUpdate += FireFlyDirInfoUpdate;
            FireFly.getInstance().PackageCameEvent += BatchProcessing;
            FireFly.getInstance().NoDataCameEvent += NoDataEventReaction;
        }

        /// <summary>
        /// Метод, реагирующий на поступление пакета FireFlyDirInfo и занимается стартовой настройкой
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void FireFlyInitialization(object sender, FireFlyDirInfoEventArgs e)
        {
            //Останавливаем таймер состояний
            this.BehaviorController.ResetStateTimer();
            if (!this.IsBasementStateInit)
            {
                if (this.LocalFireFlyInfo.CurrentDirectory == e.FireFlyDirInfo.CurrentDirectory)
                {
                    DateTime? createTime = null;
                    if (this.TransferMode == DataTransferMode.Current) 
                        createTime = DateTime.Now;
                    if (this.FilePackagesHandler.SetTransmissionParameters(this.LocalFireFlyInfo, createTime))
                    {
                        //Если настройка на конкретную директорию прошла успешно - переключаемся на другое событие, а от этого отписываемся
                        this.IsBasementStateInit = true;
                        //Если информация о новом пакете не валидна (номер пакта отрицательный), то устанавливаем его в 0
                        if (this.LocalFireFlyInfo.FilesNumber < 0) this.LocalFireFlyInfo.FilesNumber = 0;
                        FireFly.getInstance().FireFlyInfoUpdate -= FireFlyInitialization;
                        FireFly.getInstance().FireFlyInfoUpdate += FireFlyDirInfoUpdate;
                        this.BehaviorController.SetAction(new CheckCatalogAction(), 1000);
                        return;
                    }
                }
                //Если смена католога не помогла - повторяем действие
                this.BehaviorController.RequiredDirInfo = this.LocalFireFlyInfo;
                this.BehaviorController.SetAction(new ChangeDirAction(), 1000);
            }
            else
            {
                //Если настройка успешно прошла - переключаемся на другое событие, а от этого отписываемся
                //Не сможем сюда попасть
                int s = 0;
            }
        }





        public void DirectoryMonitoring(int timerDelay = 0)
        {
            this.BehaviorController.SetAction(new CheckCatalogAction(), timerDelay);
        }

        public void ChangeDirectory(FireFlyDirInfo fireFlyDirInfo)
        {
            this.LocalFireFlyInfo = fireFlyDirInfo;
            this.BehaviorController.RequiredDirInfo = this.LocalFireFlyInfo;
            this.BehaviorController.SetAction(new ChangeDirAction());
        }


        /// <summary>
        /// Метод, реагирующий на появление обновления информации о директории
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void FireFlyDirInfoUpdate(object sender, FireFlyDirInfoEventArgs e)
        {
            //Останавливаем таймер состояний
            this.BehaviorController.ResetStateTimer();
            //Получаем данные о текущем состоянии дирректории светляка
            FireFlyDirInfo info = e.FireFlyDirInfo;
            //Запоминаем текущее состояние
            this.CurrentFireFlyInfo = info;

            if (this.BehaviorController.State is ChangeDirAction)
                if (!this.ReInitState(info)) //Добавить проверку директории, не знаю какую
                {
                    this.BehaviorController.SetAction(new ChangeDirAction());
                    return;
                }


            //Если текущее состояние уже проинициализировано - начинаем логику
            if (!(this.LocalFireFlyInfo.Equals(info))
                || (this.LocalFireFlyInfo.CurrentDirectory == info.CurrentDirectory && this.LocalFireFlyInfo.DirTime == info.DirTime
                    && this.LocalFireFlyInfo.FilesChannels == info.FilesChannels && this.LocalFireFlyInfo.FilesLength == info.FilesLength
                    && this.LocalFireFlyInfo.FilesNumber <= info.FilesNumber && this.LocalFireFlyInfo.FilesSPS == info.FilesSPS
                    && this.LocalFireFlyInfo.IsSD_Memory == info.IsSD_Memory && this.LocalFireFlyInfo.NumDirectories == info.NumDirectories))
            {
                this.BehaviorController.RequiredDirInfo = this.LocalFireFlyInfo;
                if (this.LocalFireFlyInfo.CurrentDirectory == info.CurrentDirectory)
                    //Если текущие директории совпадают, то качаем файлы
                    this.BehaviorController.SetAction(new GetSpecFileAction());
                else
                    //Если нет - меняем директорию
                    this.BehaviorController.SetAction(new ChangeDirAction()); //Не соответствие? При смене директории
            }
            else
                //Состояния каталогов идентичны, поэтому продолжаем опрос устройства про каталоги, но с небольшим запозданием Downtime
                this.DirectoryMonitoring(this.Downtime);
        }


        private bool ReInitState(FireFlyDirInfo info)
        {
            if (!this.FilePackagesHandler.PackagesHandlerInit)
            {
                if (this.FilePackagesHandler.SetTransmissionParameters(info, info.DirTime))
                {
                    this.LocalFireFlyInfo.NumDirectories = info.NumDirectories;
                    if (this.LocalFireFlyInfo.FilesNumber < 0) this.LocalFireFlyInfo.FilesNumber = 0;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Метод, откликающийся на истечение таймера обработки пакетов
        /// </summary>
        void PacketProcessingTimeOut(object sender, EventArgs e)
        {
            //Сброс таймера
            this.BehaviorController.ResetStateTimer();
            //Обработка полученных пакетов
            if (this.FilePackagesHandler.CheckFilesValidity())
            {
                //Если всё окей, получаем данные
                RawDataEventArgs local = this.FilePackagesHandler.GetFilePackages();
                if (local != null)
                {
                    //И если данные сформировались - отправляем их формирователю сигнала
                    this.OnFileReady(local);
                }
                //Изменяем файл каталога
                this.LocalFireFlyInfo.FilesNumber++;
                this.BehaviorController.RequiredDirInfo = this.LocalFireFlyInfo;
            }
            else
                this.FilePackagesHandler.ClearPackagesList();
            //Скорее всего проблемы с парсингом пакета
            //Запускаем состояние считывание файла   
            this.BehaviorController.SetAction(new GetSpecFileAction());
        }

        public EventHandler<RawDataEventArgs> FileReadyEvent { get; set; }
        protected virtual void OnFileReady(RawDataEventArgs e)
        {
            EventHandler<RawDataEventArgs> handler = FileReadyEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }


        /// <summary>
        /// Метод, реагирующий на появление команды NO DATA, когда мы дочитали все данные из директории
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void NoDataEventReaction(object sender, EventArgs e)
        {
            //Сброс таймера
            this.BehaviorController.ResetStateTimer();

            if (this.CurrentFireFlyInfo.NumDirectories == this.LocalFireFlyInfo.NumDirectories)
                this.DirectoryMonitoring(this.Downtime);
            else
            {
                //Изменяем текущую директорию
                this.LocalFireFlyInfo.CurrentDirectory++;
                //Обнуляем счётчик файлов
                this.LocalFireFlyInfo.FilesNumber = 0;
                //Отправляем остатки старой директории
                RawDataEventArgs local = this.FilePackagesHandler.GetFileTail();
                if (local != null)
                    this.OnFileReady(local);
                this.BehaviorController.SetAction(new ChangeDirAction());
            }
        }



        /// <summary>
        /// Метод, реагирующий на появление данных ЭКГ
        /// </summary>
        /// <param name="sender">Объект, отправивший пакет ЭКГ</param>
        /// <param name="e">Пакет ЭКГ</param>
        void BatchProcessing(object sender, EventArgs e)
        {
            FireFlyPackage package = (e as FireFlyPackageEventArgs).Package;
            if (this.BehaviorController.State is GetSpecFileAction)
            {
                //Если мы в режиме запроса конкретного файла, то переходим в рижим ожидания парсинга пакетов
                var localPacketHandlingAction = new PacketHandlingAction();
                this.BehaviorController.SetAction(localPacketHandlingAction, localPacketHandlingAction.TimerRange);
            }
            this.FilePackagesHandler.InsertPackages(package);
        }

        public void ResetTimer()
        {
            this.BehaviorController.ResetStateTimer();
        }

        



        public void Dispose()
        {
            FireFly.getInstance().FireFlyInfoUpdate -= FireFlyInitialization;
            FireFly.getInstance().FireFlyInfoUpdate -= FireFlyDirInfoUpdate;
            this.ResetTimer();
            this.BehaviorController.PacketProcessingTimeOutEvent -= PacketProcessingTimeOut;
            FireFly.getInstance().PackageCameEvent -= BatchProcessing;
            FireFly.getInstance().NoDataCameEvent -= NoDataEventReaction;
        }
    }
}
