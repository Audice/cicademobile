﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.BluetoothPart.PackageControl.PackageControllerState
{
    public interface IActionKind
    {
        ushort TimerRange { get; set; }
        void SendCommand(PackageBehaviorController behController);
        void TimeAction(object behController);
    }
}
