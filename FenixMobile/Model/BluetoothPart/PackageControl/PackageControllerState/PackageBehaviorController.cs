﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.BluetoothPart.PackageControl.PackageControllerState
{
    public class PackageBehaviorController
    {
        public EventHandler<EventArgs> PacketProcessingTimeOutEvent;
        public void PacketProcessingTimeOut(EventArgs e)
        {
            EventHandler<EventArgs> handler = PacketProcessingTimeOutEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public EventHandler<EventArgs> FileRequestingTimeOutEvent;
        public void FileRequestingTimeOut(EventArgs e)
        {
            EventHandler<EventArgs> handler = FileRequestingTimeOutEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public FireFlyDirInfo RequiredDirInfo { get; set; } = null;
        /// <summary>
        /// Текущее
        /// </summary>
        public IActionKind State { get; private set; }
        /// <summary>
        /// Таймер состояний
        /// </summary>
        private System.Threading.Timer StateTimer = null;
        public PackageBehaviorController()
        {
            
        }

        public void SetAction(IActionKind actionState, int timerDelay = 0)
        {
            this.State = actionState;
            this.ResetStateTimer();
            this.StateTimer = new System.Threading.Timer(this.State.TimeAction, this, timerDelay, this.State.TimerRange);
        }

        public void ResetStateTimer()
        {
            if (StateTimer != null)
            {
                this.StateTimer.Dispose();
                this.StateTimer = null;
            }
        }

        public void SendCommand()
        {
            State.SendCommand(this);
        }

        public void TimeAction()
        {
            State.TimeAction(this);
        }
    }
}
