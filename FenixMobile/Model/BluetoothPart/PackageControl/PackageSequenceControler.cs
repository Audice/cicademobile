﻿using FenixMobile.Enumeration;
using FenixMobile.Interfaces;
using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using FenixMobile.Model.BluetoothPart.PackageControl.PackageControllerState;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FenixMobile.Model.BluetoothPart.PackageControl
{
    public class PackageSequenceControler : IDisposable, IPackageSequenceControler
    {
        private bool isCheckAvailability = false;
        public bool IsCheckAvailability
        {
            get { return isCheckAvailability; }
            private set
            {
                isCheckAvailability = value;
                if (!value)
                    ProcessStopEvent?.Invoke(this, EventArgs.Empty);
            }
        }
        public EventHandler<EventArgs> ProcessStopEvent;

        /// <summary>
        /// Объект отвечает за назначение состояния
        /// </summary>
        private AssigningState AssigningState { get; set; }

        DataTransferMode TransferMode { get; set; }

        public PackageSequenceControler(DataTransferMode transferMode, FireFlyDirInfo basemantDirInfo)
        {
            this.TransferMode = transferMode;
            this.TransferCorrectPackage(basemantDirInfo);
            this.AssigningState = new AssigningState(basemantDirInfo, transferMode);
            this.AssigningState.FileReadyEvent += PushFile;
            FireFly.getInstance().BluetoothConnectController.ChangeBluetoothStateEvent += DeviceStateChange;
        }
        void TransferCorrectPackage(FireFlyDirInfo basemantDirInfo)
        {
            switch (this.TransferMode)
            {
                case DataTransferMode.Archive:
                    break;
                case DataTransferMode.Current:
                    //Если мы в режиме текущего считывания - уровняем директории
                    basemantDirInfo.CurrentDirectory = basemantDirInfo.NumDirectories;
                    break;
            }
        }
        private void PushFile(object sender, RawDataEventArgs e)
        {
            this.OnPartRawDataReady(e);
        }
        /// <summary>
        /// Функция, реагирующая на изменение состояния потоков ввода\вывода. 
        /// Как только произошло отключение - 
        /// разрываем соединение и отключаем дальнейшее ожидание
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void DeviceStateChange(object sender, EventArgs e)
        {
            bool stateConnection = ((BluetoothConnectServicer)sender).IsBluetoothConnect;
            if (stateConnection)
            {
                if (!this.IsCheckAvailability) //?????
                    this.RunECGReadingProcess();
            }
            else
            {
                this.IsCheckAvailability = false;
                this.AssigningState.ResetTimer();
            }
        }
        public void RunECGReadingProcess()
        {
            this.IsCheckAvailability = true;
            this.AssigningState.DirectoryMonitoring();
        }


        public EventHandler<RawDataEventArgs> PartRawDataReadyEvent { get; set; }
        protected virtual void OnPartRawDataReady(RawDataEventArgs e)
        {
            EventHandler<RawDataEventArgs> handler = PartRawDataReadyEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public void Dispose()
        {
            this.AssigningState.Dispose();
            this.IsCheckAvailability = false;
            FireFly.getInstance().BluetoothConnectController.ChangeBluetoothStateEvent -= DeviceStateChange;
        }
    }
}
