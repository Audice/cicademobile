﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.ConclusionPart.ConclusionEventArgs
{
    public class ArrethmiaEventArgs : EventArgs
    {
        public string ArrethmiaDiscription { get; set; }
        public byte ProblemRank { get; set; }
    }
}
