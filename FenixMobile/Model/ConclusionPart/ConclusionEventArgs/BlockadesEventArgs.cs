﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.ConclusionPart.ConclusionEventArgs
{
    public class BlockadesEventArgs: EventArgs
    {
        public string BlockadesDiscription { get; set; }
        public byte ProblemRank { get; set; }
    }
}
