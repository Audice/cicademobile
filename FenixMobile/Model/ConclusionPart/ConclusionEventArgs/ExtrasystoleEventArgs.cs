﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.ConclusionPart.ConclusionEventArgs
{
    public class ExtrasystoleEventArgs: EventArgs
    {
        public string ExtrasystoleDiscription { get; set; }
        public byte ProblemRank { get; set; }

        public uint AtrialPrematureBeatsCount { get; set; }
        public uint VentricularExtrasystoleCount { get; set; }
    }
}
