﻿using FenixMobile.Enumeration;
using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.ConclusionPart.ConclusionEventArgs
{
    public class IndexChangeOptionsEventArgs : EventArgs
    {
        public HeartRateVariabilityIndex HeartRateVariabilityIndex { get; set; }
        public byte ValuePercent { get; set; }
        public short ValueChange { get; set; }
    }
}
