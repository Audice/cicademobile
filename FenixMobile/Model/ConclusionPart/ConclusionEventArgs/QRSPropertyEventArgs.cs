﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.ConclusionPart.ConclusionEventArgs
{
    public class QRSPropertyEventArgs : EventArgs
    {
        public byte ProblemRank { get; set; }

        public string P_Notch { get; set; } //В секундах
        public string P_NotchAVG { get; set; } //В секундах

        public string PQ_Notch { get; set; } //В секундах
        public string PQ_NotchAVG { get; set; } //В секундах

        public string QRS_Notch { get; set; } //В секундах
        public string QRS_NotchAVG { get; set; } //В секундах

        public string QT_Notch { get; set; } //В секундах
        public string QT_NotchAVG { get; set; } //В секундах

    }
}
