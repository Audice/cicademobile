﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.ConclusionPart.ConclusionEventArgs
{
    public class TrendsEventArgs : EventArgs
    {
        public double[] RR_Trends { get; set; }
        public double[] PP_Trends { get; set; }
    }
}
