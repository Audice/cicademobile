﻿
using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using FenixMobile.Model.BluetoothPart.DataConvert;
using FenixMobile.Model.ConclusionPart.ConclusionEventArgs;
using FenixMobile.Model.ConclusionPart.Diagnosis;
using FenixMobile.Model.ConclusionPart.HeartRateVariability;
using FenixMobile.Model.ConclusionPart.SegmentationPart;
using FenixMobile.Model.DatabasePart;
using FenixMobile.Translations;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace FenixMobile.Model.ConclusionPart
{
    public class ConclusionModel : IDisposable
    {
        // Время в _ANN распологается в первом столбце и измеряется в семплах.
        // Так как частота дискретизации сигнала у нас известна
        // при делении элемента _ANN[i, 0] на неё мы получим время данного сегмента в секундах.
        private ushort frequencySignsl = 512;
        public ushort FrequencySignsl
        {
            get { return frequencySignsl; }
            private set { 
                if (frequencySignsl != value)
                {
                    frequencySignsl = value;
                }
            }
        }

        PatientTable CurrentPatient { get; set; }

        P_SegmentCollection p_SegmentCollection { get; set; }
        T_SegmentCollection t_SegmentCollection { get; set; }
        QRS_SegmentCollection qrs_SegmentCollection { get; set; }


        //Сигментационная карта
        public List<string> SegmentatedMapList { get; private set; }

        //Поля для создание одноразмерных массивов интервалов
        private bool isConteinP;
        private bool isConteinQRS;
        private bool isConteinT;
        private int counter;
        private short[] ecgData;

        ConcurrentQueue<short> SimpleSignalStorage;
        ConcurrentQueue<short> ComplexSignalStorage;
        private uint AnalysisSimpleBoundary = 0;
        private uint AnalysisComplexBoundary = 0;
        private ushort CountSecondsSimpleConclusion = 10;
        private ushort CountSecondsComplexConclusion = 120;

        DataConverter DataConverter { get; set; }

        //Классы диагностики
        public HeartRatingCompiler HeartRatingCompiler { get; private set; }
        private HeartRateCollector HeartRateCollector { get; set; }
        public SystemTensionIndexController SystemTensionIndexController { get; private set; }
        public VegetativeBalanceController VegetativeBalanceController { get; private set; }
        public AdaptationLevel AdaptationLevel { get; private set; }

        //Классы диагностики
        public PulseAnalyzer PulseAnalyzer { get; private set; }


        /// <summary>
        /// Класс предназначен для получения ЭКГ, формирования локального хранилища для единоразовой обработки, и диагностирования отклонений
        /// </summary>
        public ConclusionModel(DataConverter dataConverter,  ushort sampleFrequancy, PatientTable currentPatient) //DataConverter в качестве парамметра позволит получать обработанные данные для работы с минутной экг
        {
            //Инициализация классов дмагностики
            this.PulseAnalyzer = new PulseAnalyzer(currentPatient.BornDate);


            this.CurrentPatient = currentPatient;
            this.HeartRatingCompiler = new HeartRatingCompiler();
            this.SystemTensionIndexController = new SystemTensionIndexController();
            this.VegetativeBalanceController = new VegetativeBalanceController();
            this.AdaptationLevel = new AdaptationLevel();
            this.HeartRateCollector = new HeartRateCollector(HeartRatingCompiler, SystemTensionIndexController, VegetativeBalanceController, AdaptationLevel);

            this.SimpleSignalStorage = new ConcurrentQueue<short>();
            this.ComplexSignalStorage = new ConcurrentQueue<short>();
            this.FrequencySignsl = sampleFrequancy;
            this.AnalysisSimpleBoundary = (uint)(this.FrequencySignsl * CountSecondsSimpleConclusion);
            this.AnalysisComplexBoundary = (uint)(this.FrequencySignsl * CountSecondsComplexConclusion);
            this.DataConverter = dataConverter;
            DataConverter.SignalsConvertedEvent += DataArrived;


        }

        //Метод, подписанный на событие поступления новой порции данных
        void DataArrived(object sender, ConvertedSignalEventArgs e)
        {
            //TODO: введи поведение для двух режимов

            //Добавление файлов в локальное хранилище для следующего этапа обработки
            if (this.SimpleSignalStorage != null && this.ComplexSignalStorage != null )
            {
                var data = e.Signals;
                if (data != null && data.Count > 0)
                {
                    for (int i=0; i < data[0].Length; i++)
                    {
                        this.SimpleSignalStorage.Enqueue(data[0][i]);
                        this.ComplexSignalStorage.Enqueue(data[0][i]);
                    }
                }

                if (this.ComplexSignalStorage.Count >= AnalysisComplexBoundary)
                {
                    bool signalReady = true;
                    short[] ecgSignal = new short[AnalysisComplexBoundary];
                    for (int i = 0; i < ecgSignal.Length; i++)
                    {
                        short resultDequeue = 0;
                        if (this.ComplexSignalStorage.TryDequeue(out resultDequeue))
                        {
                            ecgSignal[i] = resultDequeue;
                        }
                        else
                        {
                            signalReady = false;
                            break;
                        }
                    }
                    if (this.FrequencySignsl > 0)
                    {
                        if (signalReady) //Проверка экг на готовность
                        {
                            var annotation = SearchSignalAnnotation(ecgSignal);
                            if (annotation != null)
                            {
                                //Запус процесса поиска проблем
                                ComplexConclusion(annotation, ecgSignal);
                            }
                        }
                    }
                    return;
                }

                if (this.SimpleSignalStorage.Count >= AnalysisSimpleBoundary)
                {
                    bool signalReady = true;
                    short[] ecgSignal = new short[AnalysisSimpleBoundary];
                    for (int i = 0; i < ecgSignal.Length; i++)
                    {
                        short resultDequeue = 0;
                        if (this.SimpleSignalStorage.TryDequeue(out resultDequeue))
                        {
                            ecgSignal[i] = resultDequeue;
                        }
                        else
                        {
                            signalReady = false;
                            break;
                        }
                    }
                    if (this.FrequencySignsl > 0)
                    {
                        if (signalReady) //Проверка экг на готовность
                        {
                            var annotation = SearchSignalAnnotation(ecgSignal);
                            if (annotation != null)
                            {
                                //Запус процесса поиска проблем
                                SimpleConclusionFormatter(annotation, ecgSignal);
                            }
                        }
                    }
                }
            }
            else
            {
                this.SimpleSignalStorage = new ConcurrentQueue<short>();
                this.ComplexSignalStorage = new ConcurrentQueue<short>();
            }
        }

        //Поиск анотации сигнала
        int[,] SearchSignalAnnotation(short[] _ecgData)
        {
            if (_ecgData == null) return null;

            int[,] resultAnnotation = null;

            signal a = new signal();
            a.setTXTData(this.FrequencySignsl, _ecgData.Length, 200, 0, _ecgData);
            uint size = a.GetLength();
            double sr = a.GetSR();
            int h = 0, m = 0, s = 0, ms = 0;
            int msec = (int)(((double)size / sr) * 1000.0);
            a.mSecToTime(msec, ref h, ref m, ref s, ref ms);
            double[] data = a.GetData(0);
            EcgAnnotation.Annhdr annhdr = new EcgAnnotation.Annhdr();
            annhdr.isEmpty = true;
            EcgAnnotation ann = new EcgAnnotation(annhdr);
            int[,] qrsAnn = ann.GetQRS(data, (int)size, sr, "");

            if (qrsAnn != null)
            {
                ann.GetEctopics(ref qrsAnn, ann.GetQrsNumber(), sr);
                resultAnnotation = ann.GetPTU(data, (int)size, sr, "filters", qrsAnn, ann.GetQrsNumber());     //find P,T waves
            }
            return resultAnnotation;
        }

        //Блок обсчёта индекса массы тела
        #region
        bool IsBodyMassIndexInit = false;
        public EventHandler<BodyMassIndexEventArgs> BodyMassIndexChange;
        protected void OnBodyMassIndexChange(BodyMassIndexEventArgs e)
        {
            EventHandler<BodyMassIndexEventArgs> handler = BodyMassIndexChange;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public void BodyMassIndexCalculation()
        {
            if (this.IsBodyMassIndexInit) return;
            if (this.CurrentPatient == null) return;
            ushort weight = (ushort)this.CurrentPatient.Weight, growth = (ushort)this.CurrentPatient.Growth;
            if (growth < 50 || growth > 300 || weight > 500 || weight < 15 || growth == 0 || weight == 0) return;

            double bmi = (double)weight / (Math.Pow(((double)growth/100), 2));
            BodyMassIndexEventArgs args = new BodyMassIndexEventArgs();

            if (bmi <= 18.0)
            {
                args.BodyMassIndexRating = 0;
                args.BodyMassIndexColor = Color.FromHex("#00FFFF");
                args.BodyMassIndexTitleDiscription = "Недостаток веса";
                args.BodyMassIndexDiscription = "хроническая усталость, апатия, нехватка витаминов, истощение, остеопороз, анорексия.";
            }
            if (bmi > 18.0 && bmi <= 20.0)
            {
                args.BodyMassIndexRating = 1;
                args.BodyMassIndexColor = Color.FromHex("#9ACD32");
                args.BodyMassIndexTitleDiscription = "Недостаток веса";
                args.BodyMassIndexDiscription = "проблемы пищеварения, истощение, стресс, хроническая усталость, низкий имунитет, депрессия, гормональные нарушения.";
            }
            if (bmi > 20.0 && bmi <= 25.0)
            {
                args.BodyMassIndexRating = 2;
                args.BodyMassIndexColor = Color.FromHex("#4AAB3F");
                args.BodyMassIndexTitleDiscription = "Нормальный вес";
                args.BodyMassIndexDiscription = "высокий уровень энергии, хорошая физическая форма, жизнерадостность, психоэмоциональное равновесие.";
            }
            if (bmi > 25.0 && bmi <= 27.0)
            {
                args.BodyMassIndexRating = 3;
                args.BodyMassIndexColor = Color.FromHex("#F0BA00");
                args.BodyMassIndexTitleDiscription = "Лишний вес";
                args.BodyMassIndexDiscription = "хроническая усталость, проблемы с пищеварением и сердечно-сосудистой системой, варикоз и др.";
            }
            if (bmi > 27.0 && bmi <= 30.0)
            {
                args.BodyMassIndexRating = 3;
                args.BodyMassIndexColor = Color.FromHex("#FFA500");
                args.BodyMassIndexTitleDiscription = "Ожирение 1-й степени";
                args.BodyMassIndexDiscription = "повышается риск возникновения диабета, высокое давление, проблемы кровообращения, нарушение психики, проблемы с суставами и т.д.";
            }
            if (bmi > 30.0 && bmi <= 35.0)
            {
                args.BodyMassIndexRating = 4;
                args.BodyMassIndexColor = Color.FromHex("#FF6347");
                args.BodyMassIndexTitleDiscription = "Ожирение 2-й степени";
                args.BodyMassIndexDiscription = "не инсулинозависимый диабет, атеросклероз, стенокардия, инфаркт, тромбофлебит...";
            }
            if (bmi > 35.0)
            {
                args.BodyMassIndexRating = 4;
                args.BodyMassIndexColor = Color.FromHex("#FF0000");
                args.BodyMassIndexTitleDiscription = "Ожирение 3-й степени";
                args.BodyMassIndexDiscription = "Инсулинозависимый диабет, инфаркт, инсульт, возможно развитие рака.";
            }
            this.OnBodyMassIndexChange(args);
            this.IsBodyMassIndexInit = true;
        }
        #endregion


        void ComplexConclusion(int[,] _ANN, short[] _ecgData)
        {
            BodyMassIndexCalculation();

            if (_ANN == null || _ecgData == null) return;
            ecgData = _ecgData;
            SegmentatedMapList = new List<string>();
            p_SegmentCollection = new P_SegmentCollection();
            t_SegmentCollection = new T_SegmentCollection();
            qrs_SegmentCollection = new QRS_SegmentCollection();

            isConteinP = false;
            isConteinQRS = false;
            isConteinT = false;
            counter = 0;

            try
            {
                for (int i = 0; i < _ANN.GetLength(0); i++)
                {
                    try
                    {
                        if (i < (_ANN.GetLength(0) - 2) && p_SegmentCollection.Is_P_Segment(_ANN[i, 1], _ANN[i + 1, 1], _ANN[i + 2, 1]))
                        {
                            p_SegmentCollection.P_Segments.Add(new P_Segment(
                                (double)_ANN[i, 0] / FrequencySignsl,
                                (double)_ANN[i + 1, 0] / FrequencySignsl,
                                (double)_ANN[i + 2, 0] / FrequencySignsl,
                                true,
                                ecgData[_ANN[i, 0]] >= ecgData[_ANN[i + 2, 0]] ?
                                ecgData[_ANN[i + 1, 0]] - ecgData[_ANN[i + 2, 0]] : ecgData[_ANN[i + 1, 0]] - ecgData[_ANN[i, 0]]));
                            SegmentatedMapList.Add("P");
                            isConteinP = true;
                            i += 2;
                        }
                        if (i < (_ANN.GetLength(0) - 2) && qrs_SegmentCollection.Is_QRS_Segment(_ANN[i, 1], _ANN[i + 1, 1], _ANN[i + 2, 1]))
                        {
                            qrs_SegmentCollection.QRS_Segments.Add(new QRS_Segment(
                                (double)_ANN[i, 0] / FrequencySignsl,
                                (double)_ANN[i + 1, 0] / FrequencySignsl,
                                (double)_ANN[i + 2, 0] / FrequencySignsl,
                                true));
                            SegmentatedMapList.Add("QRS");
                            isConteinQRS = true;
                            i += 2;
                        }
                        if (i < (_ANN.GetLength(0) - 2) && t_SegmentCollection.Is_T_Segment(_ANN[i, 1], _ANN[i + 1, 1], _ANN[i + 2, 1]))
                        {
                            t_SegmentCollection.T_Segments.Add(new T_Segment(
                                (double)_ANN[i, 0] / FrequencySignsl,
                                (double)_ANN[i + 1, 0] / FrequencySignsl,
                                (double)_ANN[i + 2, 0] / FrequencySignsl,
                                true,
                                ecgData[_ANN[i, 0]] >= ecgData[_ANN[i + 2, 0]] ?
                                ecgData[_ANN[i + 1, 0]] - ecgData[_ANN[i + 2, 0]] : ecgData[_ANN[i + 1, 0]] - ecgData[_ANN[i, 0]]));
                            SegmentatedMapList.Add("T");
                            isConteinT = true;
                            i += 2;
                        }

                        //Данная опция позволит сохранить размер массива интервалов одинаковым.
                        //Плюсом, это позволит понять, есть ли на определённом этапе измерения
                        //проблемы с комплексом.
                        //Работает шаблонно... Требует проверки
                        /*
                        if (counter >= 0)
                        {
                            // В каждом условии можно изменит время -1 на время предыдущего измерения
                            if (isConteinP == false) p_SegmentCollection.P_Segments.Add(new P_Segment(-1, -1, -1, false));
                            if (isConteinQRS == false) qrs_SegmentCollection.QRS_Segments.Add(new QRS_Segment(-1, -1, -1, -1, false));
                            if (isConteinT == false) t_SegmentCollection.T_Segments.Add(new T_Segment(-1, -1, -1, false));

                            isConteinP = false;
                            isConteinQRS = false;
                            isConteinT = false;
                        }
                        counter++;
                        */
                    }
                    catch (Exception ex)
                    {
                        string message = ex.Message;
                        return;
                    }
                }

                //Строим массив rr интервалов
                List<double> rrIntervals = new List<double>();
                for (int i = 1; i < qrs_SegmentCollection.QRS_Segments.Count; i++)
                {
                    rrIntervals.Add(qrs_SegmentCollection.QRS_Segments[i].R_TimeStamp - qrs_SegmentCollection.QRS_Segments[i - 1].R_TimeStamp); //ПЕРЕДЕЛАЙ!!!
                }
                this.HeartRateCollector.AppendRRArray(rrIntervals.ToArray());



                //Формирование списков интервалов, для исследования
                p_SegmentCollection.CreateListPPInterval();
                t_SegmentCollection.CreateListTTInterval();
                qrs_SegmentCollection.CreateListRRInterval();

                if (p_SegmentCollection.PPinterval.Count > 0 && qrs_SegmentCollection.RRinterval.Count > 0)
                {
                    TrendsEventArgs trendsEventArgs = new TrendsEventArgs();
                    double[] localRR = null;
                    double[] localPP = null;
                    if (Check_RR_PP_Intervals(qrs_SegmentCollection, p_SegmentCollection, ref localRR, ref localPP))
                        //Инициируем рассылку полученных трендов
                        this.OnTrendsPartReady(new TrendsEventArgs()
                        {
                            RR_Trends = localRR,
                            PP_Trends = localPP
                        });
                }


                //Тест пульса
                this.PulseAnalyzer.DiagnosticProcessing(qrs_SegmentCollection);

                DiagnosisRithmType();

                diagnosForExtraSistole = "";

                SearchCompletelyIrregularRhythm();

                ArrethmiaEventArgs arrethmiaEventArgs = new ArrethmiaEventArgs();
                arrethmiaEventArgs.ArrethmiaDiscription = "";
                if (SearchAtrialFibrillation())
                {
                    this.NumAtrialFibrillation++;
                }
                if (this.NumAtrialFibrillation > this.ErrorNumAtrialFibrillation)
                {
                    arrethmiaEventArgs.ArrethmiaDiscription += "Обнаружена фибрилляция предсердий. ";
                    arrethmiaEventArgs.ProblemRank = 2;
                }
                if (SearchAtrialFlutter())
                {
                    this.NumAtrialFlutter++;
                }
                if (this.NumAtrialFlutter > this.ErrorNumAtrialFlutter)
                {
                    arrethmiaEventArgs.ArrethmiaDiscription += "Обнаружено трепетание предсердий. ";
                    arrethmiaEventArgs.ProblemRank = 2;
                }

                BlockadesEventArgs blockadesArgs = new BlockadesEventArgs();
                blockadesArgs.BlockadesDiscription = "";
                if (Search_First_degree_AV_block())
                {
                    this.NumCasesAVblock++;
                }
                if (this.NumCasesAVblock > this.ErrorNumCasesAVblock)
                {
                    blockadesArgs.BlockadesDiscription += "Обнаружена АВ блокада первой степени. ";
                    blockadesArgs.ProblemRank = 1;
                }
                if (Search_Second_degree_AV_block_first_type())
                {
                    this.NumCasesAVblockFirstType++;
                }
                if (this.NumCasesAVblockFirstType > this.ErrorNumCasesAVblockFirstType)
                {
                    blockadesArgs.BlockadesDiscription += "Обнаружена АВ блокада второй степени, первого типа";
                    blockadesArgs.ProblemRank = 1;
                }
                if (Search_Second_degree_AV_block_second_type())
                {
                    this.NumCasesAVblockSecondType++;
                }
                if (this.NumCasesAVblockSecondType > this.ErrorNumCasesAVblockSecondType)
                {
                    blockadesArgs.BlockadesDiscription += "Обнаружена АВ блокада второй степени, второго типа";
                    blockadesArgs.ProblemRank = 2;
                }
                if (blockadesArgs.BlockadesDiscription.Length == 0)
                {
                    blockadesArgs.BlockadesDiscription += "АВ блокад не обнаружено. ";
                    blockadesArgs.ProblemRank = 0;
                }
                this.OnBlockadesConclusionReady(blockadesArgs);




                //Search_Third_degree_AV_block();
                //Search_Third_degree_AV_block_number2();
                //Search_Third_degree_AV_block_number3();

                SearcNon_specificDelayedIntraventricularConduction();
                //Диагностика экстрасистолий
                this.NumAtrialPrematureBeats += SearchAtrialPrematureBeats();
                this.NumVentricularExtrasystole += SearchVentricularExtrasystole();
                ExtrasystoleEventArgs args = new ExtrasystoleEventArgs();
                if (this.NumAtrialPrematureBeats < this.ErrorNumAtrialPrematureBeats)
                {
                    args.ExtrasystoleDiscription = "Предсердных экстрасистолий не обнаружено. ";
                    args.ProblemRank = 0;
                }
                else
                {
                    if (this.NumAtrialPrematureBeats < this.MaxNumAtrialPrematureBeats)
                    {
                        args.ExtrasystoleDiscription = "Найдено незначительное количество предсердных экстрасистол. ";
                        args.ProblemRank = 1;
                    }
                    else
                    {
                        args.ExtrasystoleDiscription = "Число предсердных экстрасистолий выше нормы. ";
                        args.ProblemRank = 2;
                    }
                }
                args.AtrialPrematureBeatsCount = this.NumAtrialPrematureBeats;
                if (this.NumVentricularExtrasystole < this.ErrorNumVentricularExtrasystole)
                {
                    args.ExtrasystoleDiscription += "Желудочковых экстрасистолий не обнаружено. ";
                }
                else
                {
                    if (this.NumVentricularExtrasystole < this.MaxNumVentricularExtrasystole)
                    {
                        args.ExtrasystoleDiscription += "Найдено незначительное количество желудочковых экстрасистол. ";
                        args.ProblemRank = args.ProblemRank > 1 ? args.ProblemRank : (byte)1;
                    }
                    else
                    {
                        args.ExtrasystoleDiscription += "Число предсердных экстрасистолий выше нормы. ";
                        args.ProblemRank = 2;
                    }
                }
                args.VentricularExtrasystoleCount = this.NumVentricularExtrasystole;
                this.OnExtrasystoleConclusionReady(args);

                if (SearchSVT())
                {
                    this.NumSVT++;
                }
                if (this.NumSVT > this.ErrorNumSVT)
                {
                    arrethmiaEventArgs.ArrethmiaDiscription += "Найдены случаи супровентрикулярной тахикардии. ";
                    arrethmiaEventArgs.ProblemRank = 2;
                }
                if (Search_Unspecified_Ventricular_Tachycardia())
                {
                    this.NumVentricularTachycardia++;
                }
                if (this.NumVentricularTachycardia > this.ErrorNumVentricularTachycardia)
                {
                    arrethmiaEventArgs.ArrethmiaDiscription += "Желудочковая тахикардия. ";
                    arrethmiaEventArgs.ProblemRank = 3;
                }
                if (Search_Ventricular_Fibrillation())
                {
                    this.NumVentricularFibrillation++;
                }
                if (this.NumVentricularFibrillation > this.ErrorVentricularFibrillation)
                {
                    arrethmiaEventArgs.ArrethmiaDiscription += "Встречались случаи фибрилляции желудочков. ";
                    arrethmiaEventArgs.ProblemRank = 3;
                }
                if (arrethmiaEventArgs.ArrethmiaDiscription.Length == 0)
                {
                    arrethmiaEventArgs.ArrethmiaDiscription += "Патологий ритма и аритмий не обнаружено. ";
                    arrethmiaEventArgs.ProblemRank = 0;
                }

                this.OnArrethmiaConclusionReady(arrethmiaEventArgs);

                CalculateProperties();
                //SearchShortenedPQSpacing();
                //SearchShortenedQTinterval();
                //SearchExtendedQTinterval();
                SearchMaximumPause();
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return;
            }

            GC.Collect();
        }

        bool Check_RR_PP_Intervals(QRS_SegmentCollection qrsCollection, P_SegmentCollection pCollections, ref double[] rrs, ref double[] pps)
        {
            rrs = new double[qrsCollection.RRinterval.Count];
            pps = new double[qrsCollection.RRinterval.Count];
            //Если размеры списка сегментов не совпадают, тогда дополняем их
            if (qrsCollection.RRinterval.Count > 0 && qrsCollection.RRinterval.Count != pCollections.PPinterval.Count)
            {
                try
                {
                    for (int i = 1; i < qrsCollection.QRS_Segments.Count; i++)
                    {
                        int curIndexElem = pCollections.P_Segments.FindIndex(x => qrsCollection.QRS_Segments[i].R_TimeStamp - x.MiddleP < 0.35 &&
                            qrsCollection.QRS_Segments[i].R_TimeStamp - x.MiddleP > 0);
                        int prevIndexElem = pCollections.P_Segments.FindIndex(x => qrsCollection.QRS_Segments[i - 1].R_TimeStamp - x.MiddleP < 0.35 &&
                            qrsCollection.QRS_Segments[i - 1].R_TimeStamp - x.MiddleP > 0);
                        rrs[i - 1] = qrsCollection.QRS_Segments[i].R_TimeStamp - qrsCollection.QRS_Segments[i - 1].R_TimeStamp;
                        if (curIndexElem < 0 || prevIndexElem < 0)
                            pps[i - 1] = -1;
                        else
                            pps[i - 1] = pCollections.P_Segments[curIndexElem].MiddleP - pCollections.P_Segments[prevIndexElem].MiddleP;
                    }
                }
                catch (Exception ex)
                {
                    string exM = ex.Message;
                    return false;
                }
            }
            else
            {
                rrs = qrsCollection.RRinterval.ToArray();
                pps = pCollections.PPinterval.ToArray();
            }
            return true;
        }


        void SimpleConclusionFormatter(int[,] _ANN, short[] _ecgData)
        {
            BodyMassIndexCalculation();

            if (_ANN == null || _ecgData == null) return;
            ecgData = _ecgData;
            SegmentatedMapList = new List<string>();
            p_SegmentCollection = new P_SegmentCollection();
            t_SegmentCollection = new T_SegmentCollection();
            qrs_SegmentCollection = new QRS_SegmentCollection();

            isConteinP = false;
            isConteinQRS = false;
            isConteinT = false;
            counter = 0;

            try
            {
                for (int i = 0; i < _ANN.GetLength(0); i++)
                {
                    try
                    {
                        if (i < (_ANN.GetLength(0) - 2) && p_SegmentCollection.Is_P_Segment(_ANN[i, 1], _ANN[i + 1, 1], _ANN[i + 2, 1]))
                        {
                            p_SegmentCollection.P_Segments.Add(new P_Segment(
                                (double)_ANN[i, 0] / FrequencySignsl,
                                (double)_ANN[i + 1, 0] / FrequencySignsl,
                                (double)_ANN[i + 2, 0] / FrequencySignsl,
                                true,
                                ecgData[_ANN[i, 0]] >= ecgData[_ANN[i + 2, 0]] ?
                                ecgData[_ANN[i + 1, 0]] - ecgData[_ANN[i + 2, 0]] : ecgData[_ANN[i + 1, 0]] - ecgData[_ANN[i, 0]]));
                            SegmentatedMapList.Add("P");
                            isConteinP = true;
                            i += 2;
                        }
                        if (i < (_ANN.GetLength(0) - 2) && qrs_SegmentCollection.Is_QRS_Segment(_ANN[i, 1], _ANN[i + 1, 1], _ANN[i + 2, 1]))
                        {
                            qrs_SegmentCollection.QRS_Segments.Add(new QRS_Segment(
                                (double)_ANN[i, 0] / FrequencySignsl,
                                (double)_ANN[i + 1, 0] / FrequencySignsl,
                                (double)_ANN[i + 2, 0] / FrequencySignsl,
                                true));
                            SegmentatedMapList.Add("QRS");
                            isConteinQRS = true;
                            i += 2;
                        }
                        if (i < (_ANN.GetLength(0) - 2) && t_SegmentCollection.Is_T_Segment(_ANN[i, 1], _ANN[i + 1, 1], _ANN[i + 2, 1]))
                        {
                            t_SegmentCollection.T_Segments.Add(new T_Segment(
                                (double)_ANN[i, 0] / FrequencySignsl,
                                (double)_ANN[i + 1, 0] / FrequencySignsl,
                                (double)_ANN[i + 2, 0] / FrequencySignsl,
                                true,
                                ecgData[_ANN[i, 0]] >= ecgData[_ANN[i + 2, 0]] ?
                                ecgData[_ANN[i + 1, 0]] - ecgData[_ANN[i + 2, 0]] : ecgData[_ANN[i + 1, 0]] - ecgData[_ANN[i, 0]]));
                            SegmentatedMapList.Add("T");
                            isConteinT = true;
                            i += 2;
                        }

                        //Данная опция позволит сохранить размер массива интервалов одинаковым.
                        //Плюсом, это позволит понять, есть ли на определённом этапе измерения
                        //проблемы с комплексом.
                        //Работает шаблонно... Требует проверки
                        
                        if (counter >= 0)
                        {
                            // В каждом условии можно изменит время -1 на время предыдущего измерения
                            //if (isConteinP == false) p_SegmentCollection.P_Segments.Add(new P_Segment(-1, -1, -1, false, 0));
                            //if (isConteinQRS == false) qrs_SegmentCollection.QRS_Segments.Add(new QRS_Segment(-1, -1, -1, false));
                            //if (isConteinT == false) t_SegmentCollection.T_Segments.Add(new T_Segment(-1, -1, -1, false, 0));

                            isConteinP = false;
                            isConteinQRS = false;
                            isConteinT = false;
                        }
                        counter++;
                        
                    }
                    catch (Exception ex)
                    {
                        string message = ex.Message;
                        return;
                    }
                }

                //Формирование списков интервалов, для исследования
                p_SegmentCollection.CreateListPPInterval();
                t_SegmentCollection.CreateListTTInterval();
                qrs_SegmentCollection.CreateListRRInterval();

                if (p_SegmentCollection.PPinterval.Count > 0 && qrs_SegmentCollection.RRinterval.Count > 0)
                {
                    TrendsEventArgs trendsEventArgs = new TrendsEventArgs();
                    double[] localRR = null;
                    double[] localPP = null;
                    if (Check_RR_PP_Intervals(qrs_SegmentCollection, p_SegmentCollection, ref localRR, ref localPP))
                        //Инициируем рассылку полученных трендов
                        this.OnTrendsPartReady(new TrendsEventArgs()
                        {
                            RR_Trends = localRR,
                            PP_Trends = localPP
                        });
                }

                /*
                //Инициируем рассылку полученных трендов
                this.OnTrendsPartReady(new TrendsEventArgs() { 
                    RR_Trends = qrs_SegmentCollection.RRinterval.ToArray(), 
                    PP_Trends = p_SegmentCollection.PPinterval.ToArray()
                });
                */

                //Тест пульса
                this.PulseAnalyzer.DiagnosticProcessing(qrs_SegmentCollection);

                DiagnosisRithmType();

                diagnosForExtraSistole = "";

                SearchCompletelyIrregularRhythm();
                ArrethmiaEventArgs arrethmiaEventArgs = new ArrethmiaEventArgs();
                arrethmiaEventArgs.ArrethmiaDiscription = "";
                if (SearchAtrialFibrillation())
                {
                    this.NumAtrialFibrillation++;
                }
                if (this.NumAtrialFibrillation > this.ErrorNumAtrialFibrillation)
                {
                    arrethmiaEventArgs.ArrethmiaDiscription += "Обнаружена фибрилляция предсердий. ";
                    arrethmiaEventArgs.ProblemRank = 2;
                }
                if (SearchAtrialFlutter())
                {
                    this.NumAtrialFlutter++;
                }
                if (this.NumAtrialFlutter > this.ErrorNumAtrialFlutter)
                {
                    arrethmiaEventArgs.ArrethmiaDiscription += "Обнаружено трепетание предсердий. ";
                    arrethmiaEventArgs.ProblemRank = 2;
                }



                BlockadesEventArgs blockadesArgs = new BlockadesEventArgs();
                blockadesArgs.BlockadesDiscription = "";
                if (Search_First_degree_AV_block())
                {
                    this.NumCasesAVblock++;
                }
                if (this.NumCasesAVblock > this.ErrorNumCasesAVblock)
                {
                    blockadesArgs.BlockadesDiscription += "Обнаружена АВ блокада первой степени. ";
                    blockadesArgs.ProblemRank = 1;
                }
                if (Search_Second_degree_AV_block_first_type())
                {
                    this.NumCasesAVblockFirstType++;
                }
                if (this.NumCasesAVblockFirstType > this.ErrorNumCasesAVblockFirstType)
                {
                    blockadesArgs.BlockadesDiscription += "Обнаружена АВ блокада второй степени, первого типа";
                    blockadesArgs.ProblemRank = 1;
                }
                if (Search_Second_degree_AV_block_second_type())
                {
                    this.NumCasesAVblockSecondType++;
                }
                if (this.NumCasesAVblockSecondType > this.ErrorNumCasesAVblockSecondType)
                {
                    blockadesArgs.BlockadesDiscription += "Обнаружена АВ блокада второй степени, второго типа";
                    blockadesArgs.ProblemRank = 2;
                }
                if (blockadesArgs.BlockadesDiscription.Length == 0)
                {
                    blockadesArgs.BlockadesDiscription += "АВ блокад не обнаружено. ";
                    blockadesArgs.ProblemRank = 0;
                }
                this.OnBlockadesConclusionReady(blockadesArgs);

                //Search_Third_degree_AV_block();
                //Search_Third_degree_AV_block_number2();
                //Search_Third_degree_AV_block_number3();

                SearcNon_specificDelayedIntraventricularConduction();
                //Диагностика экстрасистолий
                this.NumAtrialPrematureBeats += SearchAtrialPrematureBeats();
                this.NumVentricularExtrasystole += SearchVentricularExtrasystole();
                ExtrasystoleEventArgs args = new ExtrasystoleEventArgs();
                if (this.NumAtrialPrematureBeats < this.ErrorNumAtrialPrematureBeats)
                {
                    args.ExtrasystoleDiscription = "Предсердных экстрасистолий не обнаружено. ";
                    args.ProblemRank = 0;
                }
                else
                {
                    if (this.NumAtrialPrematureBeats < this.MaxNumAtrialPrematureBeats)
                    {
                        args.ExtrasystoleDiscription = "Найдено незначительное количество предсердных экстрасистол. ";
                        args.ProblemRank = 1;
                    }
                    else
                    {
                        args.ExtrasystoleDiscription = "Число предсердных экстрасистолий выше нормы. ";
                        args.ProblemRank = 2;
                    }
                }
                args.AtrialPrematureBeatsCount = this.NumAtrialPrematureBeats;
                if (this.NumVentricularExtrasystole < this.ErrorNumVentricularExtrasystole)
                {
                    args.ExtrasystoleDiscription += "Желудочковых экстрасистолий не обнаружено. ";
                }
                else
                {
                    if (this.NumVentricularExtrasystole < this.MaxNumVentricularExtrasystole)
                    {
                        args.ExtrasystoleDiscription += "Найдено незначительное количество желудочковых экстрасистол. ";
                        args.ProblemRank = args.ProblemRank > 1 ? args.ProblemRank : (byte)1;
                    }
                    else
                    {
                        args.ExtrasystoleDiscription += "Число предсердных экстрасистолий выше нормы. ";
                        args.ProblemRank = 2;
                    }
                }
                args.VentricularExtrasystoleCount = this.NumVentricularExtrasystole;
                this.OnExtrasystoleConclusionReady(args);

                if (SearchSVT())
                {
                    this.NumSVT++;
                }
                if (this.NumSVT > this.ErrorNumSVT)
                {
                    arrethmiaEventArgs.ArrethmiaDiscription += "Найдены случаи супровентрикулярной тахикардии. ";
                    arrethmiaEventArgs.ProblemRank = 2;
                }
                if (Search_Unspecified_Ventricular_Tachycardia())
                {
                    this.NumVentricularTachycardia++;
                }
                if (this.NumVentricularTachycardia > this.ErrorNumVentricularTachycardia)
                {
                    arrethmiaEventArgs.ArrethmiaDiscription += "Желудочковая тахикардия. ";
                    arrethmiaEventArgs.ProblemRank = 3;
                }
                if (Search_Ventricular_Fibrillation())
                {
                    this.NumVentricularFibrillation++;
                }
                if (this.NumVentricularFibrillation > this.ErrorVentricularFibrillation)
                {
                    arrethmiaEventArgs.ArrethmiaDiscription += "Встречались случаи фибрилляции желудочков. ";
                    arrethmiaEventArgs.ProblemRank = 3;
                }
                if (arrethmiaEventArgs.ArrethmiaDiscription.Length == 0)
                {
                    arrethmiaEventArgs.ArrethmiaDiscription += "Патологий ритма и аритмий не обнаружено. ";
                    arrethmiaEventArgs.ProblemRank = 0;
                }

                this.OnArrethmiaConclusionReady(arrethmiaEventArgs);

                CalculateProperties();
                //SearchShortenedPQSpacing();
                //SearchShortenedQTinterval();
                //SearchExtendedQTinterval();
                SearchMaximumPause();
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return;
            }

            GC.Collect();
        }

        public EventHandler<TrendsEventArgs> TrendsPartReady;
        protected void OnTrendsPartReady(TrendsEventArgs e)
        {
            EventHandler<TrendsEventArgs> handler = TrendsPartReady;
            if (handler != null)
                handler(this, e);
        }


        //Группа диагностирования синусовых ритмов
        /*Длина волны P не меньше 30 миллисекунд.Высота амплитуды p волны
         * по модулю не меньше(разница между началом или концом и серидиной смотря что ниже)
         * 25 микроВольт.Если таких волн больше 50% то p-волны присутствуют.
         * Если в 50% после p-волны присутствует r-зубец то p-волны нормальные. 
         * Если p-волны нормальные и присутствуют то ритм синусовы. 
         * (перед r зубцом должна быть только одна p-волна)
         */
        bool FindSinusRithm()
        {
            try
            {
                double percentGoodPWave = 0;
                int countGoodPWave = 0;

                for (int i = 0; i < p_SegmentCollection.P_Segments.Count; i++)
                {
                    if (p_SegmentCollection.P_Segments[i].LengthComplex > 0.03)
                    {
                        if (p_SegmentCollection.P_Segments[i].Amplitude >= 25)  //
                        {
                            countGoodPWave++;
                        }
                    }
                }
                percentGoodPWave = (double)countGoodPWave / p_SegmentCollection.P_Segments.Count;

                int countNormalPWave = 0;
                for (int i = 0; i < SegmentatedMapList.Count - 3; i++)
                {
                    if (SegmentatedMapList[i + 1] == "P" && SegmentatedMapList[i + 2] == "QRS")
                    {
                        if (SegmentatedMapList[i] != "P")
                        {
                            countNormalPWave++;
                        }
                    }
                }
                double percentNormalPWave = (double)countNormalPWave / p_SegmentCollection.P_Segments.Count;
                if (percentNormalPWave >= 0.5 && percentGoodPWave > 0.5)
                {
                    //DiagnosisList.Add("Ритм синусовый");
                    return true;
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
            return false;
        }

        //Регулярный ритм
        /* Берём отношение минимального r-r интервала к среднему r-r интервалу
         * (сумма по всем, делим на количество) (минимальный.средн). 
         * Отношение больше или равно 80%. 
         * Отношение максимально r-r интервала к среднему r-r дожно быть меньше или равно 115%. 
         * Если оба условие выполнены, то ритм регулярный.
         */
        bool SearchRegularRithm()
        {
            double MinOfMid_RRinterval = 0;
            double MaxOfMid_RRinterval = 0;
            try
            {
                MinOfMid_RRinterval = (double)qrs_SegmentCollection.MinRRinterval / qrs_SegmentCollection.MidRRinterval;
                MaxOfMid_RRinterval = (double)qrs_SegmentCollection.MaxRRinterval / qrs_SegmentCollection.MidRRinterval;
                if (MinOfMid_RRinterval >= 0.8 && MaxOfMid_RRinterval <= 1.15)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
            return false;
        }

        /*
         * По всем PQ интервалам. По каждому делаем проверку:
         * Если его значение отличается больше чем на 40% от среднего PQ, то считаем его выпадающим,
         * Если количество выпадающих PQ интервалов больше 60% от общего числа, то говорим что у нас не регулярные PQ 
         * интервалы. И если синусовый ритм и регулярные PQ, то можно диагностировать тахи, бради, 
         *
         */
        bool RegularPQ()
        {
            try
            {
                List<double> listPQ = new List<double>();
                bool isSinusRithm = FindSinusRithm();
                int indexP = 0;
                int indexR = 0;
                List<double> tmpP = new List<double>();
                List<double> tmpQ = new List<double>();
                for (int i = 0; i < SegmentatedMapList.Count - 2; i++)
                {
                    if (SegmentatedMapList[i] == "P" && SegmentatedMapList[i + 1] == "QRS")
                    {
                        tmpP.Add(p_SegmentCollection.P_Segments[indexP].StartP);
                        tmpQ.Add(qrs_SegmentCollection.QRS_Segments[indexR].Q_TimeStamp);
                        indexP++;
                        indexR++;
                        i += 1;
                        continue;
                    }
                    if (SegmentatedMapList[i] == "P" && SegmentatedMapList[i + 1] != "QRS")
                    {
                        tmpP.Add(p_SegmentCollection.P_Segments[indexP].StartP);
                        indexP++;
                        tmpQ.Add(-1);
                    }
                    if (SegmentatedMapList[i] != "P" && SegmentatedMapList[i + 1] == "QRS")
                    {
                        tmpP.Add(-1);
                        tmpQ.Add(qrs_SegmentCollection.QRS_Segments[indexR].Q_TimeStamp);
                        indexR++;
                        i += 1;
                        continue;
                    }
                    if (SegmentatedMapList[i] == "QRS")
                    {
                        tmpP.Add(-1);
                        tmpQ.Add(qrs_SegmentCollection.QRS_Segments[indexR].Q_TimeStamp);
                        indexR++;
                    }

                }
                int lengthResultedTMP = Math.Min(tmpP.Count, tmpQ.Count);
                double sum = 0.0;
                int countGood = 0;
                for (int i = 0; i < lengthResultedTMP; i++)
                {
                    if (tmpQ[i] > 0 && tmpP[i] > 0)
                    {
                        listPQ.Add(tmpQ[i] - tmpP[i]);
                        sum += tmpQ[i] - tmpP[i];
                        countGood++;
                    }
                }
                double MidPQ = sum / countGood;
                int Count_PQ_Out = 0;
                for (int i = 0; i < listPQ.Count; i++)
                {
                    if (listPQ[i] >= MidPQ + MidPQ * 0.4 || listPQ[i] <= MidPQ - MidPQ * 0.4)
                    {
                        Count_PQ_Out++;
                    }
                }
                if (Count_PQ_Out / listPQ.Count >= 0.6)
                {
                    //DiagnosisList.Add("Нерегулярный PQ интервал. ");
                    return false;
                }

            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
                return false;
            }
            //DiagnosisList.Add("Регулярный PQ интервал. ");
            return true;
        }

        /* Если ритм синусовый, регулярный и пульс меньше 50 
         * ставим ритм синусовый, регулярный, брадикардия.
         * Если ритм синусовый, регулярный и пульс больше 90, 
         * то ставим ритм синусовый, регулярный, тахикардию.
         * Если ритм синусовый, регулярный и пульс от 50 до 90
         * то диагноз ритм синусовый, регулярный. 
         * Если синусовый, но не регулярный, то ставим синусовую аритмию.
         */
        void DiagnosisRithmType()
        {
            try
            {
                string discription = "";
                string smalDiscription = "";
                byte rithmRank = 0;
                bool isSinusRithm = FindSinusRithm();
                bool isRegularRithm = SearchRegularRithm();
                double pulse = (double)60 / qrs_SegmentCollection.MidRRinterval; //Правильно ли я расчитываю пульс?
                bool isRegularPQ = RegularPQ();
                if (isSinusRithm && isRegularRithm && pulse <= 50.0 && isRegularPQ) //регулярность PQ интервалов
                {
                    discription = Resource.SinusRhythm + ". " + Resource.RegularRhythm + ". " + Resource.BradycardiaTitle;
                    smalDiscription = Resource.SinusRhythm + ". " + Resource.BradycardiaTitle;
                    rithmRank = 1;
                }
                if (isSinusRithm && isRegularRithm && pulse >= 90.0 && isRegularPQ) //регулярность PQ интервалов
                {
                    discription = Resource.SinusRhythm + ". " + Resource.RegularRhythm + ". " + Resource.TachycardiaTitle;
                    smalDiscription = Resource.SinusRhythm + ". " + Resource.TachycardiaTitle;
                    rithmRank = 1;
                }
                if (isSinusRithm && isRegularRithm && pulse < 90.0 && pulse > 50.0 && isRegularPQ)//регулярность PQ интервалов
                {
                    discription = Resource.SinusRhythm + ". " + Resource.RegularRhythm;
                    smalDiscription = discription;
                    rithmRank = 0;
                }
                if (isSinusRithm && !isRegularRithm && isRegularPQ) //регулярность PQ интервалов ??????????
                {
                    discription = Resource.SinusArrhythmiaTitle;
                    smalDiscription = discription;
                    rithmRank = 1;
                }
                if (!isSinusRithm && isRegularPQ) //регулярность PQ интервалов
                {
                    discription = Resource.NonsinusRhythm;
                    smalDiscription = discription;
                    rithmRank = 1;
                }

                RithmEventArgs args = new RithmEventArgs();
                args.RithmDiscription = discription;
                args.ProblemRank = rithmRank;
                args.RithmSmallDiscription = smalDiscription;
                this.OnRithmConclusionReady(args);
                int minPuls = (int)((double)60.0 / qrs_SegmentCollection.MaxRRinterval);
                int midPuls = (int)((double)60.0 / qrs_SegmentCollection.MidRRinterval);
                int maxPuls = (int)((double)60.0 / qrs_SegmentCollection.MinRRinterval);
                PulseEventArgs argsPulse = new PulseEventArgs();
                argsPulse.Pulse = (uint)midPuls; argsPulse.MinPulse = (uint)minPuls; argsPulse.MaxPulse = (uint)maxPuls;
                if (midPuls >= 50 && midPuls <= 80) argsPulse.ProblemRank = 0;
                else argsPulse.ProblemRank = 2;                
                this.OnPulseConclusionReady(argsPulse);
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
        }
        public event EventHandler<RithmEventArgs> RithmConclusionReadyEvent;
        protected void OnRithmConclusionReady(RithmEventArgs e)
        {
            EventHandler<RithmEventArgs> handler = RithmConclusionReadyEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public event EventHandler<PulseEventArgs> PulseConclusionReadyEvent;
        protected void OnPulseConclusionReady(PulseEventArgs e)
        {
            EventHandler<PulseEventArgs> handler = PulseConclusionReadyEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }


        /* Диагностика СВТ: фибрилляций предсердия и трепетания предсердий */
        /* Абсолютная нерегулярность ритма – перебираем все r-r интервалы, 
         * если количество r-r интервалов длина которых отличается 
         * от длины среднего r-r интервала больше чем на 15% больше 33% 
         * от общего числа r-r интервалов, то диагностируем абсолютный нерегулярный ритм. 
         */
        bool SearchCompletelyIrregularRhythm()
        {
            try
            {
                int countUnnecessaryRRinterval = 0;
                double definedTenPercentMidRRinterval = (double)qrs_SegmentCollection.MidRRinterval * 0.15;
                for (int i = 0; i < qrs_SegmentCollection.RRinterval.Count; i++)
                {
                    if (Math.Abs(qrs_SegmentCollection.RRinterval[i] - qrs_SegmentCollection.MidRRinterval) > definedTenPercentMidRRinterval)
                    {
                        countUnnecessaryRRinterval++;
                    }
                }
                double percentBadRRInterval = (double)countUnnecessaryRRinterval / qrs_SegmentCollection.RRinterval.Count;
                if (percentBadRRInterval > 0.33)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
            return false;
        }


        private uint NumAtrialFibrillation = 0;
        private uint ErrorNumAtrialFibrillation = 100;

        /*Если число p-волн меньше 50% от число QRS(r - интервалов) комплексов и ритм 
         * абсолютно не регулярный, то ставим фибрилляцию предсердий?????. 
         * Если ритм не абсолютно не регулярный и число p-волн меньше 50%
         * от число QRS(r - интервалов) комплексов, то ставим трепетание предсердий.?????
         */
        bool SearchAtrialFibrillation()
        {
            try
            {
                bool isSinusRythm = FindSinusRithm();
                bool isCompletelyIrregularRhythm = SearchCompletelyIrregularRhythm();
                if ((!isSinusRythm || !RegularPQ()) && isCompletelyIrregularRhythm)
                {
                    return true;
                    //ArrethmiaEventArgs args = new ArrethmiaEventArgs();
                    //args.ArrethmiaDiscription = "Фибрилляция предсердий";
                    //args.ProblemRank = 3;
                    //this.OnArrethmiaConclusionReady(args);
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
            return false;
        }


        private uint NumAtrialFlutter = 0;
        private uint ErrorNumAtrialFlutter = 100;
        bool SearchAtrialFlutter()
        {
            try
            {
                bool isCompletelyIrregularRhythm = SearchCompletelyIrregularRhythm();
                bool isSinusRithm = FindSinusRithm();
                bool isRegularRithm = SearchRegularRithm();
                double currentPulse = (double)60 / qrs_SegmentCollection.MidRRinterval;
                if (!isCompletelyIrregularRhythm && (!isSinusRithm || !RegularPQ())) //Не регулярность PQ интервалов
                {
                    return true;
                    //ArrethmiaEventArgs args = new ArrethmiaEventArgs();
                    //args.ArrethmiaDiscription = "Трепетание предсердий";
                    //args.ProblemRank = 3;
                    //this.OnArrethmiaConclusionReady(args);
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
            return false;
        }
        private uint NumSVT = 0;
        private uint ErrorNumSVT = 100;
        // суправентрикулярная тахикардия
        bool SearchSVT()
        {
            try
            {
                bool isCompletelyIrregularRhythm = SearchCompletelyIrregularRhythm();
                bool isSinusRithm = FindSinusRithm();
                bool isRegularRithm = SearchRegularRithm();
                double currentPulse = (double)60 / qrs_SegmentCollection.MidRRinterval;
                if (!isCompletelyIrregularRhythm && (!isSinusRithm || !RegularPQ()) && isRegularRithm && currentPulse > 110) //Не регулярность PQ интервалов
                {
                    return true;
                    //ArrethmiaEventArgs args = new ArrethmiaEventArgs();
                    //args.ArrethmiaDiscription = "Супровентрикулярная тахикардия.";
                    //args.ProblemRank = 3;
                    //this.OnArrethmiaConclusionReady(args);
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
            return false;
        }


        private uint NumVentricularTachycardia = 0;
        private uint ErrorNumVentricularTachycardia = 50;
        //!!!!!!!!!!!!!АВ узловой ритм!!!!!!
        /* Неуточнённая желудочковая тахикардия */
        /*Если пульс больше 220 и отсутствует p-волны в 50% случаев от числа r-пиков(QRS), 
         * то ставим неуточнённая желудочковая тахикардия.
         */
        bool Search_Unspecified_Ventricular_Tachycardia()
        {
            try
            {
                double pulse = (double)60 / qrs_SegmentCollection.MidRRinterval;

                //!!!!!!!!! допускается ли положение комплексов типа  p - p - qrs ??????

                int countNormal_P_QRS = 0;
                for (int i = 0; i < SegmentatedMapList.Count - 3; i++)
                {
                    if (SegmentatedMapList[i + 1] == "P" && SegmentatedMapList[i + 2] == "QRS")
                    {
                        if (SegmentatedMapList[i] != "P")
                        {
                            countNormal_P_QRS++;
                        }
                    }
                }
                double ratioCountRR_PPintervals = (double)countNormal_P_QRS / qrs_SegmentCollection.RRinterval.Count;

                if (pulse > 220.0 && (ratioCountRR_PPintervals <= 0.5 || !RegularPQ())) //Не регулярность через ||
                {
                    return true;
                    //ArrethmiaEventArgs args = new ArrethmiaEventArgs();
                    //args.ArrethmiaDiscription = "Желудочковая тахикардия.";
                    //args.ProblemRank = 4;
                    //this.OnArrethmiaConclusionReady(args);
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
            return false;
        }

        private uint NumVentricularFibrillation = 0;
        private uint ErrorVentricularFibrillation = 20;
        /* Фибрилляция желудочков */
        // Если пульс свыше 400 ударов в минуту, то это фибрилляция желудочков.
        bool Search_Ventricular_Fibrillation()
        {
            try
            {
                double pulse = (double)60 / qrs_SegmentCollection.MidRRinterval; //Правильно ли я расчитываю пульс?
                if (pulse >= 400 && (!RegularPQ() || true))//Вторым условием - нет R после P !!!!! Какое количество P без отвеченых R???
                {
                    //ArrethmiaEventArgs args = new ArrethmiaEventArgs();
                    //args.ArrethmiaDiscription = "Фибрилляция желудочков.";
                    //args.ProblemRank = 4;
                    //this.OnArrethmiaConclusionReady(args);
                    return true;
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
            return false;
        }

        /* Узловой ритм
         * 
         */
        void NodalRythm()
        {
            double pulse = (double)60 / qrs_SegmentCollection.MidRRinterval;
            double middleQRS = 0.0;
            for (int i = 0; i < qrs_SegmentCollection.QRS_Segments.Count; i++)
            {
                middleQRS += qrs_SegmentCollection.QRS_Segments[i].QRS_Length;
            }

            middleQRS = (double)middleQRS / qrs_SegmentCollection.QRS_Segments.Count;

            if (!FindSinusRithm() && pulse < 40 && SearchRegularRithm() && middleQRS > 0.140) // Средняя протяжённость QRS > 140мс
            {
                ArrethmiaEventArgs args = new ArrethmiaEventArgs();
                args.ArrethmiaDiscription = "Узловой ритм.";
                args.ProblemRank = 4;
                this.OnArrethmiaConclusionReady(args);
            }
        }
        public event EventHandler<ArrethmiaEventArgs> ArrethmiaConclusionReadyEvent;
        protected void OnArrethmiaConclusionReady(ArrethmiaEventArgs e)
        {
            EventHandler<ArrethmiaEventArgs> handler = ArrethmiaConclusionReadyEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private uint NumCasesAVblock = 0;
        private uint ErrorNumCasesAVblock = 100;

        /* АВ – блокады  */
        /* АВ блокада 1 степени
         * наличие синусового ритма, средняя длина PQ больше 210 миллисекунды.
         */
        bool Search_First_degree_AV_block()
        {
            try
            {
                List<double> listPQ = new List<double>();
                bool isSinusRithm = FindSinusRithm();
                int indexP = 0;
                int indexR = 0;
                List<double> tmpP = new List<double>();
                List<double> tmpQ = new List<double>();
                for (int i = 0; i < SegmentatedMapList.Count - 2; i++)
                {
                    if (SegmentatedMapList[i] == "P" && SegmentatedMapList[i + 1] == "QRS")
                    {
                        tmpP.Add(p_SegmentCollection.P_Segments[indexP].StartP);
                        tmpQ.Add(qrs_SegmentCollection.QRS_Segments[indexR].Q_TimeStamp);
                        indexP++;
                        indexR++;
                        i += 1;
                        continue;
                    }
                    if (SegmentatedMapList[i] == "P" && SegmentatedMapList[i + 1] != "QRS")
                    {
                        tmpP.Add(p_SegmentCollection.P_Segments[indexP].StartP);
                        indexP++;
                        tmpQ.Add(-1);
                    }
                    if (SegmentatedMapList[i] != "P" && SegmentatedMapList[i + 1] == "QRS")
                    {
                        tmpP.Add(-1);
                        tmpQ.Add(qrs_SegmentCollection.QRS_Segments[indexR].Q_TimeStamp);
                        indexR++;
                        i += 1;
                        continue;
                    }
                    if (SegmentatedMapList[i] == "QRS")
                    {
                        tmpP.Add(-1);
                        tmpQ.Add(qrs_SegmentCollection.QRS_Segments[indexR].Q_TimeStamp);
                        indexR++;
                    }
                }
                int lengthResultedTMP = Math.Min(tmpP.Count, tmpQ.Count);
                double sum = 0.0;
                int countGood = 0;
                for (int i = 0; i < lengthResultedTMP; i++)
                {
                    if (tmpQ[i] > 0 && tmpP[i] > 0)
                    {
                        sum += tmpQ[i] - tmpP[i];
                        countGood++;
                    }
                }

                double MidPQ = sum / countGood;
                if (isSinusRithm && MidPQ > 0.21)
                {
                    return true;
                    //BlockadesEventArgs args = new BlockadesEventArgs();
                    //args.BlockadesDiscription = "АВ блокад первой степени";
                    //args.ProblemRank = 3;
                    //this.OnBlockadesConclusionReady(args);
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
            return false;
        }

        private uint NumCasesAVblockFirstType = 0;
        private uint ErrorNumCasesAVblockFirstType = 100;

        // АВ блокада 2 степени первый тип
        /* Наличие синусового ритма, постепенное увелечение длины r-r интервалов
         * вплоть до выпадения r-зубца(длина между r-r интервалами свыше 1.8
         * средних r-r)
         */

        bool Search_Second_degree_AV_block_first_type()
        {
            try
            {
                bool isSinusRithm = FindSinusRithm();
                int countRRintervalMoreMID1_5 = 0;
                for (int i = 0; i < qrs_SegmentCollection.RRinterval.Count; i++)
                {
                    if (qrs_SegmentCollection.RRinterval[i] > (qrs_SegmentCollection.MidRRinterval * 1.8))
                    {
                        countRRintervalMoreMID1_5++;
                    }
                }
                if (isSinusRithm && countRRintervalMoreMID1_5 > 0) // Добавить расстояние между двумя подряд идущими p волнами, расстояние между которыми >= 450 мс. Посмотри во втором типе
                {
                    return true;
                    //BlockadesEventArgs args = new BlockadesEventArgs();
                    //args.BlockadesDiscription = "АВ блокада второй степени первого типа";
                    //args.ProblemRank = 3;
                    //this.OnBlockadesConclusionReady(args);
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
            return false;
        }

        private uint NumCasesAVblockSecondType = 0;
        private uint ErrorNumCasesAVblockSecondType = 100;
        // АВ блокада 2 степени второй тип:
        /* rr
         * после которой нет зубца r, после которой идёт пауза до следующего r, 
         * больше среднего r на 50%.
         * Расстояние между P-P больше 450 миллисекунд
         */
        bool Search_Second_degree_AV_block_second_type()
        {
            try
            {
                int countRRintervalMoreMID1_5 = 0;
                for (int i = 0; i < qrs_SegmentCollection.RRinterval.Count; i++)
                {
                    if (qrs_SegmentCollection.RRinterval[i] > (qrs_SegmentCollection.MidRRinterval * 1.8))
                    {
                        countRRintervalMoreMID1_5++;
                    }
                }
                int countComlexP_P = 0;
                int indexComlexP_P = 0;
                for (int i = 0; i < SegmentatedMapList.Count - 2; i++)
                {
                    if (SegmentatedMapList[i] == "P" && SegmentatedMapList[i + 1] == "P")
                    {
                        if (p_SegmentCollection.PPinterval[indexComlexP_P] > 0.45)
                        {
                            countComlexP_P++;
                            continue;
                        }
                        indexComlexP_P++;
                    }
                    if (SegmentatedMapList[i] == "P" && SegmentatedMapList[i + 1] != "P")
                    {
                        indexComlexP_P++;
                    }
                }

                if (countRRintervalMoreMID1_5 > 0 && countComlexP_P > 0 && !FindSinusRithm())
                {
                    return true;
                    //BlockadesEventArgs args = new BlockadesEventArgs();
                    //args.BlockadesDiscription = "АВ блокада второй степени второго типа";
                    //args.ProblemRank = 4;
                    //this.OnBlockadesConclusionReady(args);
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
            return false;
        }

        //Алгоритм не валидный
        // АВ блокада 3 степени  
        /* больше 2 p – волн подряд(2 в секунду) идут без r-пика на расстоянии.
         * Частота p-p в 2 раза больше чем r-r, 
         * средний p-p интервала не меньше чем ???????
         */
        /*
        void Search_Third_degree_AV_block()
        {
            try
            {
                int countComlexP_P = 0;
                int indexComlexP_P = 0;
                for (int i = 0; i < SegmentatedMapList.Count - 2; i++)
                {
                    if (SegmentatedMapList[i] == "P" && SegmentatedMapList[i + 1] == "P")
                    {
                        if (p_SegmentCollection.PPinterval[indexComlexP_P] > 0.45)
                        {
                            countComlexP_P++;
                            continue;
                        }
                        indexComlexP_P++;
                    }
                    if (SegmentatedMapList[i] == "P" && SegmentatedMapList[i + 1] != "P")
                    {
                        indexComlexP_P++;
                    }
                }

                int countRRintervalMore1300 = 0;
                for (int i = 0; i < qrs_SegmentCollection.RRinterval.Count - 2; i++)
                {
                    if (qrs_SegmentCollection.RRinterval[i] > 1.3 && qrs_SegmentCollection.RRinterval[i + 1] > 1.3)
                    {
                        countRRintervalMore1300++;
                    }
                }

                if (countRRintervalMore1300 > 0 && countComlexP_P > 0)
                {
                    BlockadesEventArgs args = new BlockadesEventArgs();
                    args.BlockadesDiscription = "АВ блокада третьей степени";
                    args.ProblemRank = 4;
                    this.OnBlockadesConclusionReady(args);
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
        }
        void Search_Third_degree_AV_block_number2()
        {
            try
            {
                int countComlexP_P = 0;
                int indexComlexP_P = 0;
                int indexComplex_QRS = 0;
                for (int i = 0; i < SegmentatedMapList.Count - 2; i++)
                {
                    if (SegmentatedMapList[i] == "P" && SegmentatedMapList[i + 1] == "P")
                    {
                        if (p_SegmentCollection.PPinterval[indexComlexP_P] > 0.45)
                        {
                            for (int j = 0; j < i; j++)
                            {
                                if (SegmentatedMapList[i] == "QRS")
                                {
                                    indexComplex_QRS++;
                                }
                            }
                            countComlexP_P++;
                            continue;
                        }
                        indexComlexP_P++;
                    }
                    if (SegmentatedMapList[i] == "P" && SegmentatedMapList[i + 1] != "P")
                    {
                        indexComlexP_P++;
                    }
                }

                int countRRintervalMore1300 = 0;
                for (int i = 0; i < qrs_SegmentCollection.RRinterval.Count - 2; i++)
                {
                    if (qrs_SegmentCollection.RRinterval[i] > 1.3 && qrs_SegmentCollection.RRinterval[i + 1] > 1.3)
                    {
                        countRRintervalMore1300++;
                    }
                }

                if (countRRintervalMore1300 > 0 && countComlexP_P > 0 &&
                    ((double)qrs_SegmentCollection.RRinterval[indexComplex_QRS] / p_SegmentCollection.PPinterval[indexComlexP_P] > 2))
                {
                    BlockadesEventArgs args = new BlockadesEventArgs();
                    args.BlockadesDiscription = "АВ блокада третьей степени";
                    args.ProblemRank = 4;
                    this.OnBlockadesConclusionReady(args);
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
        }
        void Search_Third_degree_AV_block_number3()
        {
            try
            {
                int countComlexP_P = 0;
                int indexComlexP_P = 0;
                int indexComplex_QRS = 0;
                for (int i = 0; i < SegmentatedMapList.Count - 2; i++)
                {
                    if (SegmentatedMapList[i] == "P" && SegmentatedMapList[i + 1] == "P")
                    {
                        if (p_SegmentCollection.PPinterval[indexComlexP_P] < 0.45)
                        {
                            for (int j = 0; j < i; j++)
                            {
                                if (SegmentatedMapList[i] == "QRS")
                                {
                                    indexComplex_QRS++;
                                }
                            }
                            countComlexP_P++;
                            continue;
                        }
                        indexComlexP_P++;
                    }
                    if (SegmentatedMapList[i] == "P" && SegmentatedMapList[i + 1] != "P")
                    {
                        indexComlexP_P++;
                    }
                }

                int countRRintervalMore1300 = 0;
                for (int i = 0; i < qrs_SegmentCollection.RRinterval.Count - 2; i++)
                {
                    if (qrs_SegmentCollection.RRinterval[i] > 1.3 && qrs_SegmentCollection.RRinterval[i + 1] > 1.3)
                    {
                        countRRintervalMore1300++;
                    }
                }

                if (countRRintervalMore1300 > 0 && countComlexP_P > 0 &&
                    ((double)qrs_SegmentCollection.RRinterval[indexComplex_QRS] / p_SegmentCollection.PPinterval[indexComlexP_P] > 2))
                {
                    BlockadesEventArgs args = new BlockadesEventArgs();
                    args.BlockadesDiscription = "АВ блокада третьей степени";
                    args.ProblemRank = 4;
                    this.OnBlockadesConclusionReady(args);
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
        }
        */
        public event EventHandler<BlockadesEventArgs> BlockadesConclusionReadyEvent;
        protected void OnBlockadesConclusionReady(BlockadesEventArgs e)
        {
            EventHandler<BlockadesEventArgs> handler = BlockadesConclusionReadyEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }




        /* Неспецифическая задержка внутрижелудочкового проведения */
        /* Длина QRS больше 150 миллисекунд и таких комплексов больше 80% от общего количества QRS. */

        void SearcNon_specificDelayedIntraventricularConduction()
        {
            try
            {
                int count = 0;
                for (int i = 0; i < qrs_SegmentCollection.QRS_Segments.Count; i++)
                {
                    if (qrs_SegmentCollection.QRS_Segments[i].QRS_Length > 0.15)
                    {
                        count++;
                    }
                }

                double percentQRS = (double)count / qrs_SegmentCollection.QRS_Segments.Count;
                if (percentQRS > 0.8)
                {
                    ExtrasystoleEventArgs args = new ExtrasystoleEventArgs();
                    args.ExtrasystoleDiscription = "Неспецифическая задержка внутрижелудочкового проведения";
                    args.ProblemRank = 2;
                    //this.OnExtrasystoleConclusionReady(args);
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
        }


        private uint NumAtrialPrematureBeats = 0;
        private uint NumVentricularExtrasystole = 0;
        private readonly uint MaxNumAtrialPrematureBeats = 2000;
        private readonly uint MaxNumVentricularExtrasystole = 2000;
        private readonly uint ErrorNumAtrialPrematureBeats = 300;
        private readonly uint ErrorNumVentricularExtrasystole = 300;

        /*	Предсердная экстрасистолия */
        /* Когда длина комплекса QRS равен или +- 15% от среднего протяжения QRS в этой кардиограмме. 
         * После экстрасистолы существует хотя бы один RR интервал длительность которого есть 120% 
         * от среднего RR интервала и больше. 
         */
        string diagnosForExtraSistole = "";
        uint SearchAtrialPrematureBeats()
        {
            uint res = 0;
            try
            {
                double MidlleLengthQRS = 0.0;
                for (int i = 0; i < qrs_SegmentCollection.QRS_Segments.Count; i++)
                {
                    MidlleLengthQRS += qrs_SegmentCollection.QRS_Segments[i].QRS_Length;
                }
                MidlleLengthQRS = MidlleLengthQRS / qrs_SegmentCollection.QRS_Segments.Count;

                //Проверить
                uint countExtr = 0;
                for (int i = 1; i < qrs_SegmentCollection.QRS_Segments.Count - 2; i++)
                {
                    if ((qrs_SegmentCollection.QRS_Segments[i].QRS_Length > (MidlleLengthQRS - 0.15 * MidlleLengthQRS)) &&
                        (qrs_SegmentCollection.QRS_Segments[i].QRS_Length < (MidlleLengthQRS + 0.15 * MidlleLengthQRS)) &&
                        qrs_SegmentCollection.RRinterval[i] > (0.2 * qrs_SegmentCollection.MidRRinterval + qrs_SegmentCollection.MidRRinterval)
                        && qrs_SegmentCollection.RRinterval[i - 1] < (0.8 * qrs_SegmentCollection.MidRRinterval))
                    {
                        countExtr++;
                    }
                }
                if (countExtr > 0)
                {
                    //ExtrasystoleEventArgs args = new ExtrasystoleEventArgs();
                    //args.ExtrasystoleDiscription = "Предсердная экстрасистолия. Зафиксировано" + countExtr.ToString();
                    //args.AtrialPrematureBeatsCount = (ushort)countExtr;
                    //args.ProblemRank = 2;
                    //this.OnExtrasystoleConclusionReady(args);
                    res = countExtr;
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
                res = 0;
            }
            return res;
        }

        /* Желудочковая экстрасистолия */
        /* Когда длина комплекса QRS больше среднего QRS на 40 и больше % 
         * и перед ним не должно быть зубца p. 
         * После этого QRS комплекса, RR интервал длительность которого
         * есть 120% от среднего RR интервала и больше. 
         */
        uint SearchVentricularExtrasystole()
        {
            uint res = 0;
            try
            {
                double MidlleLengthQRS = 0.0;
                for (int i = 0; i < qrs_SegmentCollection.QRS_Segments.Count; i++)
                {
                    MidlleLengthQRS += qrs_SegmentCollection.QRS_Segments[i].QRS_Length;
                }
                MidlleLengthQRS = MidlleLengthQRS / qrs_SegmentCollection.QRS_Segments.Count;

                uint countExtr = 0;
                for (int i = 0; i < qrs_SegmentCollection.QRS_Segments.Count - 2; i++)
                {
                    if (qrs_SegmentCollection.QRS_Segments[i].QRS_Length > (MidlleLengthQRS + MidlleLengthQRS * 0.4) &&
                        qrs_SegmentCollection.RRinterval[i] > (0.2 * qrs_SegmentCollection.MidRRinterval + qrs_SegmentCollection.MidRRinterval))
                    {
                        countExtr++;
                    }
                }
                if (countExtr > 0)
                {
                    //ExtrasystoleEventArgs args = new ExtrasystoleEventArgs();
                    //args.ExtrasystoleDiscription = "Желудочковая экстрасистолия. Зафиксировано " + countExtr.ToString();
                    //args.VentricularExtrasystoleCount = (ushort)countExtr;
                    //args.ProblemRank = 2;
                    //this.OnExtrasystoleConclusionReady(args);
                    res = countExtr;
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
                res = 0;
            }
            return res;
        }
        public event EventHandler<ExtrasystoleEventArgs> ExtrasystoleConclusionReadyEvent;
        protected void OnExtrasystoleConclusionReady(ExtrasystoleEventArgs e)
        {
            EventHandler<ExtrasystoleEventArgs> handler = ExtrasystoleConclusionReadyEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }




        public event EventHandler<QRSPropertyEventArgs> QRSPropertyConclusionReadyEvent;
        protected void OnQRSPropertyConclusionReady(QRSPropertyEventArgs e)
        {
            EventHandler<QRSPropertyEventArgs> handler = QRSPropertyConclusionReadyEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        void CalculateProperties()
        {
            double correctCoef = 1;
            try
            {
                QRSPropertyEventArgs args = new QRSPropertyEventArgs();


                // P зубец
                double SomePInterval = 0.0;
                if (p_SegmentCollection.P_Segments.Count > 5)
                {
                    SomePInterval = p_SegmentCollection.P_Segments[3].LengthComplex;
                }
                else
                {
                    SomePInterval = p_SegmentCollection.P_Segments[p_SegmentCollection.P_Segments.Count - 1].LengthComplex;
                }
                double sumP = 0.0;
                for (int i = 0; i < p_SegmentCollection.P_Segments.Count; i++)
                {
                    sumP += p_SegmentCollection.P_Segments[i].LengthComplex;
                }
                sumP = (double)sumP / p_SegmentCollection.P_Segments.Count;

                string mantissaString = String.Format("{0:0.000}", SomePInterval * correctCoef);
                string mantissaStringSumP = String.Format("{0:0.000}", sumP * correctCoef);
                args.P_Notch = mantissaString;
                args.P_NotchAVG = mantissaStringSumP;


                //PQ длина
                List<double> listPQ = new List<double>();
                bool isSinusRithm = FindSinusRithm();
                int indexP = 0;
                int indexR = 0;
                List<double> tmpP = new List<double>();
                List<double> tmpQ = new List<double>();
                for (int i = 0; i < SegmentatedMapList.Count - 2; i++)
                {
                    if (SegmentatedMapList[i] == "P" && SegmentatedMapList[i + 1] == "QRS")
                    {
                        tmpP.Add(p_SegmentCollection.P_Segments[indexP].StartP);
                        tmpQ.Add(qrs_SegmentCollection.QRS_Segments[indexR].Q_TimeStamp);
                        indexP++;
                        indexR++;
                        i += 1;
                        continue;
                    }
                    if (SegmentatedMapList[i] == "P" && SegmentatedMapList[i + 1] != "QRS")
                    {
                        tmpP.Add(p_SegmentCollection.P_Segments[indexP].StartP);
                        indexP++;
                        tmpQ.Add(-1);
                    }
                    if (SegmentatedMapList[i] != "P" && SegmentatedMapList[i + 1] == "QRS")
                    {
                        tmpP.Add(-1);
                        tmpQ.Add(qrs_SegmentCollection.QRS_Segments[indexR].Q_TimeStamp);
                        indexR++;
                        i += 1;
                        continue;
                    }
                    if (SegmentatedMapList[i] == "QRS")
                    {
                        tmpP.Add(-1);
                        tmpQ.Add(qrs_SegmentCollection.QRS_Segments[indexR].Q_TimeStamp);
                        indexR++;
                    }
                }
                int lengthResultedTMP = Math.Min(tmpP.Count, tmpQ.Count);
                double sum = 0.0;
                int countGood = 0;
                for (int i = 0; i < lengthResultedTMP; i++)
                {
                    if (tmpQ[i] > 0 && tmpP[i] > 0)
                    {
                        listPQ.Add(tmpQ[i] - tmpP[i]);
                        sum += tmpQ[i] - tmpP[i];
                        countGood++;
                    }
                }
                double MidPQ = sum / countGood;
                double PQLegth = 0.0;
                if (listPQ.Count > 5)
                {
                    PQLegth = listPQ[3];
                }
                else
                {
                    PQLegth = listPQ[listPQ.Count - 1];
                }

                args.PQ_Notch = String.Format("{0:0.000}", PQLegth * correctCoef);
                args.PQ_NotchAVG = String.Format("{0:0.000}", MidPQ * correctCoef);


                //QRS длина
                double midQRS = 0.0;
                double SomeQRS = 0.0;
                if (qrs_SegmentCollection.QRS_Segments.Count > 5)
                {
                    SomeQRS = qrs_SegmentCollection.QRS_Segments[3].QRS_Length;
                }
                else
                {
                    SomeQRS = qrs_SegmentCollection.QRS_Segments[qrs_SegmentCollection.QRS_Segments.Count - 1].QRS_Length;
                }
                for (int i = 0; i < qrs_SegmentCollection.QRS_Segments.Count; i++)
                {
                    midQRS += qrs_SegmentCollection.QRS_Segments[i].QRS_Length;
                }
                midQRS = (double)midQRS / qrs_SegmentCollection.QRS_Segments.Count;

                args.QRS_Notch = String.Format("{0:0.000}", SomeQRS * correctCoef);
                args.QRS_NotchAVG = String.Format("{0:0.000}", midQRS * correctCoef);


                //QT длина
                List<double> listQRST = new List<double>();
                int indexQRS = 0;
                int indexT = 0;
                List<double> tmpQRS = new List<double>();
                List<double> tmpT = new List<double>();
                for (int i = 0; i < SegmentatedMapList.Count - 2; i++)
                {
                    if (SegmentatedMapList[i] == "QRS" && SegmentatedMapList[i + 1] == "T")
                    {
                        tmpQRS.Add(qrs_SegmentCollection.QRS_Segments[indexQRS].Q_TimeStamp);
                        tmpT.Add(t_SegmentCollection.T_Segments[indexT].FinishT);
                        indexQRS++;
                        indexT++;
                        i += 1;
                        continue;
                    }
                    if (SegmentatedMapList[i] == "QRS" && SegmentatedMapList[i + 1] != "T")
                    {
                        tmpQRS.Add(qrs_SegmentCollection.QRS_Segments[indexQRS].Q_TimeStamp);
                        indexQRS++;
                        tmpT.Add(-1);
                    }
                    if (SegmentatedMapList[i] != "QRS" && SegmentatedMapList[i + 1] == "T")
                    {
                        tmpQRS.Add(-1);
                        tmpT.Add(t_SegmentCollection.T_Segments[indexT].FinishT);
                        indexT++;
                        i += 1;
                        continue;
                    }
                    if (SegmentatedMapList[i] == "T")
                    {
                        tmpQRS.Add(-1);
                        tmpT.Add(t_SegmentCollection.T_Segments[indexT].FinishT);
                        indexT++;
                    }
                }
                int lengthResultedList = Math.Min(tmpT.Count, tmpQRS.Count);
                double sumLengthQRST = 0.0;
                int countGoodQRST = 0;
                for (int i = 0; i < lengthResultedList; i++)
                {
                    if (tmpT[i] > 0 && tmpQRS[i] > 0)
                    {
                        listQRST.Add(tmpT[i] - tmpQRS[i]);
                        sumLengthQRST += tmpT[i] - tmpQRS[i];
                        countGoodQRST++;
                    }
                }
                double MidQRST = sumLengthQRST / countGoodQRST;
                double QRSTLegth = 0.0;
                if (listQRST.Count > 5)
                {
                    QRSTLegth = listQRST[3];
                }
                else
                {
                    QRSTLegth = listQRST[listQRST.Count - 1];
                }

                args.QT_Notch = String.Format("{0:0.000}", QRSTLegth * correctCoef);
                args.QT_NotchAVG = String.Format("{0:0.000}", MidQRST * correctCoef);



                this.OnQRSPropertyConclusionReady(args);

            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;
            }
        }

        /* Укороченный интервал PQ */
        /* Если расстояние от P до Q меньше или равно 100 миллисекунд, 
         * больше чем в 50 процентах комплексов.
         */
        /*
        bool SearchShortenedPQSpacing()
        {
            try
            {
                List<double> listPQ = new List<double>();
                int indexP = 0;
                int indexR = 0;
                List<double> tmpP = new List<double>();
                List<double> tmpQ = new List<double>();
                for (int i = 0; i < SegmentatedMapList.Count - 2; i++)
                {
                    if (SegmentatedMapList[i] == "P" && SegmentatedMapList[i + 1] == "QRS")
                    {
                        tmpP.Add(p_SegmentCollection.P_Segments[indexP].StartP);
                        tmpQ.Add(qrs_SegmentCollection.QRS_Segments[indexR].Q_TimeStamp);
                        indexP++;
                        indexR++;
                        i += 1;
                        continue;
                    }
                    if (SegmentatedMapList[i] == "P" && SegmentatedMapList[i + 1] != "QRS")
                    {
                        tmpP.Add(p_SegmentCollection.P_Segments[indexP].StartP);
                        indexP++;
                        tmpQ.Add(-1);
                    }
                    if (SegmentatedMapList[i] != "P" && SegmentatedMapList[i + 1] == "QRS")
                    {
                        tmpP.Add(-1);
                        tmpQ.Add(qrs_SegmentCollection.QRS_Segments[indexR].Q_TimeStamp);
                        indexR++;
                        i += 1;
                        continue;
                    }
                    if (SegmentatedMapList[i] == "QRS")
                    {
                        tmpP.Add(-1);
                        tmpQ.Add(qrs_SegmentCollection.QRS_Segments[indexR].Q_TimeStamp);
                        indexR++;
                    }
                }

                int lengthResultedTMP = Math.Min(tmpP.Count, tmpQ.Count);
                double sum = 0.0;
                int countGood = 0;
                for (int i = 0; i < lengthResultedTMP; i++)
                {
                    if (tmpQ[i] > 0 && tmpP[i] > 0)
                    {
                        listPQ.Add(tmpQ[i] - tmpP[i]);
                        sum += tmpQ[i] - tmpP[i];
                        countGood++;
                    }
                }

                int countGoodPQ = 0;
                for (int i = 0; i < listPQ.Count; i++)
                {
                    if (listPQ[i] <= 0.1) countGoodPQ++;
                }

                if ((double)countGoodPQ / listPQ.Count > 0.5)
                {
                    //DiagnosisList.Add("Укороченный интервал PQ");
                    return true;
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
            return false;
        }
        */

        /* Укороченный интервал Q(N)T(t)) */
        /*Если интервал QT в 50 и более процентах 
         * комплексов меньше или равен 320 миллисекунд.
         */
        /*
        bool SearchShortenedQTinterval()
        {
            try
            {
                List<double> listQT = new List<double>();
                int indexQ = 0;
                int indexT = 0;
                List<double> tmpQ = new List<double>();
                List<double> tmpT = new List<double>();
                for (int i = 0; i < SegmentatedMapList.Count - 2; i++)
                {
                    if (SegmentatedMapList[i] == "QRS" && SegmentatedMapList[i + 1] == "T")
                    {
                        tmpT.Add(t_SegmentCollection.T_Segments[indexT].FinishT);
                        tmpQ.Add(qrs_SegmentCollection.QRS_Segments[indexQ].Q_TimeStamp);
                        indexT++;
                        indexQ++;
                    }
                    if (SegmentatedMapList[i] == "QRS" && SegmentatedMapList[i + 1] != "T")
                    {
                        tmpQ.Add(qrs_SegmentCollection.QRS_Segments[indexQ].Q_TimeStamp);
                        indexQ++;
                        tmpT.Add(-1);
                    }
                    if (SegmentatedMapList[i] != "QRS" && SegmentatedMapList[i + 1] == "T")
                    {
                        tmpQ.Add(-1);
                        tmpT.Add(t_SegmentCollection.T_Segments[indexT].FinishT);
                        indexT++;
                    }
                }

                int lengthResultedTMP = Math.Min(tmpT.Count, tmpQ.Count);
                double sum = 0.0;
                int countGood = 0;
                for (int i = 0; i < lengthResultedTMP; i++)
                {
                    if (tmpQ[i] > 0 && tmpT[i] > 0)
                    {
                        listQT.Add(tmpT[i] - tmpQ[i]);
                        sum += tmpT[i] - tmpQ[i];
                        countGood++;
                    }
                }

                int countGoodQT = 0;
                for (int i = 0; i < listQT.Count; i++)
                {
                    if (listQT[i] <= 0.32) countGoodQT++;
                }

                if ((double)countGoodQT / listQT.Count > 0.5)
                {
                    //DiagnosisList.Add("Укороченный интервал QT");
                    return true;
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
            return false;
        }
        */

        /* Удлинённый QT
         * Если интервал QT в 50 и более процентах комплексов больше или равен 480 миллисекунд. 
         */
        /*
        bool SearchExtendedQTinterval()
        {
            try
            {
                List<double> listQT = new List<double>();
                int indexQ = 0;
                int indexT = 0;
                List<double> tmpQ = new List<double>();
                List<double> tmpT = new List<double>();
                for (int i = 0; i < SegmentatedMapList.Count - 2; i++)
                {
                    if (SegmentatedMapList[i] == "QRS" && SegmentatedMapList[i + 1] == "T")
                    {
                        tmpT.Add(t_SegmentCollection.T_Segments[indexT].FinishT);
                        tmpQ.Add(qrs_SegmentCollection.QRS_Segments[indexQ].Q_TimeStamp);
                        indexT++;
                        indexQ++;
                    }
                    if (SegmentatedMapList[i] == "QRS" && SegmentatedMapList[i + 1] != "T")
                    {
                        tmpQ.Add(qrs_SegmentCollection.QRS_Segments[indexQ].Q_TimeStamp);
                        indexQ++;
                        tmpT.Add(-1);
                    }
                    if (SegmentatedMapList[i] != "QRS" && SegmentatedMapList[i + 1] == "T")
                    {
                        tmpQ.Add(-1);
                        tmpT.Add(t_SegmentCollection.T_Segments[indexT].FinishT);
                        indexT++;
                    }
                }

                int lengthResultedTMP = Math.Min(tmpT.Count, tmpQ.Count);
                double sum = 0.0;
                int countGood = 0;
                for (int i = 0; i < lengthResultedTMP; i++)
                {
                    if (tmpQ[i] > 0 && tmpT[i] > 0)
                    {
                        listQT.Add(tmpT[i] - tmpQ[i]);
                        sum += tmpT[i] - tmpQ[i];
                        countGood++;
                    }
                }

                int countGoodQT = 0;
                for (int i = 0; i < listQT.Count; i++)
                {
                    if (listQT[i] >= 0.48) countGoodQT++;
                }

                if ((double)countGoodQT / listQT.Count > 0.5)
                {
                    //DiagnosisList.Add("Удлинённый интервал QT");
                    return true;
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
            return false;
        }

        */

        private uint NumTwoSecondsPause = 0;
        private uint NumThreeSecondsPause = 0;
        private readonly uint MaxNumTwoSecondsPause = 300;
        private readonly uint MaxNumThreeSecondsPause = 300;
        private readonly uint ErrorNumTwoSecondsPause = 100;
        private readonly uint ErrorNumThreeSecondsPause = 100;

        /* Максимальная пауза – максимальный RR интервал */
        void SearchMaximumPause()
        {
            try
            {
                uint countMoreTwoSecond = 0;
                uint countMoreThreeSecond = 0;
                for (int i = 0; i < qrs_SegmentCollection.RRinterval.Count; i++)
                {
                    if (qrs_SegmentCollection.RRinterval[i] >= 2 && qrs_SegmentCollection.RRinterval[i] <= 3)
                    {
                        countMoreTwoSecond++;
                    }
                    if (qrs_SegmentCollection.RRinterval[i] > 3)
                    {
                        countMoreThreeSecond++;
                    }
                }

                PauseEventArgs args = new PauseEventArgs();

                if (countMoreTwoSecond > 0)
                {
                    this.NumTwoSecondsPause += countMoreTwoSecond;
                    if (this.NumTwoSecondsPause > this.MaxNumTwoSecondsPause)
                    {
                        args.PauseDiscription = "Внушительное количество духсекундных пауз между ударами сердца. ";
                        args.ProblemRank = 2;
                    }
                    else
                    {
                        if (this.NumTwoSecondsPause > this.ErrorNumTwoSecondsPause)
                        {
                            args.PauseDiscription = "Незначительное количество духсекундных пауз между ударами сердца. ";
                            args.ProblemRank = 1;
                        }
                        else
                        {
                            args.PauseDiscription = "Духсекундных пауз между ударами сердца не обнаружено. ";
                            args.ProblemRank = 0;
                        }
                    }
                    args.CountFromTwoToThreeSecondsPause = this.NumTwoSecondsPause;
                }
                if (countMoreThreeSecond > 0)
                {
                    this.NumThreeSecondsPause += countMoreThreeSecond;
                    if (this.NumThreeSecondsPause > this.MaxNumThreeSecondsPause)
                    {
                        args.PauseDiscription = "Появление трёхсекундных пауз в большом количестве.";
                        args.ProblemRank = 3;
                    }
                    else
                    {
                        if (this.NumThreeSecondsPause > this.ErrorNumThreeSecondsPause)
                        {
                            args.PauseDiscription = "Редкие трёхсекундные паузы. ";
                            args.ProblemRank = 2;
                        }
                        else
                        {
                            args.PauseDiscription = "Трёхсекундных пауз не обнаружено.";
                            args.ProblemRank = 0;
                        }
                    }
                    args.CountMoreThreeSecondsPause = this.NumThreeSecondsPause;
                }
                args.MaxPause = String.Format("{0:0.00}", qrs_SegmentCollection.RRinterval.Max());
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
        }

        public event EventHandler<PauseEventArgs> PauseConclusionReadyEvent;
        protected void OnPauseConclusionReady(PauseEventArgs e)
        {
            EventHandler<PauseEventArgs> handler = PauseConclusionReadyEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public void Dispose()
        {
            if (this.DataConverter != null)
            {
                this.DataConverter.SignalsConvertedEvent -= DataArrived;
            }
        }

        public class P_SegmentCollection
        {
            public List<P_Segment> P_Segments { get; private set; }
            public List<double> PPinterval { get; private set; }

            public double MaxPPinterval { get; private set; } = 0.0;
            public double MinPPinterval { get; private set; } = 0.0;
            public double MidPPinterval { get; private set; } = 0.0;

            public P_SegmentCollection()
            {
                P_Segments = new List<P_Segment>();
            }

            public bool Is_P_Segment(int P_Start_Code, int P_Middle_Code, int P_Finish_Code)
            {
                return (P_Start_Code == 42 && P_Middle_Code == 24 && P_Finish_Code == 43);
            }

            public void CreateListPPInterval()
            {
                PPinterval = new List<double>();
                if (P_Segments.Count > 1)
                {
                    for (int i = 0; i < P_Segments.Count - 1; i++)
                    {
                        PPinterval.Add(P_Segments[i + 1].MiddleP - P_Segments[i].MiddleP);
                    }

                    MaxPPinterval = PPinterval.Max();
                    MinPPinterval = PPinterval.Min();
                    double sum = 0.0;
                    for (int i = 0; i < PPinterval.Count; i++)
                    {
                        sum += PPinterval[i];
                    }
                    MidPPinterval = (double)sum / PPinterval.Count;
                }
            }
        }
        public class QRS_SegmentCollection
        {
            public List<QRS_Segment> QRS_Segments { get; private set; }
            public List<double> RRinterval { get; private set; }

            public double MaxRRinterval { get; private set; } = 0.0;
            public double MinRRinterval { get; private set; } = 0.0;
            public double MidRRinterval { get; private set; } = 0.0;

            public QRS_SegmentCollection()
            {
                QRS_Segments = new List<QRS_Segment>();
            }

            public bool Is_QRS_Segment(int Q_Code, int R_Code, int S_Code)
            {
                /*
                if (Q_Code == 47 || Q_Code == 48)
                {
                    if (R_Code == 49 || R_Code == 50)
                    {
                        if (S_Code == 40)
                        {
                            return true;
                        }
                    }
                }
                */


                if (Q_Code == 47 || Q_Code == 48)
                {
                    if (R_Code == 49 || R_Code == 50)
                    {
                        if (S_Code == 40)
                        {
                            return true;
                        }
                    }
                }
                
                return false;
            }

            public void CreateListRRInterval()
            {
                RRinterval = new List<double>();
                if (QRS_Segments.Count > 1)
                {
                    for (int i = 0; i < QRS_Segments.Count - 1; i++)
                    {
                        RRinterval.Add(QRS_Segments[i + 1].R_TimeStamp - QRS_Segments[i].R_TimeStamp);
                    }

                    MaxRRinterval = RRinterval.Max();
                    MinRRinterval = RRinterval.Min();
                    double sum = 0.0;
                    for (int i = 0; i < RRinterval.Count; i++)
                    {
                        sum += RRinterval[i];
                    }
                    MidRRinterval = (double)sum / RRinterval.Count;
                }
            }
        }
        public class T_SegmentCollection
        {
            public List<T_Segment> T_Segments { get; private set; }
            public List<double> TTinterval { get; private set; }

            public double MaxTTinterval { get; private set; } = 0.0;
            public double MinTTinterval { get; private set; } = 0.0;
            public double MidTTinterval { get; private set; } = 0.0;

            public T_SegmentCollection()
            {
                T_Segments = new List<T_Segment>();
            }

            public bool Is_T_Segment(int T_Start_Code, int T_Middle_Code, int T_Finish_Code)
            {
                return (T_Start_Code == 44 && T_Middle_Code == 27 && T_Finish_Code == 45);
            }

            public void CreateListTTInterval()
            {
                TTinterval = new List<double>();
                if (T_Segments.Count > 1)
                {
                    for (int i = 0; i < T_Segments.Count - 1; i++)
                    {
                        TTinterval.Add(T_Segments[i + 1].MiddleT - T_Segments[i].MiddleT);
                    }

                    MaxTTinterval = TTinterval.Max();
                    MinTTinterval = TTinterval.Min();
                    double sum = 0.0;
                    for (int i = 0; i < TTinterval.Count; i++)
                    {
                        sum += TTinterval[i];
                    }
                    MidTTinterval = (double)sum / TTinterval.Count;
                }
            }
        }

        public class P_Segment
        {
            public double StartP { get; private set; } = -1.0;
            public double MiddleP { get; private set; } = -1.0;
            public double FinishP { get; private set; } = -1.0;
            public double LengthComplex { get; private set; } = -1.0;
            public double Amplitude { get; private set; } = -1.0;

            public bool ISFound { get; private set; }

            public P_Segment(double _StartP, double _MiddleP, double _FinishP,
                bool _isFound, double _Amplitude)
            {
                StartP = _StartP;
                MiddleP = _MiddleP;
                FinishP = _FinishP;
                LengthComplex = FinishP - StartP;
                ISFound = _isFound;
                Amplitude = Math.Abs(_Amplitude);
            }
        }
        public class QRS_Segment
        {
            public double Q_TimeStamp { get; private set; }
            public double R_TimeStamp { get; private set; }
            public double S_TimeStamp { get; private set; }


            public double QR_Length { get; private set; }
            public double RS_Length { get; private set; }
            public double QS_Length { get; private set; }
            public double QRS_Length { get; private set; }

            public bool ISFound { get; private set; }

            public QRS_Segment(double _Q_TimeStamp, double _R_TimeStamp, double _S_TimeStamp,
                 bool _isFound)
            {
                Q_TimeStamp = _Q_TimeStamp;
                R_TimeStamp = _R_TimeStamp;
                S_TimeStamp = _S_TimeStamp;

                QR_Length = R_TimeStamp - Q_TimeStamp;
                QS_Length = S_TimeStamp - Q_TimeStamp;
                RS_Length = S_TimeStamp - R_TimeStamp;
                QRS_Length = S_TimeStamp - Q_TimeStamp; //Не уверен в правильности определения комплекса
                ISFound = _isFound;
            }

        }
        public class T_Segment
        {
            public double StartT { get; private set; } = -1.0;
            public double MiddleT { get; private set; } = -1.0;
            public double FinishT { get; private set; } = -1.0;
            public double LengthComplex { get; private set; } = -1.0;
            public double Amplitude { get; private set; } = -1.0;

            public bool ISFound { get; private set; }

            public T_Segment(double _StartT, double _MiddleT, double _FinishT, bool _isFound, double _Amplitude)
            {
                StartT = _StartT;
                MiddleT = _MiddleT;
                FinishT = _FinishT;
                LengthComplex = FinishT - StartT;
                ISFound = _isFound;
                Amplitude = _Amplitude;
            }
        }


        public class SegmentationMap
        {
            List<string> SegmentatedMapList;

            public SegmentationMap(int[,] _ANN)
            {

            }
        }





    }
}
