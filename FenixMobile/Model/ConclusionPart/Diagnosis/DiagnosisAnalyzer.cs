﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.ConclusionPart.Diagnosis
{
    public enum DiagnosisRank { VeryGood = 0, Good = 1, Normal = 2, Warning = 3, Danger = 4}
    public abstract class DiagnosisAnalyzer
    {
        protected MagazineHandler MagazineHandler = null;
        /// <summary>
        /// Индекс для текущей записи
        /// </summary>
        protected int DiagnosisIndex = 0;
        /// <summary>
        /// Максимальное количество элементов в журнале
        /// </summary>
        protected readonly byte MaxSizeMagazine = 100;

        public event EventHandler<DiagnosisDiscription> DiagnosisUpdate;

        protected virtual void OnDiagnosisUpdate(DiagnosisDiscription e)
        {
            EventHandler<DiagnosisDiscription> handler = DiagnosisUpdate;
            if (handler != null) handler(this, e);
        }
    }
}
