﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.ConclusionPart.Diagnosis
{
    /// <summary>
    /// Класс агрегатор для конкретного вида диагнозов
    /// </summary>
    public class MagazineHandler
    {
        /// <summary>
        /// Порог добавления диагноза в проходной лист
        /// </summary>
        private readonly byte DiagnosisThreshold = 30;
        /// <summary>
        /// Количество добавленных диагнозов
        /// </summary>
        private uint DiagnosisCount = 0;

        /// <summary>
        /// Данный словарь использует в качестве ключа DiagnosisDiscription конкретного диагноза, так как он уникален. 
        /// В качестве значения диагноза будет количество встреченных диагнозов в ходе исследования
        /// </summary>
        private Dictionary<DiagnosisDiscription, uint> DiagnosisRating;

        public MagazineHandler()
        {
            this.DiagnosisRating = new Dictionary<DiagnosisDiscription, uint>();
            this.DiagnosisCount = 0;
        }

        /// <summary>
        /// Метод, возвращающий приорететный диагноз, согласно журналу наблюдаемых отклонений
        /// </summary>
        /// <param name="currentDiagnosis">Новый диагноз, установленный при текущем исследовании</param>
        /// <returns>Список наиболее часто встречаемых диагнозов</returns>
        public List<DiagnosisDiscription> GetPriorityDiagnos(DiagnosisDiscription currentDiagnosis)
        {
            this.DiagnosisCount++;

            if (this.DiagnosisRating.ContainsKey(currentDiagnosis))
                this.DiagnosisRating[currentDiagnosis]++;
            else
                this.DiagnosisRating.Add(currentDiagnosis, 0);

            List<DiagnosisDiscription> resultList = new List<DiagnosisDiscription>();
            //Плохая идея использовать DiagnosisDiscription в качестве ключа... переделать
            foreach (KeyValuePair<DiagnosisDiscription, uint> keyValue in this.DiagnosisRating) 
            {
                byte curDiagPercent = (byte)(100.0 * (double)keyValue.Value / this.DiagnosisCount);
                if (curDiagPercent >= this.DiagnosisThreshold)
                    resultList.Add(keyValue.Key);
            }
            if (resultList.Count == 0)
            {
                resultList.Add(GetPopularDiagnosis());
            }
            return resultList;
        }
        /// <summary>
        /// Получение наиболее часто встречающегося диагноза
        /// </summary>
        /// <returns>Наиболее встречаемый диагноз</returns>
        private DiagnosisDiscription GetPopularDiagnosis()
        {
            DiagnosisDiscription localDiagnosis = null; ;
            uint maxCases = uint.MinValue;
            foreach(KeyValuePair< DiagnosisDiscription, uint> keyValuePair in this.DiagnosisRating)
            {
                if (keyValuePair.Value >= maxCases)
                {
                    localDiagnosis = keyValuePair.Key;
                    maxCases = keyValuePair.Value;
                }
            }
            return localDiagnosis;
        }

    }
}
