﻿using System;
using System.Collections.Generic;
using System.Text;
using static FenixMobile.Model.ConclusionPart.ConclusionModel;

namespace FenixMobile.Model.ConclusionPart.Diagnosis
{
    public enum DeviationsDanger { NoDanger, Warning, Danger }
    public enum AgeGroup
    {
        OneYearOld, TwoYearOld, BeforeSixYear, BeforeEightYear,
        BeforeTenYear, BeforeTwelveYear, BeforeFifteenYear, BeforeFiftyYear, BeforeSixtyYear, AfterSixtyYear
    }
    public class PulseAnalyzer : DiagnosisAnalyzer
    {
        /// <summary>
        /// Возрастная группа пациента
        /// </summary>
        public AgeGroup AgeGroup { get; private set; }

        /// <summary>
        /// Максимальный возраст пациента
        /// </summary>
        private readonly byte MaxAge = 160;
        /// <summary>
        /// Средний пульс пациента
        /// </summary>
        public ushort? Pulse { get; private set; }
        /// <summary>
        /// Минимальный пульс пациента на данном участке
        /// </summary>
        public ushort? MinPulse { get; private set; }
        /// <summary>
        /// Максимальный пульс пациента на данном участке
        /// </summary>
        public ushort? MaxPulse { get; private set; }
        /// <summary>
        /// Рекомендуемый средний пульс пациента
        /// </summary>
        public ushort? RecommendedPulse { get; private set; }
        /// <summary>
        /// Рекомендуемый минимальный пульс пациента на данном участке
        /// </summary>
        public ushort? RecommendedMinPulse { get; private set; }
        /// <summary>
        /// Рекомендуемый максимальный пульс пациента на данном участке
        /// </summary>
        public ushort? RecommendedMaxPulse { get; private set; }


        /// <summary>
        /// Возраст пациента
        /// </summary>
        public byte? PatientAge { get; private set; }

        private readonly ushort MinPulseValue = 30;
        private readonly ushort MaxPulseValue = 500;

        public PulseAnalyzer(DateTime patientDateBorn)
        {
            this.MagazineHandler = new MagazineHandler();
            this.GetAge(patientDateBorn);
            this.AgeGroupDetermination();
            this.SetPulseStandart();
        }

        private void PulseCalculate(double midRR, double minRR, double maxRR)
        {
            if (midRR == 0 || maxRR == 0 || minRR == 0)
            {
                this.Pulse = null;
                this.MaxPulse = null;
                this.MinPulse = null;
            }
            this.Pulse = (ushort)(60.0 / midRR);
            this.MaxPulse = (ushort)(60.0 / minRR);
            this.MinPulse = (ushort)(60.0 / maxRR);

            if (this.Pulse > this.MaxPulseValue || this.Pulse < this.MinPulseValue)
                this.Pulse = null;
            if (this.MaxPulse > this.MaxPulseValue || this.MaxPulse < this.MinPulseValue)
                this.MaxPulse = null;
            if (this.MinPulse > this.MaxPulseValue || this.MinPulse < this.MinPulseValue)
                this.MinPulse = null;

            DiagnosisDiscription diagnosisDiscription = PulseDiagnosisDetermination();
            if (diagnosisDiscription != null)
                this.OnDiagnosisUpdate(diagnosisDiscription);
        }

        public void DiagnosticProcessing(QRS_SegmentCollection qrs_SegmentCol)
        {
            PulseCalculate(qrs_SegmentCol.MidRRinterval, qrs_SegmentCol.MinRRinterval, qrs_SegmentCol.MaxRRinterval);
        }

        private DiagnosisDiscription PulseDiagnosisDetermination()
        {
            if (this.Pulse == null || this.MinPulse == null || this.MaxPulse == null) return null;

            DiagnosisRank diagnosisRank;
            string diagnosDiscription;
            string diagnosisRecommendation;
            string smallDiagnosisDiscription;
            string diagnosisTitle;
            string specificDiagnosisDiscription;
            ushort minmaxDelta = (ushort)(this.RecommendedMaxPulse - this.RecommendedMinPulse);
            //Расчитываем ошибку определения пульса: 20%
            ushort errorPercent = (ushort)((this.RecommendedMaxPulse - this.RecommendedMinPulse) * 0.2);


            if (this.Pulse >= this.RecommendedMinPulse && this.Pulse <= this.RecommendedMaxPulse)
            {
                //Пульс попал в границу рекомендации
                diagnosisRank = DiagnosisRank.VeryGood;
                smallDiagnosisDiscription = "Нет отклонений";
                diagnosDiscription = "Отличные показатели пульса. " + Environment.NewLine;
                diagnosisTitle = this.Pulse.Value.ToString();
                specificDiagnosisDiscription = "Минимальный пульс: " + this.MinPulse.Value.ToString() + Environment.NewLine +
                    "Средний пульс: " + this.Pulse.Value.ToString() + Environment.NewLine +
                    "Максимальный пульс: " + this.MaxPulse.Value.ToString() + Environment.NewLine;
                diagnosisRecommendation = "";
            }
            else
            {
                //Если пульс привышает рекомендованный максимум
                if (this.Pulse > this.RecommendedMaxPulse) 
                {
                    if (this.Pulse < this.RecommendedPulse + minmaxDelta)
                    {
                        diagnosisRank = DiagnosisRank.Good;
                        smallDiagnosisDiscription = "Незначительное отклонение";
                        diagnosDiscription = "Пульс незначительно превышает норму. " + Environment.NewLine;
                        diagnosisTitle = this.Pulse.Value.ToString();
                        specificDiagnosisDiscription = "Минимальный пульс: " + this.MinPulse.Value.ToString() + Environment.NewLine +
                            "Средний пульс: " + this.Pulse.Value.ToString() + Environment.NewLine +
                            "Максимальный пульс: " + this.MaxPulse.Value.ToString() + Environment.NewLine;
                        diagnosisRecommendation = "При незначительном повышении пульса, без отсутствия физических нагрузок, " +
                            "рекомендуется размять шейный отдел позвоночника.";
                    }
                    else
                    {
                        diagnosisRank = DiagnosisRank.Danger;
                        smallDiagnosisDiscription = "Наблюдается отклонение";
                        diagnosDiscription = "Пульс превышает норму. " + Environment.NewLine;
                        diagnosisTitle = this.Pulse.Value.ToString();
                        specificDiagnosisDiscription = "Минимальный пульс: " + this.MinPulse.Value.ToString() + Environment.NewLine +
                            "Средний пульс: " + this.Pulse.Value.ToString() + Environment.NewLine +
                            "Максимальный пульс: " + this.MaxPulse.Value.ToString() + Environment.NewLine;
                        diagnosisRecommendation = "При систематическом повышении пульса рекомендуется обратить на это внимание. " +
                            "Возможно, организм перенапряжён и требует отдыха.";
                    }
                }
                else
                {
                    //Если пульс слишком низкий
                    if (this.Pulse > this.RecommendedPulse - minmaxDelta)
                    {
                        diagnosisRank = DiagnosisRank.Good;
                        smallDiagnosisDiscription = "Незначительное отклонение";
                        diagnosDiscription = "Пульс ниже нормы. " + Environment.NewLine;
                        diagnosisTitle = this.Pulse.Value.ToString();
                        specificDiagnosisDiscription = "Минимальный пульс: " + this.MinPulse.Value.ToString() + Environment.NewLine +
                            "Средний пульс: " + this.Pulse.Value.ToString() + Environment.NewLine +
                            "Максимальный пульс: " + this.MaxPulse.Value.ToString() + Environment.NewLine;
                        diagnosisRecommendation = "При незначительном понижении пульса рекомендуется ободряющая разминка. ";
                    }
                    else
                    {
                        diagnosisRank = DiagnosisRank.Danger;
                        smallDiagnosisDiscription = "Наблюдается отклонение";
                        diagnosDiscription = "Показатель пульса намного ниже нормы. " + Environment.NewLine;
                        diagnosisTitle = this.Pulse.Value.ToString();
                        specificDiagnosisDiscription = "Минимальный пульс: " + this.MinPulse.Value.ToString() + Environment.NewLine +
                            "Средний пульс: " + this.Pulse.Value.ToString() + Environment.NewLine +
                            "Максимальный пульс: " + this.MaxPulse.Value.ToString() + Environment.NewLine;
                        diagnosisRecommendation = "При систематическом понижении пульса рекомендуется обратить на это внимание. " +
                            "Возможно, организм слишком слаб.";
                    }
                }
            }

            return this.MagazineHandler.GetPriorityDiagnos(new DiagnosisDiscription(diagnosisRank, smallDiagnosisDiscription, 
                diagnosDiscription, diagnosisRecommendation, specificDiagnosisDiscription, diagnosisTitle))[0];
        }



        /// <summary>
        /// Определение возрастной группы пациента
        /// </summary>
        private void AgeGroupDetermination()
        {
            if (this.PatientAge == null) return;
            if (this.PatientAge <= 1) this.AgeGroup = AgeGroup.OneYearOld;
            if (this.PatientAge > 1 && this.PatientAge <= 2) this.AgeGroup = AgeGroup.TwoYearOld;
            if (this.PatientAge > 2 && this.PatientAge <= 6) this.AgeGroup = AgeGroup.BeforeSixYear;
            if (this.PatientAge > 6 && this.PatientAge <= 8) this.AgeGroup = AgeGroup.BeforeEightYear;
            if (this.PatientAge > 8 && this.PatientAge <= 10) this.AgeGroup = AgeGroup.BeforeTenYear;
            if (this.PatientAge > 10 && this.PatientAge <= 12) this.AgeGroup = AgeGroup.BeforeTwelveYear;
            if (this.PatientAge > 12 && this.PatientAge <= 15) this.AgeGroup = AgeGroup.BeforeFifteenYear;
            if (this.PatientAge > 15 && this.PatientAge <= 50) this.AgeGroup = AgeGroup.BeforeFiftyYear;
            if (this.PatientAge > 50 && this.PatientAge <= 60) this.AgeGroup = AgeGroup.BeforeSixtyYear;
            if (this.PatientAge > 60) this.AgeGroup = AgeGroup.AfterSixtyYear;
        }

        /// <summary>
        /// Установка рекомендованных значений пульса, в зависимости от возраста
        /// </summary>
        /// <param name="minPulse">Минимальное значение пульса</param>
        /// <param name="midPulse">Среднее значение пульса</param>
        /// <param name="maxPulse">Максимальное значение</param>
        private void SetPulseStandart(ushort minPulse, ushort midPulse, ushort maxPulse)
        {
            this.RecommendedMinPulse = minPulse;
            this.RecommendedPulse = midPulse;
            this.RecommendedMaxPulse = maxPulse;
        }

        /// <summary>
        /// Метод определения стандартов пульса в соответствии с возрастом
        /// </summary>
        private void SetPulseStandart()
        {
            if (this.PatientAge == null) return;
            if (this.AgeGroup == AgeGroup.OneYearOld) SetPulseStandart(102, 135, 170);
            if (this.AgeGroup == AgeGroup.TwoYearOld) SetPulseStandart(94, 124, 154);
            if (this.AgeGroup == AgeGroup.BeforeSixYear) SetPulseStandart(86, 106, 128);
            if (this.AgeGroup == AgeGroup.BeforeEightYear) SetPulseStandart(78, 98, 118);
            if (this.AgeGroup == AgeGroup.BeforeTenYear) SetPulseStandart(68, 88, 108);
            if (this.AgeGroup == AgeGroup.BeforeTwelveYear) SetPulseStandart(60, 80, 100);
            if (this.AgeGroup == AgeGroup.BeforeFifteenYear) SetPulseStandart(55, 75, 95);
            if (this.AgeGroup == AgeGroup.BeforeFiftyYear) SetPulseStandart(60, 70, 80);
            if (this.AgeGroup == AgeGroup.BeforeSixtyYear) SetPulseStandart(64, 74, 84);
            if (this.AgeGroup == AgeGroup.AfterSixtyYear) SetPulseStandart(69, 79, 89);
        }

        /// <summary>
        /// Определение возраста пациента
        /// </summary>
        /// <param name="bornDate">Дата рождения пациента</param>
        private void GetAge(DateTime bornDate)
        {
            byte age = (byte)(DateTime.Now.Year - bornDate.Year);
            if (DateTime.Now.Month < bornDate.Month ||
               (DateTime.Now.Month == bornDate.Month && DateTime.Now.Day < bornDate.Day)) 
                age--;
            if (age > MaxAge)
                this.PatientAge = null;
            else
                this.PatientAge = age;
        }
    }
}
