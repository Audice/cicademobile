﻿using FenixMobile.Enumeration;
using FenixMobile.Model.ConclusionPart.ConclusionEventArgs;
using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.ConclusionPart.HeartRateVariability
{
    public class AdaptationLevel
    {
        public uint PrevAdaptationLevel { get; private set; } = 0;
        /// <summary>
        /// Левая граница стресса организма в %
        /// </summary>
        public byte LeftBorderControl = 0;
        /// <summary>
        /// Правая граница стресса организма в %
        /// </summary>
        public byte RightBorderControl = 100;
        /// <summary>
        /// Описание смысла индекса напряжённости регуляторных систем
        /// </summary>
        private string AdaptationLevelDiscription = "Амплитуда моды (Амо) – это мера мобилизации организма, " +
            "она отражает мобилизующее влияние симпатического отдела вегетативной нервной системы (ВНС) и степень управления сердечным ритмом со стороны " +
            "Центральной нервной системы (ЦНС). Увеличение значения Амо указывает на снижение лабильности и увеличение ригидности систем регуляции организма. " +
            "То есть, чем выше значение Амо тем больше организм мобилизует сил. По попросту говоря, это показатель энергетического тонуса.";

        //Черезмерная расслабленность
        private readonly ushort TooRelaxedStateLeft = 0;
        private readonly ushort TooRelaxedStateRight = 15;
        private readonly string TooRelaxedStateDiscription = "Застой жизненной энергии. " +
            "Дизрегуляционная патология возникшая на фоне инертности систем мобилизации энергетики";

        //Переменные, описывающие хорошее состояние индекса
        private readonly ushort GoodStateLeft = 31;
        private readonly ushort GoodStateRight = 49;
        private readonly string GoodStateDiscription = "Норма. Организм достаточно мобилизован и находится в тонусе.";

        //Переменные, описывающие хорошее пограничное состояние индекса
        private readonly ushort NotVeryStateLeft = 16;
        private readonly ushort NotVeryStateRight = 30;
        private readonly string NotVeryStateDiscription = "Избыток жизненных сил. " +
            "Организм избыточно расслаблен и готов к плавному повышению нагрузки.";

        //Переменные, описывающие ухудшающееся состояние индекса
        private readonly ushort WarningStateLeft = 50;
        private readonly ushort WarningStateRight = 79;
        private readonly string WarningStateDiscription = "Сложная адаптация. " +
            "Наблюдается высокая цена адаптации, связанная с течением основного заболевания или напряжённого периода жизни. ";

        //Переменные, описывающие критическое состояние индекса
        private readonly ushort CriticalStateLeft = 80;
        private readonly ushort CriticalStateRight = 100;
        private readonly string CriticalStateDiscription = "Критическое низкий уровень адаптации. " +
            "Крайне высокая цена адаптации на фоне истощения энергетики, указывает на возможность кризиса.";


        public event EventHandler<IndexChangeOptionsEventArgs> AdaptationLevelChangeEvent;
        protected void OnAdaptationLevelChange(IndexChangeOptionsEventArgs e)
        {
            EventHandler<IndexChangeOptionsEventArgs> handler = AdaptationLevelChangeEvent;
            if (handler != null)
                handler(this, e);
        }


        public AdaptationLevel()
        {

        }

        public void UpdateAdaptationLevel(ushort AMo)
        {
            IndexChangeOptionsEventArgs args = new IndexChangeOptionsEventArgs();
            if (AMo >= TooRelaxedStateLeft && AMo <= TooRelaxedStateRight)
            {
                args.HeartRateVariabilityIndex = HeartRateVariabilityIndex.Danger;
                args.ValuePercent = CalculatePercent(TooRelaxedStateLeft, TooRelaxedStateRight, 35, 50, AMo);
            }
            if (AMo >= GoodStateLeft && AMo <= GoodStateRight)
            {
                args.HeartRateVariabilityIndex = HeartRateVariabilityIndex.Good;
                args.ValuePercent = CalculatePercent(GoodStateLeft, GoodStateRight, 5, 25, AMo);
            }
            if (AMo >= NotVeryStateLeft && AMo <= NotVeryStateRight)
            {
                args.HeartRateVariabilityIndex = HeartRateVariabilityIndex.NotVeryGood;
                args.ValuePercent = CalculatePercent(NotVeryStateLeft, NotVeryStateRight, 25, 35, AMo);
            }
            if (AMo >= WarningStateLeft && AMo <= WarningStateRight)
            {
                args.HeartRateVariabilityIndex = HeartRateVariabilityIndex.Warning;
                args.ValuePercent = CalculatePercent(WarningStateLeft, WarningStateRight, 50, 80, AMo);
            }
            if (AMo >= CriticalStateLeft)
            {
                args.HeartRateVariabilityIndex = HeartRateVariabilityIndex.Critical;
                args.ValuePercent = CalculatePercent(CriticalStateLeft, CriticalStateRight, 80, 100, AMo);
            }
            args.ValueChange = (short)(AMo - this.PrevAdaptationLevel);
            this.PrevAdaptationLevel = AMo;
            this.OnAdaptationLevelChange(args);
        }

        private byte CalculatePercent(double leftBorder, double rightBorder, byte leftBorderPercent, byte rightBorderPercent, double realValue)
        {
            double lengthInterval = rightBorder - leftBorder;
            byte percentLength = (byte)(rightBorderPercent - leftBorderPercent);
            double percentStep = ((double)percentLength) / lengthInterval;
            double reducedValue = realValue - leftBorder;
            byte resultPercent = (byte)((reducedValue * percentStep) + leftBorderPercent);
            if (resultPercent > 100 || resultPercent < 0)
                return 100;
            return resultPercent;
        }
    }
}
