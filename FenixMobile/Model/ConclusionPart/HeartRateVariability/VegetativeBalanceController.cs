﻿using FenixMobile.Enumeration;
using FenixMobile.Model.ConclusionPart.ConclusionEventArgs;
using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.ConclusionPart.HeartRateVariability
{
    public class VegetativeBalanceController
    {
        /// <summary>
        /// Предыдущее значения индекса вегетативного равновесия
        /// </summary>
        private uint PrevVegetativeBalanceIndex = 0;
        /// <summary>
        /// Левая граница стресса организма в %
        /// </summary>
        public byte LeftBorderControl = 0;
        /// <summary>
        /// Правая граница стресса организма в %
        /// </summary>
        public byte RightBorderControl = 100;
        /// <summary>
        /// Описание смысла индекса вегетативного баланса
        /// </summary>
        private string IndexDiscription = "Соотношение между активностью симпатического и парасимпатического отделов ВНС. " +
            "Значение ИВР увеличивается при мобилизации симпатического отдела ВНС и снижается при " +
            "усилении функции парасимпатического отела ВНС. ";

        //Черезмерная расслабленность
        private readonly ushort TooRelaxedStateLeft = 0;
        private readonly ushort TooRelaxedStateRight = 29;

        //Переменные, описывающие менее хорошее состояние индекса
        private readonly ushort NotVeryStateLeft = 30;
        private readonly ushort NotVeryStateRight = 100;

        //Переменные, описывающие хорошее пограничное состояние индекса
        private readonly ushort GoodStateLeft = 101;
        private readonly ushort GoodStateRight = 350;

        //Переменные, описывающие ухудшающееся состояние индекса
        private readonly ushort WarningStateLeft = 351;
        private readonly ushort WarningStateRight = 1300;

        //Переменные, описывающие критическое состояние индекса
        private readonly ushort CriticalStateLeft = 1301;


        public event EventHandler<IndexChangeOptionsEventArgs> VegetativeBalanceIndexChangeEvent;
        protected void OnVegetativeBalanceIndexChange(IndexChangeOptionsEventArgs e)
        {
            EventHandler<IndexChangeOptionsEventArgs> handler = VegetativeBalanceIndexChangeEvent;
            if (handler != null)
                handler(this, e);
        }

        public VegetativeBalanceController()
        {

        }

        public void UpdateVegetativeBalanceIndex(ushort AMo, double DX)
        {
            if (DX == 0) return;

            uint curVegetativeBalanceIndexValue = (uint)((double)AMo / DX);


            IndexChangeOptionsEventArgs args = new IndexChangeOptionsEventArgs();
            if (curVegetativeBalanceIndexValue >= TooRelaxedStateLeft && curVegetativeBalanceIndexValue <= TooRelaxedStateRight)
            {
                args.HeartRateVariabilityIndex = HeartRateVariabilityIndex.Danger;
                args.ValuePercent = CalculatePercent(TooRelaxedStateLeft, TooRelaxedStateRight, 40, 60, curVegetativeBalanceIndexValue);
            }
            if (curVegetativeBalanceIndexValue >= GoodStateLeft && curVegetativeBalanceIndexValue <= GoodStateRight)
            {
                args.HeartRateVariabilityIndex = HeartRateVariabilityIndex.Good;
                args.ValuePercent = CalculatePercent(GoodStateLeft, GoodStateRight, 5, 25, curVegetativeBalanceIndexValue);
            }
            if (curVegetativeBalanceIndexValue >= NotVeryStateLeft && curVegetativeBalanceIndexValue <= NotVeryStateRight)
            {
                args.HeartRateVariabilityIndex = HeartRateVariabilityIndex.NotVeryGood;
                args.ValuePercent = CalculatePercent(NotVeryStateLeft, NotVeryStateRight, 25, 40, curVegetativeBalanceIndexValue);
            }
            if (curVegetativeBalanceIndexValue >= WarningStateLeft && curVegetativeBalanceIndexValue <= WarningStateRight)
            {
                args.HeartRateVariabilityIndex = HeartRateVariabilityIndex.Warning;
                args.ValuePercent = CalculatePercent(WarningStateLeft, WarningStateRight, 60, 90, curVegetativeBalanceIndexValue);
            }
            if (curVegetativeBalanceIndexValue >= CriticalStateLeft)
            {
                args.HeartRateVariabilityIndex = HeartRateVariabilityIndex.Critical;
                args.ValuePercent = this.RightBorderControl;
            }
            args.ValueChange = (short)(curVegetativeBalanceIndexValue - this.PrevVegetativeBalanceIndex);
            this.PrevVegetativeBalanceIndex = curVegetativeBalanceIndexValue;
            this.OnVegetativeBalanceIndexChange(args);
        }

        private byte CalculatePercent(double leftBorder, double rightBorder, byte leftBorderPercent, byte rightBorderPercent, double realValue)
        {
            double lengthInterval = rightBorder - leftBorder;
            byte percentLength = (byte)(rightBorderPercent - leftBorderPercent);
            double percentStep = ((double)percentLength) / lengthInterval;
            double reducedValue = realValue - leftBorder;
            byte resultPercent = (byte)((reducedValue * percentStep) + leftBorderPercent);
            if (resultPercent > 100 || resultPercent < 0)
                return 100;
            return resultPercent;
        }

    }
}
