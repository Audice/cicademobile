﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FenixMobile.Model.ConclusionPart.SegmentationPart.Filters
{
    public interface IFilter
    {
        double[] Load_TH_Filter(ref int thL, ref int thZ);
        double[] Load_TG_Filter(ref int tgL, ref int tgZ);
        double[] Load_H_Filter(ref int hL, ref int hZ);
        double[] Load_G_Filter(ref int gL, ref int gZ);
    }
}
