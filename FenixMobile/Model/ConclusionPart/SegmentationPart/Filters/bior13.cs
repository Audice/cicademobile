﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FenixMobile.Model.ConclusionPart.SegmentationPart.Filters
{
    class bior13:IFilter
    {
        public static string Title = "bior13";

        public double[] Load_TH_Filter(ref int thL, ref int thZ)
        {
            double[] TH_Filter = new double[] { 
                -0.0625,
                0.0625,
                0.5,
                0.5,
                0.0625,
                -0.0625
            };
            thL = 6;
            thZ = 3;
            return TH_Filter;
        }

        public double[] Load_TG_Filter(ref int tgL, ref int tgZ)
        {
            double[] TG_Filter = new double[] { 
                0,
                0,
                -0.5,
                0.5
            };
            tgL = 4;
            tgZ = 2;
            return TG_Filter;
        }

        public double[] Load_H_Filter(ref int hL, ref int hZ)
        {
            double[] H_Filter = new double[] { 
                0.5,
                0.5
            };
            hL = 2;
            hZ = 0;
            return H_Filter;
        }

        public double[] Load_G_Filter(ref int gL, ref int gZ)
        {
            double[] G_Filter = new double[] { 
                -0.0625,
                -0.0625,
                0.5,
                -0.5,
                0.0625,
                0.0625
            };
            gL = 6;
            gZ = 2;
            return G_Filter;
        }
    }
}
