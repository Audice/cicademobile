﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FenixMobile.Model.ConclusionPart.SegmentationPart.Filters
{
    class daub2:IFilter
    {
        public static string Title = "daub2";

        public double[] Load_TH_Filter(ref int thL, ref int thZ)
        {
            double[] TH_Filter = new double[] { 
                0.3415063509462205,
                0.59150635094586690000,
                0.15849364905377947000,
                -0.09150635094586698200
            };
            thL = 4;
            thZ = 1;
            return TH_Filter;
        }

        public double[] Load_TG_Filter(ref int tgL, ref int tgZ)
        {
            double[] TG_Filter = new double[] { 
                -0.09150635094586698200,
                -0.15849364905377947000,
                0.59150635094586690000,
                -0.34150635094622050000
            };
            tgL = 4;
            tgZ = 1;
            return TG_Filter;
        }

        public double[] Load_H_Filter(ref int hL, ref int hZ)
        {
            double[] H_Filter = new double[] { 
                0.34150635094622050000,
                0.59150635094586690000,
                0.15849364905377947000,
                -0.09150635094586698200
            };
            hL = 4;
            hZ = 1;
            return H_Filter;
        }

        public double[] Load_G_Filter(ref int gL, ref int gZ)
        {
            double[] G_Filter = new double[] { 
                -0.09150635094586698200,
                -0.15849364905377947000,
                0.59150635094586690000,
                -0.34150635094622050000
            };
            gL = 4;
            gZ = 1;
            return G_Filter;
        }
    }
}
