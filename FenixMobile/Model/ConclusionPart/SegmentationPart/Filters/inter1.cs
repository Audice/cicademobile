﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FenixMobile.Model.ConclusionPart.SegmentationPart.Filters
{
    class inter1:IFilter
    {
        public static string Title = "inter1";

        public double[] Load_TH_Filter(ref int thL, ref int thZ)
        {
            double[] TH_Filter = new double[] { 
                1.0 
            };
            thL = 1;
            thZ = 0;
            return TH_Filter;
        }

        public double[] Load_TG_Filter(ref int tgL, ref int tgZ)
        {
            double[] TG_Filter = new double[] { 
                0,
                0,
                0.25,
                -0.5,
                0.25
            };
            tgL = 5;
            tgZ = 3;
            return TG_Filter;
        }

        public double[] Load_H_Filter(ref int hL, ref int hZ)
        {
            double[] H_Filter = new double[] { 
                0.25,
                0.5,
                0.25
            };
            hL = 3;
            hZ = 1;
            return H_Filter;
        }

        public double[] Load_G_Filter(ref int gL, ref int gZ)
        {
            double[] H_Filter = new double[] { 
                1.0
            };
            gL = 1;
            gZ = 0;
            return H_Filter;
        }
    }
}
