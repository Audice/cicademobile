﻿using FenixMobile.Interfaces;
using FenixMobile.Model.BluetoothPart;
using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.DatabasePart
{
    /// <summary>
    /// Класс предназначен для обеспечения работы со старыми, не закрытыми файлами
    /// </summary>
    public class ControlOldRecords
    {
        public RecordTable UnfinishedRecord { get; private set; } = null;
        UserTable User;
        DatabaseWorker DatabaseWorker;

        public bool IsRecordBusy { get; set; } = false;

        public ControlOldRecords()
        {
            this.DatabaseWorker = DatabaseWorker.getInstance();
            this.User = this.DatabaseWorker.CurrentUser;
        }

        /// <summary>
        /// Поиск всех незавершённых записей у данного пациента
        /// </summary>
        /// <param name="patient"></param>
        /// <returns>Первую незавершённую запись</returns>
        public RecordTable SearchUnfinishedRecord(PatientTable patient)
        {
            if (this.User != null && patient != null)
            {
                List<RecordTable> patientsRecordsList = this.DatabaseWorker.GetRecordsByPatientID((int)patient.PatientID);
                if (patientsRecordsList != null && patientsRecordsList.Count > 0)
                {
                    RecordTable unfinishedRecord = null;
                    for (int i=0; i < patientsRecordsList.Count; i++)
                    {
                        if (!patientsRecordsList[i].IsFileEnd)
                        {
                            unfinishedRecord = patientsRecordsList[i];
                            this.UnfinishedRecord = unfinishedRecord;
                            i = patientsRecordsList.Count;
                        }
                    }
                    return unfinishedRecord;
                }
                else
                    return null;
            }
            return null;
        }

        public List<RecordTable> SearchUnfinishedRecords(PatientTable patient)
        {
            if (this.User != null && patient != null)
            {
                List<RecordTable> patientsRecordsList = this.DatabaseWorker.GetRecordsByPatientID((int)patient.PatientID);
                if (patientsRecordsList != null && patientsRecordsList.Count > 0)
                {
                    return patientsRecordsList.FindAll( x => !x.IsFileEnd);
                }
                else
                    return null;
            }
            return null;
        }

        /// <summary>
        /// Поиск всех записей
        /// </summary>
        /// <param name="dirInfo"></param>
        /// <param name="patient"></param>
        /// <returns></returns>
        public List<RecordTable> GetListValidUnfinishedRecords(FireFlyDirInfo dirInfo, PatientTable patient)
        {
            if (dirInfo != null && patient != null)
            {
                List<RecordTable> patientsRecordsList = this.DatabaseWorker.GetRecordsByPatientID((int)patient.ID);
                if (patientsRecordsList != null && patientsRecordsList.Count > 0)
                {
                    //Поиск совпадающих номеров каталогов у пациента и светлячка
                    return patientsRecordsList.FindAll(x => (x.CatalogNumber <= dirInfo.CurrentDirectory && !x.IsFileEnd && x.FileType == (int)(ECGreceiveMode.HolterECG)));
                }
            }
            return null;
        }

        /// <summary>
        /// Совпадает ли текущий каталог светлячка с одним из номеров каталогов записей пациента
        /// </summary>
        /// <param name="dirInfo">Информация о текущем каталоге светлячка</param>
        /// <param name="patient">Текущий пациент</param>
        /// <returns>Запись, которую необходимо продолжить</returns>
        public RecordTable CheckCatalogNumbers(FireFlyDirInfo dirInfo, PatientTable patient)
        {
            if (dirInfo != null && patient != null)
            {
                List<RecordTable> patientsRecordsList = this.DatabaseWorker.GetRecordsByPatientID((int)patient.ID);
                if (patientsRecordsList != null && patientsRecordsList.Count > 0)
                {
                    //Поиск совпадающих номеров каталогов у пациента и светлячка
                    RecordTable recordTable = patientsRecordsList.Find(x => x.CatalogNumber == dirInfo.CurrentDirectory && x.FileType == (int)(ECGreceiveMode.HolterECG));
                    if (recordTable != null && !recordTable.IsFileEnd)
                    {
                        this.UnfinishedRecord = recordTable;
                        return recordTable;
                    }
                }
            }
            return null;
        }
    }
}
