﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Xamarin.Forms;

namespace FenixMobile.Model.DatabasePart
{
    public class DatabaseWorker
    {
        public static DatabaseWorker Instance;
        private const string DatabaseName = "UserInformation.db";

        private PatientTable currentPatient = null;
        public PatientTable CurrentPatient
        {
            get { return currentPatient; }
            private set
            {
                currentPatient = value;
                CurrentPatientChange?.Invoke(this, EventArgs.Empty);
            }
        }
        public EventHandler<EventArgs> CurrentPatientChange;


        private bool hasRecordsListChange = false;
        public bool HasRecordsListChange
        {
            get { return hasRecordsListChange; }
            set
            {
                if (value)
                {
                    RecordListChange?.Invoke(this, EventArgs.Empty);
                }
            }
        }
        public EventHandler<EventArgs> RecordListChange;

        public UserTable CurrentUser
        {
            get; private set;
        } = null;

        /// <summary>
        /// Событие, для оповещения изменений базы данных пациентов
        /// </summary>
        public EventHandler<EventArgs> CurrentUserPatientsCountChange;
        public void OnPatientsDatabaseChange()
        {
            EventHandler<EventArgs> handler = CurrentUserPatientsCountChange;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }


        private UserTableWorker UsersWorker;
        private PatientsTableWorker PatientsWorker;
        private RecordTableWorker RecordsWorker;

        protected DatabaseWorker()
        {
            //Загружаем базы данных из памяти устройства. Создаём их, в случае отсутствия
            UsersWorker = new UserTableWorker(DatabaseName);
            PatientsWorker = new PatientsTableWorker(DatabaseName);
            RecordsWorker = new RecordTableWorker(DatabaseName);
        }

        //Обслуживание UserDatabase
        #region
        /// <summary>
        /// Добавление нового пользователя
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <param name="password">Пароль пользователя</param>
        /// <param name="sessionID">Токен сессии</param>
        /// <returns></returns>
        public bool AddNewUser(string login, string password, string sessionID = "")
        {
            UserTable newUser = new UserTable();
            newUser.Login = login;
            newUser.Password = password;
            newUser.SessionID = sessionID;
            newUser.MACS = "";
            return this.UsersWorker.AddUser(newUser) >= 0 ? true : false;
        }
        /// <summary>
        /// Обновление токена сессии у user
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <param name="sessionID">Новый токен сессии</param>
        /// <returns></returns>
        public bool UpdateSessionID(string login, string sessionID)
        {
            UserTable currentUser = this.UsersWorker.GetUserByLogin(login);
            if (currentUser != null)
            {
                currentUser.SessionID = sessionID;
                return this.UsersWorker.UpdateUserInformation(currentUser) >= 0 ? true : false;
            }
            return false;
        }
        public bool ClearSessionIDByUserID(int userID)
        {
            UserTable currentUser = this.UsersWorker.GetUserByID(userID);
            if (currentUser != null)
            {
                currentUser.SessionID = null;
                return this.UsersWorker.UpdateUserInformation(currentUser) >= 0 ? true : false;
            }
            return false;
        }
        /// <summary>
        /// Проверка пользователя на наличие в базе данных
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns></returns>
        public bool CheckUser(string login)
        {
            UserTable currentUser = this.UsersWorker.GetUserByLogin(login);
            return currentUser != null ? true : false;
        }
        public bool CheckSessionByLogin(string login)
        {
            UserTable currentUser = this.UsersWorker.GetUserByLogin(login);
            if (currentUser != null)
            {
                return currentUser.SessionID != null && currentUser.SessionID.Length > 3 ? true : false;
            }
            return false;
        }

        public bool SetCurrentUser(string login)
        {
            UserTable currentUser = this.UsersWorker.GetUserByLogin(login);
            if (currentUser != null)
            {
                this.CurrentUser = currentUser;
                //Установка текущего пациента
                var PatientsList = GetListPatientByUserID(currentUser.ID);
                if (PatientsList != null && PatientsList.Count > 0)
                {
                    this.SetCurrentPatient(PatientsList[0]);
                }
                return this.CurrentUser != null ? true : false;
            }
            return false;
        }

        public string GetMACDeviceCurrentUser()
        {
            if (this.CurrentUser != null && this.CurrentUser.MACS != null && this.CurrentUser.MACS.Length == 17)
            {
                return this.CurrentUser.MACS;
            }
            else
            {
                return null;
            }
        }

        public void SetDevicesMACCurrentUser(string newMAC)
        {
            if (this.CurrentUser != null)
            {
                if (newMAC != null && newMAC.Length == 17)
                {
                    var updatableUser = this.CurrentUser;
                    updatableUser.MACS = newMAC;
                    this.UsersWorker.UpdateUserInformation(updatableUser);
                }
            }
        }

        #endregion

        //Обслуживание PatientDatabase
        #region

        /// <summary>
        /// Получение списка пациентов по UserID
        /// </summary>
        /// <param name="userID">Идентификатор пользователя</param>
        /// <returns>Список пациентов, зарегистрированных на этот идентификатор</returns>
        public List<PatientTable> GetListPatientByUserID(int userID)
        {
            if (this.PatientsWorker != null)
            {
                var patients = this.PatientsWorker.GetPatientsByUserID(userID);
                return patients != null && patients.Count > 0 ? patients : null;
            }
            else
                return null;
        }

        /// <summary>
        /// Добавление нового пациента для указанного .pthf
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="patientID"></param>
        /// <param name="name"></param>
        /// <param name="surname"></param>
        /// <param name="patronymic"></param>
        /// <param name="bornDate"></param>
        /// <param name="gender"></param>
        /// <param name="growth"></param>
        /// <param name="weight"></param>
        /// <returns>Успешность добавления: true - успешно, false - нет</returns>
        public bool AddnewPatientByUserID(int userID, long patientID, string name, string surname, 
            string patronymic, DateTime bornDate, int gender, int growth, int weight)
        {
            if (this.UsersWorker == null) return false;
            if (this.UsersWorker.GetUserByID(userID) == null) return false;
            PatientTable newPatient = new PatientTable()
            {
                UserID = userID,
                PatientID = patientID,
                Name = name,
                Surname = surname,
                Patronymic = patronymic,
                BornDate = bornDate,
                Gender = gender,
                Growth = growth,
                Weight = weight
            };
            if (PatientsWorker.AddPatient(newPatient) >= 0)
            {
                PatientsWorker.CommitChange();
                return true;
            } 
                

            return false;
        }

        public void SetCurrentPatient(PatientTable newCurrentPatient)
        {
            if (newCurrentPatient != null) this.CurrentPatient = newCurrentPatient;
        }

        public void UpdatePatientsGrowth(int newGrowth)
        {
            if (this.CurrentPatient != null && newGrowth > 0 && newGrowth < 400)
            {
                this.CurrentPatient.Growth = newGrowth;
                this.PatientsWorker.UpdatePatientInformation(this.CurrentPatient);
            }
        }

        public void UpdatePatientsWeight(int newWeight)
        {
            if (this.CurrentPatient != null && newWeight > 0 && newWeight < 500)
            {
                this.CurrentPatient.Weight = newWeight;
                this.PatientsWorker.UpdatePatientInformation(this.CurrentPatient);
            }
        }

        #endregion


        //Работа с записями
        #region
        public void UpdateRecord(RecordTable recordTable)
        {
            if (this.RecordsWorker == null) return;
            if (recordTable.FileName == null) return;
            if (recordTable.FileName.Length == 0) return;
            if (!this.RecordsWorker.IsDatabaseConteinRecord(recordTable.FileName))
                this.RecordsWorker.AddRecord(recordTable);
            else
                this.RecordsWorker.UpdateRecord(recordTable);

            this.RecordsWorker.CommitChange();

        }

        public bool IsRecordCreate(string filename)
        {
            if (this.RecordsWorker == null) return false;
            return (this.RecordsWorker.IsDatabaseConteinRecord(filename));
        }

        /// <summary>
        /// Добавление новой записи в базу данных
        /// </summary>
        /// <param name="recordTable">Новая запись</param>
        /// <returns>Код успешности добавления: >= 0 - успешно, иначе нет</returns>
        public int CreateRecord(RecordTable recordTable)
        {
            if (this.RecordsWorker == null) return -1;
            int resultsRow = this.RecordsWorker.AddRecord(recordTable);
            if (resultsRow >= 0) this.RecordsWorker.CommitChange();
            return resultsRow; //>= 0 - запись успешна
        }

        public List<RecordTable> GetRecordsByPatientID(int patientID)
        {
            if (this.RecordsWorker == null) return null;
            if (patientID < 0) return null;
            return this.RecordsWorker.GetFilesByPatientID(patientID);
        }

        public List<RecordTable> GetUnfinishedRecord(int patientID)
        {
            if (this.RecordsWorker == null) return null;
            if (patientID < 0) return null;
            return this.RecordsWorker.GetUnfinishedRecords(patientID);
        }

        public void DeleteRecordByID(int recordID)
        {
            if (this.RecordsWorker == null) return;
            if (recordID < 0) return;
            this.RecordsWorker.DeleteRecordByID(recordID);
        }
        /// <summary>
        /// Получение записи по идентификатору
        /// </summary>
        /// <param name="recordID">Идентификатор записи</param>
        /// <returns>Запись о файле. NULL если запись отсутствует.</returns>
        public RecordTable GetRecordByID(int recordID)
        {
            return this.RecordsWorker.GetRecord(recordID);
        }

        public bool UpdateRecordsFileID(int recordID, long fileID)
        {
            return this.RecordsWorker.UpdateRecordsFileID(recordID, fileID);
        }

        public bool UpdateRecordsServerPart(int recordID, int part)
        {
            return this.RecordsWorker.UpdateRecordsLastServerPart(recordID, part);
        }

        public bool UpdateRecordsFileNum(int recordID, int fileNum)
        {
            return this.RecordsWorker.UpdateRecordsFileNum(recordID, fileNum);
        }
        public bool UpdateRecordsCloseFlag(int recordID, bool isClose)
        {
            return this.RecordsWorker.UpdateRecordsCloseFlag(recordID, isClose);
        }
        public bool UpdateRecordsLastFileNum(int recordID, short lastFileNum)
        {
            return this.RecordsWorker.UpdateRecordsLastFileNum(recordID, lastFileNum);
        }
        #endregion



        public string t()
        {
            return UsersWorker.ToString();
        }

        public static DatabaseWorker getInstance()
        {
            if (Instance == null)
                Instance = new DatabaseWorker();
            return Instance;
        }
    }
}
