﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.DatabasePart
{
    [Table("Patients")]
    public class PatientTable
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int ID { get; set; }
        public int UserID { get; set; }
        public long PatientID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public DateTime BornDate { get; set; }
        public int Gender { get; set; }
        public int Growth { get; set; }
        public int Weight { get; set; }
    }
}
