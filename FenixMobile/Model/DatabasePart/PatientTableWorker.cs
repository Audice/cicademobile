﻿using FenixMobile.Interfaces;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace FenixMobile.Model.DatabasePart
{
    public class PatientsTableWorker
    {
        SQLiteConnection database;
        string DatabasePath;

        public PatientsTableWorker(string dbName)
        {
            DatabasePath = DependencyService.Get<ISQLite>().GetDatabasePath(dbName);
            database = new SQLiteConnection(DatabasePath);

            database.CreateTable<PatientTable>();
        }

        public List<PatientTable> GetPatients()
        {
            return (from i in database.Table<PatientTable>() select i).ToList();
        }

        public List<PatientTable> GetPatientsByUserID(long userID)
        {
            return (from i in database.Table<PatientTable>() select i).ToList().FindAll(x => x.UserID == userID);
        }
        //TODO
        public PatientTable GetPatient(int id)
        {
            PatientTable currentPatient = null;
            if (database.Table<PatientTable>().Count() > 0)
            {
                try
                {
                    currentPatient = database.Get<PatientTable>(id);
                }
                catch (Exception ex)
                {
                    string exceptionMessage = ex.Message;
                }
            }
            return currentPatient;
        }
        //Идёт совместно с удаление всех данных
        //TODO
        public int DeletePatient(int id)
        {
            int deleteResult = -1;
            if (database.Table<PatientTable>().Count() > 0)
            {
                try
                {
                    deleteResult = database.Delete<PatientTable>(id);
                }
                catch (Exception ex)
                {
                    string exceptionMessage = ex.Message;
                    deleteResult = -1;
                }
            }
            else
            {
                deleteResult = -1;
            }
            return deleteResult;
        }

        //Обновление пригодится в случае смены пароля
        public int UpdatePatientInformation(PatientTable newPatientInfo)
        {
            //Защита от изменений UserID, Login, ID
            if (newPatientInfo.ID >= 0)
            {
                PatientTable localPatientById;
                try
                {
                    localPatientById = database.Get<PatientTable>(newPatientInfo.ID);
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                    return -3; //Запись отсутствует в базе данных
                }
                if (localPatientById.ID == newPatientInfo.ID && localPatientById.Name == newPatientInfo.Name
                    && localPatientById.Surname == newPatientInfo.Surname && localPatientById.Patronymic == newPatientInfo.Patronymic
                    && localPatientById.UserID == newPatientInfo.UserID && localPatientById.PatientID == newPatientInfo.PatientID && localPatientById.BornDate == newPatientInfo.BornDate)
                {
                    return database.Update(newPatientInfo);
                }
                else
                {
                    return -1; //одно из полей нельзя менять
                }
            }
            return -1;
        }

        public int AddPatient(PatientTable newPatient)
        {
            if (database == null)
            {
                database = new SQLiteConnection(DatabasePath);
                database.CreateTable<PatientTable>();
            }
            var patientsList = this.GetPatients();
            if (patientsList == null) return -2;
            if (patientsList.Count > 0)
            {
                if (patientsList.Find(x => x.PatientID == newPatient.PatientID) == null)
                {
                    return database.Insert(newPatient);
                }
                else
                {
                    return -1; //Ошибка добавления пользователя
                }
            }
            else
            {
                return database.Insert(newPatient);
            }
        }

        public void CommitChange()
        {
            if (database == null)
            {
                database = new SQLiteConnection(DatabasePath);
                database.CreateTable<PatientTable>();
            }
            database.Commit();
        }
    }
}
