﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace FenixMobile.Model.DatabasePart
{
    [Table("Records")]
    public class RecordTable
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int ID { get; set; }
        public int UserID { get; set; }
        public int PatientID { get; set; } = -1; //Идентификатор пациента по таблице
        /// <summary>
        /// Имя файла имеет вид: Фамилия И. О. dd-MM-yyyy hh-mm-ss
        /// </summary>
        public string FileName { get; set; } //Имя файла в хранилище. Должно быть уникальным
        public int ChannelsCount { get; set; } //Количество каналов, запись по очереди 1,2,1,2 или 1,2,3,1,2,3,
        public long FileID { get; set; } = -1; //Идентификатор файла на сервере
        public int CatalogNumber { get; set; } //Номер каталога
        public int FileType { get; set; } //10, 300, 1000 
        public int SampleFrequency { get; set; }
        public bool IsFileEnd { get; set; }
        public DateTime StartRecord { get; set; }
        public DateTime FinishRecord { get; set; }
        /// <summary>
        /// Поле необходимо для работы с флэш памятью кардиографа
        /// </summary>
        public int LastBluetoothFileNumber { get; set; } = -1;
        //Поле необходимо для контроля отправленных на сервер данных
        public int LastServerPackageNumber { get; set; } = -1;
        /// <summary>
        /// Номер последней части отправленной на сервер
        /// </summary>
        public int LastServerPartNumber { get; set; } = -1;
        /// <summary>
        /// MAC устройства необходим для восстановления обмена данными, при закрытии мобильного приложения 
        /// </summary>
        public string MAC { get; set; }
        /// <summary>
        /// Переменная, хронящая стартовый индекс серверного файла: -1 файл закрыт, удалён или завершён
        /// </summary>
        public long StartIndexServerFile { get; set; } = 0;
        /// <summary>
        /// Поле, характеризующее номер последнего файла, готового для чтения.
        /// Если поле равно -1, значит считать всё до конца
        /// </summary>
        public short LastFileNum { get; set; } = -1;

    }
}
