﻿using FenixMobile.Interfaces;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace FenixMobile.Model.DatabasePart
{
    public class RecordTableWorker
    {
        SQLiteConnection database;
        string DatabasePath;

        public RecordTableWorker(string dbName)
        {
            DatabasePath = DependencyService.Get<ISQLite>().GetDatabasePath(dbName);
            database = new SQLiteConnection(DatabasePath);

            database.CreateTable<RecordTable>();
        }

        /// <summary>
        /// Debug фича
        /// </summary>
        /// <returns></returns>
        public List<RecordTable> GetRecords()
        {
            return (from i in database.Table<RecordTable>() select i).ToList();
        }

        public bool IsDatabaseConteinRecord(string filename)
        {
            if (database == null)
            {
                database = new SQLiteConnection(DatabasePath);
                database.CreateTable<RecordTable>();
                return false;
            }
            var recordsList = this.GetRecords();
            if (recordsList == null) return false; if (filename == null) return false;
            if (filename.Length == 0) return false;

            if (recordsList.Count > 0)
            {
                if (recordsList.Find(x => x.FileName == filename) != null)
                    return true;
            }
            return false;
        }

        public int AddRecord(RecordTable newRecord)
        {
            if (database == null)
            {
                database = new SQLiteConnection(DatabasePath);
                database.CreateTable<RecordTable>();
            }

            var recordsList = this.GetRecords();
            if (recordsList == null) return -2; if (newRecord.FileName == null) return -3;
            if (newRecord.FileName.Length == 0) return -4;
            if (recordsList.Count > 0)
            {
                if (recordsList.Find(x => x.FileName == newRecord.FileName) == null)
                {
                    return database.Insert(newRecord);
                }
                else
                {
                    return -1; //Ошибка добавления пользователя
                }
            }
            else
            {
                return database.Insert(newRecord);
            }
        }

        public int UpdateRecord(RecordTable record)
        {
            //Защита от изменений UserID, Login, ID
            if (record.ID >= 0 && record.FileName != null && record.FileName.Length > 0)
            {
                RecordTable localRecord;
                try
                {
                    localRecord = database.Get<RecordTable>(record.ID);
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                    return -3; //Запись отсутствует в базе данных
                }
                if (localRecord.FileName == record.FileName && localRecord.StartRecord == record.StartRecord)
                {
                    return database.Update(record);
                }
                else
                {
                    return -1; //одно из полей нельзя менять
                }
            }
            return -1;
        }

        public bool UpdateRecordsFileID(int recordID, long fileID)
        {
            RecordTable localRecord;
            try
            {
                localRecord = database.Get<RecordTable>(recordID);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return false;
            }
            localRecord.FileID = fileID;
            if (database.Update(localRecord) >= 0)
                return true;
            return false;
        }

        public bool UpdateRecordsFileNum(int recordID, int fileNum)
        {
            RecordTable localRecord;
            try
            {
                localRecord = database.Get<RecordTable>(recordID);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return false;
            }
            localRecord.LastBluetoothFileNumber = fileNum;
            if (database.Update(localRecord) >= 0)
                return true;
            return false;
        }


        public bool UpdateRecordsLastServerPart(int recordID, int part)
        {
            RecordTable localRecord;
            try
            {
                localRecord = database.Get<RecordTable>(recordID);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return false;
            }
            localRecord.LastServerPartNumber = part;
            if (database.Update(localRecord) >= 0)
                return true;
            return false;
        }


        public bool UpdateRecordsCloseFlag(int recordID, bool isClose)
        {
            RecordTable localRecord;
            try
            {
                localRecord = database.Get<RecordTable>(recordID);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return false;
            }
            localRecord.IsFileEnd = isClose;
            if (database.Update(localRecord) >= 0)
                return true;
            return false;
        }

        public bool UpdateRecordsLastFileNum(int recordID, short lastFileNum)
        {
            RecordTable localRecord;
            try
            {
                localRecord = database.Get<RecordTable>(recordID);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return false;
            }
            localRecord.LastFileNum = lastFileNum;
            if (database.Update(localRecord) >= 0)
                return true;
            return false;
        }

        public List<RecordTable> GetFilesByPatientID(int patientID)
        {
            if (database == null)
            {
                database = new SQLiteConnection(DatabasePath);
                database.CreateTable<RecordTable>();
                return null;
            }
            return this.GetRecords().FindAll(x => x.PatientID == patientID);
        }

        public List<RecordTable> GetUnfinishedRecords(int patientID)
        {
            if (database == null)
            {
                database = new SQLiteConnection(DatabasePath);
                database.CreateTable<RecordTable>();
                return null;
            }
            return this.GetRecords().FindAll(x => x.PatientID == patientID && x.IsFileEnd == false) ;
        }

        public void DeleteRecordByID(int id)
        {
            if (id >= 0)
            {
                RecordTable localRecord;
                try
                {
                    localRecord = database.Get<RecordTable>(id);
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                    return;
                }
                if (localRecord != null)
                {
                    database.Delete(localRecord);
                }
            }
        }

        public RecordTable GetRecord(int recordID)
        {
            RecordTable localRecord = null;
            try
            {
                localRecord = database.Get<RecordTable>(recordID);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return localRecord;
        }



        public void CommitChange()
        {
            if (database == null)
            {
                database = new SQLiteConnection(DatabasePath);
                database.CreateTable<RecordTable>();
            }
            database.Commit();
        }
    }
}
