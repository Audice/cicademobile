﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.DatabasePart
{
    [Table("Users")]
    public class UserTable
    {
        [PrimaryKey, AutoIncrement, Column("_ID")]
        public int ID { get; set; }
        [Unique]
        public string Login { get; set; }
        public string Password { get; set; }
        /// <summary>
        /// SessionID это токен, для автоматической авторизации
        /// </summary>
        public string SessionID { get; set; }
        /// <summary>
        /// Поле для хранения MAC адресов устройств
        /// </summary>
        public string MACS { get; set; }
    }
}
