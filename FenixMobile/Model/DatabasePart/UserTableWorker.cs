﻿using FenixMobile.Interfaces;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace FenixMobile.Model.DatabasePart
{
    public class UserTableWorker
    {
        SQLiteConnection database;
        string DatabasePath;

        //Создание или открытие таблицы в базе данных Users
        public UserTableWorker(string dbName)
        {
            DatabasePath = DependencyService.Get<ISQLite>().GetDatabasePath(dbName);
            database = new SQLiteConnection(DatabasePath);
            database.CreateTable<UserTable>();
        }

        public IEnumerable<UserTable> GetItems()
        {
            return (from i in database.Table<UserTable>() select i).ToList();
        }

        public UserTable GetUserByID(int userID)
        {
            UserTable user = (from i in database.Table<UserTable>() select i).ToList().Find(x => x.ID == userID);
            return user;
        }

        public UserTable GetUserByLogin(string login)
        {
            if (database.Table<UserTable>().Count() > 0)
            {
                UserTable userStroke = (from i in database.Table<UserTable>() select i).ToList().Find(x => x.Login == login);
                if (userStroke != null)
                    return userStroke;
                else
                    return null;
            }
            else
                return null;
        }


        //Обновление пригодится в случае смены пароля
        public int UpdateUserInformation(UserTable newUserInfo)
        {
            //Защита от изменений UserID, Login, ID
            if (newUserInfo.ID >= 0)
            {
                UserTable localUserById;
                try
                {
                    localUserById = database.Get<UserTable>(newUserInfo.ID);
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                    return -3; //Запись отсутствует в базе данных
                }

                if (localUserById.Login == newUserInfo.Login)
                {
                    return database.Update(newUserInfo);
                }
                else
                {
                    return -1;
                }
            }
            return -1;
        }

        public int AddUser(UserTable newUser)
        {
            if (database == null)
            {
                database = new SQLiteConnection(DatabasePath);
                database.CreateTable<UserTable>();
            }

            if (database.Table<UserTable>().Count() > 0)
            {
                var currentListUsers = GetItems();
                if (currentListUsers.ToList().Find(x => x.Login == newUser.Login) == null)
                {
                    return database.Insert(newUser);
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                return database.Insert(newUser);
            }
        }

        public override string ToString()
        {
            string result = "";
            var listUsers = (from i in database.Table<UserTable>() select i).ToList();
            for (int i=0; i < listUsers.Count; i++)
            {
                result += listUsers[i] + Environment.NewLine;
            }
            return result;
        }
    }
}
