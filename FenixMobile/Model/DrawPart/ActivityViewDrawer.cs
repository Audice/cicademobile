﻿using CyberHeartMobile.Classes;
using FenixMobile.Model.ActivityModel;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TouchTracking;
using TouchTracking.Forms;

namespace FenixMobile.Model.DrawPart
{
    class ActivityViewDrawer : ContentDrawer<ActivityMode>, IDisposable
    {
        readonly ushort MaxDisplaySeconds = 600;
        /// <summary>
        /// Максимальное количество секунд изображаемого интервала
        /// </summary>
        readonly byte CountBoldSeconds = 5;
        /// <summary>
        /// На сколько нужно поделить секунду
        /// </summary>
        readonly byte SecondsDrop = 4;
        /// <summary>
        /// На сколько нужно поделить секунду
        /// </summary>
        readonly byte VerticalSecondsDrop = 10;
        /// <summary>
        /// Отступ от временной оси
        /// </summary>
        private short TimeAxisOffset = 50;

        /// <summary>
        /// Ссылка на канвас для отрисовки
        /// </summary>
        protected SKCanvasView ContentCanvas { get; set; }
        /// <summary>
        /// Ссылка на объект, отслеживающий нажатие и тип нажатия на конкретный контейнер
        /// </summary>
        protected TouchEffect ContentTouchTracking { get; set; }

        //Проблема с множественным вызовом OnCanvasViewPaintSurface!!!

        private readonly byte CountTrends = 2;
        public ActivityViewDrawer(SKCanvasView trendsCanvas, TouchEffect concretTouchTracking, int numDrawsSamples, ViewMode currentMode)
        {
            this.NumSecondsString = 100;
            this.CurrentMode = currentMode;
            if (this.CurrentMode == ViewMode.ControlMode)
            {
                SetNumDrawsSamples(numDrawsSamples);
            }
            this.ContentCanvas = trendsCanvas;
            this.ContentCanvas.PaintSurface += OnCanvasViewPaintSurface;

            if (this.ContentTouchTracking != null)
            {
                //this.ContentTouchTracking.TouchAction += OnTouchEffectAction;
            }
        }

        /// <summary>
        /// Метод, срабатывающий при обновлении канвы или вызове InvalidateSurface
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected virtual void OnCanvasViewPaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {
            UpdateCanvas(args);
            if (this.CurrentMode == ViewMode.AnimationMode)
                this.ContentCanvas.InvalidateSurface();
        }

        protected void UpdateCanvas(SKPaintSurfaceEventArgs args)
        {
            SKImageInfo info = args.Info; //Информация о дисплее смартфона. Получаем ширину и высоту канваса именно отсюда
            SKCanvas canvas = args.Surface.Canvas; //Canvas for drawing  
            

            //Начинаем расчёт параметров отрисовки. Делаем так только после изменения размеров канвы (при смене ориентации)
            if (info.Width != this.WidthCanvas && info.Height != this.HeightCanvas)
            {
                canvas.Clear(); //Каждое новое обновление - очищаем канву ... Надо обдумать... Так как меняется не всё и перерисовывать всё нет необходимости
                //Определяем новое значение размаха каждый ход
                //UpdateSignalSwing();
                this.StartHeight = 0;

                this.WidthCanvas = info.Width;
                this.HeightCanvas = info.Height;

                this.HeightCorrCoef = HeightCorrectCoefCalc((int)(ActivityMode.Tension) + 1, this.HeightCanvas);

                //Изменили размеры - обновляем параметры
                this.NumSecondsString = LinkBuffersList.Count;
                this.TimeStepSize = (double)(this.WidthCanvas - this.TimeAxisOffset - 20) / this.NumSecondsString;

                //Отрисовываем данные, если буфер данных не пуст и существует
                ContentDraw(canvas);
            }
            //Расчитываем размер размаха каждый раз по приходу данных
            if (this.LinkBuffersList != null && this.LinkBuffersList.Count > this.CurrentDataIndex && this.LinkBuffersList[0].Count > 0)
            {
                canvas.Clear(); //Каждое новое обновление - очищаем канву ... Надо обдумать... Так как меняется не всё и перерисовывать всё нет необходимости
                //Определяем новое значение размаха каждый ход
                //UpdateSignalSwing();
                this.StartHeight = 0;
                //Изменили размеры - обновляем параметры
                this.NumSecondsString = LinkBuffersList.Count;
                this.TimeStepSize = (double)(this.WidthCanvas - this.TimeAxisOffset - 20) / this.NumSecondsString;
                //Отрисовываем данные, если буфер данных не пуст и существует
                ContentDraw(canvas);
            }

        }

        private double HeightCorrectCoefCalc(double maxValue, int curHeight)
        {
            return (double)curHeight / maxValue;
        }

        private double WidthCorrectCoefCalc(double maxValue, int curWidth)
        {
            return (double)curWidth / maxValue;
        }

        protected void ContentDraw(SKCanvas canvas)
        {
            try
            {
                if (this.LinkBuffersList != null)// && LinkBuffersList[CurrentLead].Count > CurrentDataIndex)
                {
                    this.CurrentDataIndex = this.LinkBuffersList.Count;
                    SKPath activityPoints = ConstructActivity(this.StartIndexDrawnData, ref this.CurrentDataIndex);
                    if (this.CurrentDataIndex > this.MaxDisplaySeconds)
                    {
                        this.StartIndexDrawnData = this.CurrentDataIndex - this.MaxDisplaySeconds;
                    }
                    else
                    {
                        this.StartIndexDrawnData = 0;
                    }

                    //Отрисовка времени
                    DrawGrid(canvas);

                    this.DrawData(canvas, activityPoints, SKColors.Green, 10f);

                    if (isConcretTouch)
                    {
                        //TODO проверить и доделать
                        isConcretTouch = false;
                        this.DrawCloude(canvas, touchPoint);
                    }

                }
            }
            catch (Exception ex)
            {
                string exM = ex.Message;
            }
        }


        double GridTimeStep = 0.1;
        double StartWidthDrawPosition = 20;

        private void DrawGrid(SKCanvas canvas)
        {
            float textsize = CalculatePresentationSize(this.HeightCanvas);
            SKPath renderedVerticalLine = new SKPath();
            SKPath renderedBoldLine = new SKPath();
            SKPath renderedThiknessLine = new SKPath();
            short startHeightDrawPosition = (short)(this.HeightCanvas - 10); //Стартовая позиция отрисовки сетки
            short startWidthDrawPosition = 10; //Стартовая позиция отрисовки сетки

            short usedHeight = (short)(this.HeightCanvas - 20);
            short usedWidth = (short)(this.WidthCanvas - 2 * startWidthDrawPosition);

            byte countHorizontLine = CountBoldSeconds;
            short stepMilliSecondsLine = (short)(usedHeight / (countHorizontLine));

            StartWidthDrawPosition = startWidthDrawPosition;

            byte countVerticalLine = 4;
            short stepVerticalLine = (short)((double)(usedWidth - this.TimeAxisOffset) / countVerticalLine);

            //Отрисовка горизонтальной части
            for (byte i = 0; i <= countHorizontLine - 1; i++)
            {
                renderedBoldLine.MoveTo(startWidthDrawPosition, startHeightDrawPosition - i * stepMilliSecondsLine);
                renderedBoldLine.LineTo(startWidthDrawPosition + usedWidth, startHeightDrawPosition - i * stepMilliSecondsLine);
                DrawText("Ур." + i.ToString(), canvas, SKColors.White,
                    startWidthDrawPosition, startHeightDrawPosition - i * stepMilliSecondsLine - 2 * textsize, CalculatePresentationSize(this.HeightCanvas));
            }

            //Отрисовка вертикальных линий
            for (byte i = 0; i <= countVerticalLine; i++)
            {
                renderedVerticalLine.MoveTo(this.TimeAxisOffset + i * stepVerticalLine, startHeightDrawPosition);
                renderedVerticalLine.LineTo(this.TimeAxisOffset + i * stepVerticalLine, startHeightDrawPosition - usedHeight);
            }



            this.DrawData(canvas, renderedBoldLine, SKColors.White, 5f);
            this.DrawData(canvas, renderedVerticalLine, SKColors.White, 5f);
        }

        private float CalculatePresentationSize(int canvasHeight)
        {
            return canvasHeight * 4 / 100;
        }

        private void DrawText(string text, SKCanvas canvas, SKColor color, float xCoord, float yCoord, float textSize)
        {
            /*
            SKMatrix textMatrix = SKMatrix.MakeIdentity();
            canvas.Concat(ref textMatrix);

            // Rotate this by the amount the canvas has rotated as we're about to reverse that back
            // this will ensure the locations are correct
            SKMatrix rotation = SKMatrix.MakeRotation((float)(45.0 * Math.PI / 180));
            SKMatrix.PreConcat(ref textMatrix, rotation);

            // Undo the canvas rotation so text is aligned correctly.
            canvas.RotateRadians(-(float)(45.0 * Math.PI / 180));
            */

            SKPaint paintedText = new SKPaint();
            paintedText.TextSize = textSize;
            paintedText.IsAntialias = true;
            paintedText.Color = SKColors.White;
            paintedText.IsStroke = false;
            paintedText.TextAlign = SKTextAlign.Left;
            paintedText.FakeBoldText = true;
            canvas.DrawText(text, xCoord, (float)(yCoord + paintedText.TextSize), paintedText);
            this.TimeAxisOffset = (short)(paintedText.TextSize * (text.Length - 1));

            //var projected_point = textMatrix.MapPoint(xCoord, (float)(yCoord + paintedText.TextSize));
            //canvas.DrawText(text, projected_point, paintedText);

            // Restore canvas back to its previous state before this (original rotation/scale/translation etc)
            //canvas.Restore();
        }


        public int CalcStartIndex(ref int currentIndex, int secondsString)
        {
            int index = currentIndex - secondsString;
            if (index < 0)
            {
                index = 0;
            }
            else
            {
                currentIndex = index + secondsString;
            }
            return index;
        }

        private SKPath ConstructActivity(int startDataIndex, ref int endDataIndex)
        {
            short startWidthDrawPosition = this.TimeAxisOffset; //Стартовая позиция отрисовки сетки
            short startHeightDrawPosition = (short)(this.HeightCanvas - 19); //Стартовая позиция отрисовки сетки
            SKPath renderedLine = new SKPath();

            if (startDataIndex < endDataIndex)
            {
                renderedLine.MoveTo((float)(startWidthDrawPosition),
                    (float)(startHeightDrawPosition - 0));
                for (int i = startDataIndex; i < endDataIndex; i++)
                {
                    renderedLine.LineTo((float)(startWidthDrawPosition + (i + 1) * this.TimeStepSize),
                        (float)(startHeightDrawPosition - this.HeightCorrCoef * CalcMidActivity(this.LinkBuffersList[i])));
                }
            }
            return renderedLine;
        }

        private int CalcMidActivity(List<ActivityMode> activityArray)
        {
            byte countVariant = (byte)((int)ActivityMode.Tension + 1);
            List<ushort> countActivity = new List<ushort>();
            for (int i = 0; i < countVariant; i++)
                countActivity.Add(0);

            for (int i = 0; i < activityArray.Count; i++)
                countActivity[(ushort)(activityArray[i])] += 1;

            int maxIndex = 0;
            for (int i = 1; i < countActivity.Count; i++)
            {
                if (countActivity[i] > 0 && i >= maxIndex)
                    maxIndex = i;
            }
            return maxIndex;
        }

        protected override byte CalcNumSecondsString(DisplayOrientation orientation)
        {
            if (orientation == DisplayOrientation.VerticalOrientation)
                return 100;
            else
                return 200;
        }

        public void Dispose()
        {
            this.ContentCanvas.PaintSurface -= OnCanvasViewPaintSurface;

            if (this.ContentTouchTracking != null)
            {
                //this.ContentTouchTracking.TouchAction -= OnTouchEffectAction;
            }
        }



        bool isConcretTouch = false;
        SKPoint touchPoint;

        /*
        protected override void OnTouchEffectAction(object sender, TouchActionEventArgs args)
        {
            var viewPoint = args.Location;
            SKPoint point =
                new SKPoint((float)(this.ContentCanvas.CanvasSize.Width * viewPoint.X / this.ContentCanvas.Width),
                            (float)(this.ContentCanvas.CanvasSize.Height * viewPoint.Y / this.ContentCanvas.Height));

            var actionType = args.Type;



            switch (args.Type)
            {
                case TouchActionType.Pressed:
                    if (!touchDictionary.ContainsKey(args.Id))
                    {
                        touchDictionary.Add(args.Id, point);
                    }
                    break;
                case TouchActionType.Moved:
                    if (touchDictionary.ContainsKey(args.Id))
                    {
                        // Single-finger drag
                        if (touchDictionary.Count == 1)
                        {
                            SKPoint prevPoint = touchDictionary[args.Id];
                            if (isTouchAction(prevPoint, point))
                            {

                            }
                            else
                            {
                                this.WasMove = true;
                                int deltaX = (int)(prevPoint.X - point.X);
                                int resultMove = this.CurrentDataIndex + deltaX;
                                if (resultMove > this.EndIndexDrawnData)
                                    this.CurrentDataIndex = this.EndIndexDrawnData;
                                else
                                {
                                    if (resultMove <= this.NumConcretSeconds)
                                        this.CurrentDataIndex = this.NumConcretSeconds;
                                    else
                                        this.CurrentDataIndex += deltaX;
                                }

                                this.ContentCanvas.InvalidateSurface();
                            }
                        }
                        touchDictionary[args.Id] = point;
                    }
                    break;
                case TouchActionType.Released:
                    //Отпускание пальца
                    if (touchDictionary.ContainsKey(args.Id))
                    {
                        SKPoint prevPoint = touchDictionary[args.Id];
                        if (this.CurrentMode == ViewMode.ControlMode && !this.WasMove)
                        {
                            if (isTouchAction(prevPoint, point))
                            {
                                DrawDataCloud(GetDataIndex(point), point);
                                this.ContentCanvas.InvalidateSurface();
                            }
                        }
                        this.WasMove = false;
                        touchDictionary.Remove(args.Id);
                    }
                    break;

                case TouchActionType.Cancelled:
                    if (touchDictionary.ContainsKey(args.Id))
                    {
                        touchDictionary.Remove(args.Id);
                    }
                    break;
            }
        }
        */
        protected override int GetDataIndex(SKPoint touchPoint)
        {
            int findIndex = (int)(touchPoint.X / this.TimeStepSize);

            if (findIndex + this.StartIndexDrawnData <= this.CurrentDataIndex)
                return findIndex + this.StartIndexDrawnData;
            return -1;
        }

        private void DrawDataCloud(int dataIndex, SKPoint touch)
        {
            if (dataIndex >= 0)
            {
                isConcretTouch = true;
                this.touchPoint = touch;
            }
        }

        private void DrawCloude(SKCanvas canvas, SKPoint touch)
        {
            using (SKPaint generalPaint = new SKPaint())
            {
                generalPaint.Style = SKPaintStyle.Fill;
                generalPaint.Color = SKColors.White;
                generalPaint.StrokeWidth = 10;
                canvas.DrawRoundRect(touch.X, touch.Y, 100, 100, 20, 20, generalPaint);

                //canvas.DrawPoints(SKPointMode.Points, dataLine.GetPoints(dataLine.PointCount), generalPaint);
            }
        }
    }
}
