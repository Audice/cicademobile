﻿using CyberHeartMobile.Classes;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Text;
using TouchTracking;
using TouchTracking.Forms;

namespace FenixMobile.Model.DrawPart
{
    public class ConcretViewDrawer : IDisposable
    {
        /// <summary>
        /// Поле, характеризующее количество секунд в конкретном представлении сигнала. Изменяется при смене ориентации телефона.
        /// При вертикальном отображении - 2 секунды, при горизонтальном - 4
        /// </summary>
        private byte NumSecondsString = 2;
        /// <summary>
        /// Коэфицент, корректирующий сигнал по высоте
        /// </summary>
        private double HeightCorrCoef = 0.1;
        /// <summary>
        /// Частота дискретизации сигнала - сколько точек сигнала содержится в одной его секунде
        /// </summary>
        private short SamplingFrequancy = 500;
        /// <summary>
        /// Шаг сигнала по времени
        /// </summary>
        private double TimeStepSize = 0.1;
        /// <summary>
        /// Точка начала отрисовки экг по координате Y, по высоте
        /// </summary>
        private ushort StartHeight = 200;
        /// <summary>
        /// Ссылка на список с данными
        /// </summary>
        List<List<short>> LinkBuffersList;

        //Запомним ширину и высоту, дабы не пересчитывать всё каждый раз
        private int WidthConcretCanvas
        {
            get; set;
        } = -1;
        private int HeightConcretCanvas
        {
            get; set;
        } = -1;


        /// <summary>
        /// Начальный индекс для отрисовки и обсчёта данных
        /// </summary>
        private int BeginIndexDrawnData
        {
            get; set;
        } = 0;


        /// <summary>
        /// Конечный индекс для отрисовки и обсчёта данных
        /// </summary>
        private int EndIndexDrawnData
        {
            get; set;
        } = 0;

        private DisplayOrientation CurrentOrientation
        {
            get; set;
        }

        //Поиск максимального и минимального значения в сигнале, определение максимального размаха
        ushort signalSwing = 1000;
        /// <summary>
        /// Размах сигнала между низшей и висшей точкой
        /// </summary>
        public ushort SignalSwing
        {
            get { return signalSwing; }
            private set
            {
                if (value > signalSwing)
                {
                    signalSwing = value;
                }
            }
        }
        private short MaxSignalsSample { get; set; } = short.MinValue;
        private short MinSignalsSample { get; set; } = short.MaxValue;

        /// <summary>
        /// Переменная, отвечающая за текущее отведение
        /// </summary>
        private byte CurrentLead = 0;

        public SKCanvasView ConcretCanvas { get; private set; }


        private GeneralViewDrawer GeneralViewDrawer
        {
            get; set;
        }


        /// <summary>
        /// Максимальное количество сэмплов сигнала ЭКГ для отрисовки
        /// </summary>
        private int NumDrawsSamples = 0;


        public bool IsMoveOpen
        {
            get; private set;
        } = false;

        /// <summary>
        /// Ссылка на объект, отслеживающий нажатие и тип нажатия на конкретный контейнер
        /// </summary>
        TouchEffect ConcretTouchTracking { get; set; }


        public ConcretViewDrawer(GeneralViewDrawer generalViewDrawer, SKCanvasView concretCanvas, TouchEffect concretTouchTracking,
            short samplingFrequancy, int numDrawsSamples )
        {
            this.ConcretTouchTracking = concretTouchTracking;
            this.NumDrawsSamples = numDrawsSamples;
            this.GeneralViewDrawer = generalViewDrawer;
            this.SamplingFrequancy = samplingFrequancy;
            this.ConcretCanvas = concretCanvas;
            this.ConcretCanvas.PaintSurface += OnCanvasViewPaintSurface;

            if (this.ConcretTouchTracking != null)
            {
                this.ConcretTouchTracking.TouchAction += OnTouchEffectAction;
            }
        }

        public void SetNumDrawsSamples(int numDrawsSamples)
        {
            this.NumDrawsSamples = numDrawsSamples;
            this.EndIndexDrawnData = numDrawsSamples;
        }

        /// <summary>
        /// Метод, реагирующий на обновление канвы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        void OnCanvasViewPaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {
            SKImageInfo info = args.Info; //Информация о дисплее смартфона. Получаем ширину и высоту канваса именно отсюда
            SKSurface surface = args.Surface; //Save canvas
            SKCanvas canvas = surface.Canvas; //Canvas for drawing
            canvas.Clear(); //Каждое новое обновление - очищаем канву

            //Начинаем расчёт параметров отрисовки
            if (info.Width != this.WidthConcretCanvas && info.Height != this.HeightConcretCanvas)
            {
                this.WidthConcretCanvas = info.Width;
                this.HeightConcretCanvas = info.Height;
                uint numStringSampls = (uint)(this.SamplingFrequancy * this.NumSecondsString);
                this.TimeStepSize = (double)this.WidthConcretCanvas / numStringSampls;
            }
            this.BeginIndexDrawnData = SearchBeginIndex(this.EndIndexDrawnData);


            if (this.LinkBuffersList != null && this.LinkBuffersList.Count > CurrentLead && this.LinkBuffersList[CurrentLead].Count > 0)
            {
                //Определяем новое значение размаха каждый ход
                UpdateSignalSwing();
                this.HeightCorrCoef = (double)(0.9 * this.HeightConcretCanvas / this.SignalSwing);
                this.StartHeight = (ushort)(this.MaxSignalsSample * this.HeightCorrCoef);

                SKPath renderedConcretLineECG = new SKPath();

                if (this.BeginIndexDrawnData == 0)
                    renderedConcretLineECG.MoveTo(0, StartHeight);
                else
                    renderedConcretLineECG.MoveTo(0, (float)(StartHeight - LinkBuffersList[CurrentLead][this.BeginIndexDrawnData] * this.HeightCorrCoef));

                int steper = 0;

                //Определяем начало отрисовки
                for (int i = this.BeginIndexDrawnData; i < this.EndIndexDrawnData; i++)
                {
                    renderedConcretLineECG.LineTo((float)((++steper) * this.TimeStepSize),
                        (float)(StartHeight - LinkBuffersList[CurrentLead][i] * this.HeightCorrCoef));
                }

                using (SKPaint paint = new SKPaint())
                {
                    paint.Style = SKPaintStyle.Stroke;
                    paint.Color = SKColors.White;
                    paint.StrokeWidth = 7;
                    canvas.DrawPath(renderedConcretLineECG, paint);
                }
            }
        }

        public void EnableMoveConcretECG()
        {
            this.IsMoveOpen = true;
        }

        private int SearchBeginIndex(int endIndex)
        {
            if (endIndex <= 0) return 0;
            int countConcretSignalSamples = this.NumSecondsString * this.SamplingFrequancy;
            int resultIndex = endIndex - countConcretSignalSamples;
            if (resultIndex >= 0)
                return resultIndex;
            else
                return 0;
        }

        /// <summary>
        /// Обновление максимального и минимального значения сигнала и размаха
        /// TODO: проверить правильность
        /// </summary>
        private void UpdateSignalSwing()
        {
            int i = 0;
            for (i = this.BeginIndexDrawnData; i < this.LinkBuffersList[this.CurrentLead].Count; i++)
            {
                if (this.MaxSignalsSample < this.LinkBuffersList[this.CurrentLead][i])
                    this.MaxSignalsSample = this.LinkBuffersList[this.CurrentLead][i];
                if (this.MinSignalsSample > this.LinkBuffersList[this.CurrentLead][i])
                    this.MinSignalsSample = this.LinkBuffersList[this.CurrentLead][i];
            }
            ushort tmpSignalSwing = (ushort)Math.Abs(MaxSignalsSample - MinSignalsSample);
            if (tmpSignalSwing > 0) this.SignalSwing = tmpSignalSwing;
            else this.SignalSwing = 1000;
        }

        /// <summary>
        /// Пересчёт количества секунд в строке в зависимости от пложения телефона
        /// </summary>
        /// <returns>Количество секунд в строке</returns>
        public void SetNumSecondsString(byte numSeconds)
        {
            this.NumSecondsString = numSeconds;
        }

        /// <summary>
        /// Метод обновления ссылки на список с данными
        /// </summary>
        /// <param name="BuffersList">Ссылка на список с данными</param>
        public void SetDataReference(List<List<short>> BuffersList)
        {
            this.LinkBuffersList = BuffersList;
        }

        public void SetLead(byte lead)
        {
            this.CurrentLead = lead;
        }

        public void UpdateCanvas(SKPaintSurfaceEventArgs args)
        {
            SKImageInfo info = args.Info;
            SKSurface surface = args.Surface;
            SKCanvas canvas = surface.Canvas;
            canvas.Clear(); //Clear Canvas
        }

        /// <summary>
        /// Установка последнего отрисованного сигнала индекса
        /// </summary>
        /// <param name="endIndex">Конечный индекс</param>
        public void SetRenderIndex(int endIndex)
        {
            this.EndIndexDrawnData = endIndex;
        }

        // Информация о touch событиях
        Dictionary<long, SKPoint> touchDictionary = new Dictionary<long, SKPoint>();
        void OnTouchEffectAction(object sender, TouchActionEventArgs args)
        {
            var viewPoint = args.Location;
            SKPoint point =
                new SKPoint((float)(this.ConcretCanvas.CanvasSize.Width * viewPoint.X / this.ConcretCanvas.Width),
                            (float)(this.ConcretCanvas.CanvasSize.Height * viewPoint.Y / this.ConcretCanvas.Height));

            var actionType = args.Type;


            switch (args.Type)
            {
                case TouchActionType.Pressed:
                    if (!touchDictionary.ContainsKey(args.Id))
                    {
                        touchDictionary.Add(args.Id, point);
                    }
                    break;
                case TouchActionType.Moved:
                    if (touchDictionary.ContainsKey(args.Id))
                    {
                        // Single-finger drag
                        if (touchDictionary.Count == 1)
                        {
                            SKPoint prevPoint = touchDictionary[args.Id];
                            if (IsMoveOpen)
                            {
                                int deltaX = (int)(prevPoint.X - point.X);
                                int resultMove = this.EndIndexDrawnData + deltaX;
                                if (resultMove > this.NumDrawsSamples)
                                    this.EndIndexDrawnData = this.NumDrawsSamples;
                                else
                                {
                                    if (resultMove <= this.NumSecondsString * this.SamplingFrequancy)
                                        this.EndIndexDrawnData = this.NumSecondsString * this.SamplingFrequancy;
                                    else
                                        this.EndIndexDrawnData += deltaX;
                                }
                                this.GeneralViewDrawer.SetCurrentConcretDataIndex(this.EndIndexDrawnData);
                                this.ConcretCanvas.InvalidateSurface();
                            }
                        }
                        touchDictionary[args.Id] = point;
                    }
                    break;
                case TouchActionType.Released:
                    //Отпускание пальца
                    if (touchDictionary.ContainsKey(args.Id))
                    {
                        SKPoint prevPoint = touchDictionary[args.Id];
                        if (IsMoveOpen)
                        {
                            //Переместить график в эту точку... ???

                        }
                        touchDictionary.Remove(args.Id);
                    }
                    break;

                case TouchActionType.Cancelled:
                    if (touchDictionary.ContainsKey(args.Id))
                    {
                        touchDictionary.Remove(args.Id);
                    }
                    break;
            }
        }

        public void Dispose()
        {
            if (this.ConcretTouchTracking != null)
            {
                this.ConcretTouchTracking.TouchAction -= OnTouchEffectAction;
            }
        }
    }
}
