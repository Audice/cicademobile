﻿using CyberHeartMobile.Classes;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Text;
using TouchTracking;
using TouchTracking.Forms;

namespace FenixMobile.Model.DrawPart
{
    public abstract class ContentDrawer<T>
    {
        protected bool IsUserFocuses = false;
        /// <summary>
        /// Поле, характеризующее количество секунд в строке сигнала. Изменяется при смене ориентации телефона.
        /// При вертикальном отображении - 3 секунды, при горизонтальном - 6
        /// </summary>
        protected int NumSecondsString = 3;
        /// <summary>
        /// Флаг, регулирующий отрисовку сигнала. True - отрисовка разрешена, false - запрещена
        /// </summary>
        protected bool IsGraphDrawn { get; set; } = false;
        /// <summary>
        /// Ссылка на список с данными
        /// </summary>
        protected List<List<T>> LinkBuffersList;
        /// <summary>
        /// Коэфицент, корректирующий сигнал по высоте
        /// </summary>
        protected double HeightCorrCoef = 0.1;
        /// <summary>
        /// Коэфицент, корректирующий сигнал по высоте
        /// </summary>
        protected double WidthCorrCoef = 0.1;
        /// <summary>
        /// Шаг сигнала по времени
        /// </summary>
        protected double TimeStepSize = 0.1;
        /// <summary>
        /// Точка начала отрисовки графика по координате Y, по высоте
        /// </summary>
        protected ushort StartHeight = 200;
        /// <summary>
        /// Количество рассматриваемых секунд
        /// </summary>
        protected byte NumConcretSeconds
        {
            get; set;
        } = 10;

        //Запомним ширину и высоту, дабы не пересчитывать всё каждый раз
        protected int WidthCanvas
        {
            get; set;
        } = -1;
        protected int HeightCanvas
        {
            get; set;
        } = -1;


        /// <summary>
        /// Начальный индекс для отрисовки и обсчёта данных
        /// </summary>
        protected int StartIndexDrawnData = 0;
        /// <summary>
        /// Текущий индекс данных в общем хронилище
        /// </summary>
        protected int CurrentDataIndex = 0;
        /// <summary>
        /// Конечный индекс для отрисовки и обсчёта данных
        /// </summary>
        protected int EndIndexDrawnData = 0;

        /// <summary>
        /// Дефолтное значение размаха сигнала
        /// </summary>
        protected ushort signalSwing = 0;
        /// <summary>
        /// Размах сигнала между низшей и висшей точкой
        /// </summary>
        protected ushort SignalSwing
        {
            get { return signalSwing; }
            set
            {
                if (value > signalSwing)
                    signalSwing = value;
            }
        }
        /// <summary>
        /// Максимальное значение сэмпла в сигнале
        /// </summary>
        protected short MaxSignalsSample { get; set; } = short.MinValue;
        /// <summary>
        /// Минимальное значение сэмпла в сигнале
        /// </summary>
        protected short MinSignalsSample { get; set; } = short.MaxValue;
        /// <summary>
        /// Режим просмотра: активный - анимация, пассивный - просмотр пальцем
        /// </summary>
        protected ViewMode CurrentMode;
        /// <summary>
        /// Текущая ориентация дисплея
        /// </summary>
        public DisplayOrientation DisplayOrientation
        {
            get; private set;
        }



        /// <summary>
        /// Обновление canvas, в соответствии с текущими требованиями отображения
        /// </summary>
        /// <param name="args"></param>
        //protected abstract void UpdateCanvas(SKPaintSurfaceEventArgs args);

        /// <summary>
        /// Реализация отрисовки данных на канве
        /// </summary>
        /// <param name="canvas">Канва для отрисовки</param>
        //protected abstract void ContentDraw(SKCanvas canvas);

        protected virtual void SetNumDrawsSamples(int numDrawsSamples)
        {

            this.EndIndexDrawnData = numDrawsSamples - 1;
            this.CurrentDataIndex = numDrawsSamples - 1;
            this.StartIndexDrawnData = this.CurrentDataIndex - this.NumSecondsString;
            if (this.StartIndexDrawnData < 0)
                this.StartIndexDrawnData = 0;

        }

        public virtual void SetDisplayOrientation(DisplayOrientation displayOrientation)
        {
            this.DisplayOrientation = displayOrientation;
        }

        /// <summary>
        /// Метод обновления ссылки на список с данными
        /// </summary>
        /// <param name="BuffersList">Ссылка на список с данными</param>
        public virtual void SetDataReference(List<List<T>> BuffersList)
        {
            this.LinkBuffersList = BuffersList;
        }

        /// <summary>
        /// Запуск работы цикла анимирования
        /// </summary>
        public virtual void StartAnimate()
        {
            if (IsGraphDrawn) return;
            this.IsGraphDrawn = true;
            //this.ContentCanvas.InvalidateSurface();
        }
        /// <summary>
        /// Остановка анимации
        /// </summary>
        public virtual void StopAnimate()
        {
            this.IsGraphDrawn = false;
        }

        public virtual void SetFocus(bool isFocuse)
        {
            this.IsUserFocuses = isFocuse;
        }

        /// <summary>
        /// Сравнение текущего индекса отрисовки с числом сэмплов, выделенных под отрисовку
        /// </summary>
        /// <returns>True - сэмплы для отрисовку доступны, false - сэмплы сигнала закончились</returns>
        private bool CheckLastIndex()
        {
            return this.EndIndexDrawnData > this.CurrentDataIndex;
        }


        protected bool WasMove = false;
        // Информация о touch событиях
        //protected Dictionary<long, SKPoint> touchDictionary = new Dictionary<long, SKPoint>();
        //protected abstract void OnTouchEffectAction(object sender, TouchActionEventArgs args);

        /// <summary>
        /// Пересчёт количества секунд в строке в зависимости от пложения телефона
        /// </summary>
        /// <param name="orientation">Ориентация экрана смартфона</param>
        /// <returns>Количество секунд в строке</returns>
        protected abstract byte CalcNumSecondsString(DisplayOrientation orientation);

        protected virtual void DrawData(SKCanvas canvas, SKPath dataLine, SKColor color, float width)
        {
            using (SKPaint generalPaint = new SKPaint())
            {
                generalPaint.Style = SKPaintStyle.Stroke;
                generalPaint.Color = color;
                generalPaint.StrokeWidth = width;
                canvas.DrawPath(dataLine, generalPaint);
                //canvas.DrawPoints(SKPointMode.Points, dataLine.GetPoints(dataLine.PointCount), generalPaint);
            }
        }

        protected virtual void DrawPoints(SKCanvas canvas, SKPoint[] points, SKColor color, float width)
        {
            using (SKPaint generalPaint = new SKPaint())
            {
                generalPaint.Style = SKPaintStyle.Stroke;
                generalPaint.Color = color;//SKColor.FromHsv(119, 100, 50, 170);
                generalPaint.StrokeWidth = width;
                for (int i = 0; i < points.Length; i++)
                {
                    canvas.DrawCircle(points[i], 1, generalPaint);
                }
                //canvas.DrawPoints(SKPointMode.Points, points, generalPaint);
                
                //canvas.DrawPoints(SKPointMode.Points, dataLine.GetPoints(dataLine.PointCount), generalPaint);
            }
        }

        protected virtual bool isTouchAction(SKPoint fPoint, SKPoint sPoint)
        {
            double distance = Math.Sqrt(Math.Pow(fPoint.X - sPoint.X, 2) + Math.Pow(fPoint.Y - sPoint.Y, 2));
            float curMaxCanvasSize = 0;
            if (this.HeightCanvas > this.WidthCanvas)
                curMaxCanvasSize = this.HeightCanvas;
            else
                curMaxCanvasSize = this.WidthCanvas;
            double errorMovePercent = 0.01;
            if (errorMovePercent * curMaxCanvasSize >= distance)
                return true;
            else
                return false;
        }

        protected abstract int GetDataIndex(SKPoint touchPoint);

    }
}
