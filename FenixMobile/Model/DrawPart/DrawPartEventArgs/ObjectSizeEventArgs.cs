﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.DrawPart.DrawPartEventArgs
{
    public class ObjectSizeEventArgs : EventArgs
    {
        /// <summary>
        /// Ширина объекта
        /// </summary>
        public double ObjectWidth { get; set; }
        /// <summary>
        /// Высота объекта
        /// </summary>
        public double ObjectHeight { get; set; }
    }
}
