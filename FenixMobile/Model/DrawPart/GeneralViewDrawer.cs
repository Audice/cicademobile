﻿using CyberHeartMobile.Classes;
using FenixMobile.Model.DrawPart.DrawPartEventArgs;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Text;
using TouchTracking;
using TouchTracking.Forms;

namespace FenixMobile.Model.DrawPart
{
    public class GeneralViewDrawer : IDisposable
    {
        /// <summary>
        /// Количество строк в представлении
        /// </summary>
        private readonly byte CountString = 5;
        /// <summary>
        /// Поле, характеризующее количество секунд в строке сигнала. Изменяется при смене ориентации телефона.
        /// При вертикальном отображении - 3 секунды, при горизонтальном - 6
        /// </summary>
        private byte NumSecondsString = 3;
        /// <summary>
        /// Флаг, регулирующий отрисовку сигнала. True - отрисовка разрешена, false - запрещена
        /// </summary>
        private bool IsGraphDrawn { get; set; } = false;
        /// <summary>
        /// Ссылка на список с данными
        /// </summary>
        List<List<short>> LinkBuffersList;
        /// <summary>
        /// Коэфицент, корректирующий сигнал по высоте
        /// </summary>
        private double HeightCorrCoef = 0.1;
        /// <summary>
        /// Частота дискретизации сигнала - сколько точек сигнала содержится в одной его секунде
        /// </summary>
        private short SamplingFrequancy = 500;
        /// <summary>
        /// Шаг сигнала по времени
        /// </summary>
        private double TimeStepSize = 0.1;
        /// <summary>
        /// Переменная, отвечающая за текущее отведение
        /// </summary>
        private byte CurrentLead = 0;
        /// <summary>
        /// Шаг линии сигнала по высоте
        /// </summary>
        private ushort HeightStep = 1;
        /// <summary>
        /// Точка начала отрисовки экг по координате Y, по высоте
        /// </summary>
        private ushort StartHeight = 200;
        /// <summary>
        /// Количество секунд, рассматриваемых в конкретизации
        /// </summary>
        private byte NumConcretSeconds
        {
            get; set;
        } = 2;



        //Запомним ширину и высоту, дабы не пересчитывать всё каждый раз
        private int WidthGeneralCanvas
        {
            get; set;
        } = -1;
        private int HeightGeneralCanvas
        {
            get; set;
        } = -1;
        /// <summary>
        /// Начальный индекс для отрисовки и обсчёта данных
        /// </summary>
        private int StartIndexDrawnData = 0;
        /// <summary>
        /// Текущий индекс данных в общем хронилище
        /// </summary>
        private int CurrentDataIndex = 0;



        private int currentConcretDataIndex = 0;
        /// <summary>
        /// Индекс конкретной части общего графика ЭКГ
        /// </summary>
        public int CurrentConcretDataIndex
        {
            get { return currentConcretDataIndex; }
            private set
            {
                currentConcretDataIndex = value;
            }
        }


        //Поиск максимального и минимального значения в сигнале, определение максимального размаха
        #region
        /// <summary>
        /// Дефолтное значение размаха сигнала
        /// </summary>
        private ushort signalSwing = 0;
        /// <summary>
        /// Размах сигнала между низшей и висшей точкой
        /// </summary>
        public ushort SignalSwing
        {
            get { return signalSwing; }
            private set
            {
                if (value > signalSwing)
                {
                    signalSwing = value;
                }
            }
        }
        /// <summary>
        /// Максимальное значение сэмпла в сигнале
        /// </summary>
        private short MaxSignalsSample { get; set; } = short.MinValue;
        /// <summary>
        /// Минимальное значение сэмпла в сигнале
        /// </summary>
        private short MinSignalsSample { get; set; } = short.MaxValue;
        #endregion

        /// <summary>
        /// Объект-отрисовщик конкретной части ЭКГ в увеличенном формате
        /// </summary>
        ConcretViewDrawer ConcretViewDrawer { get; set; }
        /// <summary>
        /// Ссылка на канвас для отрисовки
        /// </summary>
        SKCanvasView GeneralCanvas { get; set; }
        /// <summary>
        /// Ссылка на объект, отслеживающий нажатие и тип нажатия на конкретный контейнер
        /// </summary>
        TouchEffect GeneralTouchTracking { get; set; }
        /// <summary>
        /// Максимальное количество сэмплов сигнала ЭКГ для отрисовки
        /// </summary>
        private int NumDrawsSamples = 0;


        public ViewMode CurrentMode;

        public DisplayOrientation DisplayOrientation
        {
            get; private set;
        }

        /// <summary>
        /// Конструктор объекта отрисовщика общего вида сигнала ЭКГ
        /// </summary>
        /// <param name="generalCanvas">Канвас общего вида сигнала</param>
        /// <param name="concretCanvas">Канвас конкретного участка сигнала</param>
        /// <param name="generalTouchTracking"></param>
        /// <param name="samplingFrequancy"></param>
        public GeneralViewDrawer(SKCanvasView generalCanvas, SKCanvasView concretCanvas, TouchEffect generalTouchTracking, TouchEffect concretTouchTracking,
            short samplingFrequancy, int numDrawsSamples, ViewMode currentMode)
        {
            this.CurrentMode = currentMode;
            this.NumDrawsSamples = numDrawsSamples;
            this.GeneralTouchTracking = generalTouchTracking;
            this.ConcretViewDrawer = new ConcretViewDrawer(this, concretCanvas, concretTouchTracking, samplingFrequancy, numDrawsSamples);
            if (this.CurrentMode == ViewMode.ControlMode)
            {
                SetNumDrawsSamples(numDrawsSamples);
            }
            this.SamplingFrequancy = samplingFrequancy;
            this.GeneralCanvas = generalCanvas;
            this.GeneralCanvas.PaintSurface += OnCanvasViewPaintSurface;

            if (this.GeneralTouchTracking != null)
            {
                this.GeneralTouchTracking.TouchAction += OnTouchEffectAction;
            }
        }

        public void SetNumDrawsSamples(int numDrawsSamples)
        {
            this.NumDrawsSamples = numDrawsSamples;
            this.CurrentDataIndex = numDrawsSamples;
            this.StartIndexDrawnData = this.CurrentDataIndex - this.CountString * this.SamplingFrequancy * this.NumSecondsString;
            if (this.StartIndexDrawnData < 0)
                this.StartIndexDrawnData = 0;
            this.CurrentConcretDataIndex = numDrawsSamples;
            this.ConcretViewDrawer.SetNumDrawsSamples(numDrawsSamples);
        }

        public void SetGenetalDisplayOrientation(DisplayOrientation displayOrientation)
        {
            this.DisplayOrientation = displayOrientation;
        }

        /// <summary>
        /// Метод обновления ссылки на список с данными
        /// </summary>
        /// <param name="BuffersList">Ссылка на список с данными</param>
        public void SetDataReference(List<List<short>> BuffersList)
        {
            this.LinkBuffersList = BuffersList;
            this.ConcretViewDrawer.SetDataReference(BuffersList);
        }
        /// <summary>
        /// Запуск работы цикла анимирования
        /// </summary>
        public void StartAnimate()
        {
            if (IsGraphDrawn) return;
            this.IsGraphDrawn = true;
            this.GeneralCanvas.InvalidateSurface();
        }
        /// <summary>
        /// Остановка анимации
        /// </summary>
        public void StopAnimate()
        {
            this.IsGraphDrawn = false;
        }
        /// <summary>
        /// Метод, срабатывающий при обновлении канвы или вызове InvalidateSurface
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        void OnCanvasViewPaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {
            UpdateCanvas(args);
            if (CheckLastIndex() && this.CurrentMode == ViewMode.AnimationMode)
                this.GeneralCanvas.InvalidateSurface();
            else
                this.ConcretViewDrawer.EnableMoveConcretECG();
        }

        public void DrawECGLine()
        {
            this.GeneralCanvas.InvalidateSurface();
        }

        /// <summary>
        /// Обновление canvas, в соответствии с текущими требованиями отображения
        /// </summary>
        /// <param name="args"></param>
        public void UpdateCanvas(SKPaintSurfaceEventArgs args)
        {
            SKImageInfo info = args.Info; //Информация о дисплее смартфона. Получаем ширину и высоту канваса именно отсюда
            SKCanvas canvas = args.Surface.Canvas; //Canvas for drawing  
            canvas.Clear(); //Каждое новое обновление - очищаем канву

            //Начинаем расчёт параметров отрисовки. Делаем так только после изменения размеров канвы (при смене ориентации)
            if (info.Width != this.WidthGeneralCanvas && info.Height != this.HeightGeneralCanvas)
            {
                this.WidthGeneralCanvas = info.Width;
                this.HeightGeneralCanvas = info.Height;
                //Изменили размеры - обновляем параметры
                this.NumSecondsString = CalcNumSecondsString(this.DisplayOrientation);
                uint numStringSampls = (uint)(this.SamplingFrequancy * this.NumSecondsString);
                this.TimeStepSize = (double)this.WidthGeneralCanvas / numStringSampls;

                this.NumConcretSeconds = this.CalcNumConcretSeconds(this.DisplayOrientation);
                this.ConcretViewDrawer.SetNumSecondsString(this.NumConcretSeconds);

            }
            //Расчитываем размер размаха каждый раз по приходу данных
            if (this.LinkBuffersList != null && this.LinkBuffersList.Count > CurrentLead && this.LinkBuffersList[CurrentLead].Count > 0)
            {
                //Определяем новое значение размаха каждый ход
                UpdateSignalSwing();
                double curHeightGeneral = this.SignalSwing * this.CountString;
                this.HeightCorrCoef = this.HeightGeneralCanvas / curHeightGeneral;
                this.StartHeight = (ushort)(this.MaxSignalsSample * this.HeightCorrCoef + 1 + CalculatePresentationSize(info.Height));
                this.HeightStep = (ushort)(signalSwing * this.HeightCorrCoef);
                //Отрисовываем данные, если буфер данных не пуст и существует
                GeneralDraw(canvas);
            }
        }

        /// <summary>
        /// Обновление максимального и минимального значения сигнала и размаха
        /// TODO: проверить правильность... Должна быть некая оценка... но вот какая((((((
        /// </summary>
        private void UpdateSignalSwing()
        {
            int i = 0;

            for (i = this.StartIndexDrawnData; i < this.LinkBuffersList[this.CurrentLead].Count; i++)
            {
                if (this.MaxSignalsSample < this.LinkBuffersList[this.CurrentLead][i])
                    this.MaxSignalsSample = this.LinkBuffersList[this.CurrentLead][i];
                if (this.MinSignalsSample > this.LinkBuffersList[this.CurrentLead][i])
                    this.MinSignalsSample = this.LinkBuffersList[this.CurrentLead][i];
            }
            ushort tmpSignalSwing = (ushort)Math.Abs(MaxSignalsSample - MinSignalsSample);
            if (tmpSignalSwing > 0) this.SignalSwing = tmpSignalSwing;
            else this.SignalSwing = 1000;
        }

        /// <summary>
        /// Пересчёт количества секунд в строке в зависимости от пложения телефона
        /// </summary>
        /// <param name="orientation">Ориентация экрана смартфона</param>
        /// <returns>Количество секунд в строке</returns>
        private byte CalcNumSecondsString(DisplayOrientation orientation)
        {
            if (orientation == DisplayOrientation.VerticalOrientation)
                return 5;
            else
                return 10;
        }
        /// <summary>
        /// Подсчёт количества секунд в конкретной части
        /// </summary>
        /// <param name="orientation">Ориентация экрана смартфона</param>
        /// <returns>Количество секунд конкретной части</returns>
        private byte CalcNumConcretSeconds(DisplayOrientation orientation)
        {
            if (orientation == DisplayOrientation.VerticalOrientation)
                return 2; //Если вертикальная ориентация - даём 2 секунды
            else
                return 4; //Если горизонтальная ориентация - даём 4 секунды
        }

        private void GeneralDraw(SKCanvas canvas)
        {
            try
            {
                if (this.LinkBuffersList != null && LinkBuffersList.Count >= CurrentLead)// && LinkBuffersList[CurrentLead].Count > CurrentDataIndex)
                {
                    //Расчёт текущего количества строк
                    int stringCount = this.LinkBuffersList.Count / (this.SamplingFrequancy * this.NumSecondsString);
                    //Расчёт количества сэмплов в строке
                    ushort stringSamples = (ushort)(this.SamplingFrequancy * this.NumSecondsString);
                    //Построение ЭКГ линии общего вида
                    SKPath renderedLinesECG = DrawGeneralLine(canvas, ref this.StartIndexDrawnData, ref this.CurrentDataIndex, stringSamples);
                    //Обновление последнего индекса для отрисовки конкретизирующей линии при анимации. Инача нет
                    if (CheckLastIndex() && this.CurrentMode == ViewMode.AnimationMode)
                        this.CurrentConcretDataIndex = this.CurrentDataIndex;
                    //Расчитываем позицию по Х для линии конкретизации на общем графике ЭКГ
                    int indexStartConcretX = GetConcretDrawPartIndex(this.CurrentConcretDataIndex, this.NumConcretSeconds);
                    //Строим линию конкретизации на общем графике экг
                    SKPath concretECGPath = DrawConcretLine(canvas, indexStartConcretX, this.CurrentConcretDataIndex,
                        this.StartIndexDrawnData, this.CurrentDataIndex, stringSamples, this.StartHeight);
                    //Добавление линии общего представления сигнала на канвас
                    this.DrawSignal(canvas, renderedLinesECG, SKColors.Green, 5);
                    //Добавление линии конкретного представления сигнала на канвас общего
                    this.DrawSignal(canvas, concretECGPath, SKColors.Red, 7);

                    DrawEcgGeneralTime(canvas, this.WidthGeneralCanvas, this.HeightGeneralCanvas,
                        CalculatePresentationSize(this.HeightGeneralCanvas),
                        CalculateECGIndexTime(this.StartIndexDrawnData),
                        CalculateECGIndexTime(this.CurrentDataIndex));

                    DrawLines(canvas, SKColors.White);

                    //Установка последнего отрисованного индекса
                    this.ConcretViewDrawer.SetRenderIndex(this.CurrentConcretDataIndex);
                    //Обновление канвы
                    this.ConcretViewDrawer.ConcretCanvas.InvalidateSurface();
                }
            }
            catch (Exception ex)
            {
                string exM = ex.Message;
            }
        }

        /// <summary>
        /// Сравнение текущего индекса отрисовки с числом сэмплов, выделенных под отрисовку
        /// </summary>
        /// <returns>True - сэмплы для отрисовку доступны, false - сэмплы сигнала закончились</returns>
        private bool CheckLastIndex()
        {
            return this.NumDrawsSamples > this.CurrentDataIndex;
        }

        //ОШИБКА - КРАСНАЯ ЛИНИЯ НА ОБЩЕМ ГРАФИКЕ ПРИ ЛИНИЯХ БОЛЬШЕ 5 отрисовывается неверно
        /// <summary>
        /// Возвращает реальную координату области отрисовки по X
        /// </summary>
        /// <param name="currentIndex"></param>
        /// <param name="readBatch"></param>
        /// <returns></returns>
        private int GetConcretDrawPartIndex(int currentIndex, byte numSec = 1)
        {
            if (currentIndex - numSec * this.SamplingFrequancy <= 0)
                return 0;
            else
                return currentIndex - numSec * this.SamplingFrequancy;
        }

        private SKPath DrawGeneralLine(SKCanvas canvas, ref int startIndex, ref int endIndex, ushort stringSamples)
        {
            SKPath renderedGeneralLinesECG = new SKPath();
            uint stringShift = 0;
            //Расчёт размера текущей части отрисовки
            int currentReadBatch = CorrectDrawnBatch(LinkBuffersList[CurrentLead].Count - endIndex);

            if (this.CurrentMode == ViewMode.AnimationMode)
            {
                //Расчитываем стартовый индекс данных, которые нужно отрисовать
                if ((endIndex - startIndex) >= (this.CountString * stringSamples))
                    startIndex += stringSamples;
            }
            else
            {
                int tmpStartIndex = endIndex - (this.CountString * stringSamples);
                if (tmpStartIndex < 0) //Ошибка на 10 секундах
                {
                    startIndex = 0;
                    endIndex = this.CountString * stringSamples;
                    if (endIndex > this.NumDrawsSamples)
                    {
                        endIndex = this.NumDrawsSamples;
                    }
                }
                else
                    startIndex = tmpStartIndex;
            }

            int localEndIndex = endIndex + currentReadBatch - startIndex;

            renderedGeneralLinesECG.MoveTo(0, (float)(stringShift + StartHeight - LinkBuffersList[CurrentLead][startIndex] * this.HeightCorrCoef));

            //Ошибка на 10 секундах
            for (int i = 1; i < localEndIndex; i++)
            {
                if (i % stringSamples == 0)
                {
                    stringShift += this.HeightStep;
                    renderedGeneralLinesECG.MoveTo(0, (float)(stringShift + StartHeight - LinkBuffersList[CurrentLead][i + startIndex] * this.HeightCorrCoef));
                    continue;
                }
                renderedGeneralLinesECG.LineTo((float)((i % stringSamples) * this.TimeStepSize),
                    (float)(stringShift + StartHeight - LinkBuffersList[CurrentLead][i + startIndex] * this.HeightCorrCoef));
            }
            int valEndIndex = endIndex + currentReadBatch;
            //Обновляем текущий последний индекс
            if (valEndIndex > this.NumDrawsSamples)
                endIndex = this.NumDrawsSamples;
            else
                endIndex += currentReadBatch;

            return renderedGeneralLinesECG;
        }

        /// <summary>
        /// Метод отрисовки конкретизирующей части сигнала на общем сигнале
        /// </summary>
        /// <param name="canvas">Канвас, на котором и будет проходить отрисовка</param>
        /// <param name="startIndex">Стартовый индек отрисовочных конкретных данных</param>
        /// <param name="endIndex">Финишный индек отрисовочных конкретных данных</param>
        /// <param name="stringSamples">Сэмплов в строке</param>
        /// <param name="startHeight">Начальное отклонение графика по координате Y</param>
        /// <returns></returns>
        private SKPath DrawConcretLine(SKCanvas canvas, int startIndex, int endIndex, int startGeneralIndex, int endGeneralIndex, ushort stringSamples,
            ushort startHeight)
        {
            SKPath renderedConcretLineECG = new SKPath();

            int localGeneralStartIndex = startGeneralIndex;
            int localGeneralEndIndex = startGeneralIndex;

            if (this.CurrentMode == ViewMode.AnimationMode)
            {
                //Расчитываем стартовый индекс данных, которые нужно отрисовать
                if ((endGeneralIndex - startGeneralIndex) >= (this.CountString * stringSamples))
                    localGeneralStartIndex += stringSamples;
            }
            else
            {
                int tmpStartIndex = endGeneralIndex - (this.CountString * stringSamples);
                if (tmpStartIndex < 0)
                {
                    localGeneralStartIndex = 0;
                    localGeneralEndIndex = this.CountString * stringSamples;
                }
                else
                {
                    localGeneralStartIndex = tmpStartIndex;
                    localGeneralEndIndex = endGeneralIndex;
                }

            }

            localGeneralEndIndex = localGeneralEndIndex - localGeneralStartIndex;
            int localConcretEndIndex = endIndex - localGeneralStartIndex;
            int localConcretStartIndex = localConcretEndIndex - this.NumConcretSeconds * this.SamplingFrequancy;

            if (localConcretStartIndex < 0) localConcretStartIndex = 0;

            uint stringShift = (uint)(localConcretStartIndex / stringSamples) * this.HeightStep;

            ///ТУТ проблема...
            ///Научись отрисовывать красную линию вконце сохранённого участка

            renderedConcretLineECG.MoveTo((float)((localConcretStartIndex % stringSamples) * this.TimeStepSize),
                (float)(stringShift + startHeight - LinkBuffersList[CurrentLead][localGeneralStartIndex + localConcretStartIndex] * this.HeightCorrCoef));



            for (int i = localConcretStartIndex + 1; i < localConcretEndIndex; i++)
            {
                if (i != 0 && i % stringSamples == 0)
                {
                    //Косяк отрисовки для нескольких линий
                    stringShift += this.HeightStep;
                    renderedConcretLineECG.MoveTo(0,
                        (float)(stringShift + startHeight - LinkBuffersList[CurrentLead][i + localGeneralStartIndex] * this.HeightCorrCoef));
                }
                renderedConcretLineECG.LineTo((float)((i % stringSamples) * this.TimeStepSize),
                    (float)(stringShift + startHeight - LinkBuffersList[CurrentLead][i + localGeneralStartIndex] * this.HeightCorrCoef));
            }
            return renderedConcretLineECG;
        }

        private string CalculateECGIndexTime(int curIndex)
        {
            uint numMillis = (uint)(curIndex * 1000 / this.SamplingFrequancy);
            DateTime localTime = new DateTime();
            localTime = localTime.AddMilliseconds(numMillis);
            string resultTime = localTime.ToString("HH:mm:ss.fff");
            return resultTime;
        }

        private float CalculatePresentationSize(int canvasHeight)
        {
            return canvasHeight * 4 / 100;
        }

        private void DrawEcgGeneralTime(SKCanvas canvas, int canvasWidth, int canvasHeight, float textSize, string startTimeText, string endTimeText)
        {
            SKPaint startTime = new SKPaint();
            startTime.TextSize = textSize;
            startTime.IsAntialias = true;
            startTime.Color = SKColors.White;
            startTime.IsStroke = false;
            startTime.TextAlign = SKTextAlign.Left;
            startTime.FakeBoldText = true;
            canvas.DrawText(startTimeText, 0, (float)(canvasHeight * 0.01 + startTime.TextSize), startTime);

            SKPaint endTime = new SKPaint();
            endTime.TextSize = textSize;
            endTime.IsAntialias = true;
            endTime.Color = SKColors.White;
            endTime.IsStroke = false;
            endTime.TextAlign = SKTextAlign.Right;
            endTime.FakeBoldText = true;
            canvas.DrawText(endTimeText, canvasWidth, canvasHeight - startTime.TextSize - 20, endTime);
        }

        /// <summary>
        /// Метод добавления сформированных путей ЭКГ на канву
        /// </summary>
        /// <param name="canvas">Канва для отрисовки</param>
        /// <param name="generalPath">Линии общего вида сигнала ЭКГ</param>
        /// <param name="concretPath">Линия конкретного участка экг</param>
        private void DrawSignal(SKCanvas canvas, SKPath path, SKColor pathColor, float pathWidth = 5)
        {
            using (SKPaint generalPaint = new SKPaint())
            {
                generalPaint.Style = SKPaintStyle.Stroke;
                generalPaint.Color = pathColor;
                generalPaint.StrokeWidth = pathWidth;
                canvas.DrawPath(path, generalPaint);
            }
        }

        private void DrawLines(SKCanvas canvas, SKColor pathColor, float pathWidth = 5)
        {
            SKPath lineUp = new SKPath();
            lineUp.MoveTo(0, 0);
            lineUp.LineTo(this.WidthGeneralCanvas, 0);

            SKPath lineDown = new SKPath();
            lineUp.MoveTo(0, this.HeightGeneralCanvas);
            lineUp.LineTo(this.WidthGeneralCanvas, this.HeightGeneralCanvas);

            using (SKPaint generalPaint = new SKPaint())
            {
                generalPaint.Style = SKPaintStyle.Stroke;
                generalPaint.Color = pathColor;
                generalPaint.StrokeWidth = 20;
                canvas.DrawPath(lineDown, generalPaint);
            }
            using (SKPaint generalPaint = new SKPaint())
            {
                generalPaint.Style = SKPaintStyle.Stroke;
                generalPaint.Color = pathColor;
                generalPaint.StrokeWidth = 20;
                canvas.DrawPath(lineUp, generalPaint);
            }
        }

        private int CorrectDrawnBatch(int sizeReadyData)
        {
            if (this.CurrentMode == ViewMode.ControlMode) return 0;
            if (sizeReadyData <= 0) return 0;
            if (sizeReadyData > 1000) return 20;
            if (sizeReadyData > 500) return 15;
            if (sizeReadyData > 200) return 9;
            if (sizeReadyData > 50) return 5;
            //Обработка конца отрисовки
            int lastPartSize = this.NumDrawsSamples - this.CurrentDataIndex;
            if (lastPartSize > 0 && lastPartSize < this.SamplingFrequancy)
                return lastPartSize;
            return 0;
        }

        private bool WasMove = false;
        // Информация о touch событиях
        Dictionary<long, SKPoint> touchDictionary = new Dictionary<long, SKPoint>();
        void OnTouchEffectAction(object sender, TouchActionEventArgs args)
        {
            var viewPoint = args.Location;
            SKPoint point =
                new SKPoint((float)(this.GeneralCanvas.CanvasSize.Width * viewPoint.X / this.GeneralCanvas.Width),
                            (float)(this.GeneralCanvas.CanvasSize.Height * viewPoint.Y / this.GeneralCanvas.Height));

            var actionType = args.Type;

            

            switch (args.Type)
            {
                case TouchActionType.Pressed:
                    if (!touchDictionary.ContainsKey(args.Id))
                    {
                        touchDictionary.Add(args.Id, point);
                    }
                    break;
                case TouchActionType.Moved:
                    if (touchDictionary.ContainsKey(args.Id))
                    {
                        // Single-finger drag
                        if (touchDictionary.Count == 1)
                        {
                            SKPoint prevPoint = touchDictionary[args.Id];
                            if (isTouchAction(prevPoint, point))
                            {

                            }
                            else {
                                if (!CheckLastIndex() || this.CurrentMode == ViewMode.ControlMode)
                                {
                                    this.WasMove = true;
                                    int deltaX = (int)(prevPoint.X - point.X);
                                    int resultMove = this.CurrentConcretDataIndex + deltaX;
                                    if (resultMove > this.NumDrawsSamples)
                                        this.CurrentConcretDataIndex = this.NumDrawsSamples;
                                    else
                                    {
                                        if (resultMove <= this.NumConcretSeconds * this.SamplingFrequancy)
                                            this.CurrentConcretDataIndex = this.NumConcretSeconds * this.SamplingFrequancy;
                                        else
                                            this.CurrentConcretDataIndex += deltaX;
                                    }


                                    int deltaY = (int)(prevPoint.Y - point.Y);
                                    if (Math.Abs(deltaY) > 200)
                                    {
                                        int resultGeneralVerticalMove = this.CurrentDataIndex + Math.Sign(deltaY) * this.SamplingFrequancy * this.NumSecondsString;
                                        if (resultGeneralVerticalMove >= this.NumDrawsSamples)
                                        {
                                            this.CurrentDataIndex = this.NumDrawsSamples - 1;
                                        }
                                        else
                                        {
                                            if (resultGeneralVerticalMove <= this.SamplingFrequancy * this.NumSecondsString * this.CountString)
                                            {
                                                this.CurrentDataIndex = this.SamplingFrequancy * this.NumSecondsString * this.CountString;
                                            }
                                            else
                                            {
                                                this.CurrentDataIndex = resultGeneralVerticalMove;
                                            }
                                        }
                                        /*
                                        int resultConcretVerticalMove = this.CurrentConcretDataIndex + Math.Sign(deltaY) * this.SamplingFrequancy * this.NumSecondsString;
                                        int resultGeneralVerticalMove = this.CurrentDataIndex + Math.Sign(deltaY) * this.SamplingFrequancy * this.NumSecondsString;

                                        if (resultGeneralVerticalMove > 0 
                                            && resultGeneralVerticalMove < this.CountString * this.NumSecondsString * this.SamplingFrequancy)
                                        {
                                            this.CurrentConcretDataIndex = this.CountString * this.NumSecondsString * this.SamplingFrequancy;
                                            this.CurrentDataIndex = this.CountString * this.NumSecondsString * this.SamplingFrequancy;
                                        }
                                        else
                                        {
                                            if (resultGeneralVerticalMove >= this.NumDrawsSamples)
                                            {
                                                this.CurrentConcretDataIndex = this.NumDrawsSamples - 1;
                                                this.CurrentDataIndex = this.NumDrawsSamples - 1;
                                            }
                                            else
                                            {
                                                this.CurrentConcretDataIndex = resultConcretVerticalMove;
                                                this.CurrentDataIndex = resultGeneralVerticalMove;
                                            }
                                        }
                                        */
                                    }
                                    this.GeneralCanvas.InvalidateSurface();
                                }
                            }
                        }
                        touchDictionary[args.Id] = point;
                    }
                    break;
                case TouchActionType.Released:
                    //Отпускание пальца
                    if (touchDictionary.ContainsKey(args.Id))
                    {
                        SKPoint prevPoint = touchDictionary[args.Id];
                        if (this.CurrentMode == ViewMode.ControlMode && !this.WasMove)
                        {
                            if (isTouchAction(prevPoint, point))
                            {
                                CalculateTouchPointOnSignal(point);
                                this.GeneralCanvas.InvalidateSurface();
                            }
                        }
                        this.WasMove = false;
                        touchDictionary.Remove(args.Id);
                    }
                    break;

                case TouchActionType.Cancelled:
                    if (touchDictionary.ContainsKey(args.Id))
                    {
                        touchDictionary.Remove(args.Id);
                    }
                    break;
            }
        }


        void CalculateTouchPointOnSignal(SKPoint touchPoint)
        {
            double correctXCoef = ((double)(this.NumSecondsString * this.SamplingFrequancy)) / this.WidthGeneralCanvas;
            uint shiftX = (uint)(touchPoint.X * correctXCoef);
            byte chosenString = (byte)(touchPoint.Y / this.HeightStep);
            uint shiftY = chosenString * (uint)(this.NumSecondsString * this.SamplingFrequancy);

            this.CurrentConcretDataIndex = (int)(this.StartIndexDrawnData + shiftX + shiftY + (this.NumConcretSeconds * this.SamplingFrequancy) / 2);
            if (this.CurrentConcretDataIndex > this.NumDrawsSamples)
                this.CurrentConcretDataIndex = this.NumDrawsSamples;
            else
            {
                if (this.CurrentConcretDataIndex < 0)
                    this.CurrentConcretDataIndex = this.NumConcretSeconds * this.SamplingFrequancy;
            }
        }

        bool isTouchAction(SKPoint fPoint, SKPoint sPoint)
        {
            double distance = Math.Sqrt(Math.Pow(fPoint.X - sPoint.X, 2) + Math.Pow(fPoint.Y - sPoint.Y, 2));
            float curMaxCanvasSize = 0;
            if (this.HeightGeneralCanvas > this.WidthGeneralCanvas)
                curMaxCanvasSize = this.HeightGeneralCanvas;
            else
                curMaxCanvasSize = this.WidthGeneralCanvas;
            double errorMovePercent = 0.01;
            if (errorMovePercent * curMaxCanvasSize >= distance)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Метод обратной связи для настройки линии конкретизации на общем графике
        /// </summary>
        /// <param name="newConcretIndex"></param>
        public void SetCurrentConcretDataIndex(int newConcretIndex)
        {
            this.CurrentConcretDataIndex = newConcretIndex;
            this.GeneralCanvas.InvalidateSurface();
        }

        public void Dispose()
        {
            if (this.GeneralTouchTracking != null)
            {
                this.GeneralTouchTracking.TouchAction -= OnTouchEffectAction;
            }
        }
    }
}
