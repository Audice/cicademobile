﻿using FenixMobile.Interfaces;
using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using FenixMobile.Model.BluetoothPart.DataConvert;
using FenixMobile.Model.DrawPart;
using FenixMobile.Model.DrawPart.DrawPartEventArgs;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TouchTracking;
using TouchTracking.Forms;

namespace CyberHeartMobile.Classes
{
    public enum ViewMode { AnimationMode, ControlMode }
    public enum DisplayOrientation { VerticalOrientation, HorizontalOrientation }
    public class SkiaPaintController : IDisposable
    {
        /// <summary>
        /// Буфер для хранения сигнала, предназначенный для отрисовки ЭКГ
        /// </summary>
        private List<List<short>> BuffersList
        {
            get; set;
        } = null;


        private int MaxCountData = 10;

        private byte CurLead = 0;

        /// <summary>
        /// Поле-ссылка на класс, предоставляющий доступ к сигналам ЭКГ
        /// </summary>
        DataConverter DataConverter { get; set; }


        /// <summary>
        /// Класс-контроллер общего представления сигнала
        /// </summary>
        GeneralViewDrawer GeneralViewDrawer { get; set; }

        //Остановка ЭКГ
        #region
        bool isECGStreamStop = false;
        public bool IsECGStreamStop
        {
            get { return isECGStreamStop; }
            private set
            {
                isECGStreamStop = value;
                DataFilledEvent?.Invoke(this, EventArgs.Empty);
            }
        }
        public EventHandler<EventArgs> DataFilledEvent;
        #endregion

        public SkiaPaintController(SKCanvasView concretCanvas, SKCanvasView generalCanvas, DataConverter dataConverter, 
            TouchEffect generalTouchTracking, TouchEffect concretTouchTracking, DisplayOrientation displayOrientation,
            string cutedName, string bornDate, string growth, string weight, 
            string dateAnalisys, ECGreceiveMode receiveMode, ViewMode viewMode, short sampleFreq, uint channelsNum)
        {
            this.DisplayOrientation = displayOrientation;
            if (viewMode == ViewMode.AnimationMode)
            {
                this.MaxCountData = (int)(sampleFreq * (uint)receiveMode);
            }
            else
            {
                this.MaxCountData = -1;
            }
            this.GeneralViewDrawer = new GeneralViewDrawer(generalCanvas, concretCanvas, generalTouchTracking, concretTouchTracking, sampleFreq, 
                this.MaxCountData, viewMode);
            this.SetDisplayOrientation(displayOrientation);
            this.DataConverter = dataConverter;
            this.SamplingFrequancy = sampleFreq;


            //Заполняем информацию о пациенте
            this.CutedName = cutedName;
            this.BornDate += bornDate;
            this.Growth += growth;
            this.Weight += weight;
            this.DateAnalisys = dateAnalisys;


            if (dataConverter != null)
            {
                //Событие при появлении нового пакета для отрисовки
                this.DataConverter.SignalsConvertedEvent += ReceiveNewDrawPart;
                IsDrawEnd = false;
            }
            else
                IsDrawEnd = true;

        }

        public bool IsDrawECGStart
        {
            get; private set;
        } = false;

        public DisplayOrientation DisplayOrientation
        {
            get; private set;
        }

        public void SetDisplayOrientation(DisplayOrientation displayOrientation)
        {
            this.DisplayOrientation = displayOrientation;
            if (this.GeneralViewDrawer != null) this.GeneralViewDrawer.SetGenetalDisplayOrientation(displayOrientation);
        }

        /// <summary>
        /// Выделение памяти под каналы ЭКГ
        /// </summary>
        /// <param name="countPath">Количество отведений в ЭКГ</param>
        void InitLocalDataBuffers(int countPath)
        {
            this.BuffersList = new List<List<short>>();
            for (int i = 0; i < countPath; i++)
            {
                //Для каждого отведения выделяем память под данные
                this.BuffersList.Add(new List<short>());
            }
            //Предоставляем ссылку на данные буфера классам отрисовки
            SetDataReference();
        }
        /// <summary>
        /// Метод предоставления ссылки на данные буфера классам отрисовки
        /// </summary>
        void SetDataReference()
        {
            this.GeneralViewDrawer.SetDataReference(this.BuffersList);
        }

        void ReceiveNewDrawPart(object sender, ConvertedSignalEventArgs e)
        {
            List<short[]> data = e.Signals;
            if (data != null && data.Count > 0)
            {
                //Если буфер не проинициализирован, то выделяем память и обнуляем
                if (this.BuffersList == null) InitLocalDataBuffers(data.Count);
                for (int i=0; i < data.Count; i++)
                    for (int j = 0; j < data[i].Length; j++)
                        BuffersList[i].Add(data[i][j]);

                CheckDataFilled();

                if (!IsDrawECGStart && !this.IsDrawEnd) //??
                {
                    IsDrawECGStart = true;
                    this.GeneralViewDrawer.StartAnimate();
                }
            }
        }

        void CheckDataFilled()
        {
            if (this.MaxCountData < this.BuffersList[CurLead].Count)
            {
                IsECGStreamStop = true;
                this.GeneralViewDrawer.StopAnimate();
            }
        }




        public void SetECGData(List<List<short>> newData)
        {
            BuffersList = newData;
            SetDataReference();
            this.GeneralViewDrawer.SetNumDrawsSamples(BuffersList[CurLead].Count);
            this.GeneralViewDrawer.DrawECGLine();
        }

        int RecomendedSize(List<List<short>> data)
        {
            List<int> sizes = new List<int>();
            for (int i = 0; i < data.Count; i++)
                sizes.Add(data[i].Count);
            return sizes.Min();
        }
        

        //Информаия о пациенте
        #region
        /// <summary>
        /// Имя обследуемого
        /// </summary>
        private string CutedName = "";
        /// <summary>
        /// Дата рождения пациента
        /// </summary>
        private string BornDate = "Дата рожд. ";
        /// <summary>
        /// Рост пациента
        /// </summary>
        private string Growth = "Рост: ";
        /// <summary>
        /// Вес пациента
        /// </summary>
        private string Weight = "Вес: ";
        /// <summary>
        /// Дата и время анализа
        /// </summary>
        private string DateAnalisys = "";
        /// <summary>
        /// Размер текста, рачитанный по максимальному количеству символов
        /// </summary>
        private float? TextSize = null;
        /// <summary>
        /// Масштабирующий коэфицент для сжатия текста
        /// </summary>
        private float ScaleCoef = 0.5f;
        #endregion


        /// <summary>
        /// Максимальное значение для индексатора визуализиремых данных
        /// </summary>
        public int CounterMaxValue
        {
            get;
            private set;
        }
        public bool IsDrawEnd
        {
            get;
            set;
        }
        public void SetEndDrawFlag()
        {
            IsDrawEnd = true;
        }
        /// <summary>
        /// Изменение максимального индекса данных для отрисовки
        /// </summary>
        /// <param name="maxValue">Новое значение максимального индекса</param>
        public void SetCounterMaxValue(int maxValue)
        {
            this.CounterMaxValue = maxValue;
        }
        public int Counter
        {
            get; set;
        }
        public int WaitTimeBetwienDraw
        {
            get; private set;
        }
        public int CountDrawData
        {
            get; private set;
        }

        //Private fields
        private short SamplingFrequancy = -1;


        
        
        private float CalculateTextSize(int canvasWidth, int canvasHeight)
        {
            int headCanvasSize = canvasWidth >= canvasHeight ? canvasHeight : canvasWidth;
            int sizeCutedName = this.CutedName.Length;
            string headSymbolString = this.CutedName.Length >= this.BornDate.Length ? this.CutedName : this.BornDate;
            SKPaint textPaint = new SKPaint();
            float textWidth = textPaint.MeasureText(headSymbolString);
            return 0.5f * headCanvasSize * textPaint.TextSize / textWidth;
        }

/*
        

        private void DrawPatientInformation(SKCanvas canvas, int canvasWidth, int canvasHeight, float textSize)
        {
            if (this.TextSize != null)
            {
                SKPaint cutedNamePaint = new SKPaint();
                cutedNamePaint.TextSize = this.TextSize.Value;
                cutedNamePaint.IsAntialias = true;
                cutedNamePaint.Color = SKColors.Black;
                cutedNamePaint.IsStroke = false;
                cutedNamePaint.TextAlign = SKTextAlign.Center;
                cutedNamePaint.FakeBoldText = true;
                cutedNamePaint.TextScaleX = this.ScaleCoef;
                canvas.DrawText(this.CutedName, canvasWidth / 4, cutedNamePaint.TextSize * 1.5f, cutedNamePaint);

                SKPaint dateBornPaint = new SKPaint();
                dateBornPaint.TextSize = this.TextSize.Value;
                dateBornPaint.IsAntialias = true;
                dateBornPaint.Color = SKColors.Black;
                dateBornPaint.IsStroke = false;
                dateBornPaint.TextAlign = SKTextAlign.Center;
                dateBornPaint.FakeBoldText = true;
                dateBornPaint.TextScaleX = this.ScaleCoef;
                canvas.DrawText(this.BornDate, 3f * canvasWidth / 4, dateBornPaint.TextSize, dateBornPaint);

                SKPaint growthWeightPaint = new SKPaint();
                growthWeightPaint.TextSize = this.TextSize.Value;
                growthWeightPaint.IsAntialias = true;
                growthWeightPaint.Color = SKColors.Black;
                growthWeightPaint.IsStroke = false;
                growthWeightPaint.TextAlign = SKTextAlign.Center;
                growthWeightPaint.FakeBoldText = true;
                growthWeightPaint.TextScaleX = this.ScaleCoef;
                canvas.DrawText(this.Growth + "  " + this.Weight, 3f * canvasWidth / 4, 2 * dateBornPaint.TextSize, dateBornPaint);

                SKPaint dateAnalisysPaint = new SKPaint();
                dateAnalisysPaint.TextSize = this.TextSize.Value;
                dateAnalisysPaint.IsAntialias = true;
                dateAnalisysPaint.Color = SKColors.Black;
                dateAnalisysPaint.IsStroke = false;
                dateAnalisysPaint.TextAlign = SKTextAlign.Center;
                dateAnalisysPaint.FakeBoldText = true;
                dateAnalisysPaint.TextScaleX = this.ScaleCoef;
                canvas.DrawText(this.DateAnalisys, canvasWidth / 2, (float)canvasHeight, dateAnalisysPaint);

            }
        }


        /*
        private void DrawECGNetwork(SKCanvas canvas, int currentWidth, int currentHeight, short samplingFrequancy)
        {
            //Calculate network step
            int maxSeconds = currentWidth / samplingFrequancy;
            if (maxSeconds < 1)
                maxSeconds = 1;
            maxSeconds = maxSeconds * 1000; //Transform seconds to milliseconds
            float sizeMillisecond = (float)currentWidth / maxSeconds;

            using (SKPaint paint = new SKPaint())
            {
                paint.Style = SKPaintStyle.Stroke;
                paint.Color = SKColors.LightGray;
                paint.StrokeWidth = 1;
                int countMilliseconds = 0;
                //Draw vertical line
                for (float i = 0; i <= currentWidth; i += SizeSmallDropcost * sizeMillisecond)
                {
                    if (countMilliseconds % SizeLargeDropCost == 0)
                        paint.StrokeWidth = 2;
                    else
                        paint.StrokeWidth = 1;
                    canvas.DrawLine(i, 0, i, currentHeight, paint);
                    countMilliseconds += SizeSmallDropcost;
                }
                //Draw horizontal line
                countMilliseconds = 0;
                for (float i = 0; i <= currentHeight; i += SizeSmallDropcost * sizeMillisecond)
                {
                    if (countMilliseconds % SizeLargeDropCost == 0)
                        paint.StrokeWidth = 2;
                    else
                        paint.StrokeWidth = 1;
                    canvas.DrawLine(0, i, currentWidth, i, paint);
                    countMilliseconds += SizeSmallDropcost;
                }
            }
        }
        */






        public void Dispose()
        {
            if (this.DataConverter != null)
            {
                this.DataConverter.SignalsConvertedEvent -= ReceiveNewDrawPart;
            }
            if (this.GeneralViewDrawer != null)
            {
                this.GeneralViewDrawer.Dispose();
            }
        }
    }
}

