﻿using CyberHeartMobile.Classes;
using FenixMobile.Model.ActivityModel;
using FenixMobile.Model.ConclusionPart;
using FenixMobile.Model.ConclusionPart.ConclusionEventArgs;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Text;
using TouchTracking.Forms;

namespace FenixMobile.Model.DrawPart
{
    public class TrendsViewController : IDisposable
    {
        /// <summary>
        /// Список трендов. По первому индексу - RR, по второму: PP
        /// </summary>
        private List<List<double>> Trends;
        /// <summary>
        /// Массив хранения активностей
        /// </summary>
        private List<List<ActivityMode>> Activity;
        /// <summary>
        /// Массив хранения сырой активностей
        /// </summary>
        private List<ActivityMode> RawActivity;

        private ConclusionModel ConclusionModel;

        private TrendsViewDrawer TrendsViewDrawer { get; set; }

        private AccelerometerWorker AccelerometerWorker = null;

        private ScaterogrammViewDrawer ScaterogrammViewDrawer { get; set; }

        private ActivityViewDrawer ActivityViewDrawer { get; set; }

        public TrendsViewController(ConclusionModel conclusionModel)
        {
            this.Trends = new List<List<double>>();
            this.Activity = new List<List<ActivityMode>>();
            this.RawActivity = new List<ActivityMode>();
            this.AccelerometerWorker = new AccelerometerWorker();

            this.AccelerometerWorker.AccelerometerValueChange += CollectActivity;
            this.AccelerometerWorker.ToggleAccelerometer();

            this.ConclusionModel = conclusionModel;
            if (this.ConclusionModel != null)
                this.ConclusionModel.TrendsPartReady += CollectTrends;
        }

        private void CollectActivity(object sender, AccelerometerEventArgs e)
        {
            if (Activity == null) return;
            this.RawActivity.Add(e.ActivityMode);
        }

        public void SetCanvas(SKCanvasView trendsCanvas, TouchEffect trendsTouchTracking, 
            int numDrawsSamples, ViewMode currentMode)
        {
            this.TrendsViewDrawer = new TrendsViewDrawer(trendsCanvas, trendsTouchTracking, numDrawsSamples, currentMode);
            this.TrendsViewDrawer.SetDataReference(this.Trends);
        }

        public void SetScaterogrammCanvas(SKCanvasView scaterogrammCanvas,
            int numDrawsSamples, ViewMode currentMode)
        {
            this.ScaterogrammViewDrawer = new ScaterogrammViewDrawer(scaterogrammCanvas, numDrawsSamples, currentMode);
            this.ScaterogrammViewDrawer.SetDataReference(this.Trends);
        }

        public void SetActivityCanvas(SKCanvasView activityCanvas, TouchEffect activityTouchTracking,
    int numDrawsSamples, ViewMode currentMode)
        {
            this.ActivityViewDrawer = new ActivityViewDrawer(activityCanvas, activityTouchTracking, numDrawsSamples, currentMode);
            this.ActivityViewDrawer.SetDataReference(this.Activity);
        }



        public void SetScaterogrammFocus()
        {
            if (this.ScaterogrammViewDrawer != null)
            {
                this.ScaterogrammViewDrawer.SetFocus(true);
            }
        }
        public void SetScaterogrammUnfocus()
        {
            if (this.ScaterogrammViewDrawer != null)
            {
                this.ScaterogrammViewDrawer.SetFocus(false);
            }
        }

        private readonly ushort CountSeconds = 600;

        private void CollectTrends(object sender, TrendsEventArgs e)
        {
            if (e != null && e.PP_Trends != null && e.RR_Trends != null)
            {
                if (this.Trends.Count == 0) InitTrendsList();
                if (this.Trends.Count == 2)
                {
                    this.Trends[0].AddRange(e.RR_Trends);
                    this.Trends[1].AddRange(e.PP_Trends);
                    if (this.RawActivity.Count != 0)
                    {
                        this.Activity.Add(new List<ActivityMode>(this.RawActivity));
                        this.RawActivity.Clear();
                    }
                }
            }
            //Удаляем данные, если их накопилось более 10 минут
            RegulatesData();
        }

        void RegulatesData() {
            if (this.Trends != null && this.Trends.Count == 2)
            {
                if (this.Trends[0].Count > this.CountSeconds)
                    this.Trends[0].RemoveRange(0, this.Trends[0].Count - this.CountSeconds);
                if (this.Trends[1].Count > this.CountSeconds)
                    this.Trends[1].RemoveRange(0, this.Trends[1].Count - this.CountSeconds);
            }
            if (this.Activity != null && this.Activity.Count > (this.CountSeconds / 10))
                this.Activity.RemoveRange(0, this.Activity.Count - (this.CountSeconds / 10));
        }

        public void SetTrendsDisplayOrientation(DisplayOrientation displayOrientation)
        {
            if (this.TrendsViewDrawer != null)
            {
                this.TrendsViewDrawer.SetDisplayOrientation(displayOrientation);
                this.ActivityViewDrawer.SetDisplayOrientation(displayOrientation);
            }
        }

        public void SetScaterogrammDisplayOrientation(DisplayOrientation displayOrientation)
        {
            if (this.ScaterogrammViewDrawer != null)
            {
                this.ScaterogrammViewDrawer.SetDisplayOrientation(displayOrientation);
            }
        }

        private void InitTrendsList()
        {
            if (this.Trends.Count == 0)
            {
                this.Trends.Add(new List<double>()); //Добавляем список под RR
                this.Trends.Add(new List<double>()); //Добавляем список под PP
                if (this.TrendsViewDrawer != null)
                {
                    this.TrendsViewDrawer.SetDataReference(this.Trends);
                }
                if (this.ScaterogrammViewDrawer != null)
                {
                    this.ScaterogrammViewDrawer.SetDataReference(this.Trends);
                }
            }
        }

        public void Dispose()
        {
            if (this.ConclusionModel != null)
                this.ConclusionModel.TrendsPartReady -= CollectTrends;

            this.AccelerometerWorker.ToggleAccelerometer();
            this.AccelerometerWorker.AccelerometerValueChange -= CollectActivity;
        }
    }
}
