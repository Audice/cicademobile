﻿using CyberHeartMobile.Classes;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Text;
using TouchTracking;
using TouchTracking.Forms;

namespace FenixMobile.Model.DrawPart
{
    public class TrendsViewDrawer : ContentDrawer<double>, IDisposable
    {
        /// <summary>
        /// Максимальное количество секунд изображаемого интервала
        /// </summary>
        readonly byte CountBoldSeconds = 3;
        /// <summary>
        /// На сколько нужно поделить секунду
        /// </summary>
        readonly byte SecondsDrop = 4;
        /// <summary>
        /// На сколько нужно поделить секунду
        /// </summary>
        readonly byte VerticalSecondsDrop = 10;
        /// <summary>
        /// Отступ от временной оси
        /// </summary>
        private short TimeAxisOffset = 50;

        /// <summary>
        /// Ссылка на канвас для отрисовки
        /// </summary>
        protected SKCanvasView ContentCanvas { get; set; }
        /// <summary>
        /// Ссылка на объект, отслеживающий нажатие и тип нажатия на конкретный контейнер
        /// </summary>
        protected TouchEffect ContentTouchTracking { get; set; }


        private readonly byte CountTrends = 2;
        public TrendsViewDrawer(SKCanvasView trendsCanvas, TouchEffect concretTouchTracking, int numDrawsSamples, ViewMode currentMode)
        {
            this.NumSecondsString = 100;
            this.CurrentMode = currentMode;
            if (this.CurrentMode == ViewMode.ControlMode)
            {
                SetNumDrawsSamples(numDrawsSamples);
            }
            this.ContentCanvas = trendsCanvas;
            this.ContentCanvas.PaintSurface += OnCanvasViewPaintSurface;

            if (this.ContentTouchTracking != null)
            {
                //this.ContentTouchTracking.TouchAction += OnTouchEffectAction;
            }
        }

        /// <summary>
        /// Метод, срабатывающий при обновлении канвы или вызове InvalidateSurface
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected virtual void OnCanvasViewPaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {
            UpdateCanvas(args);
            if (this.CurrentMode == ViewMode.AnimationMode)
                this.ContentCanvas.InvalidateSurface();
        }

        protected void UpdateCanvas(SKPaintSurfaceEventArgs args)
        {
            SKImageInfo info = args.Info; //Информация о дисплее смартфона. Получаем ширину и высоту канваса именно отсюда
            SKCanvas canvas = args.Surface.Canvas; //Canvas for drawing  
            canvas.Clear(); //Каждое новое обновление - очищаем канву

            //Начинаем расчёт параметров отрисовки. Делаем так только после изменения размеров канвы (при смене ориентации)
            if (info.Width != this.WidthCanvas && info.Height != this.HeightCanvas)
            {
                this.WidthCanvas = info.Width;
                this.HeightCanvas = info.Height;

                this.HeightCorrCoef = HeightCorrectCoefCalc(this.CountBoldSeconds, 0, this.HeightCanvas);
            }
            //Расчитываем размер размаха каждый раз по приходу данных
            if (this.LinkBuffersList != null && this.LinkBuffersList.Count == this.CountTrends 
                && this.LinkBuffersList[0].Count > 0)
            {
                //Определяем новое значение размаха каждый ход
                //UpdateSignalSwing();
                this.StartHeight = 0;
                //Изменили размеры - обновляем параметры
                this.NumSecondsString = LinkBuffersList[0].Count;
                this.TimeStepSize = (double)(this.WidthCanvas - this.TimeAxisOffset - 20) / this.NumSecondsString;
                //Отрисовываем данные, если буфер данных не пуст и существует
                ContentDraw(canvas);
            }
        }

        private double HeightCorrectCoefCalc(double maxValue, double minValue, int curHeight)
        {
            double delta = maxValue;
            return (double)curHeight / maxValue;
        }

        private int CurrentPPDataIndex = 0;

        protected void ContentDraw(SKCanvas canvas)
        {
            try
            {
                if (this.LinkBuffersList != null && LinkBuffersList.Count == this.CountTrends)// && LinkBuffersList[CurrentLead].Count > CurrentDataIndex)
                {
                    //this.StartIndexDrawnData = CalcStartIndex(ref this.CurrentDataIndex, this.NumSecondsString);
                    this.CurrentDataIndex = this.LinkBuffersList[0].Count;
                    this.CurrentPPDataIndex = this.LinkBuffersList[1].Count;
                    SKPoint[] rrTrendLine = ConstructRRTrend(0, ref this.CurrentDataIndex);
                    SKPoint[] ppTrendLine = ConstructPPTrend(0, ref this.CurrentPPDataIndex);

                    //Отрисовка времени
                    DrawGrid(canvas);

                    //Добавление линии RR тренда на канвас
                    this.DrawPoints(canvas, rrTrendLine, SKColors.Green, 20);
                    this.DrawPoints(canvas, ppTrendLine, SKColors.Gold, 15);
                    //Добавление линии PP тренда на канвас
                    //this.DrawData(canvas, ppTrendLine, SKColors.Red, 5);

                    if (isConcretTouch)
                    {
                        //TODO проверить и доделать
                        isConcretTouch = false;
                        this.DrawCloude(canvas, touchPoint);
                    }
                }
            }
            catch (Exception ex)
            {
                string exM = ex.Message;
            }
        }

        


        double GridTimeStep = 0.1;
        

        private void DrawGrid(SKCanvas canvas)
        {
            SKPath renderedVerticalLine = new SKPath();
            SKPath renderedBoldLine = new SKPath();
            SKPath renderedThiknessLine = new SKPath();
            short startHeightDrawPosition = (short)(this.HeightCanvas - 10); //Стартовая позиция отрисовки сетки
            short startWidthDrawPosition = 10; //Стартовая позиция отрисовки сетки

            short usedHeight = (short)(this.HeightCanvas - 20);
            short usedWidth = (short)(this.WidthCanvas - 2 * startWidthDrawPosition);

            byte countHorizontLine = (byte)(this.SecondsDrop * this.CountBoldSeconds);
            short stepMilliSecondsLine = (short)(usedHeight / (countHorizontLine));

            byte countVerticalLine = 4;
            short stepVerticalLine = (short)((double)(usedWidth - this.TimeAxisOffset) / countVerticalLine);

            //Отрисовка вертикальных линий
            for (byte i = 0; i <= countVerticalLine; i++)
            {
                renderedVerticalLine.MoveTo(this.TimeAxisOffset + i * stepVerticalLine, startHeightDrawPosition);
                renderedVerticalLine.LineTo(this.TimeAxisOffset + i * stepVerticalLine, startHeightDrawPosition - usedHeight);
            }

            //Отрисовка горизонтальной части
            for (byte i = 0; i <= countHorizontLine; i++)
            {
                if (i % this.SecondsDrop == 0)
                {
                    renderedBoldLine.MoveTo(startWidthDrawPosition, startHeightDrawPosition - i * stepMilliSecondsLine);
                    renderedBoldLine.LineTo(startWidthDrawPosition + usedWidth, startHeightDrawPosition - i * stepMilliSecondsLine);
                    if (i != 0)
                        DrawText(((ushort)(i / this.SecondsDrop) * 1000).ToString(), canvas, SKColors.White,
                            startWidthDrawPosition, startHeightDrawPosition - i * stepMilliSecondsLine, CalculatePresentationSize(this.HeightCanvas));
                }
                else
                {
                    renderedThiknessLine.MoveTo(startWidthDrawPosition, startHeightDrawPosition - i * stepMilliSecondsLine);
                    renderedThiknessLine.LineTo(startWidthDrawPosition + usedWidth, startHeightDrawPosition - i * stepMilliSecondsLine);
                    DrawText(((ushort)(((double)i / this.SecondsDrop)*1000)).ToString(), canvas, SKColors.White,
                        startWidthDrawPosition, startHeightDrawPosition - i * stepMilliSecondsLine, CalculatePresentationSize(this.HeightCanvas));
                }
            }


            this.DrawData(canvas, renderedBoldLine, SKColors.White, 5f);
            this.DrawData(canvas, renderedThiknessLine, SKColors.White, 2f);
            this.DrawData(canvas, renderedVerticalLine, SKColors.White, 5f);
        }

        private float CalculatePresentationSize(int canvasHeight)
        {
            return canvasHeight * 4 / 100;
        }

        private void DrawText(string text, SKCanvas canvas, SKColor color, float xCoord, float yCoord, float textSize)
        {
            /*
            SKMatrix textMatrix = SKMatrix.MakeIdentity();
            canvas.Concat(ref textMatrix);

            // Rotate this by the amount the canvas has rotated as we're about to reverse that back
            // this will ensure the locations are correct
            SKMatrix rotation = SKMatrix.MakeRotation((float)(45.0 * Math.PI / 180));
            SKMatrix.PreConcat(ref textMatrix, rotation);

            // Undo the canvas rotation so text is aligned correctly.
            canvas.RotateRadians(-(float)(45.0 * Math.PI / 180));
            */

            SKPaint paintedText = new SKPaint();
            paintedText.TextSize = textSize;
            paintedText.IsAntialias = true;
            paintedText.Color = SKColors.White;
            paintedText.IsStroke = false;
            paintedText.TextAlign = SKTextAlign.Left;
            paintedText.FakeBoldText = true;
            canvas.DrawText(text, xCoord, (float)(yCoord + paintedText.TextSize), paintedText);
            this.TimeAxisOffset = (short)(paintedText.TextSize * (text.Length - 1));

            //var projected_point = textMatrix.MapPoint(xCoord, (float)(yCoord + paintedText.TextSize));
            //canvas.DrawText(text, projected_point, paintedText);

            // Restore canvas back to its previous state before this (original rotation/scale/translation etc)
            //canvas.Restore();
        }


        public int CalcStartIndex(ref int currentIndex, int secondsString)
        {
            int index = currentIndex - secondsString;
            if (index < 0)
            {
                index = 0;
            }
            else
            {
                currentIndex = index + secondsString;
            }
            return index;
        }

        private SKPoint[] ConstructRRTrend(int startDataIndex, ref int endDataIndex)
        {
            short startWidthDrawPosition = this.TimeAxisOffset; //Стартовая позиция отрисовки сетки
            short startHeightDrawPosition = (short)(this.HeightCanvas - 10); //Стартовая позиция отрисовки сетки
            SKPath renderedLine = new SKPath();

            renderedLine.MoveTo(startWidthDrawPosition, (float)(startHeightDrawPosition - this.HeightCorrCoef * this.LinkBuffersList[0][startDataIndex]));
            for (int i= 0; i < endDataIndex; i++)
            {
                renderedLine.LineTo((float)(startWidthDrawPosition + i * this.TimeStepSize), 
                    (float)(startHeightDrawPosition - this.HeightCorrCoef * this.LinkBuffersList[0][i + startDataIndex]));
            }
            return renderedLine.GetPoints(renderedLine.PointCount);
        }

        private SKPoint[] ConstructPPTrend(int startDataIndex, ref int endDataIndex)
        {
            short startWidthDrawPosition = this.TimeAxisOffset; //Стартовая позиция отрисовки сетки
            short startHeightDrawPosition = (short)(this.HeightCanvas - 10); //Стартовая позиция отрисовки сетки
            SKPath renderedLine = new SKPath();

            renderedLine.MoveTo(startWidthDrawPosition, (float)(startHeightDrawPosition - this.HeightCorrCoef * this.LinkBuffersList[1][startDataIndex]));
            for (int i = 0; i < endDataIndex; i++)
            {
                if (this.LinkBuffersList[1][i + startDataIndex] > 0)
                {
                    renderedLine.LineTo((float)(startWidthDrawPosition + i * this.TimeStepSize),
                        (float)(startHeightDrawPosition - this.HeightCorrCoef * this.LinkBuffersList[1][i + startDataIndex]));
                }
            }
            return renderedLine.GetPoints(renderedLine.PointCount);
        }

        protected override byte CalcNumSecondsString(DisplayOrientation orientation)
        {
            if (orientation == DisplayOrientation.VerticalOrientation)
                return 100;
            else
                return 200;
        }

        public void Dispose()
        {
            this.ContentCanvas.PaintSurface -= OnCanvasViewPaintSurface;

            if (this.ContentTouchTracking != null)
            {
                //this.ContentTouchTracking.TouchAction -= OnTouchEffectAction;
            }
        }



        bool isConcretTouch = false;
        SKPoint touchPoint;
        /*
        protected override void OnTouchEffectAction(object sender, TouchActionEventArgs args)
        {
            var viewPoint = args.Location;
            SKPoint point =
                new SKPoint((float)(this.ContentCanvas.CanvasSize.Width * viewPoint.X / this.ContentCanvas.Width),
                            (float)(this.ContentCanvas.CanvasSize.Height * viewPoint.Y / this.ContentCanvas.Height));

            var actionType = args.Type;



            switch (args.Type)
            {
                case TouchActionType.Pressed:
                    if (!touchDictionary.ContainsKey(args.Id))
                    {
                        touchDictionary.Add(args.Id, point);
                    }
                    break;
                case TouchActionType.Moved:
                    if (touchDictionary.ContainsKey(args.Id))
                    {
                        // Single-finger drag
                        if (touchDictionary.Count == 1)
                        {
                            SKPoint prevPoint = touchDictionary[args.Id];
                            if (isTouchAction(prevPoint, point))
                            {

                            }
                            else
                            {
                                this.WasMove = true;
                                int deltaX = (int)(prevPoint.X - point.X);
                                int resultMove = this.CurrentDataIndex + deltaX;
                                if (resultMove > this.EndIndexDrawnData)
                                    this.CurrentDataIndex = this.EndIndexDrawnData;
                                else
                                {
                                    if (resultMove <= this.NumConcretSeconds)
                                        this.CurrentDataIndex = this.NumConcretSeconds;
                                    else
                                        this.CurrentDataIndex += deltaX;
                                }

                                this.ContentCanvas.InvalidateSurface();
                            }
                        }
                        touchDictionary[args.Id] = point;
                    }
                    break;
                case TouchActionType.Released:
                    //Отпускание пальца
                    if (touchDictionary.ContainsKey(args.Id))
                    {
                        SKPoint prevPoint = touchDictionary[args.Id];
                        if (this.CurrentMode == ViewMode.ControlMode && !this.WasMove)
                        {
                            if (isTouchAction(prevPoint, point))
                            {
                                DrawDataCloud(GetDataIndex(point), point);
                                this.ContentCanvas.InvalidateSurface();
                            }
                        }
                        this.WasMove = false;
                        touchDictionary.Remove(args.Id);
                    }
                    break;

                case TouchActionType.Cancelled:
                    if (touchDictionary.ContainsKey(args.Id))
                    {
                        touchDictionary.Remove(args.Id);
                    }
                    break;
            }
        }
        */
        protected override int GetDataIndex(SKPoint touchPoint)
        {
            int findIndex = (int)(touchPoint.X / this.TimeStepSize);

            if (findIndex + this.StartIndexDrawnData <= this.CurrentDataIndex)
                return findIndex + this.StartIndexDrawnData;
            return -1;
        }

        private void DrawDataCloud(int dataIndex, SKPoint touch)
        {
            if (dataIndex >= 0)
            {
                isConcretTouch = true;
                this.touchPoint = touch;
            }
        }

        private void DrawCloude(SKCanvas canvas, SKPoint touch)
        {
            using (SKPaint generalPaint = new SKPaint())
            {
                generalPaint.Style = SKPaintStyle.Fill;
                generalPaint.Color = SKColors.White;
                generalPaint.StrokeWidth = 10;
                canvas.DrawRoundRect(touch.X, touch.Y, 100, 100, 20, 20, generalPaint);
                
                //canvas.DrawPoints(SKPointMode.Points, dataLine.GetPoints(dataLine.PointCount), generalPaint);
            }
        }

    }
}
