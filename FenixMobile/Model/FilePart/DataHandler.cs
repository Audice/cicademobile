﻿using FenixMobile.Interfaces;
using FenixMobile.Model.BluetoothPart;
using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using FenixMobile.Model.BluetoothPart.DataConvert;
using FenixMobile.Model.BluetoothPart.PackageControl;
using FenixMobile.Model.DatabasePart;
using FenixMobile.Model.FilePart.FileEventArgs;
using FenixMobile.Model.ServerPart;
using FenixMobile.Model.ServerPart.ServerAction;
using FenixMobile.View.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace FenixMobile.Model.FilePart
{
    /// <summary>
    /// Класс-обработчик данных. Занимается сохранением данных, и оповещением классов DataSender и DataParser о появлении новой порции данных
    /// </summary>
    public class DataHandler : IDisposable
    {
        /// <summary>
        /// Локальная копия объекта, обрабатывающие пакеты от bluetooth
        /// </summary>
        DataConverter DataConverter;
        /// <summary>
        /// Локальная ссылка на экземпляр базы данных
        /// </summary>
        DatabaseWorker DatabaseWorker;
        /// <summary>
        /// Текущая запись, характеризующая строку в базе данных
        /// </summary>
        private RecordTable CurrentRecord
        {
            get; set;
        } = null;

        /// <summary>
        /// Номер файла
        /// Служит суфиксом для файлов записи, а также номером части, отправляемой на сервер
        /// </summary>
        ushort FileNum 
        { 
            get; set; 
        } = 0;


        public DataHandler(DataConverter dataConverter)
        {
            this.DatabaseWorker = DatabaseWorker.getInstance();
            this.DataConverter = dataConverter;
            this.DataConverter.DeltsDataReadyEvent += DeltsDataReceived;
        }
        /// <summary>
        /// Обработка поступающего файла данных c дельтами сигнала
        /// Принимает дельты, сохроняет их в файлы 
        /// </summary>
        /// <param name="sender">Объект - отправитель</param>
        /// <param name="e">Аргумент, содержащий парамметры пакета данных</param>
        private void DeltsDataReceived(object sender, DeltsDataEventArgs e)
        {
            //Если нам пришло время, значит был создан файл, значит необходимо создать новую запись в базе данных
            if (e.FilesCreateTime != null)
            {
                //Если запись была создана - необходимо закрыть файл на сервере
                if (this.CurrentRecord != null) 
                    this.OnPartRecordReady(new SpecificFileReadyEventArgs()
                    {
                        FileNum = this.FileNum,
                        RecordID = this.CurrentRecord.ID,
                        ServerAction = new CloseServerFile()
                    });

                RecordTable newRecord = InitNewRecord(e.FileDirectory, e.ChannelsNum, e.SamplePerSecond, e.FilesCreateTime.Value);
                int appendresult = this.DatabaseWorker.CreateRecord(newRecord);
                if (appendresult >= 0)
                {
                    this.FileNum = 0;
                    this.CurrentRecord = newRecord;
                    this.OnPartRecordReady(new SpecificFileReadyEventArgs()
                    {
                        FileNum = this.FileNum,
                        RecordID = this.CurrentRecord.ID,
                        ServerAction = new CreateServerFile()
                    });
                }
                //Если же создание новой записи провалилось - ничего не делаем, продалжая писать в старый файл
                //TODO: возможно есть решение лучше
            }
            //Задаём имя для нового файла
            string partFilename = this.CurrentRecord.FileName + " №" + this.FileNum.ToString();
            //Записываем новые данные в файл
            if (this.WriteData(partFilename, e.DeltasData))
            {
                //Обновляем номер последнего сохранённого файла
                this.DatabaseWorker.UpdateRecordsFileNum(this.CurrentRecord.ID, this.FileNum);
                //Оповещаем о появлении нового файла в буфере хранилища для дальнейшего анализа
                this.OnPartRecordReady(new SpecificFileReadyEventArgs()
                {
                    FileNum = this.FileNum,
                    RecordID = this.CurrentRecord.ID,
                    ServerAction = new AppendServerFile()
                });
                //Увеличиваем счётчик файлов на единицу
                this.FileNum++;
            }
            //Иначе ничего не делаем... Скорее всего есть проблемы с памятью телефона... TODO - решить и протестировать
        }

        private RecordTable InitNewRecord(int currentDirectory, byte channels, int sps, DateTime dateRecord)
        {
            if (this.DatabaseWorker.CurrentPatient != null && this.DatabaseWorker.CurrentUser != null)
            {
                RecordTable record = new RecordTable()
                {
                    CatalogNumber = currentDirectory, //Получаем из DirInfo
                    ChannelsCount = channels, //Получаем из DirInfo
                    UserID = this.DatabaseWorker.CurrentUser.ID,
                    PatientID = this.DatabaseWorker.CurrentPatient.ID,
                    FileName = this.DatabaseWorker.CurrentPatient.Surname + " " + this.DatabaseWorker.CurrentPatient.Name[0] 
                            + ". " + this.DatabaseWorker.CurrentPatient.Patronymic[0] + ". "
                            + String.Format("{0:dd-MM-yyyy hh-mm-ss}", dateRecord), //Имя фала необходимо для идентификации частей его
                    FileID = -1, //Появляется при устойчивом интернет соединении и создание файла
                    FileType = (int)ECGreceiveMode.HolterECG,
                    SampleFrequency = sps, //Получаем из DirInfo
                    IsFileEnd = false,
                    StartRecord = dateRecord,
                    FinishRecord = DateTime.MinValue,
                    LastBluetoothFileNumber = -1,
                    LastServerPackageNumber = -1,
                    LastServerPartNumber = -1,
                    MAC = this.DatabaseWorker.CurrentUser.MACS
                };
                return record;
            }
            return null;
        }


        /// <summary>
        /// Метод записи данных в файл
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="data"></param>
        /// <returns>True - запись прошла успешно, false - проблемы записи</returns>
        bool WriteData(string filename, List<short[]> data)
        {
            //Запись в файл методом CreateFile зависит от OS и принимает имя файла и массив дельт
            if (DependencyService.Get<IFileWorker>().CreateFile(filename, ConvertData(data)))
                return true;
            return false;
        }
        /// <summary>
        /// Преобразование списка дельт в массив дельт, пригодный для отправки на сервер
        /// </summary>
        /// <param name="data">Список массивов дельт</param>
        /// <returns>Список дельт</returns>
        List<short> ConvertData(List<short[]> data)
        {
            List<short> signalsData = new List<short>();
            int samplesNum = data[0].Length; //??
            for (int i = 0; i < samplesNum; i++)
                for (int j = 0; j < data.Count; j++)
                    signalsData.Add(data[j][i]);
            return signalsData;
        }

        public EventHandler<SpecificFileReadyEventArgs> PartRecordReadyEvent;
        private void OnPartRecordReady(SpecificFileReadyEventArgs e)
        {
            EventHandler<SpecificFileReadyEventArgs> handler = PartRecordReadyEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public void Dispose()
        {
            if (this.DataConverter != null)
                this.DataConverter.DeltsDataReadyEvent -= DeltsDataReceived;
            //Делаем проверку на создание в бд необработанных записей
            FireFly.getInstance().DirectoryController.RegisteringUnreadFiles(this.CurrentRecord.ID);
        }
    }
}
