﻿using FenixMobile.Model.ServerPart;
using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.FilePart.FileEventArgs
{
    public class SpecificFileReadyEventArgs : EventArgs
    {
        /// <summary>
        /// Идентификатор записи в базе данных, для которого производится серверное действие
        /// </summary>
        public int RecordID { get; set; }
        /// <summary>
        /// Номер файла
        /// </summary>
        public ushort FileNum { get; set; }
        /// <summary>
        /// Конкретное серверное действие
        /// </summary>
        public IServerAction ServerAction { get; set; }

    }
}
