﻿using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using FenixMobile.Model.BluetoothPart.DataConvert;
using FenixMobile.Model.DatabasePart;
using FenixMobile.View.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace FenixMobile.Model.FilePart
{
    public class FileWorker : IDisposable
    {
        DatabaseWorker DatabaseWorker;
        private RecordTable record;
        public RecordTable Record { 
            get { return this.record; } 
            private set {
                if (this.record != value)
                {
                    this.record = value;
                    RecordChangeEvent?.Invoke(this, new RecordChangeEventArgs() { Record = record });
                }
            } 
        }
        public EventHandler<RecordChangeEventArgs> RecordChangeEvent;

        private void OnRecordChange(RecordChangeEventArgs e)
        {
            EventHandler<RecordChangeEventArgs> handler = RecordChangeEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }


        public string FileName
        {
            get; private set;
        } = null;

        bool isFileCreate = false;
        public bool IsFileCreate
        {
            get { return isFileCreate; }
            private set
            {
                isFileCreate = value;
                if (isFileCreate)
                    FileCreated?.Invoke(this, EventArgs.Empty);
            }
        }
        public EventHandler<EventArgs> FileCreated;

        bool isServerFileCreate = false;
        public bool IsServerFileCreate
        {
            get { return isServerFileCreate; }
            private set
            {
                isServerFileCreate = value;
                if (isServerFileCreate)
                    ServerFileCreated?.Invoke(this, EventArgs.Empty);
            }
        }
        public EventHandler<EventArgs> ServerFileCreated;

        DataConverter DataConverter;

        public FileWorker(DataConverter dataConverter, RecordTable record)
        {
            this.DataConverter = dataConverter;
            DatabaseWorker = DatabaseWorker.getInstance();
            this.Record = record;
            if (this.Record != null)
            {
                this.FileName = record.FileName;
                if (this.FileName != null && this.FileName.Length > 0)
                {
                    //Создадим запись в БД
                    if (!DatabaseWorker.IsRecordCreate(this.FileName))
                    {
                        DatabaseWorker.CreateRecord(this.Record);
                    }
                    this.DataConverter.SignalsConvertedEvent += PacketCatcher;
                }
            }
        }

        void PacketCatcher(object sender, ConvertedSignalEventArgs e)
        {
            if (e.Signals != null && e.Signals.Count > 0 && e.Signals[0].Length > 0)
            {
                //Если имени файла нет, то ничего не сохраняем
                if (this.FileName != null && this.FileName.Length > 0)
                {
                    if (!this.IsFileCreate)
                    {
                        //Создаём файл
                        FileCreate(this.FileName);
                    }

                    int minSizeArray = RecomendedSize(e.Signals);

                    if (minSizeArray > 0)
                    {
                        List<short[]> signals = e.Signals; 
                        List<short> data = new List<short>();
                        for (int i=0; i < minSizeArray; i++)
                        {
                            for (int j = 0; j < signals.Count; j++)
                                data.Add(signals[j][i]);
                        }
                        int resultAppend = DependencyService.Get<IFileWorker>().AppendFile(this.FileName, data.ToArray());
                        LastBluetoothNumberUpdate((int)e.PackageNumber);
                    }
                }
            }
        }

        void LastBluetoothNumberUpdate(int number)
        {
            if (this.Record != null && this.Record.FileName != null && this.Record.FileName.Length > 0 && number >= 0)
            {
                this.Record.LastBluetoothFileNumber = number;
                UpdateRecord();
            }
        }
        void LastSendPackageNumberUpdate(int number)
        {
            if (this.Record != null && this.Record.FileName != null && this.Record.FileName.Length > 0 && number >= 0)
            {
                this.Record.LastServerPackageNumber = number;
                UpdateRecord();
            }
        }

        public void CloseRecord()
        {
            if (this.Record != null && !this.Record.IsFileEnd && this.Record.FileName != null && this.Record.FileName.Length > 0)
            {
                this.Record.IsFileEnd = true;
                this.Record.FinishRecord = DateTime.Now;
                UpdateRecord();
            }
        }

        public void FileIDUpdate(long fileID)
        {
            if (this.Record != null && this.Record.FileName != null && this.Record.FileName.Length > 0 && fileID >= 0)
            {
                this.Record.FileID = fileID;
                UpdateRecord();
            }
        }

        public void LastServerPartUpdate(int lastPart)
        {
            if (this.Record != null && this.Record.FileName != null && this.Record.FileName.Length > 0 && lastPart >= 0)
            {
                this.Record.LastServerPartNumber = lastPart;
                UpdateRecord();
            }
        }

        object locker = new object();
        void UpdateRecord() //?????
        {
            lock (locker)
            {
                if (DatabaseWorker != null)
                {
                    DatabaseWorker.UpdateRecord(this.Record);
                }
            }
        }

        int RecomendedSize(List<short[]> data)
        {
            List<int> sizes = new List<int>();
            for (int i=0; i < data.Count; i++)
                sizes.Add(data[i].Length);
            return sizes.Min();
        }

        void FileCreate(string fileName)
        {
            if (!DependencyService.Get<IFileWorker>().ExistsFile(FileName))
            {
                if (DependencyService.Get<IFileWorker>().CreateFile(FileName) >= 0)
                    this.IsFileCreate = true;
                else
                    this.IsFileCreate = false;
            }
            else
            {
                this.IsFileCreate = true;
            }
        }

        public void UpdateLastByteServersFile(long newIndex)
        {
            if (this.Record != null && this.Record.FileName != null && this.Record.FileName.Length > 0)
            {
                this.Record.StartIndexServerFile = newIndex;
                UpdateRecord();
            }
        }

        //Код для работы с фалом-хранилищем серверных данных. Имена файлов совпадают, но при этом лежат в разных директориях
        #region
        public void CreateServerDataFile()
        {
            if (DependencyService.Get<IFileWorker>().CreateServerFile(FileName))
            {
                if (this.Record.StartIndexServerFile == -1) this.UpdateLastByteServersFile(0);
                this.IsServerFileCreate = true;
            }
            else
                this.IsServerFileCreate = false;
        }
        public bool AppenServerData(short[] data) // Не оивидная логика
        {
            
            if (DependencyService.Get<IFileWorker>().ServerFileExist(FileName))
            {
                DependencyService.Get<IFileWorker>().AppendServerFile(FileName, data);
                if (this.Record.StartIndexServerFile == -1) this.UpdateLastByteServersFile(0);
                return true;
            }
            else
            {
                if (DependencyService.Get<IFileWorker>().CreateServerFile(FileName))
                {
                    this.IsServerFileCreate = true;
                    DependencyService.Get<IFileWorker>().AppendServerFile(FileName, data);
                    if (this.Record.StartIndexServerFile == -1) this.UpdateLastByteServersFile(0);
                    return true;
                }
            }
            return false;
        }

        public bool IsSeverFileExist()
        {
            return DependencyService.Get<IFileWorker>().ServerFileExist(FileName);
        }

        public List<short> GetSavedServerData(string fileName)
        {
            if (DependencyService.Get<IFileWorker>().ServerFileExist(FileName))
                return DependencyService.Get<IFileWorker>().GetServerFile(FileName);
            else
                return null;
        }

        public List<short> GetSavedPartServerData(long position, ref long readCount)
        {
            if (DependencyService.Get<IFileWorker>().ServerFileExist(FileName))
                return DependencyService.Get<IFileWorker>().GetPartServerFile(FileName, position, ref readCount);
            else
                return null;
        }

        public void DeleteServerData()
        {
            if (DependencyService.Get<IFileWorker>().ServerFileExist(FileName))
                DependencyService.Get<IFileWorker>().DeleteServerFile(FileName);
        }

        public long GetFileSize()
        {
            if (DependencyService.Get<IFileWorker>().ServerFileExist(FileName))
            {
                long fileSize = DependencyService.Get<IFileWorker>().GetBytesCount(FileName);
                return fileSize;
            }
            return -1;
        }
        #endregion




        private RecordTable GetCopy(RecordTable recordTable)
        {
            RecordTable copyRecord = new RecordTable() {
                CatalogNumber = recordTable.CatalogNumber,
                ChannelsCount = recordTable.ChannelsCount,
                ID = recordTable.ID,
                UserID = recordTable.UserID,
                PatientID = recordTable.PatientID,
                FileName = recordTable.FileName,
                FileID = recordTable.FileID,
                FileType = recordTable.FileType,
                SampleFrequency = recordTable.SampleFrequency,
                IsFileEnd = recordTable.IsFileEnd,
                StartRecord = recordTable.StartRecord,
                FinishRecord = recordTable.FinishRecord,
                LastBluetoothFileNumber = recordTable.LastBluetoothFileNumber,
                LastServerPackageNumber = recordTable.LastServerPackageNumber
            };
            return copyRecord;
        }

        public void Dispose()
        {
            if (this.Record != null)
            {
                this.Record = null;
                if (this.FileName != null && this.FileName.Length > 0)
                {
                    this.DataConverter.SignalsConvertedEvent -= PacketCatcher;
                }
            }
        }
    }
}
