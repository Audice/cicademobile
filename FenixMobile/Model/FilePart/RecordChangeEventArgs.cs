﻿using FenixMobile.Model.DatabasePart;
using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.FilePart
{
    public class RecordChangeEventArgs : EventArgs
    {
        public RecordTable Record { get; set; }
    }
}
