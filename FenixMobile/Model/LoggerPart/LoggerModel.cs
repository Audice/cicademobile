﻿using FenixMobile.View.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace FenixMobile.Model.LoggerPart
{
    public class LoggerModel
    {
        private static object sync = new object();

        /// <summary>
        /// Статический метод записи логов в файл логирования. Файл пишется построчно. 
        /// Символ разделения элементов: $\/|\/$
        /// Символ разделения строк: $/\|/\$
        /// </summary>
        /// <param name="ColorCode">Цветовая индикация сообщения: 0 - зелёный(всё хорошо), 
        /// 1 - жёлтый(есть неполадки), 2 - красный(есть проблема)</param>
        /// <param name="StringInformation">Общая информация о событии</param>
        /// <param name="LocalClassName">Имя класса с проблемным участком</param>
        /// <param name="LocalMethodName">Имя метода с проблемным участком</param>
        /// <param name="ProblemString">Проблемная строчка</param>
        public static async void WriteToLogfile(string ColorCode, string StringInformation,
            string LocalClassName = "", string LocalMethodName = "", string ProblemString = "", string MacAdress = "")
        {
            string outputString =
                String.Format("{0:dd-MM-yyyy  HH-mm-ss}", DateTime.Now) + @"$\/|\/$" + //Дата и время
                ColorCode + @"$\/|\/$" + //
                StringInformation + @"$\/|\/$" +
                LocalClassName + @"$\/|\/$" +
                LocalMethodName + @"$\/|\/$" +
                ProblemString + @"$\/|\/$" +
                MacAdress + @"$/\|/\$";

            await DependencyService.Get<IFileWorker>().AddNewLogInfo(outputString);
        }
        /// <summary>
        /// Функция возвращает список кортежей следуюхего формата:
        /// Дата , Цветовой код , Информация, Имя класса, Имя метода, Номер проблемной строки, MAC устройства
        /// </summary>
        /// <returns></returns>
        public static List<Tuple<string, string, string, string, string, string, string>> GetLogsList()
        {
            List<Tuple<string, string, string, string, string, string, string>> logList = new List<Tuple<string, string, string, string, string, string, string>>();
            string RawLogStrings = DependencyService.Get<IFileWorker>().GetLogs();
            if (RawLogStrings == null) return null;
            if (RawLogStrings.Length <= 0) return null;

            string betweenElementTemplate = @"$\/|\/$";
            string betweenStrokeTemplate = @"$/\|/\$";
            List<string> currentStroke = new List<string>();
            string currentElement = "";

            for (int i = 0; i < RawLogStrings.Length - 6; i++)
            {
                if (RawLogStrings[i].Equals(betweenElementTemplate[0])
                    && RawLogStrings[i + 1].Equals(betweenElementTemplate[1])
                    && RawLogStrings[i + 2].Equals(betweenElementTemplate[2])
                    && RawLogStrings[i + 3].Equals(betweenElementTemplate[3])
                    && RawLogStrings[i + 4].Equals(betweenElementTemplate[4])
                    && RawLogStrings[i + 5].Equals(betweenElementTemplate[5])
                    && RawLogStrings[i + 6].Equals(betweenElementTemplate[6]))
                {
                    currentStroke.Add(currentElement);
                    currentElement = "";
                    // Увеличиваем индекс
                    i += 6;
                }
                else
                {
                    if (RawLogStrings[i].Equals(betweenStrokeTemplate[0])
                        && RawLogStrings[i + 1].Equals(betweenStrokeTemplate[1])
                        && RawLogStrings[i + 2].Equals(betweenStrokeTemplate[2])
                        && RawLogStrings[i + 3].Equals(betweenStrokeTemplate[3])
                        && RawLogStrings[i + 4].Equals(betweenStrokeTemplate[4])
                        && RawLogStrings[i + 5].Equals(betweenStrokeTemplate[5])
                        && RawLogStrings[i + 6].Equals(betweenStrokeTemplate[6]))
                    {
                        if (currentStroke.Count == 7)
                        {
                            logList.Add(new Tuple<string, string, string, string, string, string, string>(
                                currentStroke[0], currentStroke[1], currentStroke[2], currentStroke[3],
                                currentStroke[4], currentStroke[5], currentStroke[6]));
                        }
                        else
                        {
                            List<string> emptyList = new List<string>() { "", "", "", "", "", "", "" };
                            if (currentStroke.Count < 7)
                            {
                                for (int j = 0; j < currentStroke.Count; j++)
                                    emptyList[j] = currentStroke[j];
                            }
                            else
                            {
                                for (int j = 0; j < emptyList.Count; j++)
                                    emptyList[j] = currentStroke[j];
                            }
                            logList.Add(new Tuple<string, string, string, string, string, string, string>(
                                        emptyList[0], emptyList[1], emptyList[2], emptyList[3],
                                        emptyList[4], emptyList[5], emptyList[6]));

                        }
                        currentStroke = new List<string>();
                        currentElement = "";
                        // Увеличиваем индекс
                        i += 6;
                    }
                    else
                    {
                        currentElement += RawLogStrings[i];
                    }
                }
            }
            return logList;
        }

        public static string StringForSending(List<Tuple<string, string, string, string, string, string, string>> logList)
        {
            string outputString = "";
            for (int i = 0; i < logList.Count; i++)
            {
                outputString += logList[i].Item1 + @"$\/|\/$" + logList[i].Item2 + @"$\/|\/$"
                    + logList[i].Item3 + @"$\/|\/$" + logList[i].Item4 + @"$\/|\/$" + logList[i].Item5 + @"$\/|\/$"
                    + logList[i].Item6 + @"$\/|\/$" + logList[i].Item7 + @"$/\|/\$";
            }
            return "";
        }

        public static void FillExceptionLog(Exception ex, object ObjectClass, string MethodName, string MAC)
        {
            //Template для всех exception
            var st = new System.Diagnostics.StackTrace(ex, true);
            var frame = st.GetFrame(0);
            int line = frame.GetFileLineNumber();
            LoggerModel.WriteToLogfile("2", ex.Message, ObjectClass.ToString(), MethodName, ex.StackTrace, MAC);
        }
    }
}
