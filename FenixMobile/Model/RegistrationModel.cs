﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace FenixMobile.Model
{
    public class RegistrationModel
    {
        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Фамилия пользователя
        /// </summary>
        public string UserSurname { get; set; }
        /// <summary>
        /// Отчёство пользователя
        /// </summary>
        public string UserPatronomyc { get; set; }
        /// <summary>
        /// Логин пользователя есть его электронный адресс
        /// </summary>
        public string UserEmail { get; set; }
        /// <summary>
        /// Пароль пользователя профелем
        /// </summary>
        public string UserPassword { get; set; }
        public string UserGrowth { get; set; }
        public string UserWeight { get; set; }
        public string UserGender { get; set; }
        public DateTime UserBornDate { get; set; }

        public List<string> ValidationFilledField()
        {
            List<string> errorsDiscription = new List<string>();
            IsNameValid(errorsDiscription);
            IsSurnameValid(errorsDiscription);
            IsPatronomycValid(errorsDiscription);
            IsMailValid(errorsDiscription);
            IsPasswordValid(errorsDiscription);
            IsGrowthValid(errorsDiscription);
            IsWeightValid(errorsDiscription);
            IsGenderValid(errorsDiscription);
            IsBornDateValid(errorsDiscription);

            return errorsDiscription;
        }

        private const string BadSymbolList = @" !@#$;%^:*()_-+=`~{}[]'\|/?.,<>&1234567890" + "\"";
        private const string EmailPattern = @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                            @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$";
        void IsNameValid(List<string> errors)
        {
            if (this.UserName == null)
            {
                errors.Add("Поле 'Имя' не заполнено!");
                return;
            }
            if (this.UserName.Length > 1)
            {
                for (int i = 0; i < this.UserName.Length; i++)
                {
                    for (int j=0; j < BadSymbolList.Length; j++)
                    {
                        if (BadSymbolList[j] == this.UserName[i])
                        {
                            errors.Add("В имени используется недопустимый символ: '" + this.UserName[i] + "'!");
                            i = this.UserName.Length;
                            j = BadSymbolList.Length;
                        }
                    }
                }
            }
            else
            {
                if (this.UserName.Length == 0) errors.Add("Поле 'Имя' не заполнено!");
                else errors.Add("Слишком короткое имя!");
            }
        }
        void IsSurnameValid(List<string> errors)
        {
            if (this.UserSurname == null)
            {
                errors.Add("Поле 'Фамилия' не заполнено!");
                return;
            }
            if (this.UserSurname.Length > 1)
            {
                for (int i = 0; i < this.UserSurname.Length; i++)
                {
                    for (int j = 0; j < BadSymbolList.Length; j++)
                    {
                        if (BadSymbolList[j] == this.UserSurname[i])
                        {
                            errors.Add("В фамилии используется недопустимый символ: '" + this.UserSurname[i] + "'!");
                            i = this.UserSurname.Length;
                            j = BadSymbolList.Length;
                        }
                    }
                }
            }
            else
            {
                if (this.UserSurname.Length == 0) errors.Add("Поле 'Фамилия' не заполнено!");
                else errors.Add("Слишком короткая фамилия!");
            }
        }
        void IsPatronomycValid(List<string> errors)
        {
            if (this.UserPatronomyc == null)
            {
                errors.Add("Поле 'Отчество' не заполнено!");
                return;
            }
            if (this.UserPatronomyc.Length > 1)
            {
                for (int i = 0; i < this.UserPatronomyc.Length; i++)
                {
                    for (int j = 0; j < BadSymbolList.Length; j++)
                    {
                        if (BadSymbolList[j] == this.UserPatronomyc[i])
                        {
                            errors.Add("В отчестве используется недопустимый символ: '" + this.UserPatronomyc[i] + "'!");
                            i = this.UserPatronomyc.Length;
                            j = BadSymbolList.Length;
                        }
                    }
                }
            }
            else
            {
                if (this.UserPatronomyc.Length == 0) errors.Add("Поле 'Отчество' не заполнено!");
                else errors.Add("Слишком короткое отчество!");
            }
        }
        void IsMailValid(List<string> errors)
        {
            if (this.UserEmail == null)
            {
                errors.Add("Поле 'Почта' не заполнено!");
                return;
            }
            if (this.UserEmail.Length > 3)
            {
                if (Regex.IsMatch(UserEmail,
                    EmailPattern,
                    RegexOptions.CultureInvariant))
                {
                    return;
                }
                else
                {
                    errors.Add("Почта не соответствует формату");
                }
            }
            else
            {
                if (this.UserEmail.Length == 0) errors.Add("Поле 'Почта' не заполнено!");
                else errors.Add("Слишком короткая почта!");
            }
        }
        void IsPasswordValid(List<string> errors)
        {
            if (this.UserPassword == null)
            {
                errors.Add("Поле 'Пароль' не заполнено!");
                return;
            }
            if (this.UserPassword.Length >= 3)
            {
                return;
            }
            else
            {
                if (this.UserPassword.Length == 0) errors.Add("Поле 'Пароль' не заполнено!");
                else errors.Add("Слишком короткий пароль! Рекомендуемый размер: более 5 символов");
            }
        }
        void IsGrowthValid(List<string> errors)
        {
            if (this.UserGrowth == null)
            {
                errors.Add("Поле 'Рост' не заполнено!");
                return;
            }
            if (this.UserGrowth.Length > 1)
            {
                //Проверку на число проведём конвертером
                float growth = -1;
                if (float.TryParse(UserGrowth, out growth))
                {
                    if (growth < 50 && growth > 300)
                    {
                        errors.Add("Рост задан некорректно!");
                        return;
                    }
                }
                else
                {
                    errors.Add("Поле 'Рост' содержит неверные данные!");
                    return;
                }
            }
            else
            {
                if (this.UserGrowth.Length == 0) errors.Add("Поле 'Рост' не заполнено!");
                else errors.Add("Не верный рост!");
            }
        }
        void IsWeightValid(List<string> errors)
        {
            if (this.UserWeight == null)
            {
                errors.Add("Поле 'Вес' не заполнено!");
                return;
            }
            if (this.UserWeight.Length > 1)
            {
                //Проверку на число проведём конвертером
                float weight = -1;
                if (float.TryParse(UserWeight, out weight))
                {
                    if (weight < 10 && weight > 330)
                    {
                        errors.Add("Вес задан некорректно!");
                        return;
                    }
                }
                else
                {
                    errors.Add("Поле 'Вес' содержит неверные данные!");
                    return;
                }
            }
            else
            {
                if (this.UserWeight.Length == 0) errors.Add("Поле 'вес' не заполнено!");
                else errors.Add("Вес задан некорректно!");
            }
        }
        void IsGenderValid(List<string> errors)
        {
            if (this.UserGender == null)
            {
                errors.Add("Поле 'Пол' не заполнено!");
                return;
            }
            else
            {
                if (this.UserGender == "Мужской" || this.UserGender == "Женский")
                {

                }
                else
                {
                    errors.Add("Поле 'Пол' не заполнено!");
                    return;
                }
            }
        }
        void IsBornDateValid(List<string> errors)
        {
            if (this.UserBornDate == null)
            {
                errors.Add("Поле 'Дата рождения' не заполнено!");
                return;
            }
        }
    }
}
