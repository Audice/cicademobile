﻿using FenixMobile.Model.FilePart;
using FenixMobile.Model.FilePart.FileEventArgs;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FenixMobile.Model.ServerPart
{
    public class DataSender : IDisposable
    {
        private ConcurrentQueue<SpecificFileReadyEventArgs> ServerEventQueue
        {
            get; set;
        } = null;
        /// <summary>
        /// Поставщик данных-дельт
        /// </summary>
        DataHandler DataHandler { get; set; } = null;

        ServerWorker ServerWorker { get; set; } = null;

        bool IsSendTaskStop { get; set; } = false;

        bool SendProcessRun { get; set; } = false;
        /// <summary>
        /// Флаг активности контроллирующего процесса
        /// </summary>
        bool ProcessStop { get; set; } = true;

        /// <summary>
        /// Исходный токен для серверной активности
        /// </summary>
        CancellationTokenSource SourceServerActivityToken = null;
        /// <summary>
        /// Токен выхода из задачи. Необходим для прекращения работы циклической задачи
        /// </summary>
        CancellationToken ServerActivityCancellationToken;

        public DataSender(DataHandler dataHandler)
        {
            this.SourceServerActivityToken = new CancellationTokenSource();
            this.ServerActivityCancellationToken = this.SourceServerActivityToken.Token;

            this.ProcessStop = false;
            this.IsSendTaskStop = false;
            this.ServerWorker = ServerWorker.getInstance();
            this.ServerEventQueue = new ConcurrentQueue<SpecificFileReadyEventArgs>();
            this.DataHandler = dataHandler;
            this.DataHandler.PartRecordReadyEvent += AcceptSentData;
        }

        private void AcceptSentData(object sender, SpecificFileReadyEventArgs e)
        {
            //Добавление событий в очередь
            this.ServerEventQueue.Enqueue(e);
            if (!SendProcessRun)
            {
                SendProcessRun = true;
                this.ServerActivityImplementation().ContinueWith(RunnerProcessTasks);
            }
        }

        Task ServerActivityImplementation()
        {
            return Task.Factory.StartNew(
                () =>
                {
                    if (this.ServerWorker.IsInternetConnection && this.ServerEventQueue.Count > 0)
                    {
                        SpecificFileReadyEventArgs currentAction = null;
                        if (this.ServerEventQueue.TryPeek(out currentAction))
                        {
                            //Если мы успешно взяли посмотреть событие из очереди, начинаем его отправлять
                            int actionResult = currentAction.ServerAction.ServerActionRun(currentAction);
                            if (actionResult >= 0)
                            {
                                currentAction = null;
                                //Если успешно отправили, тогда извлекаем из очереди, пока не извлечём
                                while (!(this.ServerEventQueue.TryDequeue(out currentAction)))
                                    Task.Delay(1000).GetAwaiter().GetResult();
                            }
                        }
                    }
                    else
                    {
                        //Ждём подключения к интернету, либо появление данных
                        Task.Delay(30000).GetAwaiter().GetResult();
                    }
                }, this.ServerActivityCancellationToken
            );
        }

        

        /// <summary>
        /// Циклический метод реализации , используюя ContinueWith
        /// </summary>
        /// <param name="t"></param>
        void RunnerProcessTasks(Task t)
        {            
            if (!IsSendTaskStop)
                ServerActivityImplementation().ContinueWith(RunnerProcessTasks);
        }
        public void Dispose()
        {
            //Отписываемся от прихода новых порций данных
            this.DataHandler.PartRecordReadyEvent -= AcceptSentData;
            //Флаг прекращения отправки на сервер
            this.IsSendTaskStop = true;
            //Принудительная остановка задачи, занимающаяся серверной активностью
            //TODO: возможны проблемы как с серверной частью, так и с хранилищем - учесть это
            this.SourceServerActivityToken.Cancel();
        }
    }
}
