﻿using FenixMobile.Model.FilePart.FileEventArgs;
using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.ServerPart
{
    public interface IServerAction
    {
        int ServerActionRun(SpecificFileReadyEventArgs args);
    }
}
