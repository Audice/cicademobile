﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.ServerPart
{
    public class IncrementFilter
    {
        private List<short> StartSignals;
        public IncrementFilter()
        {
            this.StartSignals = new List<short>();
        }

        public List<List<short>> GetDelts(List<short[]> realSignal)
        {
            List<List<short>> resultsList = InitSignalsList(realSignal.Count);

            if (this.StartSignals.Count == 0)
            {
                for (int i=0; i < realSignal.Count; i++)
                {
                    this.StartSignals.Add(realSignal[i][0]);
                    resultsList[i].Add(realSignal[i][0]);
                }
                //Далее вычитаем из сигналов обновлённые значения
                for (int i = 0; i < realSignal.Count; i++)
                {
                    for (int j = 1; j < realSignal[i].Length; j++)
                    {
                        resultsList[i].Add((short)(realSignal[i][j] - this.StartSignals[i]));
                        this.StartSignals[i] = realSignal[i][j];
                    }
                }
            }
            else
            {
                //Далее вычитаем из сигналов обновлённые значения
                for (int i = 0; i < realSignal.Count; i++)
                {
                    for (int j = 0; j < realSignal[i].Length; j++)
                    {
                        resultsList[i].Add((short)(realSignal[i][j] - this.StartSignals[i]));
                        this.StartSignals[i] = realSignal[i][j];
                    }
                }
            }


            return resultsList;

        }

        private List<List<short>> InitSignalsList(int countSignals)
        {
            List<List<short>> results = new List<List<short>>();
            for (int i=0; i < countSignals; i++)
            {
                results.Add(new List<short>());
            }
            return results;
        }

    }
}
