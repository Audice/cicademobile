﻿using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using FenixMobile.Model.BluetoothPart.DataConvert;
using FenixMobile.Model.DatabasePart;
using FenixMobile.Model.FilePart;
using SoapConstruct;
using SoapConstruct.SoapFunction;
using SoapConstruct.SoapFunction.AttribConstruct;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FenixMobile.Model.ServerPart
{
    public class SOAPOldPackageSender : IDisposable
    {
        long CurrentFileID = -1;

        private readonly int PackageSize = 1000;
        private bool isSenderRun = false;

        private bool isFileCreate = false;
        public bool IsFileCreate
        {
            get { return isFileCreate; }
            private set
            {
                isFileCreate = value;
            }
        }

        private long partNumber = 0;
        public long PartNumber
        {
            get { return partNumber; }
            private set
            {
                partNumber = value;
            }
        }

        public bool IsSenderRun
        {
            get { return isSenderRun; }
            private set
            {
                isSenderRun = value;
            }
        }



        //Переменные обеспечения работы с файлами на сервере
        bool IsSendProcessRun = false;
        bool IsServerFileCreate = false;
        bool IsPartSend = false;
        bool IsFileClose = false;

        bool isAllProcessStop = false;
        public bool IsAllProcessStop
        {
            get; set;
        }



        IncrementFilter Increments;

        //Необходимо хранилище готовых дельт сигнала
        private List<ConcurrentQueue<short>> SignalsList;
        private DataConverter DataConverter = null;
        private FileWorker FileWorker;
        private RecordTable CurrentRecord;
        private UserTable CurrentUser;
        private PatientTable CurrentPatient;
        private ServerWorker ServerWorkerInstance;
        private DatabaseWorker DatabaseWorker;

        public SOAPOldPackageSender(DataConverter dataConvert, FileWorker fileWorker, RecordTable recordTable)
        {
            this.DatabaseWorker = DatabaseWorker.getInstance();
            this.ServerWorkerInstance = ServerWorker.getInstance();
            this.DataConverter = dataConvert;
            this.FileWorker = fileWorker;
            this.CurrentRecord = recordTable;


            this.CurrentUser = this.DatabaseWorker.CurrentUser;
            this.CurrentPatient = this.DatabaseWorker.CurrentPatient;
            this.Increments = new IncrementFilter();
            this.SignalsList = new List<ConcurrentQueue<short>>();
            this.DataConverter.SignalsConvertedEvent += CollectConvertedPackage;
        }

        int s = 0;

        /// <summary>
        /// Подписываемся на получение готовых пакетов данных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">EventArgs хорнящий список сигналов по отведениям</param>
        private void CollectConvertedPackage(object sender, ConvertedSignalEventArgs e)
        {
            if (e.Signals == null) return;
            if (this.Increments == null) this.Increments = new IncrementFilter();
            if (this.SignalsList == null || this.SignalsList.Count == 0) this.SignalsList = InitStorage(e.Signals.Count);

            List<List<short>> delta = this.Increments.GetDelts(e.Signals);


            for (int i = 0; i < this.SignalsList.Count; i++)
            {
                for (int j = 0; j < delta[i].Count; j++)
                {
                    this.SignalsList[i].Enqueue(delta[i][j]);
                }
            }

            if (!IsSendProcessRun)
            {
                this.IsSendProcessRun = true;
                //Запустит Task по отправке данных
                this.CreateServerFileTask().ContinueWith(RunnerProcessTasks);
            }
        }

        private List<ConcurrentQueue<short>> InitStorage(int signalsNumber)
        {
            if (signalsNumber == 0) return null;
            List<ConcurrentQueue<short>> localSignalsList = new List<ConcurrentQueue<short>>();
            for (int i = 0; i < signalsNumber; i++)
                localSignalsList.Add(new ConcurrentQueue<short>());
            return localSignalsList;
        }

        private bool CheckRequiredQuantity()
        {
            if (this.SignalsList.Count > 0)
            {
                bool isQueueFull = true;
                for (int i = 0; i < this.SignalsList.Count; i++)
                {
                    if (this.SignalsList[i].Count < this.PackageSize)
                    {
                        isQueueFull = false;
                        break;
                    }
                }
                return isQueueFull;
            }
            else
                return false;
        }

        Task PartSenderTask()
        {
            return Task.Factory.StartNew(
                () =>
                {
                    if (!this.IsAllProcessStop && this.CurrentUser != null && this.CurrentUser.SessionID != null && this.CurrentUser.SessionID.Length > 0
                    && this.CurrentRecord.FileID > 0 && this.IsServerFileCreate && CheckRequiredQuantity())
                    {
                        if (this.ServerWorkerInstance.IsInternetConnection)
                        {
                            short[] deltsArray = new short[this.SignalsList.Count * this.PackageSize];
                            for (int i = 0; i < deltsArray.Length; i += this.SignalsList.Count)
                            {
                                for (int j = 0; j < this.SignalsList.Count; j++)
                                {
                                    short deltaValue = 0;
                                    this.SignalsList[j].TryDequeue(out deltaValue);
                                    deltsArray[i + j] = deltaValue;
                                }
                            }

                            IRequest sendPart = new AddFileBlock(this.CurrentUser.SessionID, this.CurrentRecord.FileID, this.PartNumber, deltsArray);
                            int sendResult = -1;

                            while (sendResult < 0 && !this.IsAllProcessStop)
                            {
                                try
                                {
                                    sendResult = this.ServerWorkerInstance.AddBlock(sendPart).GetAwaiter().GetResult();
                                }
                                catch (Exception ex)
                                {
                                    string exceptionMesage = ex.Message;
                                    sendResult = -1;
                                }
                            }
                            if (sendResult >= 0)
                            {
                                this.FileWorker.LastServerPartUpdate((int)this.PartNumber);
                                this.PartNumber++; //Не правильная логика
                            }
                        }
                        else
                        {
                            //Ждём подключения к интернету
                            Task.Delay(5000).GetAwaiter().GetResult();
                        }
                    }
                }
                );
        }


        Task CreateServerFileTask()
        {
            return Task.Factory.StartNew(
            () =>
            {
                if (this.IsSendProcessRun && !this.IsServerFileCreate && this.CurrentUser != null
                && this.CurrentRecord != null && this.CurrentUser.SessionID != null && this.CurrentUser.SessionID.Length > 0)
                {
                    if (this.ServerWorkerInstance.IsInternetConnection)
                    {
                        //Создаём файл на сервере.
                        long currentFileID = -1;
                        FileAttribute fileAttribute = new FileAttribute((short)this.CurrentRecord.SampleFrequency, (byte)this.CurrentRecord.ChannelsCount, "SYGNAL_CHANNELS");

                        //Пока что CodeECG всегда 106
                        IRequest request = new CreateFile(this.CurrentUser.SessionID, CodeECG.HolterECG, this.CurrentRecord.StartRecord, this.CurrentRecord.FileName,
                            this.CurrentUser.MACS, "FF-1", fileAttribute, this.CurrentPatient.PatientID);

                        try
                        {
                            currentFileID = this.ServerWorkerInstance.CreateFile(request).GetAwaiter().GetResult();
                        }
                        catch (Exception ex)
                        {
                            string exceptionMessage = ex.Message;
                            currentFileID = -1;
                        }

                        if (currentFileID >= 0)
                        {
                            this.IsServerFileCreate = true;
                            this.CurrentFileID = currentFileID;
                            this.FileWorker.FileIDUpdate(this.CurrentFileID);
                        }
                        else
                        {
                            this.IsServerFileCreate = false;
                            this.CurrentFileID = -1;
                        }
                    }
                    else
                    {
                        //Будем проверять подключение каждые 5 секунд
                        Task.Delay(5000).GetAwaiter().GetResult();
                    }
                }
            });
        }

        Task CloseServerFileTask()
        {
            return Task.Factory.StartNew(
                () =>
                {
                    if (this.IsSendProcessRun && this.IsServerFileCreate && this.CurrentUser != null
                && this.CurrentRecord != null && this.CurrentUser.SessionID != null && this.CurrentUser.SessionID.Length > 0)
                    {
                        if (this.ServerWorkerInstance.IsInternetConnection)
                        {
                            //Закрываем файл на сервере.
                            IRequest request = new CloseFile(this.CurrentUser.SessionID, this.CurrentRecord.FileID);
                            int closeCodeResponse = -1;
                            try
                            {
                                closeCodeResponse = this.ServerWorkerInstance.CloseFile(request).GetAwaiter().GetResult();
                            }
                            catch (Exception ex)
                            {
                                string exceptionMessage = ex.Message;
                                closeCodeResponse = -1;
                            }
                            if (closeCodeResponse >= 0)
                            {
                                this.IsFileClose = true;
                            }
                            else
                            {
                                this.IsFileClose = false;
                            }
                        }
                        else
                        {
                            //Будем проверять подключение каждые 5 секунд
                            Task.Delay(5000).GetAwaiter().GetResult();
                        }
                    }
                    this.IsFileClose = true; //Лучше не придумал
                });
        }

        /// <summary>
        /// Метод, запускающий опрашивающую поток данных задачу, используюя ContinueWith
        /// </summary>
        /// <param name="t"></param>
        void RunnerProcessTasks(Task t)
        {
            if (this.IsSendProcessRun)
            {
                if (this.IsServerFileCreate)
                {
                    if (!this.IsAllProcessStop)
                    {
                        //Если файл создан и процесс не останвливался - начинаем и продолжаем отправку данных на сервер.
                        PartSenderTask().ContinueWith(RunnerProcessTasks);
                    }
                    else
                    {
                        //Если процесс остановлен - начинаем попытки закрытия файла
                        if (!this.IsFileClose)
                        {
                            CloseServerFileTask().ContinueWith(RunnerProcessTasks);
                        }
                    }
                }
                else
                {
                    //Если файл не создан, продолжаем операцию создания файла на сервере
                    CreateServerFileTask().ContinueWith(RunnerProcessTasks);
                }
            }
        }

        public void Dispose()
        {
            if (this.DataConverter != null)
                this.DataConverter.SignalsConvertedEvent -= CollectConvertedPackage;
            this.IsAllProcessStop = true;
            this.IsSendProcessRun = false;
            this.ServerWorkerInstance = null;
            this.Increments = null;
            this.SignalsList = null;
        }
    }
}
