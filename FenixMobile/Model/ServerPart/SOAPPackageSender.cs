﻿using FenixMobile.Interfaces;
using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using FenixMobile.Model.BluetoothPart.DataConvert;
using FenixMobile.Model.DatabasePart;
using FenixMobile.Model.FilePart;
using SoapConstruct;
using SoapConstruct.SoapFunction;
using SoapConstruct.SoapFunction.AttribConstruct;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FenixMobile.Model.ServerPart
{
    public class SOAPPackageSender : IDisposable
    {
        long CurrentFileID = -1;

        private readonly int PackageSize = 2000;
        private readonly int SavedPackageSize = 100000;


        public enum SendMode
        {
            OnlineMode = 1,
            OfflineMode = 2
        }


        public SendMode SendingMode
        {
            get; private set;
        }

        public void ChangeSendMode(SendMode sendMode)
        {
            this.SendingMode = sendMode;
        }


        private uint partNumber = 0;
        public uint PartNumber
        {
            get { return partNumber; }
            private set
            {
                if (partNumber != value)
                {
                    partNumber = value;
                    PartNumberChangeEvent?.Invoke(this, EventArgs.Empty);
                }
            }
        }
        public EventHandler<EventArgs> PartNumberChangeEvent;

        private uint signalListSize = 0;
        public uint SignalListSize
        {
            get { return signalListSize; }
            private set
            {
                if (signalListSize != value)
                {
                    signalListSize = value;
                    SignalListSizeChangeEvent?.Invoke(this, EventArgs.Empty);
                }
            }
        }
        public EventHandler<EventArgs> SignalListSizeChangeEvent;



        private bool isSenderRun = false;
        public bool IsSenderRun
        {
            get { return isSenderRun; }
            private set
            {
                isSenderRun = value;
            }
        }



        //Переменные обеспечения работы с файлами на сервере
        bool IsSendProcessRun = false;


        public bool isServerFileCreate = false;
        public bool IsServerFileCreate { 
            get { return isServerFileCreate; }
            private set
            {
                if (isServerFileCreate != value)
                {
                    isServerFileCreate = value;
                    ServerFileCreateEvent?.Invoke(this, EventArgs.Empty);
                }
            } 
        }
        public EventHandler<EventArgs> ServerFileCreateEvent;


        public bool isServerFileClose = false;
        public bool IsServerFileClose
        {
            get { return isServerFileClose; }
            private set
            {
                if (isServerFileClose != value)
                {
                    isServerFileClose = value;
                    ServerFileCloseEvent?.Invoke(this, EventArgs.Empty);
                }
            }
        }
        public EventHandler<EventArgs> ServerFileCloseEvent;



        public bool IsAllProcessStop
        {
            get; set;
        } = false;


        private bool isSavedServerDataMode = false;
        public bool IsSavedServerDataMode
        {
            get { return isSavedServerDataMode; }
            private set
            {
                if (isSavedServerDataMode != value)
                {
                    isSavedServerDataMode = value;
                    if (value)
                        this.SendingMode = SendMode.OfflineMode;
                }
            }
        }
        


        IncrementFilter Increments;

        //Необходимо хранилище готовых дельт сигнала
        private List<ConcurrentQueue<short>> SignalsList;
        private DataConverter DataConverter = null;
        private FileWorker FileWorker;
        private UserTable CurrentUser;
        private PatientTable CurrentPatient;
        private ServerWorker ServerWorkerInstance;
        private DatabaseWorker DatabaseWorker;

        public SOAPPackageSender(DataConverter dataConvert, FileWorker fileWorker, RecordTable recordTable, SendMode sendMode)
        {
            this.SendingMode = sendMode;
            this.DatabaseWorker = DatabaseWorker.getInstance();
            this.ServerWorkerInstance = ServerWorker.getInstance();
            this.DataConverter = dataConvert;
            this.FileWorker = fileWorker;

            if (this.FileWorker.Record.FileID >= 0)
            {
                this.IsServerFileCreate = true;
                if (this.FileWorker.Record.LastServerPartNumber > 0)
                {
                    //Если косячим, нужно обратиться на сервер, и спросить какая часть реально последняя
                    if (this.SendingMode == SendMode.OfflineMode)
                    {
                        this.PartNumber = (uint)this.FileWorker.Record.LastServerPartNumber + 1;
                    }
                    else
                    {
                        this.PartNumber = (uint)this.FileWorker.Record.LastServerPartNumber;
                    }
                    
                }
            }
            this.CurrentUser = this.DatabaseWorker.CurrentUser;
            this.CurrentPatient = this.DatabaseWorker.CurrentPatient;
            //this.Increments = new IncrementFilter();
            this.SignalsList = new List<ConcurrentQueue<short>>();
            this.DataConverter.DeltsDataReadyEvent += CollectConvertedPackage;

            //Проверим, есть ли в серверном хранилище этот файл
            if (this.FileWorker.IsSeverFileExist())
            {
                //Если такой файл есть в хранилище, то проверим его размер и последний индекс отправленных данных
                if (this.FileWorker.Record.StartIndexServerFile >= 0 && this.FileWorker.Record.StartIndexServerFile < this.FileWorker.GetFileSize())
                {
                    //И если он меньше, то включаем загрузку из файла
                    this.IsSavedServerDataMode = true;
                }
            }
        }

        /// <summary>
        /// Подписываемся на получение готовых пакетов данных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">EventArgs хорнящий список сигналов по отведениям</param>
        private void CollectConvertedPackage(object sender, DeltsDataEventArgs e)
        {
            //TODO: при появлении времени в пакете DeltsDataEventArgs, создать новый файл

            if (e.DeltasData == null) return;
            if (this.SignalsList == null || this.SignalsList.Count == 0) this.SignalsList = InitStorage(e.DeltasData.Count);


            for (int i=0; i < this.SignalsList.Count; i++)
            {
                for (int j=0; j < e.DeltasData[i].Length; j++)
                {
                    this.SignalsList[i].Enqueue(e.DeltasData[i][j]);
                }
            }
            this.SignalListSize = (uint)(this.SignalsList[0].Count);

            if (!IsSendProcessRun)
            {
                this.IsSendProcessRun = true;
                //Запустит Task по отправке данных
                this.CreateServerFileTask().ContinueWith(RunnerProcessTasks);
            }
        }

        private List<ConcurrentQueue<short>> InitStorage(int signalsNumber)
        {
            if (signalsNumber == 0) return null;
            List<ConcurrentQueue<short>> localSignalsList = new List<ConcurrentQueue<short>>();
            for (int i = 0; i < signalsNumber; i++)
                localSignalsList.Add(new ConcurrentQueue<short>());
            return localSignalsList;
        }

        private int CheckRequiredQuantity()
        {
            if (this.SignalsList != null && this.SignalsList.Count > 0)
            {
                //Проверяем доступное количество данных
                //В зависимости от текущего режима, выбираем один из сценариев установки размера отправляемого пакета
                int countReadyData = -1;
                if (this.SendingMode == SendMode.OnlineMode)
                {
                    if (this.SignalsList[0].Count >= this.PackageSize)
                        countReadyData = this.PackageSize;
                    else
                        countReadyData = -1;
                }
                if (this.SendingMode == SendMode.OfflineMode)
                    countReadyData = this.SignalsList[0].Count;

                return countReadyData;
            }
            else
                return -1;
        }

        private int EasyCheckRequiredQuantity()
        {
            if (this.SignalsList != null && this.SignalsList.Count > 0)
            {
                int countReadyData = 0;

                if (this.SignalsList[0].Count >= this.PackageSize)
                    countReadyData = this.PackageSize;
                return countReadyData;
            }
            else
                return 0;
        }

        Task PartSenderTask()
        {
            return Task.Factory.StartNew(
                () =>
                {
                    int BatchSize = CheckRequiredQuantity();
                    if (!this.IsAllProcessStop && this.CurrentUser != null && this.CurrentUser.SessionID != null && this.CurrentUser.SessionID.Length > 0
                    && this.FileWorker.Record.FileID > 0 && this.IsServerFileCreate && BatchSize > 0)
                    {
                        if (this.ServerWorkerInstance.IsInternetConnection)
                        {
                            short[] deltsArray = null;
                            long realByteCount = this.SavedPackageSize;

                            deltsArray = new short[this.SignalsList.Count * BatchSize];
                            for (int i = 0; i < deltsArray.Length; i += this.SignalsList.Count)
                            {
                                for (int j = 0; j < this.SignalsList.Count; j++)
                                {
                                    short deltaValue = 0;
                                    this.SignalsList[j].TryDequeue(out deltaValue);
                                    deltsArray[i + j] = deltaValue;
                                }
                            }

                            try
                            {
                                if (this.IsSavedServerDataMode)
                                {
                                    //Запись данных в файл хранилища
                                    this.FileWorker.AppenServerData(deltsArray);
                                    //Если в серверном хранилище есть файл и достаточное количество байт
                                    if (this.FileWorker.Record.StartIndexServerFile <= this.FileWorker.GetFileSize())
                                    {
                                        //Считываем данные, либо все, либо часть из файла
                                        deltsArray = this.FileWorker.GetSavedPartServerData(this.FileWorker.Record.StartIndexServerFile, ref realByteCount).ToArray();
                                    }
                                    else
                                    {
                                        deltsArray = null;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                string exM = ex.Message;
                            }


                            if (deltsArray != null)
                            {
                                //Формируем запрос
                                IRequest sendPart = new AddFileBlock(this.CurrentUser.SessionID, this.FileWorker.Record.FileID, this.PartNumber, deltsArray);
                                //Отправляем данные
                                int sendResult = -1;
                                try
                                {
                                    sendResult = this.ServerWorkerInstance.AddBlock(sendPart).GetAwaiter().GetResult();
                                }
                                catch (Exception ex)
                                {
                                    string exceptionMesage = ex.Message;
                                    sendResult = -1;
                                }

                                if (sendResult >= 0)
                                {
                                    this.FileWorker.LastServerPartUpdate((int)this.PartNumber);
                                    this.PartNumber++;
                                    if (this.IsSavedServerDataMode)
                                    {
                                        //Обновляем данные в файлохранилище
                                        this.FileWorker.UpdateLastByteServersFile(realByteCount);
                                        //Проверка на выход из режима отправки из локального файла
                                        if (this.FileWorker.Record.StartIndexServerFile == this.FileWorker.GetFileSize())
                                        {
                                            //Мы прочли весь серверный файл и можем выйти из режима считывания из него, удалить, и занулить
                                            this.IsSavedServerDataMode = false;
                                            this.FileWorker.UpdateLastByteServersFile(-1);
                                            this.FileWorker.DeleteServerData();
                                        }
                                    }
                                }
                                else
                                {
                                    if (sendResult == -2)
                                    {
                                        //Ошибка с отправкой конкретной части решение: увеличить счётчик на 1
                                        this.PartNumber++;
                                    }
                                    if (!this.IsSavedServerDataMode)
                                    {
                                        //Сохроняем не отправленные данные в файл, все данные. После чего занимаемся попытками отправить данные, каждый раз выгружая файлы
                                        this.IsSavedServerDataMode = true;
                                        this.FileWorker.AppenServerData(deltsArray);
                                    }
                                }
                            }
                        }
                        else
                        {
                            this.IsSavedServerDataMode = true;
                            //Сохраняем данные в файл сервера
                            if (BatchSize > 0)
                            {
                                int countData = BatchSize;
                                short[] deltsArray = new short[this.SignalsList.Count * countData];
                                for (int i = 0; i < deltsArray.Length; i += this.SignalsList.Count)
                                {
                                    for (int j = 0; j < this.SignalsList.Count; j++)
                                    {
                                        short deltaValue = 0;
                                        this.SignalsList[j].TryDequeue(out deltaValue);
                                        deltsArray[i + j] = deltaValue;
                                    }
                                }
                                //Запись данных в файл хранилища
                                this.FileWorker.AppenServerData(deltsArray);
                            }
                            //Ждём подключения к интернету
                            Task.Delay(5000).GetAwaiter().GetResult();
                        }
                    }
                }
            );
        }

        Task EasyPartSenderTask()
        {
            return Task.Factory.StartNew(
                () =>
                {
                    int BatchSize = EasyCheckRequiredQuantity();
                    if (!this.IsAllProcessStop && this.CurrentUser != null && this.CurrentUser.SessionID != null && this.CurrentUser.SessionID.Length > 0
                    && this.FileWorker.Record.FileID > 0 && this.IsServerFileCreate && BatchSize > 0)
                    {
                        if (this.ServerWorkerInstance.IsInternetConnection)
                        {
                            short[] deltsArray = null;
                            deltsArray = new short[this.SignalsList.Count * BatchSize];
                            for (int i = 0; i < deltsArray.Length; i += this.SignalsList.Count)
                            {
                                for (int j = 0; j < this.SignalsList.Count; j++)
                                {
                                    short deltaValue = 0;
                                    this.SignalsList[j].TryDequeue(out deltaValue);
                                    deltsArray[i + j] = deltaValue;
                                }
                            }
                            this.SignalListSize = (uint)(this.SignalsList[0].Count);
                            //Формируем запрос
                            IRequest sendPart = new AddFileBlock(this.CurrentUser.SessionID, this.FileWorker.Record.FileID, this.PartNumber, deltsArray);
                            //Отправляем данные
                            int sendResult = -1;
                            while (sendResult < 0)
                            {
                                try
                                {
                                    sendResult = this.ServerWorkerInstance.AddBlock(sendPart).GetAwaiter().GetResult();
                                }
                                catch (Exception ex)
                                {
                                    string exceptionMesage = ex.Message;
                                    sendResult = -1;
                                }

                                if (sendResult >= 0)
                                {
                                    this.FileWorker.LastServerPartUpdate((int)this.PartNumber);
                                    this.PartNumber++;
                                }
                                else
                                {
                                    if (sendResult == -2)
                                    {
                                        //Ошибка с отправкой конкретной части решение: увеличить счётчик на 1
                                        this.PartNumber++;
                                    }
                                    //Файл не создан, возможно протухла сессия. Проверим это
                                    //-101 это код протухщей сессии
                                    if (sendResult == -101)
                                    {
                                        //Обновляем токен
                                        string sessionToken = this.ServerWorkerInstance.Authorization(
                                            new CreateSession(this.CurrentUser.Login, this.CurrentUser.Password)).GetAwaiter().GetResult();

                                        if (sessionToken != null && sessionToken.Length > 10)
                                        {
                                            this.DatabaseWorker.UpdateSessionID(this.CurrentUser.Login, sessionToken);
                                            this.DatabaseWorker.SetCurrentUser(this.CurrentUser.Login);
                                            this.CurrentUser = this.DatabaseWorker.CurrentUser;
                                        }

                                        this.IsServerFileClose = true;
                                        this.IsAllProcessStop = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            //Ждём подключения к интернету
                            Task.Delay(5000).GetAwaiter().GetResult();
                        }
                    }
                    else
                    {
                        if (BatchSize <= 0)
                        {
                            //Ждём подключения к интернету
                            Task.Delay(1000).GetAwaiter().GetResult();
                        }
                    }
                }
            );
        }

        byte LeadsCountCalc(int channelsCount)
        {
            if (channelsCount > 0 && channelsCount <= 8)
            {
                byte leadsCount = 1;
                if (channelsCount == 2)
                    leadsCount = 6;
                if (channelsCount == 8)
                    leadsCount = 12;
                return leadsCount;
            }
            return 0;
        }

        Task CreateServerFileTask()
        {
            return Task.Factory.StartNew(
            () =>
            {
                if (!IsAllProcessStop && this.IsSendProcessRun && !this.IsServerFileCreate && this.CurrentUser != null
                && this.FileWorker.Record != null && this.CurrentUser.SessionID != null && this.CurrentUser.SessionID.Length > 0)
                {
                    if (this.ServerWorkerInstance.IsInternetConnection)
                    {
                        //Создаём файл на сервере.
                        long currentFileID = -1;

                        FileAttribute fileAttribute = new FileAttribute((short)this.FileWorker.Record.SampleFrequency, 
                            LeadsCountCalc(this.FileWorker.Record.ChannelsCount), "SYGNAL_CHANNELS");

                        //Пока что CodeECG всегда 106
                        IRequest request = new CreateFile(this.CurrentUser.SessionID, CodeECG.HolterECG, 
                            this.FileWorker.Record.StartRecord, this.FileWorker.Record.FileName,
                            this.CurrentUser.MACS, "FF-1", fileAttribute, this.CurrentPatient.PatientID);

                        try
                        {
                            currentFileID = this.ServerWorkerInstance.CreateFile(request).GetAwaiter().GetResult();
                        }
                        catch (Exception ex)
                        {
                            string exceptionMessage = ex.Message;
                            currentFileID = -1;
                        }

                        if (currentFileID >= 0)
                        {
                            this.IsServerFileCreate = true;
                            this.IsServerFileClose = false;
                            this.CurrentFileID = currentFileID;
                            this.FileWorker.FileIDUpdate(this.CurrentFileID);
                        }
                        else
                        {
                            //Файл не создан, возможно протухла сессия. Проверим это
                            //-101 это код протухщей сессии
                            if (currentFileID == -101)
                            {
                                //Обновляем токен
                                string sessionToken = this.ServerWorkerInstance.Authorization(
                                    new CreateSession(this.CurrentUser.Login, this.CurrentUser.Password)).GetAwaiter().GetResult();

                                if (sessionToken != null && sessionToken.Length > 10)
                                {
                                    this.DatabaseWorker.UpdateSessionID(this.CurrentUser.Login, sessionToken);
                                    this.DatabaseWorker.SetCurrentUser(this.CurrentUser.Login);
                                    this.CurrentUser = this.DatabaseWorker.CurrentUser;
                                }
                            }
                            this.IsServerFileCreate = false;
                            this.CurrentFileID = -1;
                        }
                    }
                    else
                    {
                        //Будем проверять подключение каждые 5 секунд
                        Task.Delay(5000).GetAwaiter().GetResult();
                    }
                }
            });
        }

        //Выносить из циклической таски.
        Task CloseServerFileTask()
        {
            return Task.Factory.StartNew(
                () =>
                {
                    if (!IsAllProcessStop && this.IsSendProcessRun && this.IsServerFileCreate && this.CurrentUser != null
                && this.FileWorker.Record != null && this.CurrentUser.SessionID != null && this.CurrentUser.SessionID.Length > 0)
                    {
                        if (this.ServerWorkerInstance.IsInternetConnection)
                        {
                            //Закрываем файл на сервере.
                            IRequest request = new CloseFile(this.CurrentUser.SessionID, this.FileWorker.Record.FileID);
                            int closeCodeResponse = -1;
                            try
                            {
                                closeCodeResponse = this.ServerWorkerInstance.CloseFile(request).GetAwaiter().GetResult();
                            }
                            catch (Exception ex)
                            {
                                string exceptionMessage = ex.Message;
                                closeCodeResponse = -1;
                            }
                            if (closeCodeResponse >= 0)
                            {
                                this.IsServerFileClose = true;
                            }
                            else
                            {
                                this.IsServerFileClose = false;
                            }
                        }
                        else
                        {
                            //Будем проверять подключение каждые 5 секунд
                            Task.Delay(5000).GetAwaiter().GetResult();
                        }
                    }
                    this.IsServerFileClose = true; //Лучше не придумал
                });
        }

        /// <summary>
        /// Метод, запускающий опрашивающую поток данных задачу, используюя ContinueWith
        /// </summary>
        /// <param name="t"></param>
        void RunnerProcessTasks(Task t)
        {
            if (this.IsSendProcessRun)
            {
                if (this.IsServerFileCreate)
                {
                    if (!this.IsAllProcessStop)
                    {
                        //Если файл создан и процесс не останвливался - начинаем и продолжаем отправку данных на сервер.
                        //PartSenderTask().ContinueWith(RunnerProcessTasks);
                        EasyPartSenderTask().ContinueWith(RunnerProcessTasks);
                    }
                }
                else
                {
                    //Если файл не создан, продолжаем операцию создания файла на сервере
                    CreateServerFileTask().ContinueWith(RunnerProcessTasks);
                }
            }
        }

        //Запускаем таску на закрытие файла
        /*
        public Task CloseServerFile()
        {
            return Task.Factory.StartNew(
            () =>
            {
                if (!this.IsServerFileClose && this.CurrentFileID >= 0 && this.FileWorker.Record != null && this.CurrentUser != null 
                && this.CurrentUser.SessionID != null && this.CurrentUser.SessionID.Length > 0)
                {
                    short tryCount = 0;
                    int closeCodeResponse = -1;
                    while (closeCodeResponse < 0 && tryCount < 3)
                    {
                        if (this.ServerWorkerInstance.IsInternetConnection)
                        {
                            //Закрываем файл на сервере.
                            IRequest request = new CloseFile(this.CurrentUser.SessionID, this.FileWorker.Record.FileID);
                            try
                            {
                                closeCodeResponse = this.ServerWorkerInstance.CloseFile(request).GetAwaiter().GetResult();
                            }
                            catch (Exception ex)
                            {
                                string exceptionMessage = ex.Message;
                                closeCodeResponse = -1;
                            }
                            if (closeCodeResponse >= 0)
                            {
                                this.IsServerFileClose = true;
                            }
                            else
                            {
                                this.IsServerFileClose = false;
                            }
                        }
                        else
                        {
                            //Будем проверять подключение каждые 2 секунд
                            Task.Delay(2000).GetAwaiter().GetResult();
                        }
                        if (closeCodeResponse < 0) tryCount++;
                        else
                        {
                            this.FileWorker.UpdateLastByteServersFile(-1);
                        }
                    }
                }
            });
        }
        */

        public async Task<bool> CloseServerFile()
        {
            bool closeResult = false;

            if (!this.IsServerFileClose && this.CurrentFileID >= 0 && this.FileWorker.Record != null && this.CurrentUser != null
                && this.CurrentUser.SessionID != null && this.CurrentUser.SessionID.Length > 0)
            {
                short tryCount = 0;
                int closeCodeResponse = -1;
                while (closeCodeResponse < 0 && tryCount < 3)
                {
                    if (this.ServerWorkerInstance.IsInternetConnection)
                    {
                        //Закрываем файл на сервере.
                        IRequest request = new CloseFile(this.CurrentUser.SessionID, this.FileWorker.Record.FileID);
                        try
                        {
                            closeCodeResponse = await this.ServerWorkerInstance.CloseFile(request);
                        }
                        catch (Exception ex)
                        {
                            string exceptionMessage = ex.Message;
                            closeCodeResponse = -1;
                        }
                        if (closeCodeResponse >= 0)
                        {
                            this.IsServerFileClose = true;
                            closeResult = true;
                        }
                        else
                        {
                            this.IsServerFileClose = false;
                            closeResult = false;
                        }
                    }
                    else
                    {
                        //Будем проверять подключение каждые 2 секунд
                        await Task.Delay(2000);
                    }
                    if (closeCodeResponse < 0) tryCount++;
                    else
                    {
                        this.FileWorker.UpdateLastByteServersFile(-1);
                    }
                }
            }

            return closeResult;
        }


        public void Dispose()
        {
            if (this.DataConverter != null)
                this.DataConverter.DeltsDataReadyEvent -= CollectConvertedPackage;
            this.IsAllProcessStop = true;
            this.IsSendProcessRun = false;
            this.ServerWorkerInstance = null;
            this.Increments = null;
            this.SignalsList = null;
        }

        private short[] ConvertSignals(List<List<short>> serverData)
        {
            if (serverData != null && serverData.Count > 0)
            {
                List<short> resultsArray = new List<short>();
                int dataCount = serverData[0].Count;

                for (int i = 0; i < dataCount; i++)
                {
                    for (int j = 0; j < serverData.Count; j++)
                    {
                        resultsArray.Add(serverData[j][i]);
                    }
                }
                if (resultsArray.Count > 0) return resultsArray.ToArray();
            }
            return null;
        }
    }
}
