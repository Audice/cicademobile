﻿using FenixMobile.Model.DatabasePart;
using FenixMobile.Model.FilePart.FileEventArgs;
using FenixMobile.View.Interface;
using SoapConstruct;
using SoapConstruct.SoapFunction;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace FenixMobile.Model.ServerPart.ServerAction
{
    public class AppendServerFile : IServerAction
    {
        /// <summary>
        /// Добавление новой порции данных на сервер
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public int ServerActionRun(SpecificFileReadyEventArgs args)
        {
            //Получаем экземпляр сервера для отправки данных
            ServerWorker serverWorker = ServerWorker.getInstance();
            //Получение экземпляра базы данных
            DatabaseWorker databaseWorker = DatabaseWorker.getInstance();
            RecordTable record = databaseWorker.GetRecordByID(args.RecordID);
            UserTable currentUser = databaseWorker.CurrentUser;
            PatientTable currentPatient = databaseWorker.CurrentPatient;
            List<short> dataPart = DependencyService.Get<IFileWorker>().GetFile(record.FileName + " №" + args.FileNum);
            IRequest sendPart = new AddFileBlock(currentUser.SessionID, record.FileID, args.FileNum, dataPart.ToArray());
            int appendResult = -1;
            try
            {
                appendResult = serverWorker.AddBlock(sendPart).GetAwaiter().GetResult();
            }
            catch (Exception ex)
            {
                string exceptionMesage = ex.Message;
                appendResult = -1;
            }
            if (appendResult >= 0)
                databaseWorker.UpdateRecordsServerPart(args.RecordID, args.FileNum);

            return appendResult;
        }
    }
}
