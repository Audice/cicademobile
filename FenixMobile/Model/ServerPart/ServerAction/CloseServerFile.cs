﻿using FenixMobile.Model.DatabasePart;
using FenixMobile.Model.FilePart.FileEventArgs;
using SoapConstruct;
using SoapConstruct.SoapFunction;
using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.ServerPart.ServerAction
{
    public class CloseServerFile : IServerAction
    {
        public int ServerActionRun(SpecificFileReadyEventArgs args)
        {
            //Получаем экземпляр сервера для отправки данных
            ServerWorker serverWorker = ServerWorker.getInstance();
            //Получение экземпляра базы данных
            DatabaseWorker databaseWorker = DatabaseWorker.getInstance();
            RecordTable record = databaseWorker.GetRecordByID(args.RecordID);
            UserTable currentUser = databaseWorker.CurrentUser;
            //Закрываем файл на сервере.
            IRequest request = new CloseFile(currentUser.SessionID, record.FileID);
            int closeCodeResponse = -1;
            while (closeCodeResponse < 0)
            {
                try
                {
                    closeCodeResponse = serverWorker.CloseFile(request).GetAwaiter().GetResult();
                }
                catch (Exception ex)
                {
                    string exceptionMessage = ex.Message;
                    closeCodeResponse = -1;
                    return -1;
                }
            }
            //Оповещаем бд о закрытие файла
            databaseWorker.UpdateRecordsCloseFlag(record.ID, true);

            return closeCodeResponse;

        }
    }
}
