﻿using FenixMobile.Model.DatabasePart;
using FenixMobile.Model.FilePart.FileEventArgs;
using SoapConstruct;
using SoapConstruct.SoapFunction;
using SoapConstruct.SoapFunction.AttribConstruct;
using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.ServerPart.ServerAction
{
    public class CreateServerFile : IServerAction
    {
        public int ServerActionRun(SpecificFileReadyEventArgs args)
        {
            //Получаем экземпляр сервера для отправки данных
            ServerWorker serverWorker = ServerWorker.getInstance();
            //Получение экземпляра базы данных
            DatabaseWorker databaseWorker = DatabaseWorker.getInstance();
            RecordTable record = databaseWorker.GetRecordByID(args.RecordID);
            UserTable currentUser = databaseWorker.CurrentUser;
            PatientTable currentPatient = databaseWorker.CurrentPatient;
            //Создаём файл на сервере.
            long currentFileID = -1;
            FileAttribute fileAttribute = new FileAttribute((short)record.SampleFrequency,
                            LeadsCountCalc(record.ChannelsCount), "SYGNAL_CHANNELS");
            //Пока что CodeECG всегда 106
            IRequest request = new CreateFile(currentUser.SessionID, CodeECG.HolterECG,
                record.StartRecord, record.FileName,
                record.MAC, "FF-1", fileAttribute, currentPatient.PatientID);
            try
            {
                currentFileID = serverWorker.CreateFile(request).GetAwaiter().GetResult();
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;
            }
            if (currentFileID > 0)
            {
                databaseWorker.UpdateRecordsFileID(args.RecordID, currentFileID);
                return 1;
            }
            else
            {
                //Возможная проблема - протухшая сессия. Код ошибки -101
                if (currentFileID == -101)
                {
                    //Обновляем токен
                    string sessionToken = serverWorker.Authorization(
                        new CreateSession(currentUser.Login, currentUser.Password)).GetAwaiter().GetResult();
                    if (sessionToken != null && sessionToken.Length > 10)
                    {
                        databaseWorker.UpdateSessionID(currentUser.Login, sessionToken);
                        databaseWorker.SetCurrentUser(currentUser.Login);
                    }
                }
            }
            return -1; //Ошибка работы метода
        }


        /// <summary>
        /// Метод формирования информации о числе отведений в сигнале
        /// </summary>
        /// <param name="channelsCount">Количество каналов кардиографа</param>
        /// <returns>Число сформированных отведений</returns>
        byte LeadsCountCalc(int channelsCount)
        {
            if (channelsCount > 0 && channelsCount <= 8)
            {
                byte leadsCount = 1;
                if (channelsCount == 2)
                    leadsCount = 6;
                if (channelsCount == 8)
                    leadsCount = 12;
                return leadsCount;
            }
            return 0;
        }
    }
}
