﻿using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using SoapConstruct;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FenixMobile.Model.ServerPart
{
    /// <summary>
    /// Класс, отвечающий за отправку и приём информации 
    /// </summary>
    public class ServerWorker
    {
        private static ServerWorker ServerWorkerInstance;


        bool isInternetConnection = false;
        /// <summary>
        /// Свойство, характеризующее текущее состояние подключение к сети интернет
        /// </summary>
        public bool IsInternetConnection
        {
            get { return isInternetConnection; }
            private set
            {
                isInternetConnection = value;
                ChangeConnectionState?.Invoke(this, EventArgs.Empty);
            }
        }
        public EventHandler<EventArgs> ChangeConnectionState;

        public string URLServer { get; private set; } = "https://healing-pebbles.ru:8444/ws?wsdl";

        /// <summary>
        /// Объект отправщик soap пакетов
        /// </summary>
        private HttpClient MessageSender;

        protected ServerWorker()
        {
            MessageSender = new HttpClient();
            MessageSender.DefaultRequestHeaders.Add("SOAPAction", URLServer);

            this.IsInternetConnection = CrossConnectivity.Current.IsConnected;

            CrossConnectivity.Current.ConnectivityChanged += ConnectivityChanged;
        }

        /// <summary>
        /// Реакция на изменение состояния подключения к сети интернет
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            this.IsInternetConnection = e.IsConnected;
        }

        public static ServerWorker getInstance()
        {
            if (ServerWorkerInstance == null)
            {
                ServerWorkerInstance = new ServerWorker();
            }
            return ServerWorkerInstance;
        }


        //Следующие методы адаптированы для работы с конкретным функционлом сервера: создание файла-дополнение-закрытие, регистрации, добавления юзеров
        public async Task<string> Authorization(IRequest authorizationMessage)
        {
            if (!this.IsInternetConnection || this.MessageSender == null) return "";
            SoapConstructor soapConstructor = new SoapConstructor();
            string response = await SendPackage(soapConstructor.Construct(authorizationMessage));
            if (response == null) return null;
            //Обработка ответа
            if (response.Contains("null")) return null;
            else return response;
        }

        public async Task<long> CreateFile(IRequest createFileMessage)
        {
            if (!this.IsInternetConnection || this.MessageSender == null) return -4;
            SoapConstructor soapConstructor = new SoapConstructor();
            string response = await SendPackage(soapConstructor.Construct(createFileMessage));
            if (response == null) return -5;
            //Обработка ответа
            long responseCode = -6;
            long.TryParse(response, out responseCode);

            return responseCode; //-2: проблема с парсингом ответа. Всё что больше или равно 0 хороший ответ.
        }


        public async Task<int> AddBlock(IRequest addBlockMessage)
        {
            if (!this.IsInternetConnection || this.MessageSender == null) return -4;
            SoapConstructor soapConstructor = new SoapConstructor();
            string response = await SendPackage(soapConstructor.Construct(addBlockMessage));
            if (response == null) return -5;
            //Обработка ответа
            int responseCode = -6;
            int.TryParse(response, out responseCode);
            return responseCode; //-6: проблема с парсингом ответа. Всё что больше или равно 0 хороший ответ.
        }

        public async Task<int> CloseFile(IRequest closeFileMessage)
        {
            if (!this.IsInternetConnection || this.MessageSender == null) return -4;
            SoapConstructor soapConstructor = new SoapConstructor();
            string response = await SendPackage(soapConstructor.Construct(closeFileMessage));
            if (response == null) return -5;
            //Обработка ответа
            int responseCode = -6;
            int.TryParse(response, out responseCode);
            return responseCode; //-6: проблема с парсингом ответа. Всё что больше или равно 0 хороший ответ.
        }


        public async Task<long> CreatePatient(IRequest createFileMessage)
        {
            if (!this.IsInternetConnection || this.MessageSender == null) return -4;
            SoapConstructor soapConstructor = new SoapConstructor();
            string response = await SendPackage(soapConstructor.Construct(createFileMessage));
            if (response == null) return -5;
            //Обработка ответа
            long responseCode = -6;
            long.TryParse(response, out responseCode);
            return responseCode; //-6: проблема с парсингом ответа. Всё что больше или равно 0 хороший ответ.
        }



        /// <summary>
        /// Задача отправки soap пакета на сервер
        /// </summary>
        /// <param name="soapPackage">Текстовое представление SOAP пакета</param>
        /// <returns>Ответ от сервера в виде строки</returns>
        private async Task<string> SendPackage(string soapPackage)
        {
            if (MessageSender == null || soapPackage == null) return null;
            var soapResponse = "";
            try
            {
                var content = new StringContent(soapPackage, Encoding.UTF8, "text/xml");
                var response = await MessageSender.PostAsync(new Uri(URLServer), content);
                soapResponse = await response.Content.ReadAsStringAsync();

                string returnNode = GetReturnNode(soapResponse, "return");
                return returnNode;
            }
            catch (Exception e)
            {
                return null;
            }
        }





        /// <summary>
        /// Поиск значения в узле NodeName в ответе от сервера
        /// </summary>
        /// <param name="Response">Ответ от сервера</param>
        /// <param name="NodeName">Целевой узел</param>
        /// <returns></returns>
        private string GetReturnNode(string Response, string NodeName)
        {
            string resultString = "";
            try
            {
                var xmlReader = XmlReader.Create(new StringReader(Response));

                do xmlReader.Read();
                while (!xmlReader.Name.Contains(NodeName) && !xmlReader.Name.Equals(""));

                xmlReader.Read();
                resultString = xmlReader.Value;
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
                resultString = null;
            }
            if (resultString != null && resultString.Length == 0) resultString = null;

            return resultString;
        }


    }
}
