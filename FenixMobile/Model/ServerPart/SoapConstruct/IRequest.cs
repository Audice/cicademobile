﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SoapConstruct
{
    public interface IRequest
    {
        string MethodName { get; }
        XElement GetNodes(XNamespace methodNamespace);
    }

    public enum CodeECG
    {
        ShortlengthECG = 101, //??
        MediumlengthECG = 102, //?
        HolterECG = 106
    }
}
