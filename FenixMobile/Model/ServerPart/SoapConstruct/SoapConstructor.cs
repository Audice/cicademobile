﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace SoapConstruct
{
    public class SoapConstructor
    {
        private readonly string SoapEnvelopeNamespaceReference = "http://schemas.xmlsoap.org/soap/envelope/";
        private readonly string SoapEncodingNamespaceReference = "http://schemas.xmlsoap.org/soap/encoding/";
        private readonly string XSINamespaceReference = "http://www.w3.org/2001/XMLSchema-instance";
        private readonly string XSDNamespaceReference = "http://www.w3.org/2001/XMLSchema";
        private string HeadNamespaceReference = "http://webservice.cyberheart.nnsu.com/";

        private readonly string EnvelopeNamespace = "SOAP-ENV";
        private readonly string HeadNamespace = "ns1";

        public SoapConstructor(string firstNamespace = null)
        {
            if (firstNamespace != null) this.HeadNamespaceReference = firstNamespace;
        }

        public string Construct(IRequest SoapRequestPackage)
        {
            if (SoapRequestPackage == null) return null;

            XDocument soapRequest = new XDocument(new XDeclaration("1.0", "utf-8", ""));
            XElement titleNode = ConstructNamespaceTitleNode();
            XElement bodyNode = ConstructBodyNode(SoapRequestPackage);
            titleNode.Add(bodyNode);
            soapRequest.Add(titleNode);
            return XDocumentToString(soapRequest);
        }

        private XElement ConstructBodyNode(IRequest SoapRequestPackage)
        {
            XNamespace bodyNamespace = SoapEnvelopeNamespaceReference;
            XElement bodyNode = new XElement(bodyNamespace + "Body");
            XNamespace methodNamespace = HeadNamespaceReference;
            bodyNode.Add(SoapRequestPackage.GetNodes(methodNamespace));
            return bodyNode;
        }

        private XElement ConstructNamespaceTitleNode()
        {
            XNamespace nsa = SoapEnvelopeNamespaceReference;
            XElement titleNode = new XElement(nsa + "Envelope");
            titleNode.Add(new XAttribute(XNamespace.Xmlns + EnvelopeNamespace, SoapEnvelopeNamespaceReference));
            titleNode.Add(new XAttribute(XNamespace.Xmlns + "SOAP-ENC", SoapEncodingNamespaceReference));
            titleNode.Add(new XAttribute(XNamespace.Xmlns + "xsi", XSINamespaceReference));
            titleNode.Add(new XAttribute(XNamespace.Xmlns + "xsd", XSDNamespaceReference));
            titleNode.SetAttributeValue(XNamespace.Xmlns + HeadNamespace, HeadNamespaceReference);

            return titleNode;
        }

        private string XDocumentToString(XDocument xDocument)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.Encoding = Encoding.UTF8;
            string result = "";
            using (var stringWriter = new StringWriterWithEncoding(Encoding.UTF8))
            using (var xmlTextWriter = XmlWriter.Create(stringWriter, settings))
            {
                xDocument.WriteTo(xmlTextWriter);
                xmlTextWriter.Flush();
                result = stringWriter.GetStringBuilder().ToString();
            }
            return result;
        }

        public class StringWriterWithEncoding : StringWriter
        {
            private readonly Encoding encoding;
            public StringWriterWithEncoding() { }
            public StringWriterWithEncoding(Encoding encoding)
            {
                this.encoding = encoding;
            }
            public override Encoding Encoding
            {
                get { return encoding; }
            }
        }

    }
}
