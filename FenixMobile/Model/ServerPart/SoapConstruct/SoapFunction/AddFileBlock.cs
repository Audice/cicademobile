﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SoapConstruct.SoapFunction
{
    public class AddFileBlock : IRequest
    {
        public string MethodName
        {
            get;
            private set;
        }

        private string valueSessionID = "";
        public string ValueSessionID
        {
            get { return valueSessionID; }
            set
            {
                valueSessionID = value;
                UpdateParameter(ParameterSessionID, value);
            }
        }
        private long valueFileID;
        public long ValueFileID
        {
            get { return valueFileID; }
            set
            {
                valueFileID = value;
                UpdateParameter(ParameterFileID, value.ToString());
            }
        }
        private long valuePartSequence;
        public long ValuePartSequence
        {
            get { return valuePartSequence; }
            set
            {
                valuePartSequence = value;
                UpdateParameter(ParameterPartSequence, value.ToString());
            }
        }

        private short[] valueData;
        public short[] ValueData
        {
            get { return valueData; }
            set
            {
                valueData = value;
                UpdateParameter(ParameterData, CovertData(value));
            }
        }

        //Имена парамметров запроса
        private readonly string ParameterSessionID = "session_id";
        private readonly string ParameterFileID = "file_id";
        private readonly string ParameterPartSequence = "part_seq";
        private readonly string ParameterData = "data";

        /// <summary>
        /// Словарь значений: имя SOAP параметра - значение параметра
        /// </summary>
        private Dictionary<string, string> Parameters = null;

        public AddFileBlock(string sessionID, long fileID, long partSequence, short[] data)
        {
            this.Parameters = new Dictionary<string, string>();
            this.MethodName = "addFileBlock";
            this.ValueSessionID = sessionID;
            this.ValueFileID = fileID;
            this.ValuePartSequence = partSequence;
            this.ValueData = data;
        }

        private void UpdateParameter(string parameterName, string parameterValue)
        {
            if (this.Parameters == null) this.Parameters = new Dictionary<string, string>();
            if (this.Parameters.ContainsKey(parameterName)) this.Parameters[parameterName] = parameterValue;
            else this.Parameters.Add(parameterName, parameterValue);
        }

        private string CovertData(short[] data)
        {
            byte[] bytesArray = new byte[data.Length * sizeof(Int16)];
            Buffer.BlockCopy(data, 0, bytesArray, 0, bytesArray.Length);
            return Convert.ToBase64String(bytesArray);
        }

        public XElement GetNodes(XNamespace methodNamespace)
        {
            if (this.Parameters == null) this.Parameters = new Dictionary<string, string>();
            XElement methodNode = new XElement(methodNamespace + this.MethodName);

            foreach (KeyValuePair<string, string> keyValue in this.Parameters)
            {
                methodNode.Add(new XElement(keyValue.Key, keyValue.Value));
            }

            return methodNode;
        }
    }
}
