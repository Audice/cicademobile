﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using static SoapConstruct.SoapConstructor;

namespace SoapConstruct.SoapFunction.AttribConstruct
{
    public class FileAttribute
    {
        private readonly string[] Channels = new string[] { "I", "II", "III", "AVR", "AVL", "AVF"};
        private readonly string ParameterFileAttribute = "file_attrib";

        private readonly string defaultXML = "<ROOT>< Measurement >< SampleRate > 250 </ SampleRate >< Channels >< C0 >< Name > I </ Name ></ C0 ></ Channels ></ Measurement ></ ROOT >";

        public XElement AttributeXML
        {
            get; private set;
        } = null;

        public FileAttribute(short sampleRate, byte countChannels, string attributeName)
        {
            AttributeConstruct(sampleRate, countChannels, attributeName);
        }

        private void AttributeConstruct(short sampleRate, byte countChannels, string attributeName)
        {
            XElement headElement = new XElement(ParameterFileAttribute);
            XElement nameNode = new XElement("name", attributeName); headElement.Add(nameNode);
            XElement valueNode = new XElement("value", ValueNodeConstruct(sampleRate, countChannels)); headElement.Add(valueNode);
            this.AttributeXML = headElement;
        }

        private string ValueNodeConstruct(short sampleRate, byte countChannels)
        {
            XDocument valueXML = new XDocument(new XDeclaration("1.0", "utf-8", ""));
            XElement rootNode = new XElement("ROOT");
            XElement measurementNode = new XElement("Measurement");
            XElement samplerateNode = new XElement("SampleRate", sampleRate);
            XElement channelsNode = new XElement("Channels");

            for (int i=0; i < countChannels; i++)
            {
                XElement currentChannelNode = new XElement("C" + i.ToString());
                XElement currentChannelNameNode = new XElement("Name", Channels[i]);
                currentChannelNode.Add(currentChannelNameNode);
                channelsNode.Add(currentChannelNode);
            }
            measurementNode.Add(samplerateNode);
            measurementNode.Add(channelsNode);
            rootNode.Add(measurementNode);
            valueXML.Add(rootNode);

            return XDocumentToString(valueXML);
        }

        private string XDocumentToString(XDocument xDocument)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.Encoding = Encoding.UTF8;
            string result = "";
            using (var stringWriter = new StringWriterWithEncoding(Encoding.UTF8))
            using (var xmlTextWriter = XmlWriter.Create(stringWriter, settings))
            {
                xDocument.WriteTo(xmlTextWriter);
                xmlTextWriter.Flush();
                result = stringWriter.GetStringBuilder().ToString();
            }
            return result;
        }

        public override string ToString()
        {
            if (AttributeXML != null)
            {
                return AttributeXML.ToString();
            }
            return defaultXML;
        }
    }


}
