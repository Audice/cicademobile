﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoapConstruct.SoapFunction
{
    public class CheckPatient
    {
        public string MethodName
        {
            get; private set;
        }

        private string valueSessionID = "";
        public string ValueSessionID
        {
            get { return valueSessionID; }
            set
            {
                valueSessionID = value;
                UpdateParameter(ParameterSessionID, value);
            }
        }
        private string valueFIO = "";
        public string ValueFIO
        {
            get { return valueFIO; }
            set
            {
                valueFIO = value;
                UpdateParameter(ParameterFIO, value);
            }
        }
        private int valueGender;
        public int ValueGender
        {
            get { return valueGender; }
            set
            {
                valueGender = value;
                UpdateParameter(ParameterGender, value.ToString());
            }
        }
        private DateTime valueBirthday;
        public DateTime ValueBirthday
        {
            get { return valueBirthday; }
            set
            {
                valueBirthday = value;
                UpdateParameter(ParameterBirthday, FormattedData(value));
            }
        }
        
        //Имена парамметров запроса
        private readonly string ParameterSessionID = "session_id";
        private readonly string ParameterFIO = "FIO";
        private readonly string ParameterGender = "gender";
        private readonly string ParameterBirthday = "birthday";


        /// <summary>
        /// Словарь значений: имя SOAP параметра - значение параметра
        /// </summary>
        private Dictionary<string, string> Parameters = null;


        public CheckPatient(string sessionId, string FIO, int gender, DateTime birthDay)
        {
            this.Parameters = new Dictionary<string, string>();
            this.MethodName = "checkPatient";
            this.ValueSessionID = sessionId;
            this.ValueFIO = FIO;
            this.ValueGender = gender;
            this.ValueBirthday = birthDay;
        }

        private void UpdateParameter(string parameterName, string parameterValue)
        {
            if (this.Parameters == null) this.Parameters = new Dictionary<string, string>();
            if (this.Parameters.ContainsKey(parameterName)) this.Parameters[parameterName] = parameterValue;
            else this.Parameters.Add(parameterName, parameterValue);
        }

        public Dictionary<string, string> GetParametrs()
        {
            return this.Parameters != null ? this.Parameters : new Dictionary<string, string>();
        }

        private string FormattedData(DateTime curentDateTime)
        {
            //Example^ 2017-10-03T00:00:00.000+03:00
            return String.Format("{0:yyyy-MM-ddTHH:mm:ss.fff+03:00}", curentDateTime);
        }
    }
}
