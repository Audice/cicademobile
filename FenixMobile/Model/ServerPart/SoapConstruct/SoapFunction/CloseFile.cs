﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SoapConstruct.SoapFunction
{
    public class CloseFile : IRequest
    {
        public string MethodName
        {
            get; private set;
        }

        private string valueSessionID = "";
        public string ValueSessionID
        {
            get { return valueSessionID; }
            set
            {
                valueSessionID = value;
                UpdateParameter(ParameterSessionID, value);
            }
        }
        private long valueFileID;
        public long ValueFileID
        {
            get { return valueFileID; }
            set
            {
                valueFileID = value;
                UpdateParameter(ParameterFileID, value.ToString());
            }
        }
        //Имена парамметров запроса
        private readonly string ParameterSessionID = "session_id";
        private readonly string ParameterFileID = "file_id";
        /// <summary>
        /// Словарь значений: имя SOAP параметра - значение параметра
        /// </summary>
        private Dictionary<string, string> Parameters = null;


        public CloseFile(string sessionId, long fileId)
        {
            this.Parameters = new Dictionary<string, string>();
            this.MethodName = "closeFile";
            this.ValueSessionID = sessionId;
            this.ValueFileID = fileId;
        }

        private void UpdateParameter(string parameterName, string parameterValue)
        {
            if (this.Parameters == null) this.Parameters = new Dictionary<string, string>();
            if (this.Parameters.ContainsKey(parameterName)) this.Parameters[parameterName] = parameterValue;
            else this.Parameters.Add(parameterName, parameterValue);
        }
        public XElement GetNodes(XNamespace methodNamespace)
        {
            if (this.Parameters == null) this.Parameters = new Dictionary<string, string>();
            XElement methodNode = new XElement(methodNamespace + this.MethodName);

            foreach (KeyValuePair<string, string> keyValue in this.Parameters)
            {
                methodNode.Add(new XElement(keyValue.Key, keyValue.Value));
            }

            return methodNode;
        }
    }
}
