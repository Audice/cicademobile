﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SoapConstruct.SoapFunction
{
    public class Hello : IRequest
    {
        public string MethodName { get; private set; }

        private string valueName = "";
        public string ValueName
        {
            get { return valueName; }
            set
            {
                valueName = value;
                UpdateParameter(ParameterName, value);
            }
        }
        //Имена парамметров запроса
        private readonly string ParameterName = "name";
        /// <summary>
        /// Словарь значений: имя SOAP параметра - значение параметра
        /// </summary>
        private Dictionary<string, string> Parameters = null;

        public Hello(string yourName)
        {
            this.Parameters = new Dictionary<string, string>();
            this.MethodName = "hello";
            this.ValueName = yourName;
        }

        private void UpdateParameter(string parameterName, string parameterValue)
        {
            if (this.Parameters == null) this.Parameters = new Dictionary<string, string>();
            if (this.Parameters.ContainsKey(parameterName)) this.Parameters[parameterName] = parameterValue;
            else this.Parameters.Add(parameterName, parameterValue);
        }

        public XElement GetNodes(XNamespace methodNamespace)
        {
            if (this.Parameters == null) this.Parameters = new Dictionary<string, string>();
            XElement methodNode = new XElement(methodNamespace + this.MethodName);

            foreach (KeyValuePair<string, string> keyValue in this.Parameters)
            {
                methodNode.Add(new XElement(keyValue.Key, keyValue.Value));
            }

            return methodNode;
        }
    }
}
