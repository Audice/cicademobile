﻿using SoapConstruct.SoapFunction.AttribConstruct;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SoapConstruct.SoapFunction
{
    public class CreateFile : IRequest
    {
        public string MethodName { get; private set; }

        private string valueSessionID = "";
        public string ValueSessionID
        {
            get { return valueSessionID; }
            set
            {
                valueSessionID = value;
                UpdateParameter(ParameterSessionID, value);
            }
        }
        private CodeECG valueFileType;
        public CodeECG ValueFileType
        {
            get { return valueFileType; }
            set
            {
                valueFileType = value;
                UpdateParameter(ParameterFileType, ((int)value).ToString());
            }
        }

        private DateTime valueStartDatetime;
        public DateTime ValueStartDatetime
        {
            get { return valueStartDatetime; }
            set
            {
                valueStartDatetime = value;
                string formattedDate = FormattedData(value);
                UpdateParameter(ParameterStartDatetime, formattedDate);
            }
        }
        private string valueFileName = "";
        public string ValueFileName
        {
            get { return valueFileName; }
            set
            {
                valueFileName = value;
                UpdateParameter(ParameterFileName, value);
            }
        }
        private string valueDiveceID = "";
        /// <summary>
        /// MAC-адрес устройства
        /// </summary>
        public string ValueDiveceID
        {
            get { return valueDiveceID; }
            set
            {
                valueDiveceID = value;
                UpdateParameter(ParameterDiveceID, value);
            }
        }

        private string valueDeviceModel = "";
        /// <summary>
        /// Модель регистратора: FF-1 или FF-2
        /// </summary>
        public string ValueDeviceModel
        {
            get { return valueDeviceModel; }
            set
            {
                valueDeviceModel = value;
                UpdateParameter(ParameterDeviceModel, value);
            }
        }

        private FileAttribute valueFileAttribute;
        /// <summary>
        /// Модель регистратора: FF-1 или FF-2
        /// </summary>
        public FileAttribute ValueFileAttribute
        {
            get { return valueFileAttribute; }
            set
            {
                valueFileAttribute = value;
                //UpdateParameter(ParameterFileAttribute, value.ToString());
            }
        }

        private long valuePatientID;
        /// <summary>
        /// Модель регистратора: FF-1 или FF-2
        /// </summary>
        public long ValuePatientID
        {
            get { return valuePatientID; }
            set
            {
                valuePatientID = value;
                UpdateParameter(ParameterPatientID, value.ToString());
            }
        }


        //Имена парамметров запроса
        private readonly string ParameterSessionID = "session_id";
        private readonly string ParameterFileType = "file_type";
        private readonly string ParameterStartDatetime = "start_datetime";
        private readonly string ParameterFileName = "file_name";
        private readonly string ParameterDiveceID = "device_id";
        private readonly string ParameterDeviceModel = "device_model";
        private readonly string ParameterPatientID = "patient_id";


        /// <summary>
        /// Словарь значений: имя SOAP параметра - значение параметра
        /// </summary>
        private Dictionary<string, string> Parameters = null;

        public CreateFile(string sessionID, CodeECG codeECG, DateTime startRecord, 
            string uniqName, string devicesMac, string devicesModel, FileAttribute fileAttribute, long patientID)
        {
            this.Parameters = new Dictionary<string, string>();
            this.MethodName = "createFile";


            this.ValueSessionID = sessionID;
            this.ValueFileType = codeECG;
            this.ValueStartDatetime = startRecord;
            this.ValueFileName = uniqName;
            this.ValueDiveceID = devicesMac;
            this.ValueDeviceModel = devicesModel;
            this.ValueFileAttribute = fileAttribute;
            this.ValuePatientID = patientID;
        }

        private void UpdateParameter(string parameterName, string parameterValue)
        {
            if (this.Parameters == null) this.Parameters = new Dictionary<string, string>();
            if (this.Parameters.ContainsKey(parameterName)) this.Parameters[parameterName] = parameterValue;
            else this.Parameters.Add(parameterName, parameterValue);
        }

        public XElement GetNodes(XNamespace methodNamespace)
        {
            if (this.Parameters == null) this.Parameters = new Dictionary<string, string>();
            XElement methodNode = new XElement(methodNamespace + this.MethodName);

            foreach (KeyValuePair<string, string> keyValue in this.Parameters)
            {
                methodNode.Add(new XElement(keyValue.Key, keyValue.Value));
            }

            //Добавление аттрибутов
            methodNode.Add(this.ValueFileAttribute.AttributeXML);

            return methodNode;
        }

        private string FormattedData(DateTime curentDateTime)
        {
            //Example^ 2017-10-03T00:00:00.000+03:00
            return String.Format("{0:yyyy-MM-ddTHH:mm:ss.fff+03:00}", curentDateTime);
        }
    }
}
