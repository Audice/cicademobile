﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SoapConstruct.SoapFunction
{
    class CreateSession : IRequest
    {
        public string MethodName
        {
            get; private set;
        }

        private string valueLogin = "";
        public string ValueLogin
        {
            get { return valueLogin; }
            set
            {
                valueLogin = value;
                UpdateParameter(ParameterLogin, value);
            }
        }

        private string valuePassword = "";
        public string ValuePassword
        {
            get { return valuePassword; }
            set
            {
                valuePassword = value;
                UpdateParameter(ParameterPassword, value);
            }
        }

        //Имена парамметров запроса
        private readonly string ParameterLogin = "userId";
        private readonly string ParameterPassword = "password";
        /// <summary>
        /// Словарь значений: имя SOAP параметра - значение параметра
        /// </summary>
        private Dictionary<string, string> Parameters = null;

        public CreateSession(string login, string password)
        {
            this.Parameters = new Dictionary<string, string>();
            this.MethodName = "createSession";
            this.ValueLogin = login;
            this.ValuePassword = password;
        }

        public XElement GetNodes(XNamespace methodNamespace)
        {
            if (this.Parameters == null) this.Parameters = new Dictionary<string, string>();
            XElement methodNode = new XElement(methodNamespace + this.MethodName);

            foreach (KeyValuePair<string, string> keyValue in this.Parameters)
            {
                methodNode.Add(new XElement(keyValue.Key, keyValue.Value));
            }

            return methodNode;
        }

        private void UpdateParameter(string parameterName, string parameterValue)
        {
            if (this.Parameters == null) this.Parameters = new Dictionary<string, string>();
            if (this.Parameters.ContainsKey(parameterName)) this.Parameters[parameterName] = parameterValue;
            else this.Parameters.Add(parameterName, parameterValue);
        }
    }
}
