﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.Model.SettingsPart
{
    public class SettingsModel
    {
        private static SettingsModel SettingsInstance;

        public string ServerURL = "https://healing-pebbles.ru:8444/ws?wsdl";

        protected SettingsModel()
        {
            //Выгрузка данных из пропертей
        }

        public bool IsOldRecordProcessingRun { get; set; } = false;
        public bool IsECGProcessingRun { get; set; } = false;

        public static SettingsModel getSettingsInstance()
        {
            if (SettingsInstance == null)
                SettingsInstance = new SettingsModel();
            return SettingsInstance;
        }
        /// <summary>
        /// Получить URL сервера из сохронений
        /// </summary>
        public void GetURL()
        {
            object url = "";
            if (App.Current.Properties.TryGetValue("ServerURL", out url))
            {
                this.ServerURL = (string)url;
            }
            else
            {
                ServerURL = null;
            }
        }
        /// <summary>
        /// Сохранить в свойствах приложеня новый 
        /// </summary>
        /// <param name="newURL">Новый URL сервера</param>
        public void SetURL(string newURL)
        {
            App.Current.Properties["ServerURL"] = newURL;
            this.ServerURL = newURL;
        }


        //Запомнить последнего вошедшего
        /// <summary>
        /// Задать последного пользователя, зашедшего в систему
        /// </summary>
        /// <param name="userLogin">Логин пользователя</param>
        /// <param name="userPassword">Пароль пользователя</param>
        public static void SetLastUser(string userLogin, string userPassword)
        {
            App.Current.Properties["LastUserLogin"] = userLogin;
            App.Current.Properties["LastUserPassword"] = userPassword;
        }
        /// <summary>
        /// Возвращает последного юзера, работающего в системе
        /// </summary>
        /// <returns>Кортеж |логин, пароль| и null если пользователя не было</returns>
        public static Tuple<string, string> GetLastUserInfo()
        {
            object login = "", password = "";
            if (App.Current.Properties.TryGetValue("LastUserLogin", out login) && App.Current.Properties.TryGetValue("LastUserPassword", out password))
            {
                return new Tuple<string, string>((string)login, (string)password);
            }
            else
            {
                return null;
            }
        }

        public static string GetLastUserLogin()
        {
            object login = "";
            if (App.Current.Properties.TryGetValue("LastUserLogin", out login))
            {
                return (string)login;
            }
            else
            {
                return null;
            }
        }

        public void SetSaveFilesFlag(bool flagValue)
        {
            App.Current.Properties["SaveFilesFlag"] = flagValue;
        }

        public bool GetSaveFilesFlag()
        {
            object flagState;
            if (App.Current.Properties.TryGetValue("SaveFilesFlag", out flagState))
            {
                return (bool)flagState;
            }
            else
            {
                SetSaveFilesFlag(false);
                return false;
            }
        }

    }
}
