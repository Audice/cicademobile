﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FenixMobile.ProtectedMechanism
{
    public struct DoubleTapSaver
    {
        // control whether the button events are executed
        public static bool AllowTap = true;

        // wait for 200ms after allowing another button event to be executed
        public static async void ResumeTap()
        {
            await Task.Delay(200);
            AllowTap = true;
        }
    }
}
