﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace FenixMobile.View.CustomControl
{
    public class CircleButton : Xamarin.Forms.View
    {
        public CircleButton() { }

        public CircleButton(CircleButton currentTextInCircle)
        {
            this.HorizontalOptions = currentTextInCircle.HorizontalOptions;
            //this.VerticalOptions = currentTextInCircle.VerticalOptions;
            this.Text = currentTextInCircle.Text;
            this.BackgroundColor = currentTextInCircle.BackgroundColor;
            this.HeightRequest = currentTextInCircle.Height;
            this.WidthRequest = currentTextInCircle.Width;
            this.GradientCenter = currentTextInCircle.GradientCenter;
            this.CustomBackgroundColor = currentTextInCircle.CustomBackgroundColor;
        }

        public event EventHandler ClickEvent;
        public void FireClick(EventArgs e)
        {
            if (this.ClickEvent != null)
                this.ClickEvent(this, e);
        }

        public static readonly BindableProperty TextProperty =
    BindableProperty.Create("Text", typeof(string), typeof(CircleButton), string.Empty);
        public string Text
        {
            set
            {
                SetValue(TextProperty, value);
            }
            get
            {
                return (string)GetValue(TextProperty);
            }
        }

        public static readonly BindableProperty GradientCenterProperty =
            BindableProperty.Create("GradientRadius", typeof(Tuple<float, float>), typeof(CircleButton), new Tuple<float, float>(0.5f, 0.5f));
        public Tuple<float, float> GradientCenter
        {
            set
            {
                SetValue(GradientCenterProperty, value);
            }
            get
            {
                return (Tuple<float, float>)GetValue(GradientCenterProperty);
            }
        }

        public static readonly BindableProperty BackgroundColProperty =
    BindableProperty.Create("BackgroundColor", typeof(Color), typeof(CircleButton), Color.Default);
        public Color CustomBackgroundColor
        {
            set
            {
                SetValue(BackgroundColProperty, value);
            }
            get
            {
                return (Color)GetValue(BackgroundColProperty);
            }
        }


        public static readonly BindableProperty CustomHeightProperty =
            BindableProperty.Create(nameof(Height), typeof(double), typeof(CircleButton), -1.0);
        public double CustomHeight
        {
            set
            {
                SetValue(CustomHeightProperty, value);
            }
            get
            {
                return (double)GetValue(HeightProperty);
            }
        }

        public static readonly BindableProperty CustomWidthProperty =
            BindableProperty.Create(nameof(Width), typeof(double), typeof(CircleButton), -1.0);
        public double CustomWidht
        {
            set
            {
                SetValue(CustomWidthProperty, value);
            }
            get
            {
                return (double)GetValue(CustomWidthProperty);
            }
        }

    }
}
