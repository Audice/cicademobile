﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace FenixMobile.View.CustomControl
{
    public class XamarinVerticalSlider : Xamarin.Forms.View
    {
        /// <summary>
        /// The ValueChanged event is fired when the Value property changes.
        /// </summary>
        public event EventHandler<ValueChangedEventArgs> ValueChanged;
        /// <summary>
        /// The MaximumChanged event is fired when the Maximum of the slider changes!
        /// </summary>
        public event EventHandler<ValueChangedEventArgs> MaximumChanged;
        /// <summary>
        /// The MinimumChanged event is fired when the Minimum of the slider changes!
        /// </summary>
        public event EventHandler<ValueChangedEventArgs> MinimumChanged;
        /// <summary>
        /// Value of the Slider ranging between Maximum and Minimum. This is a bindable property.
        /// </summary>
        public static readonly BindableProperty ValueProperty = BindableProperty.Create<XamarinVerticalSlider, int>(p => p.Value, 0);
        /// <summary>
        /// Identifies the Maximum bindable property.
        /// </summary>
        public static readonly BindableProperty MaximumProperty = BindableProperty.Create<XamarinVerticalSlider, int>(p => p.Maximum, 100);
        /// <summary>
        /// Identifies the Minimum bindable property.
        /// </summary>
        public static readonly BindableProperty MinimumProperty = BindableProperty.Create<XamarinVerticalSlider, int>(p => p.Minimum, 0);

        public int Value
        {
            get
            {
                return (int)GetValue(ValueProperty);
            }

            set
            {
                if (ValueChanged != null)
                    ValueChanged(this, new ValueChangedEventArgs((int)GetValue(ValueProperty), value));
                this.SetValue(ValueProperty, value);
            }
        }



        /// <summary>
        /// Gets or sets the maximum selectable value for the Slider. This is a bindable property.
        /// </summary>
        public int Maximum
        {
            get
            {
                return (int)GetValue(MaximumProperty);
            }

            set
            {
                if (MaximumChanged != null)
                    MaximumChanged(this, new ValueChangedEventArgs((int)GetValue(MaximumProperty), value));
                this.SetValue(MaximumProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the minimum selectable value for the Slider. This is a bindable property.
        /// </summary>
        public int Minimum
        {
            get
            {
                return (int)GetValue(MinimumProperty);
            }

            set
            {
                if (MinimumChanged != null)
                    MinimumChanged(this, new ValueChangedEventArgs((int)GetValue(MinimumProperty), value));
                this.SetValue(MinimumProperty, value);
            }
        }

        public static readonly BindableProperty SliderHeightProperty =
    BindableProperty.Create(nameof(SliderHeightProperty), typeof(int), typeof(XamarinVerticalSlider), 100);
        public int SliderHeight
        {
            set
            {
                SetValue(SliderHeightProperty, value);
            }
            get
            {
                return (int)GetValue(SliderHeightProperty);
            }
        }

        public static readonly BindableProperty SliderWidthProperty =
            BindableProperty.Create(nameof(SliderWidthProperty), typeof(int), typeof(XamarinVerticalSlider), 100);
        public int SliderWidht
        {
            set
            {
                SetValue(SliderWidthProperty, value);
            }
            get
            {
                return (int)GetValue(SliderWidthProperty);
            }
        }



        public static readonly BindableProperty ThumbHeightProperty =
    BindableProperty.Create(nameof(ThumbHeightProperty), typeof(int), typeof(XamarinVerticalSlider), 10);
        public int ThumbHeight
        {
            set
            {
                SetValue(SliderHeightProperty, value);
            }
            get
            {
                return (int)GetValue(ThumbHeightProperty);
            }
        }

        public static readonly BindableProperty ThumbWidthProperty =
            BindableProperty.Create(nameof(ThumbWidthProperty), typeof(int), typeof(XamarinVerticalSlider), 10);
        public int ThumbWidht
        {
            set
            {
                SetValue(ThumbWidthProperty, value);
            }
            get
            {
                return (int)GetValue(ThumbWidthProperty);
            }
        }


        public static readonly BindableProperty ThumbRadiusProperty =
    BindableProperty.Create(nameof(ThumbRadiusProperty), typeof(int), typeof(XamarinVerticalSlider), 10);
        public int ThumbRadius
        {
            set
            {
                SetValue(ThumbRadiusProperty, value);
            }
            get
            {
                return (int)GetValue(ThumbRadiusProperty);
            }
        }


        public static readonly BindableProperty SliderRadiusProperty =
BindableProperty.Create(nameof(SliderRadiusProperty), typeof(int), typeof(XamarinVerticalSlider), 10);
        public int SliderRadius
        {
            set
            {
                SetValue(SliderRadiusProperty, value);
            }
            get
            {
                return (int)GetValue(SliderRadiusProperty);
            }
        }


        public static readonly BindableProperty SliderColorProperty =
            BindableProperty.Create(nameof(SliderColorProperty), typeof(Color), typeof(XamarinVerticalSlider), Color.Blue);
        public Color SliderColor
        {
            set
            {
                SetValue(SliderColorProperty, value);
            }
            get
            {
                return (Color)GetValue(SliderColorProperty);
            }
        }


        public static readonly BindableProperty ThumbColorProperty =
    BindableProperty.Create(nameof(ThumbColorProperty), typeof(Color), typeof(XamarinVerticalSlider), Color.White);
        public Color ThumbColor
        {
            set
            {
                SetValue(ThumbColorProperty, value);
            }
            get
            {
                return (Color)GetValue(ThumbColorProperty);
            }
        }
    }
}
