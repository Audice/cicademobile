﻿using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.CustomDisplayAlert
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CustomLoadPage : PopupPage
    {
        public CustomLoadPage(string loadTitle, string loadInfo)
        {
            InitializeComponent();
            this.LoadTitle.Text = loadTitle;
            this.LoadInfo.Text = loadInfo;
        }
    }
}