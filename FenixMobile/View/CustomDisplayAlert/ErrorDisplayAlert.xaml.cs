﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.CustomDisplayAlert
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ErrorDisplayAlert : PopupPage
    {
        public ErrorDisplayAlert(string text)
        {
            InitializeComponent();
            this.textError.Text = text;
        }

        private async void Error_Exit_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.PopAsync();
        }
    }
}