﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.CustomDisplayAlert
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InformationDisplayAlert : PopupPage
    {
        public InformationDisplayAlert(string text, string title = "", byte starCount = 0)
        {
            InitializeComponent();
            textInfo.Text = text;
            titleInfo.Text = title;
            if (starCount > 0)
            {
                switch (starCount)
                {
                    case 1:
                        this.StarImage.Source = "OneStars.png";
                        break;
                    case 2:
                        this.StarImage.Source = "TwoStars.png";
                        break;
                    case 3:
                        this.StarImage.Source = "ThreeStars.png";
                        break;
                    case 4:
                        this.StarImage.Source = "FourStars.png";
                        break;
                    default:
                        this.StarImage.Source = "FiveStars.png";
                        break;
                }
            }
        }

        private async void Information_Exit_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.PopAsync();
        }
    }
}