﻿using FenixMobile.View.ViewEventArgs;
using FenixMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.HeadMasterMenu.ECGViewerPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChaosgramPage : ContentPage
    {
        /// <summary>
        /// Список углов поворота указателя. Количество углов равно 5.
        /// </summary>
        List<double> AnglesList;
        double width = 0;
        double height = 0;

        HolterECGViewModel ViewModel = null;
        public ChaosgramPage(HolterECGViewModel viewModel)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, true);
            InitializeComponent();
            ChangeGradientShift(new Tuple<float, float>(2f, 2f));
            InitAngelsList();
            this.IconImageSource = "hr.png";
            this.ViewModel = viewModel;
            this.BindingContext = this.ViewModel;
            this.ViewModel.PointerAngleChange += ChangePointersAngle;
        }

        void ChangePointersAngle(object sender, PointerAnglesEventArgs e)
        {
            if (AnglesList != null && AnglesList.Count == 5)
            {
                ChangePointersAngle(e.RotateIndex);
            }
        }

        async void ChangePointersAngle(byte anglesIndex)
        {
            if (anglesIndex >= 0 && anglesIndex < AnglesList.Count)
            {
                await this.Pointer.RotateTo(AnglesList[anglesIndex], 1000, Easing.CubicInOut);
            }
            else
            {
                await this.Pointer.RotateTo(0, 1000, Easing.CubicInOut);
            }
        }

        async void ChangeDiscriptionsFade()
        {
            if (this.DiscriptionTitle != null)
            {
                await this.DiscriptionScroll.FadeTo(1, 10000, Easing.CubicInOut);
            }
        }



        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (width != this.width || height != this.height)
            {
                this.width = width;
                this.height = height;

                if (width > height)
                {
                    HeadStack.Orientation = StackOrientation.Horizontal;
                    GridStack.WidthRequest = width / 2.1;
                    GridStack.HeightRequest = height / 2;
                    RRStateGrid.WidthRequest = height - 50;
                    RRStateGrid.HeightRequest = height - 50;
                    DiscriptionStack.WidthRequest = width / 2;
                    DiscriptionStack.HeightRequest = width / 2;
                    DiscriptionScroll.Margin = new Thickness(10, 10, 10, 10);

                    int newHeightCircle = (int)(width / 12);
                    this.VeryGoodState.WidthRequest = newHeightCircle;
                    this.VeryGoodState.HeightRequest = newHeightCircle;
                    this.GoodState.WidthRequest = newHeightCircle;
                    this.GoodState.HeightRequest = newHeightCircle;
                    this.WarningState.WidthRequest = newHeightCircle;
                    this.WarningState.HeightRequest = newHeightCircle;
                    this.WarningBadState.WidthRequest = newHeightCircle;
                    this.WarningBadState.HeightRequest = newHeightCircle;
                    this.BadState.WidthRequest = newHeightCircle;
                    this.BadState.HeightRequest = newHeightCircle;

                    ChangePointersAngle(0);

                }
                else
                {
                    HeadStack.Orientation = StackOrientation.Vertical;
                    GridStack.WidthRequest = height / 2.1;
                    GridStack.HeightRequest = height / 2;
                    RRStateGrid.WidthRequest = width - 30;
                    RRStateGrid.HeightRequest = width - 30;
                    //scrolView.WidthRequest = width - 5;
                    HeadStack.WidthRequest = width - 15;

                    int newHeightCircle = (int)(width / 6);
                    this.VeryGoodState.WidthRequest = newHeightCircle;
                    this.VeryGoodState.HeightRequest = newHeightCircle;
                    this.GoodState.WidthRequest = newHeightCircle;
                    this.GoodState.HeightRequest = newHeightCircle;
                    this.WarningState.WidthRequest = newHeightCircle;
                    this.WarningState.HeightRequest = newHeightCircle;
                    this.WarningBadState.WidthRequest = newHeightCircle;
                    this.WarningBadState.HeightRequest = newHeightCircle;
                    this.BadState.WidthRequest = newHeightCircle;
                    this.BadState.HeightRequest = newHeightCircle;
                }




                ChangeGradientShift(new Tuple<float, float>(2f, 2f));
                InitAngelsList();
            }
        }

        void ChangeGradientShift(Tuple<float, float> gradientShift)
        {
            this.VeryGoodState.GradientCenter = gradientShift;
            this.GoodState.GradientCenter = gradientShift;
            this.WarningState.GradientCenter = gradientShift;
            this.WarningBadState.GradientCenter = gradientShift;
            this.BadState.GradientCenter = gradientShift;
        }

        void InitAngelsList()
        {
            if (AnglesList == null) AnglesList = new List<double>();
            AnglesList.Clear();
            double centerPointerX = this.Pointer.X + (Pointer.Width / 2);
            double centerPointerY = Pointer.Y + (Pointer.Height / 2);
            double x1 = this.VeryGoodState.X + (VeryGoodState.Width / 2);
            double y1 = VeryGoodState.Y + (VeryGoodState.Height / 2);
            double c1 = Math.Sqrt(Math.Pow((centerPointerY - y1), 2) + Math.Pow((centerPointerX - x1), 2));
            if (c1 == 0)
            {
                AnglesList.Add(-90);
            }
            else
            {
                double firstAngle = Math.Asin((centerPointerY - y1) / c1) * 180 / Math.PI;
                AnglesList.Add(-90 + firstAngle);
            }

            double x2 = this.GoodState.X + (this.GoodState.Width / 2);
            double y2 = this.GoodState.Y + (this.GoodState.Height / 2);
            double c2 = Math.Sqrt(Math.Pow((centerPointerY - y2), 2) + Math.Pow((centerPointerX - x2), 2));
            double secondAngle = Math.Asin((centerPointerY - y2) / c2) * 180 / Math.PI;
            AnglesList.Add(-90 + secondAngle);
            double x3 = this.WarningState.X + (this.WarningState.Width / 2);
            double y3 = this.WarningState.Y + (this.WarningState.Height / 2);
            double c3 = Math.Sqrt(Math.Pow((centerPointerY - y3), 2) + Math.Pow((centerPointerX - x3), 2));
            double thirdAngle = Math.Asin((centerPointerY - y3) / c3) * 180 / Math.PI;
            AnglesList.Add(-90 + thirdAngle);
            double x4 = this.WarningBadState.X + (this.WarningBadState.Width / 2);
            double y4 = this.WarningBadState.Y + (this.WarningBadState.Height / 2);
            double c4 = Math.Sqrt(Math.Pow((centerPointerY - y4), 2) + Math.Pow((x4 - centerPointerX), 2));
            double fourthAngle = Math.Asin((centerPointerY - y4) / c4) * 180 / Math.PI;
            AnglesList.Add(90 - fourthAngle);
            double x5 = this.BadState.X + (this.BadState.Width / 2);
            double y5 = this.BadState.Y + (this.BadState.Height / 2);
            double c5 = Math.Sqrt(Math.Pow((centerPointerY - y5), 2) + Math.Pow((x5 - centerPointerX), 2));
            double fifthAngle = Math.Asin((centerPointerY - y5) / c5) * 180 / Math.PI;
            AnglesList.Add(90 - fifthAngle);
        }

    }
}