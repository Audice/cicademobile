﻿using FenixMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.HeadMasterMenu.ECGViewerPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HeartRateVariabilityView : ContentPage
    {
        HolterECGViewModel ViewModel = null;
        public HeartRateVariabilityView(HolterECGViewModel viewModel)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, true);
            InitializeComponent();
            this.IconImageSource = "VSRIcon.png";
            this.ViewModel = viewModel;
            this.BindingContext = this.ViewModel;
        }
    }
}