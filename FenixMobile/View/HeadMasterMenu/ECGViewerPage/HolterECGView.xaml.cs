﻿using FenixMobile.Model.BluetoothPart;
using FenixMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.HeadMasterMenu.ECGViewerPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HolterECGView : ContentPage
    {
        HolterECGViewModel ViewModel = null;
        public HolterECGView(HolterECGViewModel viewModel)
        {
            InitializeComponent();
            this.IconImageSource = "DiagIcon.png";
            NavigationPage.SetHasNavigationBar(this, false);
            //this.ViewModel = new HolterECGViewModel() { Navigation = this.Navigation};
            this.ViewModel = viewModel;
            this.BindingContext = this.ViewModel;
        }



        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);
            if (this.ViewModel != null)
            {
                uint actualHeight = (uint)(height - (this.TitleStack.Height + this.LineBox.Height));
                this.ViewModel.UdateFramesSize(width, actualHeight);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }
    }
}