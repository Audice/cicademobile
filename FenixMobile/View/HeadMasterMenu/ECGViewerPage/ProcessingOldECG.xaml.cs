﻿using FenixMobile.Model.DatabasePart;
using FenixMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.HeadMasterMenu.ECGViewerPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProcessingOldECG : ContentPage
    {
        ProcessingOldECGViewModel ViewModel = null;
        public ProcessingOldECG(RecordTable oldRecord)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.ViewModel = new ProcessingOldECGViewModel(oldRecord) { Navigation = this.Navigation};
            this.BindingContext = this.ViewModel;
        }

        protected override bool OnBackButtonPressed()
        {
            this.BindingContext = null;
            this.ViewModel.Dispose();
            this.ViewModel = null;
            return base.OnBackButtonPressed();
        }
    }
}