﻿using FenixMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.HeadMasterMenu.ECGViewerPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProgramStatePage : ContentPage
    {
        HolterECGViewModel ViewModel = null;
        public ProgramStatePage(HolterECGViewModel viewModel)
        {
            InitializeComponent();
            this.IconImageSource = "hr.png";
            NavigationPage.SetHasNavigationBar(this, false);
            //this.ViewModel = new HolterECGViewModel() { Navigation = this.Navigation};
            this.ViewModel = viewModel;
            this.BindingContext = this.ViewModel;
        }



        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);
            if (this.ViewModel != null)
            {
                //this.ViewModel.UdateFramesSize(width, height);
            }
        }
    }
}