﻿using FenixMobile.Model.DatabasePart;
using FenixMobile.View.Interface;
using FenixMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.HeadMasterMenu.ECGViewerPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SavedECGView : ContentPage
    {
        double width = 0;
        double height = 0;

        FileViewerViewModel LocalFileViewerViewModel = null;
        public SavedECGView(RecordTable recordTable)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.LocalFileViewerViewModel = new FileViewerViewModel(recordTable, this.GeneralCanvasView, 
                this.ConcretCanvasView, this.GeneralTouchEffect, this.ConcretTouchEffect)
            {
                Navigation = this.Navigation
            };
            this.BindingContext = this.LocalFileViewerViewModel;
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (width != this.width || height != this.height)
            {
                this.width = width;
                this.height = height;

                this.LocalFileViewerViewModel.SetDisplayOrientation(this.width, this.height);

                if (width < height)
                {
                    this.width = width;
                    this.height = height;
                    //Вычисление актуальной высоты
                    uint actualHeight = (uint)(this.height - (this.TitleStack.Height + this.LineBox.Height + this.ToolsStack.Height));
                    double heightGeneralCanvas = (double)(2.0 * actualHeight) / 3.0;
                    double heightConcretCanvas = (double)(actualHeight) / 3.0;

                    this.PreviewGraphGrid.HeightRequest = heightGeneralCanvas;
                    this.ConcretGraphGrid.HeightRequest = heightConcretCanvas;
                    this.GeneralCanvasView.HeightRequest = heightGeneralCanvas;
                    this.ConcretCanvasView.HeightRequest = heightConcretCanvas;
                }
                else
                {
                    this.width = width;
                    this.height = height;
                    //Вычисление актуальной высоты
                    uint actualHeight = (uint)(this.height - (this.TitleStack.Height + this.LineBox.Height + this.ToolsStack.Height));
                    double heightGeneralCanvas = (double)(actualHeight) / 2.0;
                    double heightConcretCanvas = (double)(actualHeight) / 2.0;

                    this.PreviewGraphGrid.HeightRequest = heightGeneralCanvas;
                    this.ConcretGraphGrid.HeightRequest = heightConcretCanvas;
                    this.GeneralCanvasView.HeightRequest = heightGeneralCanvas;
                    this.ConcretCanvasView.HeightRequest = heightConcretCanvas;
                }
            }
        }

        protected override bool OnBackButtonPressed()
        {
            this.BindingContext = null;
            this.LocalFileViewerViewModel.Dispose();
            this.LocalFileViewerViewModel = null;
            return base.OnBackButtonPressed();
        }
    }
}