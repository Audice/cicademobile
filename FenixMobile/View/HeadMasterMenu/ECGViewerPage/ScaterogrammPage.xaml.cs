﻿using FenixMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.HeadMasterMenu.ECGViewerPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ScaterogrammPage : ContentPage
    {
        double width = 0;
        double height = 0;
        HolterECGViewModel ViewModel = null;
        public ScaterogrammPage(HolterECGViewModel viewModel)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, true);
            InitializeComponent();

            this.IconImageSource = "ScatIcon.png";
            this.ViewModel = viewModel;
            this.ViewModel.SetScaterogrammCanvas(this.ScaterogrammCanvasView);
            this.BindingContext = this.ViewModel;
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (width != this.width || height != this.height)
            {
                this.width = width;
                this.height = height;

                this.ViewModel.SetScaterogrammDisplayOrientation(this.width, this.height);

                this.width = width;
                this.height = height;
                //Вычисление актуальной высоты
                uint actualHeight = 0;
                if (this.height >= this.width)
                {
                    actualHeight = (uint)(this.height - (this.TitleStack.Height + this.LineBox.Height) - this.height / 11);
                }
                else
                {
                    actualHeight = (uint)(this.height - (this.TitleStack.Height + this.LineBox.Height) - this.height / 6);
                }

                
                uint actualWidth = (uint)(this.width);

                this.ScaterogrammGrid.HeightRequest = actualHeight;
                this.ScaterogrammGrid.WidthRequest = actualWidth;

                this.ScaterogrammCanvasView.HeightRequest = actualHeight;
                this.ScaterogrammCanvasView.HeightRequest = actualWidth;
            }
        }

        protected override void OnAppearing()
        {
            if (this.ViewModel != null)
                this.ViewModel.SetScaterogramFocus(true);
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            if (this.ViewModel != null)
                this.ViewModel.SetScaterogramFocus(false);
            base.OnDisappearing();
        }
    }
}