﻿using FenixMobile.Interfaces;
using FenixMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.HeadMasterMenu.ECGViewerPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SimpleECGView : ContentPage
    {
        double width = 0;
        double height = 0;
        SimpleECGViewModel ViewModel = null;
        public SimpleECGView(ECGreceiveMode recieveMode)
        {
            
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.ViewModel = new SimpleECGViewModel(recieveMode, this.GeneralCanvasView, this.ConcretCanvasView) { 
                Navigation = this.Navigation, GeneralECGTouchEffect = this.GeneralTouchEffect, ConcretECGTouchEffect = this.ConcretTouchEffect
            };
            this.BindingContext = this.ViewModel;
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (width != this.width || height != this.height)
            {
                this.width = width;
                this.height = height;

                this.ViewModel.SetDisplayOrientation(this.width, this.height);

                if (width < height)
                {
                    this.width = width;
                    this.height = height;
                    //Вычисление актуальной высоты
                    uint actualHeight = (uint)(this.height - (this.TitleStack.Height + this.LineBox.Height + this.ToolsStack.Height));
                    double heightGeneralCanvas = (double)(2.0 * actualHeight) / 3.0;
                    double heightConcretCanvas = (double)(actualHeight) / 3.0;

                    this.PreviewGraphGrid.HeightRequest = heightGeneralCanvas;
                    this.ConcretGraphGrid.HeightRequest = heightConcretCanvas;
                    this.GeneralCanvasView.HeightRequest = heightGeneralCanvas;
                    this.ConcretCanvasView.HeightRequest = heightConcretCanvas;
                }
                else
                {
                    this.width = width;
                    this.height = height;
                    //Вычисление актуальной высоты
                    uint actualHeight = (uint)(this.height - (this.TitleStack.Height + this.LineBox.Height + this.ToolsStack.Height));
                    double heightGeneralCanvas = (double)(actualHeight) / 2.0;
                    double heightConcretCanvas = (double)(actualHeight) / 2.0;

                    this.PreviewGraphGrid.HeightRequest = heightGeneralCanvas;
                    this.ConcretGraphGrid.HeightRequest = heightConcretCanvas;
                    this.GeneralCanvasView.HeightRequest = heightGeneralCanvas;
                    this.ConcretCanvasView.HeightRequest = heightConcretCanvas;
                }
            }
        }

        protected override bool OnBackButtonPressed()
        {
            this.BindingContext = null;
            this.ViewModel.IsLoadingAborted = true;
            this.ViewModel.Dispose();
            this.ViewModel = null;
            return base.OnBackButtonPressed();
        }
    }
}