﻿using FenixMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.HeadMasterMenu.ECGViewerPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TrendsViewPage : ContentPage
    {
        double width = 0;
        double height = 0;
        HolterECGViewModel ViewModel = null;
        public TrendsViewPage(HolterECGViewModel viewModel)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, true);
            InitializeComponent();
            this.IconImageSource = "hr.png";
            this.ViewModel = viewModel;
            this.ViewModel.SetTrendsCanvas(this.TrendsCanvasView, this.TrendsTouchEffect, this.ActivityCanvasView, this.ActivityTouchEffect);
            this.BindingContext = this.ViewModel;
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (width != this.width || height != this.height)
            {
                this.width = width;
                this.height = height;

                this.ViewModel.SetTrendsDisplayOrientation(this.width, this.height);

                this.width = width;
                this.height = height;
                //Вычисление актуальной высоты
                uint actualHeight = 0;
                if (this.height >= this.width)
                {
                    actualHeight = (uint)(this.height - (this.TitleStack.Height + this.LineBox.Height) - this.height / 12);
                }
                else
                {
                    actualHeight = (uint)(this.height - (this.TitleStack.Height + this.LineBox.Height) - this.height / 6);
                }
                double heightTrendsCanvas = (double)(actualHeight) / 2.0;
                double heightActivityCanvas = (double)(actualHeight) / 2.0;

                this.TrendsGrid.HeightRequest = heightTrendsCanvas;
                this.ActivityGrid.HeightRequest = heightActivityCanvas;
                this.TrendsCanvasView.HeightRequest = heightTrendsCanvas;
                this.ActivityCanvasView.HeightRequest = heightActivityCanvas;
            }
        }
    }
}