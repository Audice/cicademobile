﻿using FenixMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.HeadMasterMenu.MenuItemPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArchiveWorkPage : ContentPage
    {
        ArchiveWorkViewModel ArchiveWorkViewModel { get; set; } = null;
        public ArchiveWorkPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            ArchiveWorkViewModel = new ArchiveWorkViewModel() { Navigation = this.Navigation };
            this.BindingContext = ArchiveWorkViewModel;
        }
    }
}