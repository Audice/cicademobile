﻿using FenixMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.HeadMasterMenu.MenuItemPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ModeSelectPage : ContentPage
    {
        public ModeSelectPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.IconImageSource = "ecgIcon.png";
            this.TenSecondsMode.WidthRequest = App.ScreenWidth / 2;
            this.FiveMinutesMode.WidthRequest = App.ScreenWidth / 2;
            this.MonitorMode.WidthRequest = App.ScreenWidth / 2;
            this.BindingContext = new ModeSelectViewModel() { Navigation = this.Navigation };
        }
    }
}