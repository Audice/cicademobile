﻿using FenixMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.HeadMasterMenu.MenuItemPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SavedFilePage : ContentPage
    {
        SavedFileViewModel LocalSavedFileViewModel;
        public SavedFilePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.IconImageSource = "dataIcon.png";
            LocalSavedFileViewModel = new SavedFileViewModel() { Navigation = this.Navigation };
            this.BindingContext = LocalSavedFileViewModel;
        }

        protected override bool OnBackButtonPressed()
        {
            return base.OnBackButtonPressed();
        }
    }
}