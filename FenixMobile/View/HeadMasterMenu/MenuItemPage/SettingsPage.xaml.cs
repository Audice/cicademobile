﻿using FenixMobile.View.CustomControl;
using FenixMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.HeadMasterMenu.MenuItemPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingsPage : ContentPage
    {
        readonly string[] ChannelsItem = new string[] { "1", "2" };
        readonly string[] SPSItem = new string[] { "500", "1000", "2000", "4000", "8000" };
        SettingsViewModel SettingsViewModel = null;
        public SettingsPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.IconImageSource = "settingsIcon.png"; 
            CountChanels.ItemsSource = ChannelsItem;
            SamplingFreq.ItemsSource = SPSItem;
            this.SettingsViewModel = new SettingsViewModel() { Navigation = this.Navigation };
            this.BindingContext = this.SettingsViewModel;
            CountChanels.WidthRequest = App.ScreenWidth / 4;
            SamplingFreq.WidthRequest = App.ScreenWidth / 4;
        }

        private void OnFocuse(object sender, EventArgs e)
        {
            ElypsedEntry currentEntry = (ElypsedEntry)sender;
            currentEntry.BorderColor = Color.FromHex("#79e0c7");
            currentEntry.BorderWidth += 2;
        }
        private void UnFocuse(object sender, EventArgs e)
        {
            ElypsedEntry currentEntry = (ElypsedEntry)sender;
            currentEntry.BorderColor = Color.FromHex("#359cff");
            currentEntry.BorderWidth -= 2;
        }
        private void OnFocuseDevicePicker(object sender, EventArgs e)
        {
            ElypsedPicker currentEntry = (ElypsedPicker)sender;
            currentEntry.BorderColor = Color.FromHex("#79e0c7");
            currentEntry.BorderWidth += 2;
        }


        private void OnFocusePicker(object sender, EventArgs e)
        {
            ElypsedPicker currentEntry = (ElypsedPicker)sender;
            currentEntry.BorderColor = Color.FromHex("#79e0c7");
            currentEntry.BorderWidth += 2;
        }
        private void UnFocusePicker(object sender, EventArgs e)
        {
            ElypsedPicker currentEntry = (ElypsedPicker)sender;
            currentEntry.BorderColor = Color.FromHex("#359cff");
            currentEntry.BorderWidth -= 2;
        }
    }
}