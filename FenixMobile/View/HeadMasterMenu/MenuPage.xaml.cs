﻿using FenixMobile.Interfaces;
using FenixMobile.Model.BluetoothPart;
using FenixMobile.Model.DatabasePart;
using FenixMobile.Model.SettingsPart;
using FenixMobile.View.CustomControl;
using FenixMobile.View.HeadMasterMenu.ECGViewerPage;
using FenixMobile.View.HeadMasterMenu.MenuItemPage;
using FenixMobile.View.Interface;
using ImageCircle.Forms.Plugin.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.HeadMasterMenu
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : MasterDetailPage
    {
        DatabaseWorker Database;
        FireFly FireFly;
        SettingsModel SettingsModel;

        ListView filesList;
        List<MenuViewCell> customListViewRecords;
        public MenuPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.Database = DatabaseWorker.getInstance();
            filesList = new ListView() { RowHeight = App.ScreenHeight / 8 };
            filesList.ItemTemplate = new DataTemplate(typeof(DataCellView));
            customListViewRecords = new List<MenuViewCell>();
            customListViewRecords.Add(new MenuViewCell("Состояние системы", "Описание состояния подключений", "connectIcon.png", new ModeSelectPage()));
            customListViewRecords.Add(new MenuViewCell("Снятие ЭКГ", "Переход к режиму снятия электрокардиограммы", "ecgIcon.png", new ModeSelectPage()));
            customListViewRecords.Add(new MenuViewCell("Сохранённые ЭКГ", "Список ранее записанных ЭКГ", "dataIcon.png", new SavedFilePage()));
            customListViewRecords.Add(new MenuViewCell("Настройки", "Настройке сети, профиля, мобильного кардиографа", "settingsIcon.png", new SettingsPage()));

            filesList.ItemsSource = customListViewRecords;
            filesList.SelectedItem = null;


            Frame patientProfile = new Frame()
            {
                BorderColor = Color.White,
                BackgroundColor = Color.Transparent,
                CornerRadius = 15,
                HeightRequest = App.ScreenHeight / 9,
                Margin = new Thickness(10, 20, 10, 20),
            };
            // Оброботчик события нажатия на Frame
            var actionAfterTap = new TapGestureRecognizer();
            actionAfterTap.Tapped += async delegate {
                if (this.Database != null && this.Database.CurrentPatient != null)
                {
                    string patientName = this.Database.CurrentPatient.Surname + " " + this.Database.CurrentPatient.Name + " " + this.Database.CurrentPatient.Patronymic;
                    string patientInfo = "Дата рождения: " + this.Database.CurrentPatient.BornDate.ToString("dd-MM-yyyy") + Environment.NewLine;
                    patientInfo += "Пол: " + (this.Database.CurrentPatient.Gender == 1 ? "Мужской" : "Женский") + Environment.NewLine;
                    patientInfo += "Рост: " + this.Database.CurrentPatient.Growth + Environment.NewLine;
                    patientInfo += "Вес: " + this.Database.CurrentPatient.Weight + Environment.NewLine;
                    if (await DisplayAlert(patientName, patientInfo, "Выход из профиля", "Закрыть"))
                    {
                        //Эксперемент
                        this.Database.ClearSessionIDByUserID(this.Database.CurrentUser.ID);
                        DependencyService.Get<ISystemFunction>().CloseApplication();
                    }
                }
            };
            patientProfile.GestureRecognizers.Add(actionAfterTap);

            Grid profileGrid = new Grid
            {
                RowDefinitions =
            {
                new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
                new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
                new RowDefinition { Height = new GridLength(1, GridUnitType.Star) }
            },
                ColumnDefinitions =
            {
                new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
            },
                HorizontalOptions = LayoutOptions.Fill,
                VerticalOptions = LayoutOptions.Fill,
                BackgroundColor = Color.Transparent
            };
            CircleImage portretPhoto = new CircleImage()
            {
                BorderColor = Color.White,
                BorderThickness = 0.5f,
                Aspect = Aspect.AspectFill,
                HorizontalOptions = LayoutOptions.EndAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                Source = "Portret.jpg"
            };
            Label SNPLabel = new Label()
            {
                Text = "Карчков Д. А.",
                TextColor = Color.White,
                FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)),
                FontAttributes = FontAttributes.Bold,
                VerticalOptions = LayoutOptions.EndAndExpand,
                HorizontalOptions = LayoutOptions.StartAndExpand
            };
            Label AgeLabel = new Label()
            {
                Text = "25 лет",
                TextColor = Color.White,
                FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)),
                FontAttributes = FontAttributes.Bold,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.StartAndExpand
            };
            Label StatePatientLabel = new Label()
            {
                Text = "Состояние: ",
                TextColor = Color.White,
                FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)),
                FontAttributes = FontAttributes.Bold,
                VerticalOptions = LayoutOptions.StartAndExpand,
                HorizontalOptions = LayoutOptions.StartAndExpand
            };


            profileGrid.Children.Add(portretPhoto, 0, 0);
            Grid.SetColumnSpan(portretPhoto, 2);
            Grid.SetRowSpan(portretPhoto, 3);
            profileGrid.Children.Add(SNPLabel, 2, 0);
            Grid.SetColumnSpan(SNPLabel, 3);
            Grid.SetRowSpan(SNPLabel, 1);
            profileGrid.Children.Add(AgeLabel, 2, 1);
            Grid.SetColumnSpan(AgeLabel, 2);
            Grid.SetRowSpan(AgeLabel, 1);
            profileGrid.Children.Add(StatePatientLabel, 2, 2);
            Grid.SetColumnSpan(StatePatientLabel, 2);
            Grid.SetRowSpan(StatePatientLabel, 1);



            patientProfile.Content = profileGrid;


            Label header = new Label
            {
                Text = "MasterDetailPage",
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                HorizontalOptions = LayoutOptions.Center
            };



            // Create the master page with the ListView.
            this.Master = new ContentPage
            {
                Title = header.Text,
                BackgroundColor = Color.Transparent,
                Content = new GradientBackgroundPage
                {
                    StartColor = Color.FromHex("#79e0c7"),
                    EndColor = Color.FromHex("#359cff"),
                    Children =
                    {
                        patientProfile,
                        filesList
                    }
                }
            };

            if (customListViewRecords != null && customListViewRecords.Count > 0)
            {
                this.Detail = customListViewRecords[0].Page;
            }
            else
            {
                this.Detail = new SettingsPage();
            }



            // Define a selected handler for the ListView.
            filesList.ItemSelected += (sender, args) =>
            {
                this.IsPresented = false;
                if (filesList.SelectedItem != null)
                    this.Detail = ((MenuViewCell)filesList.SelectedItem).Page;
                filesList.SelectedItem = null;
            };


            //Переход со страницы настроек при дозагрузке - не правильный. Будем перехватывать это тут
            SettingsModel = SettingsModel.getSettingsInstance();
            FireFly = FireFly.getInstance();
            //DatabaseWorkerInstance.CurrentPatientChange += CheckOldFile;
            //FireFly.RawDataWorker.FireFlyDirInfoEvent += CheckOldFile;

            //Воспроизведение музыки
            //DependencyService.Get<ISignalsPlay>().PlayBadPulse();
            //PlaySoundTask().ContinueWith(RunnerPlaySoundTask);

        }


        Task PlaySoundTask()
        {
            return Task.Factory.StartNew(
                () =>
                {
                    DependencyService.Get<ISignalsPlay>().PlayBadPulse();
                    Task.Delay(2 * 60 * 1000).GetAwaiter().GetResult();
                });
        }

        void RunnerPlaySoundTask(Task t)
        {
            PlaySoundTask().ContinueWith(RunnerPlaySoundTask);
        }

        class DataCellView : ViewCell
        {
            public DataCellView()
            {
                //instantiate each of our views
                var menuStrokeTitle = new Label()
                {
                    HorizontalOptions = LayoutOptions.StartAndExpand,
                    VerticalOptions = LayoutOptions.EndAndExpand,
                    BackgroundColor = Color.Transparent,
                    Margin = new Thickness(0, 10, 10, 0),
                    TextColor = Color.White
                };
                var menuStrokeDiscription = new Label()
                {
                    HorizontalOptions = LayoutOptions.StartAndExpand,
                    VerticalOptions = LayoutOptions.StartAndExpand,
                    BackgroundColor = Color.Transparent,
                    Margin = new Thickness(0, 0, 10, 10),
                    TextColor = Color.LightGray
                };
                var menuIcon = new Image()
                {
                    HorizontalOptions = LayoutOptions.Center,
                    VerticalOptions = LayoutOptions.Center,
                    BackgroundColor = Color.Transparent,
                    Margin = new Thickness(10, 10, 10, 10)
                };



                Grid grid = new Grid
                {
                    RowDefinitions =
                    {
                        new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
                        new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
                        new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
                        new RowDefinition { Height = new GridLength(1, GridUnitType.Star) }
                    },
                    ColumnDefinitions =
                    {
                        new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                        new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                        new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                        new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                        new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                        new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
                    },
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                };

                menuStrokeTitle.SetBinding(Label.TextProperty, new Binding("MenuStrokeName"));
                menuStrokeDiscription.SetBinding(Label.TextProperty, new Binding("MenuStrokeDiscription"));
                menuIcon.SetBinding(Image.SourceProperty, new Binding("IconSource"));

                menuStrokeTitle.FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label));
                menuStrokeDiscription.FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label));

                grid.Children.Add(menuIcon, 0, 0);
                Grid.SetColumnSpan(menuIcon, 1);
                Grid.SetRowSpan(menuIcon, 4);

                grid.Children.Add(menuStrokeTitle, 1, 0);
                Grid.SetColumnSpan(menuStrokeTitle, 5);
                Grid.SetRowSpan(menuStrokeTitle, 2);

                grid.Children.Add(menuStrokeDiscription, 1, 2);
                Grid.SetColumnSpan(menuStrokeDiscription, 5);
                Grid.SetRowSpan(menuStrokeDiscription, 2);


                View = grid;
            }

        }

        class MenuViewCell
        {
            public string MenuStrokeName { get; set; }
            public string MenuStrokeDiscription { get; set; }
            public string IconSource { get; set; }
            public ContentPage Page { get; set; }

            public MenuViewCell(string menuStrokeName, string menuStrokeDiscription, string iconSource, ContentPage page)
            {
                this.MenuStrokeName = menuStrokeName;
                this.MenuStrokeDiscription = menuStrokeDiscription;
                this.IconSource = iconSource;
                this.Page = page;
            }

        }
    }
}