﻿using FenixMobile.Enumeration;
using FenixMobile.Model.BluetoothPart;
using FenixMobile.Model.DatabasePart;
using FenixMobile.View.HeadMasterMenu.ECGViewerPage;
using FenixMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.HeadMasterMenu
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MonitoringTabbedPage : TabbedPage
    {
        HolterECGViewModel ViewModel = null;
        public MonitoringTabbedPage(DataTransferMode transferMode, RecordTable referenceRecord = null)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, true);
            InitializeComponent();
            this.ViewModel = new HolterECGViewModel(transferMode, referenceRecord) { Navigation = this.Navigation};
            //this.BindingContext = this.ViewModel;
            Children.Add(new HolterECGView(this.ViewModel));
            //Children.Add(new ChaosgramPage(this.ViewModel));
            Children.Add(new HeartRateVariabilityView(this.ViewModel));
            Children.Add(new TrendsViewPage(this.ViewModel));
            Children.Add(new ScaterogrammPage(this.ViewModel));
            //Children.Add(new ProgramStatePage(this.ViewModel));
        }

        protected override bool OnBackButtonPressed()
        {
            this.ViewModel.IsLoadingAborted = true;
            this.ViewModel.Dispose();
            this.ViewModel = null;
            return base.OnBackButtonPressed();
        }
    }
}