﻿using FenixMobile.View.HeadMasterMenu.MenuItemPage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.HeadMasterMenu
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NotCoolMenu : TabbedPage
    {
        public NotCoolMenu()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, true);
            InitializeComponent();
            //this.BindingContext = this.ViewModel;
            Children.Add(new ModeSelectPage());
            Children.Add(new SavedFilePage());
            Children.Add(new SettingsPage());
        }
    }
}