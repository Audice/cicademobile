﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FenixMobile.View.Interface
{
    public interface IFileWorker
    {
        /// <summary>
        /// Создание файла с дельтами, используется как буферные
        /// </summary>
        /// <param name="filename">Имя файла</param>
        /// <param name="data">Список дельт</param>
        /// <returns></returns>
        bool CreateFile(string filename, List<short> data);

        /// <summary>
        /// Метод записи двумерного сигнала в файл
        /// 
        /// </summary>
        /// <param name="filename">Имя файла</param>
        /// <param name="data">Данные</param>
        /// <returns>True если запись прошла успешно, false - в противном случае</returns>
        bool WriteByteData(string filename, byte[] data);


        /// <summary>
        /// Проверка существования файла
        /// </summary>
        /// <param name="filename">Имя файла</param>
        /// <returns>True - такой файл уже существует, False - такого файла в памяти устройства нет</returns>
        bool ExistsFile(string filename); // проверка существования файла
        /// <summary>
        /// Создание файла в памяти устройства
        /// </summary>
        /// <param name="filename">Имя файла</param>
        /// <returns></returns>
        int CreateFile(string filename);
        /// <summary>
        /// Добавить данные в файл. В случае если каналов несколько, то запись сэмплов идёт поочереди: 1канал[0], 2канал[0], 1канал[1], 2канал[1]....
        /// </summary>
        /// <param name="filename">Имя файла</param>
        /// <param name="data">Данные, в ввиде массива short: 1канал[0], 2канал[0], 1канал[1], 2канал[1].... </param>
        /// <returns>Код операции завершения: >=0 - запись успешна, <0 - ошибка </returns>
        int AppendFile(string filename, short[] data);
        /// <summary>
        /// Загрузка данных из файла
        /// </summary>
        /// <param name="filename">Имя файла</param>
        /// <returns>Массив байт </returns>
        List<short> GetFile(string filename);  // загрузка данных из файла
        /// <summary>
        /// Удаление файла из памяти телефона
        /// </summary>
        /// <param name="filename">Имя файла</param>
        void DeleteFile(string filename);  // удаление файла

        /// <summary>
        /// Получение данных с конкретной позиции
        /// </summary>
        /// <param name="filename">Имя файла</param>
        /// <param name="position">Позиция, с которой начать чтения файла</param>
        /// <returns>Данные из файла с конкретной позиции</returns>
        List<short> GetDataFromSpecificByte(string filename, long position);



        /// <summary>
        /// Создание файла для хранения отправляемых на сервер файлов
        /// </summary>
        /// <param name="filename">Имя файла в хранилище. Совпадает с файлами в простом хранилище и на сервере.</param>
        bool CreateServerFile(string filename);

        /// <summary>
        /// Добавление серверных данных в файл локального хранилища
        /// </summary>
        /// <param name="filename">Имя файла в хранилище. Совпадает с файлами в простом хранилище и на сервере.</param>
        bool AppendServerFile(string filename, short[] data);

        /// <summary>
        /// Получить данные для сервера
        /// </summary>
        /// <param name="filename">Имя файла в хранилище. Совпадает с файлами в простом хранилище и на сервере.</param>
        List<short> GetServerFile(string filename);
        /// <summary>
        /// Удаление файла серверного хранилища из памяти телефона
        /// </summary>
        /// <param name="filename">Имя файла</param>
        void DeleteServerFile(string filename);

        /// <summary>
        /// Получить данные для сервера с определённого байта
        /// </summary>
        /// <param name="filename">Имя файла в хранилище. Совпадает с файлами в простом хранилище и на сервере.</param>
        List<short> GetPartServerFile(string filename, long startByte, ref long count);

        /// <summary>
        /// Получить количество байт в файле
        /// </summary>
        /// <param name="filename">Имя файла в хранилище. Совпадает с файлами в простом хранилище и на сервере.</param>
        long GetBytesCount(string filename);


        /// <summary>
        /// Получить количество байт в файле
        /// </summary>
        /// <param name="filename">Имя файла в хранилище. Совпадает с файлами в простом хранилище и на сервере.</param>
        bool ServerFileExist(string filename);


        /// <summary>
        /// Добавить новый лог в файл логирования
        /// </summary>
        /// <param name="LogString">Строка лога</param>
        Task AddNewLogInfo(string LogString);
        /// <summary>
        /// Получить логи
        /// </summary>
        /// <returns>Строка логов</returns>
        string GetLogs();

        /// <summary>
        /// Удалить текущие логи
        /// </summary>
        void DeleteLogs();
    }
}
