﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View.Pages.Conclusion
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HeadConclusion : ContentPage
    {
        public HeadConclusion()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}