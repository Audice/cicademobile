﻿using FenixMobile.Model.DatabasePart;
using FenixMobile.Model.ServerPart;
using FenixMobile.Model.SettingsPart;
using FenixMobile.ProtectedMechanism;
using FenixMobile.View.CustomDisplayAlert;
using FenixMobile.View.HeadMasterMenu;
using Rg.Plugins.Popup.Services;
using SoapConstruct.SoapFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        double width = 0;
        double height = 0;
        DatabaseWorker DatabaseWorker;

        public Login()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            this.DatabaseWorker = DatabaseWorker.getInstance();

            var AutorizationData = SettingsModel.GetLastUserInfo();
            if (AutorizationData != null)
            {
                this.LoginField.Text = AutorizationData.Item1;
                this.PasswordField.Text = AutorizationData.Item2;
            }
        }



        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (width != this.width || height != this.height)
            {
                this.width = width;
                this.height = height;

                PasswordField.WidthRequest = LogoImage.Width * 2;
                LoginField.WidthRequest = LogoImage.Width * 2;
                PasswordField.MinimumWidthRequest = LogoImage.Width * 2;
                LoginField.MinimumWidthRequest = LogoImage.Width * 2;

                LoginButton.WidthRequest = LogoImage.Width * 1.3;
                RegistrationButton.WidthRequest = LogoImage.Width * 1.3;
                LoginButton.MinimumWidthRequest = LogoImage.Width * 1.3;
                RegistrationButton.MinimumWidthRequest = LogoImage.Width * 1.3;


                if (width > height)
                {
                    HeadStack.Orientation = StackOrientation.Horizontal;
                    this.LogoStack.VerticalOptions = LayoutOptions.FillAndExpand;
                    this.LogoStack.HorizontalOptions = LayoutOptions.FillAndExpand;
                    this.LogoImage.VerticalOptions = LayoutOptions.CenterAndExpand;
                    this.LogoImage.HorizontalOptions = LayoutOptions.CenterAndExpand;
                    this.FrameStack.VerticalOptions = LayoutOptions.CenterAndExpand;
                    this.FrameStack.HorizontalOptions = LayoutOptions.CenterAndExpand;
                }
                else
                {
                    HeadStack.Orientation = StackOrientation.Vertical;
                    this.LogoStack.VerticalOptions = LayoutOptions.EndAndExpand;
                    this.LogoStack.HorizontalOptions = LayoutOptions.FillAndExpand;
                    this.LogoImage.VerticalOptions = LayoutOptions.EndAndExpand;
                    this.LogoImage.HorizontalOptions = LayoutOptions.CenterAndExpand;
                    this.FrameStack.VerticalOptions = LayoutOptions.StartAndExpand;
                    this.FrameStack.HorizontalOptions = LayoutOptions.FillAndExpand;
                }
            }

        }

        private async void LoginEvent(object sender, EventArgs e)
        {
            if (!DoubleTapSaver.AllowTap) return; DoubleTapSaver.AllowTap = false;


            //Переход к меню
            //await Navigation.PushAsync(new MenuPage());

            //Рабочая версия авторизации

            if (LoginField.Text != null && PasswordField.Text != null && LoginField.Text.Length > 0 && PasswordField.Text.Length > 0)
            {
                CustomLoadPage customLoadPage = new CustomLoadPage("Авторизация", "Выполняется вход");
                await PopupNavigation.Instance.PushAsync(customLoadPage);

                ServerWorker serverWorker = ServerWorker.getInstance();
                //Авторизация
                string sessionToken = await serverWorker.Authorization(new CreateSession(this.LoginField.Text, this.PasswordField.Text));
                if (sessionToken != null && sessionToken.Length > 10)
                {
                    SettingsModel.SetLastUser(LoginField.Text, PasswordField.Text);
                    //Проверка юзера на наличие в базе данных
                    if (this.DatabaseWorker.CheckUser(LoginField.Text))
                    {
                        //Обновим данные юзера
                        if (this.DatabaseWorker.UpdateSessionID(LoginField.Text, sessionToken))
                        {
                            if (this.DatabaseWorker.SetCurrentUser(LoginField.Text))
                                await Navigation.PushAsync(new NotCoolMenu());
                            else
                                await PopupNavigation.Instance.PushAsync(new ErrorDisplayAlert("Проблема сохранения данных. В доступе отказано"));
                        }
                        else
                            await PopupNavigation.Instance.PushAsync(new ErrorDisplayAlert("Проблема сохранения данных. В доступе отказано"));
                    }
                    else
                    {
                        //Нет в базе - добавим
                        if (!this.DatabaseWorker.AddNewUser(LoginField.Text, PasswordField.Text, sessionToken))
                            await PopupNavigation.Instance.PushAsync(new ErrorDisplayAlert("Проблема сохранения данных. В доступе отказано"));
                        else
                        {
                            if (this.DatabaseWorker.SetCurrentUser(LoginField.Text))
                                await Navigation.PushAsync(new NotCoolMenu());
                            else
                                await PopupNavigation.Instance.PushAsync(new ErrorDisplayAlert("Проблема сохранения данных. В доступе отказано"));

                        }
                    }
                    await PopupNavigation.Instance.RemovePageAsync(customLoadPage);
                }
                else
                {
                    await PopupNavigation.Instance.RemovePageAsync(customLoadPage);
                    await PopupNavigation.Instance.PushAsync(new ErrorDisplayAlert("Ошибка аутентификации"));
                }
                //await PopupNavigation.Instance.RemovePageAsync(customLoadPage);
            }
            else
            {
                var errorDisplayAlert = new ErrorDisplayAlert("Поля авторизации не заполнены!");
                await PopupNavigation.Instance.PushAsync(errorDisplayAlert);
            }
            DoubleTapSaver.ResumeTap();
        }

        private async void RegistrationEvent(object sender, EventArgs e)
        {
            if (!DoubleTapSaver.AllowTap) return; DoubleTapSaver.AllowTap = false;
            await Navigation.PushAsync(new Registration());
            DoubleTapSaver.ResumeTap();
        }
    }
}