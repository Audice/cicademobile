﻿using FenixMobile.Model.DatabasePart;
using FenixMobile.View.CustomControl;
using FenixMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FenixMobile.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Registration : ContentPage
    {
        string[] PickerValue = new string[] { "Мужской", "Женский"};
        public Registration()
        {
            InitializeComponent();
            this.BindingContext = new RegistrationViewModel() { Navigation = this.Navigation};
            this.UserGender.ItemsSource = PickerValue;
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void OnFocuse(object sender, EventArgs e)
        {
            ElypsedEntry currentEntry = (ElypsedEntry)sender;
            currentEntry.BorderColor = Color.FromHex("#79e0c7");
            currentEntry.BorderWidth += 2; 
        }
        private void UnFocuse(object sender, EventArgs e)
        {
            ElypsedEntry currentEntry = (ElypsedEntry)sender;
            currentEntry.BorderColor = Color.FromHex("#359cff");
            currentEntry.BorderWidth -= 2;
        }

        private void OnFocusePicker(object sender, EventArgs e)
        {
            ElypsedPicker currentEntry = (ElypsedPicker)sender;
            currentEntry.BorderColor = Color.FromHex("#79e0c7");
            currentEntry.BorderWidth += 2;
        }
        private void UnFocusePicker(object sender, EventArgs e)
        {
            ElypsedPicker currentEntry = (ElypsedPicker)sender;
            currentEntry.BorderColor = Color.FromHex("#359cff");
            currentEntry.BorderWidth -= 2;
        }

        private void OnFocuseDatePicker(object sender, EventArgs e)
        {
            ElypsedDatePicker currentEntry = (ElypsedDatePicker)sender;
            currentEntry.BorderColor = Color.FromHex("#79e0c7");
            currentEntry.BorderWidth += 2;
        }
        private void UnFocuseDatePicker(object sender, EventArgs e)
        {
            ElypsedDatePicker currentEntry = (ElypsedDatePicker)sender;
            currentEntry.BorderColor = Color.FromHex("#359cff");
            currentEntry.BorderWidth -= 2;
        }

        private async void RegistrationAction(object sender, EventArgs e)
        {
            //Нет реализации на сервере для регистрации нового юзера или пользователя...
            await Task.Delay(1);
            //await Navigation.PushAsync(new Registration());
        }
    }
}