﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FenixMobile.View.ViewEventArgs
{
    public class PointerAnglesEventArgs : EventArgs
    {
        public byte RotateIndex { get; set; }
    }
}
