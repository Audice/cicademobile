﻿using FenixMobile.Model.BluetoothPart;
using FenixMobile.Model.DatabasePart;
using FenixMobile.View.CustomDisplayAlert;
using FenixMobile.View.HeadMasterMenu;
using FenixMobile.View.HeadMasterMenu.ECGViewerPage;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FenixMobile.ViewModel
{
    public class ArchiveWorkViewModel : INotifyPropertyChanged, IDisposable
    {
        public INavigation Navigation { get; set; }
        public ArchiveWorkViewModel()
        {
            InitListView();
        }

        void InitListView()
        {
            PatientTable currentPatient = DatabaseWorker.getInstance().CurrentPatient;
            if (currentPatient != null)
            {
                List<RecordTable> records = DatabaseWorker.getInstance().GetUnfinishedRecord(currentPatient.ID);
                if (records != null && records.Count > 0)
                {
                    List<ArchiveRecordItem> localListViewSource = new List<ArchiveRecordItem>();
                    //Формируем список представления и обновляем страницу
                    for (int i = 0; i < records.Count; i++)
                    {
                        localListViewSource.Add(new ArchiveRecordItem()
                        {
                            Record = records[i],
                            CatalogNumber = "Директория " + records[i].CatalogNumber.ToString(),
                            ArchiveDate = "Дата: " + string.Format("{0:dd-MM-yyyy}", records[i].StartRecord)
                        });
                    }
                    this.ListViewSource = localListViewSource;
                }
                else
                {
                    //Список пуст
                    this.ListViewSource = null;
                }
            }
            else
            {
                //Не выбран профиль - пишем что не выбран пациент
                this.ListViewSource = null;
            }
        }

        private List<ArchiveRecordItem> listViewSource = null;
        public List<ArchiveRecordItem> ListViewSource
        {
            get
            {
                return this.listViewSource;
            }
            set
            {
                if (listViewSource != value && value != null)
                {
                    listViewSource = value;
                    OnPropertyChanged("ListViewSource");
                }
            }
        }

        private ArchiveRecordItem archiveRecordListItem = null;
        public ArchiveRecordItem ArchiveRecordListItem
        {
            get
            {
                return archiveRecordListItem;
            }
            set
            {
                if (archiveRecordListItem != value)
                {
                    archiveRecordListItem = value;
                    if (value != null)
                    {
                        //Переходим на страницу визуализации
                        ViewSavedECG(value.Record);
                        archiveRecordListItem = null;
                        OnPropertyChanged("ArchiveRecordListItem");
                    }
                }
            }
        }


        async void ViewSavedECG(RecordTable record)
        {
            if (await this.CheckCardiographStatus(record))
                await Navigation.PushAsync(new MonitoringTabbedPage(Enumeration.DataTransferMode.Archive, record));
        }

        private async Task<bool> CheckCardiographStatus(RecordTable record)
        {
            if (!(FireFly.getInstance().BluetoothConnectController.IsBluetoothConnect))
            {
                await PopupNavigation.Instance.PushAsync(new ErrorDisplayAlert("Кардиограф не активен! Возможно, устройство отключено."));
                return false;
            }
            if (record.MAC != FireFly.getInstance().BluetoothConnectController.DeviceMAC)
            {
                await PopupNavigation.Instance.PushAsync(new ErrorDisplayAlert("Ошибка! Обращение к архивам другого кардиографа. В доступе отказано!"));
                return false;
            }
            if (DatabaseWorker.getInstance().CurrentPatient == null)
            {
                await PopupNavigation.Instance.PushAsync(new ErrorDisplayAlert("Не выбран текущий пациент!"));
                return false;
            }
            if (FireFly.getInstance().GetFireFlyDirInfo() == null)
            {
                await PopupNavigation.Instance.PushAsync(new ErrorDisplayAlert("Кардиограф не отвечает!"));
                return false;
            }
            if (!FireFly.getInstance().GetFireFlyDirInfo().IsSD_Memory)
            {
                await PopupNavigation.Instance.PushAsync(new ErrorDisplayAlert("Ошибка! Устройство не укомплектовано долгосрочной памятью!"));
                return false;
            }
            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public class ArchiveRecordItem
        {
            public RecordTable Record { get; set; }
            public string CatalogNumber { get; set; }
            public string ArchiveDate { get; set; }
        }
    }
}
