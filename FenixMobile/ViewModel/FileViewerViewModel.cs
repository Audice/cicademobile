﻿using CyberHeartMobile.Classes;
using FenixMobile.Model.BluetoothPart;
using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using FenixMobile.Model.DatabasePart;
using FenixMobile.Model.FilePart;
using FenixMobile.Model.ServerPart;
using FenixMobile.View.Interface;
using FenixMobile.View.Pages.Conclusion;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using TouchTracking.Forms;
using Xamarin.Forms;

namespace FenixMobile.ViewModel
{
    public class FileViewerViewModel : INotifyPropertyChanged, IDisposable
    {
        //ServerState icon
        private readonly string BadConnectIcon = "ConnectBadState.png";
        private readonly string GoodConnectIcon = "ConnectGootState.png";
        //BluetoothState icon
        private readonly string FireFlyNotFoundIcon = "NoFireFly.png";
        private readonly string FireFlyFullCharageIcon = "FullFireFly.png";
        private readonly string FireFlyNotFullCharageIcon = "NotFullFireFly.png";
        private readonly string FireFlyMiddleCharageIcon = "MiddleFireFly.png";
        private readonly string FireFlyLowCharageIcon = "LowFireFly.png";

        public INavigation Navigation { get; set; }


        RecordTable CurrentRecord = null;

        DatabaseWorker Database;
        SkiaPaintController SkiaPaintController;
        ServerWorker ServerWorkerInstance;
        FireFly FireFly;

        SKCanvasView GeneralCanvas;
        SKCanvasView ConcretCanvas;

        private TouchEffect GeneralECGTouchEffect { get; set; }
        private TouchEffect ConcretECGTouchEffect { get; set; }

        public DisplayOrientation DisplayOrientation
        {
            get; private set;
        } = DisplayOrientation.VerticalOrientation;

        public FileViewerViewModel(RecordTable recordTable, SKCanvasView generalCanvas, SKCanvasView concretCanvas,
            TouchEffect generalECGTouchEffect, TouchEffect concretECGTouchEffect)
        {
            this.CurrentRecord = recordTable;

            this.ConcretCanvas = concretCanvas;
            this.GeneralCanvas = generalCanvas;
            this.GeneralECGTouchEffect = generalECGTouchEffect;
            this.ConcretECGTouchEffect = concretECGTouchEffect;

            FireFly = FireFly.getInstance();
            FireFly.BluetoothConnectController.ChangeBluetoothStateEvent += BluetoothStateChangeConnect;
            FireFly.BatteryHandler.BattaryStateChangeEvent += NewBattaryState;
            this.BluetoothDeviceState = FireFly.CharageLevel > 0 ? FireFly.CharageLevel.ToString() : "--";
            this.BluetoothDeviceStateIcon = GetBatteryIcon(FireFly.CharageLevel);

            ServerWorkerInstance = ServerWorker.getInstance();
            ServerStateIcon = ServerWorkerInstance.IsInternetConnection ? "ConnectGootState.png" : "ConnectBadState.png";
            ServerWorkerInstance.ChangeConnectionState += ConnectionStateChange;

            Database = DatabaseWorker.getInstance();

            
            SkiaPaintController = new SkiaPaintController(this.ConcretCanvas, this.GeneralCanvas, null, 
                this.GeneralECGTouchEffect, ConcretECGTouchEffect,
                this.DisplayOrientation, 
                Database.CurrentPatient.Surname + " " + Database.CurrentPatient.Name[0] + ". " + Database.CurrentPatient.Patronymic[0],
                Database.CurrentPatient.BornDate.ToString("dd-MM-yyyy"), Database.CurrentPatient.Growth.ToString(), Database.CurrentPatient.Weight.ToString(),
                DateTime.Now.ToString("dd-MM-yyyy"), 
                (Interfaces.ECGreceiveMode)recordTable.FileType, ViewMode.ControlMode,
                (short)recordTable.SampleFrequency, (uint)recordTable.ChannelsCount);


            

            List<List<short>> viewData = GetAdaptiveData(recordTable);
            if (viewData != null)
                SkiaPaintController.SetECGData(viewData);

        }

        public void SetDisplayOrientation(double width, double height)
        {
            if (width < height)
                this.DisplayOrientation = DisplayOrientation.VerticalOrientation;
            else
                this.DisplayOrientation = DisplayOrientation.HorizontalOrientation;

            if (this.SkiaPaintController != null)
                this.SkiaPaintController.SetDisplayOrientation(this.DisplayOrientation);
        }

        List<List<short>> GetAdaptiveData(RecordTable record)
        {
            List<short> rawData = DependencyService.Get<IFileWorker>().GetFile(record.FileName);
            if (rawData != null && rawData.Count > 0)
            {
                List<List<short>> resultsData = new List<List<short>>();
                for (int j = 0; j < record.ChannelsCount; j++)
                    resultsData.Add(new List<short>());


                for (int i = 0; i < rawData.Count; i += record.ChannelsCount)
                    for (int j = 0; j < record.ChannelsCount; j++)
                        resultsData[j].Add(rawData[i + j]);

                return resultsData;
            }
            return null;
        }


        void NewBattaryState(object sender, BatteryEventArgs e)
        {
            ushort chargeLevel = e.ChargeLevel;

            lock (locker)
            {
                try
                {
                    this.BluetoothDeviceState = chargeLevel.ToString();
                    this.BluetoothDeviceStateIcon = GetBatteryIcon(chargeLevel);
                }
                catch (Exception ex)
                {
                    string exm = ex.Message;
                }
            }
        }

        string GetBatteryIcon(int chargeLevel)
        {
            if (chargeLevel > 90) return this.FireFlyFullCharageIcon;
            if (chargeLevel <= 90 && chargeLevel > 60) return this.FireFlyNotFullCharageIcon;
            if (chargeLevel <= 60 && chargeLevel > 40) return this.FireFlyMiddleCharageIcon;
            if (chargeLevel <= 40 && chargeLevel >= 0) return this.FireFlyLowCharageIcon;
            return this.FireFlyNotFoundIcon;
        }

        object locker = new object();
        void BluetoothStateChangeConnect(object sender, EventArgs e)
        {
            BluetoothConnectServicer bluetoothConnectServicer = sender as BluetoothConnectServicer;
            if (!(bluetoothConnectServicer.IsBluetoothConnect))
            {
                lock (locker)
                {
                    this.BluetoothDeviceStateIcon = this.FireFlyNotFoundIcon;
                    this.BluetoothDeviceState = "--";
                }
            }
        }

        string bluetoothDeviceStateIcon = "NoFireFly.png";
        public string BluetoothDeviceStateIcon
        {
            get { return bluetoothDeviceStateIcon; }
            set
            {
                if (bluetoothDeviceStateIcon != value)
                {
                    bluetoothDeviceStateIcon = value;
                    OnPropertyChanged("BluetoothDeviceStateIcon");
                }
            }
        }

        string bluetoothDeviceState = "--";
        public string BluetoothDeviceState
        {
            get { return bluetoothDeviceState + "%"; }
            set
            {
                if (bluetoothDeviceState != value)
                {
                    bluetoothDeviceState = value;
                    OnPropertyChanged("BluetoothDeviceState");
                }
            }
        }

        void ConnectionStateChange(object sender, EventArgs e)
        {
            bool isConnected = (sender as ServerWorker).IsInternetConnection;
            if (isConnected)
            {
                this.ServerStateIcon = GoodConnectIcon;
            }
            else
                this.ServerStateIcon = BadConnectIcon;
        }

        string serverStateIcon = "ConnectBadState.png";
        public string ServerStateIcon
        {
            get { return serverStateIcon; }
            set
            {
                if (serverStateIcon != value)
                {
                    serverStateIcon = value;
                    OnPropertyChanged("ServerStateIcon");
                }
            }
        }


        //Команды
        public ICommand DeleteRecord => new Command(async() => {
            string fileName = this.CurrentRecord.FileName;
            Database.DeleteRecordByID(this.CurrentRecord.ID);
            DependencyService.Get<IFileWorker>().DeleteFile(fileName);
            this.Database.HasRecordsListChange = true;
            Dispose(); //??
            await Navigation.PopAsync();
        });
        public ICommand ToConclusion => new Command(async() => {
            await Navigation.PushAsync(new HeadConclusion());
        });


        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Dispose()
        {
            this.CurrentRecord = null;

            FireFly.BluetoothConnectController.ChangeBluetoothStateEvent -= BluetoothStateChangeConnect;
            FireFly.BatteryHandler.BattaryStateChangeEvent -= NewBattaryState;
            ServerWorkerInstance.ChangeConnectionState -= ConnectionStateChange;

            FireFly = null;
            ServerWorkerInstance = null;            
            Database = null;

            SkiaPaintController.Dispose();
            SkiaPaintController = null;
        }
    }
}
