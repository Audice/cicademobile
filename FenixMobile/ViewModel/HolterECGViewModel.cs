﻿using CyberHeartMobile.Classes;
using FenixMobile.Enumeration;
using FenixMobile.Interfaces;
using FenixMobile.Model.BluetoothPart;
using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using FenixMobile.Model.BluetoothPart.DataConvert;
using FenixMobile.Model.BluetoothPart.PackageControl;
using FenixMobile.Model.ConclusionPart;
using FenixMobile.Model.ConclusionPart.ConclusionEventArgs;
using FenixMobile.Model.ConclusionPart.Diagnosis;
using FenixMobile.Model.ConclusionPart.HeartRateVariability;
using FenixMobile.Model.DatabasePart;
using FenixMobile.Model.DrawPart;
using FenixMobile.Model.FilePart;
using FenixMobile.Model.ServerPart;
using FenixMobile.Model.SettingsPart;
using FenixMobile.Translations;
using FenixMobile.View.CustomDisplayAlert;
using FenixMobile.View.ViewEventArgs;
using Rg.Plugins.Popup.Services;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using TouchTracking.Forms;
using Xamarin.Forms;

namespace FenixMobile.ViewModel
{
    public class HolterECGViewModel : INotifyPropertyChanged, IDisposable
    {
        //ServerState icon
        private readonly string BadConnectIcon = "ConnectBadState.png";
        private readonly string GoodConnectIcon = "ConnectGootState.png";
        //BluetoothState icon
        private readonly string FireFlyNotFoundIcon = "NoFireFly.png";
        private readonly string FireFlyFullCharageIcon = "FullFireFly.png";
        private readonly string FireFlyNotFullCharageIcon = "NotFullFireFly.png";
        private readonly string FireFlyMiddleCharageIcon = "MiddleFireFly.png";
        private readonly string FireFlyLowCharageIcon = "LowFireFly.png";

        //Цветовая индикация
        static readonly string GoodStateHEX = "#4AAB3F";
        static readonly string So_SoStateHEX = "#F0BA00";
        static readonly string BadStateHEX = "#FF6552";

        //ConclusionModel ConclusionModel { get; set; } = null;

        public INavigation Navigation { get; set; }

        ECGreceiveMode ECGreceiveMode = ECGreceiveMode.HolterECG;

        FireFly FireFly;
        DatabaseWorker Database;
        PackageSequenceControler PackageSequenceControler { get; set; } = null;
        DataConverter DataConverter;
        ServerWorker ServerWorkerInstance;
        DataHandler DataHandler;
        //FileWorker FileWorker;
        //SOAPPackageSender SOAPPackageSender;
        DataSender DataSender;
        SettingsModel SettingsModel;
        ConclusionModel ConclusionModel;

        //Флаги контроля
        private bool IsProcessValueInit { get; set; } = false;


        private bool IsInfoCame { get; set; } = false;
        public bool IsLoadingAborted { get; set; } = false;

        FireFlyDirInfo FireFlyDirInfo { get; set; } = null;


        public TrendsViewController TrendsViewController { get; private set; }


        private SKCanvasView TrendsCanvas { get; set; }
        private TouchEffect TrendsTouchEffect { get; set; }
        private SKCanvasView ActivityCanvas { get; set; }
        private TouchEffect ActivityTouchEffect { get; set; }


        private SKCanvasView ScaterogrammCanvas { get; set; }
        //private TouchEffect ScaterogrammTouchEffect { get; set; }
        public void SetScaterogramFocus(bool isFocus)
        {
            if (isFocus)
                this.TrendsViewController.SetScaterogrammFocus();
            else
                this.TrendsViewController.SetScaterogrammUnfocus();
        }

        DataTransferMode CurrentTransferMode { get; set; }

        public HolterECGViewModel(DataTransferMode transferMode, RecordTable referenceTable = null)
        {
            this.CurrentTransferMode = transferMode;

            SettingsModel = SettingsModel.getSettingsInstance();
            SettingsModel.IsECGProcessingRun = true;

            //UdateFramesSize(App.ScreenWidth, App.ScreenHeight);

            FireFly = FireFly.getInstance();
            FireFly.BluetoothConnectController.ChangeBluetoothStateEvent += BluetoothStateChangeConnect;
            FireFly.BatteryHandler.BattaryStateChangeEvent += NewBattaryState;
            this.BluetoothDeviceState = FireFly.BatteryHandler.ChargePercent > 0 ? FireFly.BatteryHandler.ChargePercent.ToString() : "--";
            this.BluetoothDeviceStateIcon = GetBatteryIcon(FireFly.BatteryHandler.ChargePercent);
            Database = DatabaseWorker.getInstance();
            ServerWorkerInstance = ServerWorker.getInstance();
            ServerStateIcon = ServerWorkerInstance.IsInternetConnection ? "ConnectGootState.png" : "ConnectBadState.png";
            ServerWorkerInstance.ChangeConnectionState += ConnectionStateChange;
            FireFly.FireFlyInfoUpdate += InfoCame;
            FireFly.NoDataCameEvent += NoDataCome;
            if (referenceTable == null)
            {
                //1. Устанавливаем директорию в актуальное значение
                FireFly.SetActualDirectory();
                //2. Запускаем ожидание ответа на команду информации
                RunParallelTask();
            }
            else
            {
                //1. Устанавливаем директорию в актуальное значение
                FireFly.SetActualDirectory((short)referenceTable.CatalogNumber);
                //2. Запускаем ожидание ответа на команду информации
                RunParallelTask();
            }


            DependencyService.Get<ISignalsPlay>().PlayBadPulse();
        }

        private void NoDataCome(object sender, EventArgs e)
        {
            this.IsNoDataCome = "+";
        }

        string isNoDataCome = "-";
        public string IsNoDataCome
        {
            get { return isNoDataCome; }
            set
            {
                if (isNoDataCome != value)
                {
                    isNoDataCome = value;
                    OnPropertyChanged("IsNoDataCome");
                }
            }
        }

        string timeLastDirReq = "--:--:--";
        public string TimeLastDirReq
        {
            get { return timeLastDirReq; }
            set
            {
                if (timeLastDirReq != value)
                {
                    timeLastDirReq = value;
                    OnPropertyChanged("TimeLastDirReq");
                }
            }
        }


        string timeLastFileReq = "--:--:--";
        public string TimeLastFileReq
        {
            get { return timeLastFileReq; }
            set
            {
                if (timeLastFileReq != value)
                {
                    timeLastFileReq = value;
                    OnPropertyChanged("TimeLastFileReq");
                }
            }
        }

        string lastFileReqNum = "--";
        public string LastFileReqNum
        {
            get { return lastFileReqNum; }
            set
            {
                if (lastFileReqNum != value)
                {
                    lastFileReqNum = value;
                    OnPropertyChanged("LastFileReqNum");
                }
            }
        }


        void InfoCame(object sender, FireFlyDirInfoEventArgs e)
        {
            FireFlyDirInfo = e.FireFlyDirInfo;
            this.IsInfoCame = (e.FireFlyDirInfo != null);
        }


        private async void RunParallelTask()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken token = cts.Token;
            cts.CancelAfter(TimeSpan.FromSeconds(10)); //На подготовку даём 10 секунд

            CustomLoadPage customLoadPage = new CustomLoadPage("Соединение", "Получение информации от устройства");
            await PopupNavigation.Instance.PushAsync(customLoadPage);

            //Run waiter and wait 10 seconds
            try
            {
                Task taskWaiter = Task.Run(() =>
                {
                    while (!this.IsInfoCame && !this.IsLoadingAborted)
                    {
                        token.ThrowIfCancellationRequested();
                    }
                }, token);
                await Task.WhenAll(taskWaiter);
            }
            catch (OperationCanceledException) {}
            if (!IsLoadingAborted)
            {
                if (this.IsInfoCame && (this.FireFlyDirInfo != null))
                    InitStage();
                else
                {
                    var informationDisplayAlert = new ErrorDisplayAlert("Кардиограф не исправен.\nПерегрузите его и повторить попытку.");
                    await PopupNavigation.Instance.PushAsync(informationDisplayAlert);
                    await Navigation.PopAsync();
                    this.Dispose();
                }
            }
            cts.Dispose();
            await PopupNavigation.Instance.RemovePageAsync(customLoadPage);
        }


        void InitStage()
        {
            this.FireFly.FireFlyInfoUpdate -= InfoCame;
            this.PackageSequenceControler = new PackageSequenceControler(this.CurrentTransferMode, this.FireFlyDirInfo);
            this.DataConverter = new DataConverter(PackageSequenceControler, (uint)this.FireFlyDirInfo.FilesChannels);
            this.DataHandler = new DataHandler(this.DataConverter);
            this.DataSender = new DataSender(this.DataHandler);
            this.ConclusionModel = new ConclusionModel(DataConverter, (ushort)this.FireFlyDirInfo.FilesSPS, this.Database.CurrentPatient);

            this.ConclusionModel.PulseAnalyzer.DiagnosisUpdate += this.PulseUpdate;
            this.ConclusionModel.RithmConclusionReadyEvent += this.RithmStyleChange;
            this.ConclusionModel.BodyMassIndexChange += this.BodyMassIndexChange;
            this.ConclusionModel.HeartRatingCompiler.HeartrateRatingUpdateEvent += HeartRatingChange;
            this.ConclusionModel.SystemTensionIndexController.TensionIndexChangeEvent += TensionIndexChange;
            this.ConclusionModel.AdaptationLevel.AdaptationLevelChangeEvent += AdaptationLevelChange;
            this.ConclusionModel.VegetativeBalanceController.VegetativeBalanceIndexChangeEvent += VegetativeBalanceIndexChange;
            this.ConclusionModel.ExtrasystoleConclusionReadyEvent += ExtrasistoleReaction;
            this.ConclusionModel.PauseConclusionReadyEvent += PauseReaction;
            this.ConclusionModel.BlockadesConclusionReadyEvent += BlockadesReaction;
            this.ConclusionModel.ArrethmiaConclusionReadyEvent += ArrethmiaReaction;

            this.TrendsViewController = new TrendsViewController(this.ConclusionModel);
            this.SetTrendsAttr(TrendsCanvas, TrendsTouchEffect, ActivityCanvas, ActivityTouchEffect, -1, ViewMode.AnimationMode);
            this.SetScatterogrammAttr(ScaterogrammCanvas, -1, ViewMode.AnimationMode);
            IsProcessValueInit = true;

            //Запуск обмена данными с кардиографом
            //Так как это онлайн мониторинг - поправим пакет на необходимые директории
            this.PackageSequenceControler.RunECGReadingProcess();
        }
        private void ArrethmiaReaction(object sender, ArrethmiaEventArgs e)
        {
            this.DiscriptArethmia = e.ArrethmiaDiscription;
            switch (e.ProblemRank)
            {
                case 0:
                    this.ArethmiaRank = 5;
                    this.SmallDiscriptArethmia = Resource.NoDeviations;
                    this.ColorArethmia = Color.FromHex(GoodStateHEX);
                    break;
                case 1:
                    this.ArethmiaRank = 4;
                    this.SmallDiscriptArethmia = Resource.MinorDeviations;
                    this.ColorArethmia = Color.FromHex(GoodStateHEX);
                    break;
                case 2:
                    this.ArethmiaRank = 3;
                    this.SmallDiscriptArethmia = Resource.SmallProblems;
                    this.ColorArethmia = Color.FromHex(So_SoStateHEX);
                    break;
                default:
                    this.ColorArethmia = Color.FromHex(BadStateHEX);
                    this.SmallDiscriptArethmia = Resource.AttentionMessage;
                    this.ArethmiaRank = 2;
                    break;
            }
        }
        private void BlockadesReaction(object sender, BlockadesEventArgs e)
        {
            this.BlockadesDiscription = e.BlockadesDiscription;
            switch (e.ProblemRank)
            {
                case 0:
                    this.BlockadeRank = 5;
                    this.SmallBlockadesDiscription = Resource.NoDeviations;
                    this.ColorBlockades = Color.FromHex(GoodStateHEX);
                    break;
                case 1:
                    this.BlockadeRank = 4;
                    this.SmallBlockadesDiscription = Resource.MinorDeviations;
                    this.ColorBlockades = Color.FromHex(GoodStateHEX);
                    break;
                default:
                    this.ColorBlockades = Color.FromHex(BadStateHEX);
                    this.SmallBlockadesDiscription = Resource.AttentionMessage;
                    this.BlockadeRank = 3;
                    break;
            }
        }
        private void PauseReaction(object sender, PauseEventArgs e)
        {
            this.PauseDiscription = e.PauseDiscription;
            switch (e.ProblemRank)
            {
                case 0:
                    this.PauseRank = 5;
                    this.SmallPauseDiscription = Resource.NoDeviations;
                    this.ColorPause = Color.FromHex(GoodStateHEX);
                    break;
                case 1:
                    this.PauseRank = 5;
                    this.SmallPauseDiscription = Resource.MinorDeviations;
                    this.ColorPause = Color.FromHex(GoodStateHEX);
                    break;
                case 2:
                    this.ColorPause = Color.FromHex(So_SoStateHEX);
                    this.SmallPauseDiscription = Resource.SmallProblems;
                    this.PauseRank = 4;
                    this.PauseDiscription += Environment.NewLine;
                    this.PauseDiscription += Environment.NewLine;
                    this.PauseDiscription += "Двухсекундных пауз: " + e.CountFromTwoToThreeSecondsPause.ToString();
                    this.PauseDiscription += Environment.NewLine;
                    this.PauseDiscription += "Трёхсекундных пауз: " + e.CountMoreThreeSecondsPause.ToString();
                    break;
                default:
                    this.PauseRank = 3;
                    this.ColorPause = Color.FromHex(BadStateHEX);
                    this.SmallPauseDiscription = Resource.AttentionMessage;
                    this.PauseDiscription += Environment.NewLine;
                    this.PauseDiscription += Environment.NewLine;
                    this.PauseDiscription += "Двухсекундных пауз: " + e.CountFromTwoToThreeSecondsPause.ToString();
                    this.PauseDiscription += Environment.NewLine;
                    this.PauseDiscription += "Трёхсекундных пауз: " + e.CountMoreThreeSecondsPause.ToString();
                    break;
            }
        }
        public void SetTrendsCanvas(SKCanvasView canvas, TouchEffect touchEffect, SKCanvasView activityCanvas, TouchEffect activityTouchEffect)
        {
            this.TrendsCanvas = canvas;
            this.TrendsTouchEffect = touchEffect;
            this.ActivityCanvas = activityCanvas;
            this.ActivityTouchEffect = activityTouchEffect;

            if (this.TrendsViewController != null)
            {
                this.TrendsViewController.SetCanvas(canvas, touchEffect, -1, ViewMode.AnimationMode);
                this.TrendsViewController.SetActivityCanvas(activityCanvas, activityTouchEffect, -1, ViewMode.AnimationMode);
            } 
        }
        public void SetScaterogrammCanvas(SKCanvasView canvas)
        {
            this.ScaterogrammCanvas = canvas;
            if (this.TrendsViewController != null)
                this.TrendsViewController.SetScaterogrammCanvas(canvas, -1, ViewMode.AnimationMode);
        }
        public void SetTrendsAttr(SKCanvasView canvas, TouchEffect trendsTouchTracking, SKCanvasView activityCanvas, TouchEffect activityTouchEffect,
            int numDrawsSamples, ViewMode currentMode)
        {
            if (this.TrendsViewController != null)
            {
                this.TrendsViewController.SetCanvas(canvas, trendsTouchTracking, -1, ViewMode.AnimationMode);
                this.TrendsViewController.SetActivityCanvas(activityCanvas, activityTouchEffect, -1, ViewMode.AnimationMode);
            }
        }
        public void SetScatterogrammAttr(SKCanvasView scaterogrammCanvas,
            int numDrawsSamples, ViewMode currentMode)
        {
            if (this.TrendsViewController != null)
            {
                this.TrendsViewController.SetScaterogrammCanvas(scaterogrammCanvas, -1, ViewMode.AnimationMode);
            }
        }
        /// <summary>
        /// Событие, для организации контролируемого поворота указателя
        /// </summary>
        public event EventHandler<PointerAnglesEventArgs> PointerAngleChange;
        protected void OnPointerAngleChange(PointerAnglesEventArgs e)
        {
            EventHandler<PointerAnglesEventArgs> handler = PointerAngleChange;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        //Регион, отвечающий за изменение GUI связанного с ВСР
        #region

        private void TensionIndexChange(object sender, IndexChangeOptionsEventArgs e)
        {
            if (!isTensionIndexInit)
            {
                this.TensionIndexMin = ((SystemTensionIndexController)(sender)).LeftBorderControl;
                this.TensionIndexMax = ((SystemTensionIndexController)(sender)).RightBorderControl;
                isTensionIndexInit = true;
            }
            this.TensionIndex = e.ValuePercent;
            switch (e.HeartRateVariabilityIndex)
            {
                case HeartRateVariabilityIndex.Good:
                    this.TensionIndexDiscription = Resource.SystemTensionIndexGood;
                    break;
                case HeartRateVariabilityIndex.NotVeryGood:
                    this.TensionIndexDiscription = Resource.SystemTensionIndexNotGood;
                    break;
                case HeartRateVariabilityIndex.Warning:
                    this.TensionIndexDiscription = Resource.SystemTensionIndexWarning;
                    break;
                case HeartRateVariabilityIndex.Danger:
                    this.TensionIndexDiscription = Resource.SystemTensionIndexDanger;
                    break;
                case HeartRateVariabilityIndex.Critical:
                    this.TensionIndexDiscription = Resource.SystemTensionIndexCritical;
                    break;
                default:
                    this.TensionIndexDiscription = "";
                    break;
            }
        }
        private void AdaptationLevelChange(object sender, IndexChangeOptionsEventArgs e)
        {
            if (!isAdaptationLevelInit)
            {
                this.AdaptationLevelIndexMin = ((AdaptationLevel)(sender)).LeftBorderControl;
                this.AdaptationLevelIndexMax = ((AdaptationLevel)(sender)).RightBorderControl;
                isAdaptationLevelInit = true;
            }
            this.AdaptationLevelIndex = e.ValuePercent;
            switch (e.HeartRateVariabilityIndex)
            {
                case HeartRateVariabilityIndex.Good:
                    this.AdaptationLevelIndexDiscription = Resource.AdaptationLevelGood;
                    break;
                case HeartRateVariabilityIndex.NotVeryGood:
                    this.AdaptationLevelIndexDiscription = Resource.AdaptationLevelNotVeryGood;
                    break;
                case HeartRateVariabilityIndex.Warning:
                    this.AdaptationLevelIndexDiscription = Resource.AdaptationLevelWarning;
                    break;
                case HeartRateVariabilityIndex.Danger:
                    this.AdaptationLevelIndexDiscription = Resource.AdaptationLevelDanger;
                    break;
                case HeartRateVariabilityIndex.Critical:
                    this.AdaptationLevelIndexDiscription = Resource.AdaptationLevelCritical;
                    break;
                default:
                    this.AdaptationLevelIndexDiscription = "";
                    break;
            }
        }
        private void VegetativeBalanceIndexChange(object sender, IndexChangeOptionsEventArgs e)
        {
            if (!isVegetativeBalanceInit)
            {
                this.VegetativeBalanceIndexMin = ((VegetativeBalanceController)(sender)).LeftBorderControl;
                this.VegetativeBalanceIndexMax = ((VegetativeBalanceController)(sender)).RightBorderControl;
                isVegetativeBalanceInit = true;
            }
            this.VegetativeBalanceIndex = e.ValuePercent;
            switch (e.HeartRateVariabilityIndex)
            {
                case HeartRateVariabilityIndex.Good:
                    this.VegetativeBalanceIndexDiscription = Resource.VegetativeBalanceGood;
                    break;
                case HeartRateVariabilityIndex.NotVeryGood:
                    this.VegetativeBalanceIndexDiscription = Resource.VegetativeBalanceNotVeryGood;
                    break;
                case HeartRateVariabilityIndex.Warning:
                    this.VegetativeBalanceIndexDiscription = Resource.VegetativeBalanceWarning;
                    break;
                case HeartRateVariabilityIndex.Danger:
                    this.VegetativeBalanceIndexDiscription = Resource.VegetativeBalanceDanger;
                    break;
                case HeartRateVariabilityIndex.Critical:
                    this.VegetativeBalanceIndexDiscription = Resource.VegetativeBalanceCritical;
                    break;
                default:
                    this.VegetativeBalanceIndexDiscription = "";
                    break;
            }
        }

        private bool isTensionIndexInit = false;
        private bool isAdaptationLevelInit = false;
        private bool isVegetativeBalanceInit = false;

        ushort tensionIndex = 0;
        public ushort TensionIndex
        {
            get { return tensionIndex; }
            set
            {
                if (tensionIndex != value)
                {
                    tensionIndex = value;
                    OnPropertyChanged("TensionIndex");
                }
            }
        }
        ushort tensionIndexMax = 100;
        public ushort TensionIndexMax
        {
            get { return tensionIndexMax; }
            set
            {
                if (tensionIndexMax != value)
                {
                    tensionIndexMax = value;
                    OnPropertyChanged("TensionIndexMax");
                }
            }
        }
        ushort tensionIndexMin = 0;
        public ushort TensionIndexMin
        {
            get { return tensionIndexMin; }
            set
            {
                if (tensionIndexMin != value)
                {
                    tensionIndexMin = value;
                    OnPropertyChanged("TensionIndexMin");
                }
            }
        }
        string tensionIndexDiscription = "Процесс расчёта уровня стресса...";
        public string TensionIndexDiscription
        {
            get { return tensionIndexDiscription; }
            set
            {
                if (tensionIndexDiscription != value)
                {
                    tensionIndexDiscription = value;
                    OnPropertyChanged("TensionIndexDiscription");
                }
            }
        }


        ushort adaptationLevelIndex = 0;
        public ushort AdaptationLevelIndex
        {
            get { return adaptationLevelIndex; }
            set
            {
                if (adaptationLevelIndex != value)
                {
                    adaptationLevelIndex = value;
                    OnPropertyChanged("AdaptationLevelIndex");
                }
            }
        }
        ushort adaptationLevelIndexMax = 100;
        public ushort AdaptationLevelIndexMax
        {
            get { return adaptationLevelIndexMax; }
            set
            {
                if (adaptationLevelIndexMax != value)
                {
                    adaptationLevelIndexMax = value;
                    OnPropertyChanged("AdaptationLevelIndexMax");
                }
            }
        }
        ushort adaptationLevelIndexMin = 0;
        public ushort AdaptationLevelIndexMin
        {
            get { return adaptationLevelIndexMin; }
            set
            {
                if (adaptationLevelIndexMin != value)
                {
                    adaptationLevelIndexMin = value;
                    OnPropertyChanged("AdaptationLevelIndexMin");
                }
            }
        }
        string adaptationLevelDiscription = "Процесс расчёта уровня адаптации...";
        public string AdaptationLevelIndexDiscription
        {
            get { return adaptationLevelDiscription; }
            set
            {
                if (adaptationLevelDiscription != value)
                {
                    adaptationLevelDiscription = value;
                    OnPropertyChanged("AdaptationLevelIndexDiscription");
                }
            }
        }
        ushort vegetativeBalanceIndex = 0;
        public ushort VegetativeBalanceIndex
        {
            get { return vegetativeBalanceIndex; }
            set
            {
                if (vegetativeBalanceIndex != value)
                {
                    vegetativeBalanceIndex = value;
                    OnPropertyChanged("VegetativeBalanceIndex");
                }
            }
        }
        ushort vegetativeBalanceIndexMax = 100;
        public ushort VegetativeBalanceIndexMax
        {
            get { return vegetativeBalanceIndexMax; }
            set
            {
                if (vegetativeBalanceIndexMax != value)
                {
                    vegetativeBalanceIndexMax = value;
                    OnPropertyChanged("VegetativeBalanceIndexMax");
                }
            }
        }
        ushort vegetativeBalanceIndexMin = 0;
        public ushort VegetativeBalanceIndexMin
        {
            get { return vegetativeBalanceIndexMin; }
            set
            {
                if (vegetativeBalanceIndexMin != value)
                {
                    vegetativeBalanceIndexMin = value;
                    OnPropertyChanged("VegetativeBalanceIndexMin");
                }
            }
        }
        string vegetativeBalanceIndexDiscription = "Процесс расчёта индекса равновесия нервной системы...";
        public string VegetativeBalanceIndexDiscription
        {
            get { return vegetativeBalanceIndexDiscription; }
            set
            {
                if (vegetativeBalanceIndexDiscription != value)
                {
                    vegetativeBalanceIndexDiscription = value;
                    OnPropertyChanged("VegetativeBalanceIndexDiscription");
                }
            }
        }
        #endregion


        private void HeartRatingChange(object sender, HeartrateRatingEventArgs e)
        {
            this.HeartrateRatingTitle = e.HeartrateRatingTitle;
            OnPointerAngleChange(new PointerAnglesEventArgs() { RotateIndex = e.HeartrateRatingCode });
        }


        
        void NewBattaryState(object sender, BatteryEventArgs e)
        {
            ushort chargeLevel = e.ChargeLevel;

            lock (locker)
            {
                try
                {
                    this.BluetoothDeviceState = chargeLevel.ToString();
                    this.BluetoothDeviceStateIcon = GetBatteryIcon(chargeLevel);
                }
                catch (Exception ex)
                {
                    string exm = ex.Message;
                }
            }
        }
        string GetBatteryIcon(int chargeLevel)
        {
            if (chargeLevel > 90) return this.FireFlyFullCharageIcon;
            if (chargeLevel <= 90 && chargeLevel > 60) return this.FireFlyNotFullCharageIcon;
            if (chargeLevel <= 60 && chargeLevel > 40) return this.FireFlyMiddleCharageIcon;
            if (chargeLevel <= 40 && chargeLevel >= 0) return this.FireFlyLowCharageIcon;
            return this.FireFlyNotFoundIcon;
        }
        object locker = new object();
        void BluetoothStateChangeConnect(object sender, EventArgs e)
        {
            BluetoothConnectServicer bluetoothConnectServicer = sender as BluetoothConnectServicer;
            if (!(bluetoothConnectServicer.IsBluetoothConnect))
            {
                lock (locker)
                {
                    this.BluetoothDeviceStateIcon = this.FireFlyNotFoundIcon;
                    this.BluetoothDeviceState = "--";
                }
            }
        }
        //Данные по принятым пакетам
        string heartrateRatingTitle = "Идёт обработка...";
        public string HeartrateRatingTitle
        {
            get { return heartrateRatingTitle; }
            set
            {
                if (heartrateRatingTitle != value)
                {
                    heartrateRatingTitle = value;
                    OnPropertyChanged("HeartrateRatingTitle");
                }
            }
        }





        string fireFlyPackageNumber = "--";
        public string FireFlyPackageNumber
        {
            get { return fireFlyPackageNumber; }
            set
            {
                if (fireFlyPackageNumber != value)
                {
                    fireFlyPackageNumber = value;
                    OnPropertyChanged("FireFlyPackageNumber");
                }
            }
        }
        string fireFlyMaxPackageNumber = "--";
        public string FireFlyMaxPackageNumber
        {
            get { return fireFlyMaxPackageNumber; }
            set
            {
                if (fireFlyMaxPackageNumber != value)
                {
                    fireFlyMaxPackageNumber = value;
                    OnPropertyChanged("FireFlyMaxPackageNumber");
                }
            }
        }
        //Либо Bl либо sd
        string fireFlyReceiveState = "--";
        public string FireFlyReceiveState
        {
            get { return fireFlyReceiveState; }
            set
            {
                if (fireFlyReceiveState != value)
                {
                    fireFlyReceiveState = value;
                    OnPropertyChanged("FireFlyReceiveState");
                }
            }
        }
        //+ создан, - нет
        string serverFileState = "-";
        public string ServerFileState
        {
            get { return serverFileState; }
            set
            {
                if (serverFileState != value)
                {
                    serverFileState = value;
                    OnPropertyChanged("ServerFileState");
                }
            }
        }
        string currentServerPart = "--";
        public string CurrentServerPart
        {
            get { return currentServerPart; }
            set
            {
                if (currentServerPart != value)
                {
                    currentServerPart = value;
                    OnPropertyChanged("CurrentServerPart");
                }
            }
        }
        string serverFileClose = "no";
        public string ServerFileClose
        {
            get { return serverFileClose; }
            set
            {
                if (serverFileClose != value)
                {
                    serverFileClose = value;
                    OnPropertyChanged("ServerFileClose");
                }
            }
        }

        string signalListCount = "--";
        public string SignalListCount
        {
            get { return signalListCount; }
            set
            {
                if (signalListCount != value)
                {
                    signalListCount = value;
                    OnPropertyChanged("SignalListCount");
                }
            }
        }


        string bluetoothDeviceStateIcon = "NoFireFly.png";
        public string BluetoothDeviceStateIcon
        {
            get { return bluetoothDeviceStateIcon; }
            set
            {
                if (bluetoothDeviceStateIcon != value)
                {
                    bluetoothDeviceStateIcon = value;
                    OnPropertyChanged("BluetoothDeviceStateIcon");
                }
            }
        }

        string bluetoothDeviceState = "--";
        public string BluetoothDeviceState
        {
            get { return bluetoothDeviceState + "%"; }
            set
            {
                if (bluetoothDeviceState != value)
                {
                    bluetoothDeviceState = value;
                    OnPropertyChanged("BluetoothDeviceState");
                }
            }
        }

        void ConnectionStateChange(object sender, EventArgs e)
        {
            bool isConnected = (sender as ServerWorker).IsInternetConnection;
            if (isConnected)
            {
                this.ServerStateIcon = GoodConnectIcon;
            }
            else
                this.ServerStateIcon = BadConnectIcon;
        }

        string serverStateIcon = "ConnectBadState.png";
        public string ServerStateIcon
        {
            get { return serverStateIcon; }
            set
            {
                if (serverStateIcon != value)
                {
                    serverStateIcon = value;
                    OnPropertyChanged("ServerStateIcon");
                }
            }
        }

        //Аритмия
        #region
        private byte ArethmiaRank = 5;
        private string smallDiscriptArethmia = Resource.NoDeviations;
        public string SmallDiscriptArethmia
        {
            get { return smallDiscriptArethmia; }
            set
            {
                if (smallDiscriptArethmia != value)
                {
                    smallDiscriptArethmia = value;
                    OnPropertyChanged("SmallDiscriptArethmia");
                }
            }
        }

        private string discriptArethmia = Resource.NoDeviations;
        public string DiscriptArethmia
        {
            get { return discriptArethmia; }
            set
            {
                if (discriptArethmia != value)
                {
                    discriptArethmia = value;
                    OnPropertyChanged("DiscriptArethmia");
                }
            }
        }
        private Color colorArethmia = Color.FromHex(GoodStateHEX);
        public Color ColorArethmia
        {
            get { return colorArethmia; }
            set
            {
                if (colorArethmia != value)
                {
                    colorArethmia = value;
                    OnPropertyChanged("ColorArethmia");
                }
            }
        }
        private int widhtArethmiaFrame = 100;
        public int WidhtArethmiaFrame
        {
            get { return widhtArethmiaFrame; }
            set
            {
                if (widhtArethmiaFrame != value)
                {
                    widhtArethmiaFrame = value;
                    OnPropertyChanged("WidhtArethmiaFrame");
                }
            }
        }
        private int heightArethmiaFrame = 100;
        public int HeightArethmiaFrame
        {
            get { return heightArethmiaFrame; }
            set
            {
                if (heightArethmiaFrame != value)
                {
                    heightArethmiaFrame = value;
                    OnPropertyChanged("HeightArethmiaFrame");
                }
            }
        }
        #endregion

        //Пульс
        #region
        void PulseUpdate(object sender, DiagnosisDiscription e)
        {
            if (e != null)
            {
                this.PulseTitle = e.DiagnosisTitle;
                this.PulseSmallDisc = e.SmallDiagnosisDiscription;
                this.ColorPulse = CalculateColor(e.DiagnosisRank);
                this.PulseDiscription = e.DiagnosDiscription + Environment.NewLine + e.SpecificDiagnosisDiscription + Environment.NewLine
                    + "Рекомендации: " + e.DiagnosisRecommendation;
            }
            else
            {
                this.PulseTitle = "--";
                this.PulseSmallDisc = "";
                this.ColorPulse = CalculateColor(DiagnosisRank.Normal);
                this.PulseDiscription = "Возникли проблемы при определении пульса. " + Environment.NewLine
                    + "Рекомендации: отрегулируйте положение мобильного кардиографа и проверьте контакт электродов с телом или устройством. ";
            }

        }

        private string pulseTitle = "--";
        public string PulseTitle
        {
            get { return pulseTitle; }
            set
            {
                if (pulseTitle != value)
                {
                    pulseTitle = value;
                    OnPropertyChanged("PulseTitle");
                }
            }
        }
        private string pulseSmallDisc = "Расчёт...";
        public string PulseSmallDisc
        {
            get { return pulseSmallDisc; }
            set
            {
                if (pulseSmallDisc != value)
                {
                    pulseSmallDisc = value;
                    OnPropertyChanged("PulseSmallDisc");
                }
            }
        }

        private string pulseDiscription = "Расчёт...";
        public string PulseDiscription
        {
            get { return pulseDiscription; }
            set
            {
                if (pulseDiscription != value)
                {
                    pulseDiscription = value;
                    OnPropertyChanged("PulseDiscription");
                }
            }
        }


        private Color colorPulse = Color.FromHex(GoodStateHEX);
        public Color ColorPulse
        {
            get { return colorPulse; }
            set
            {
                if (colorPulse != value)
                {
                    colorPulse = value;
                    OnPropertyChanged("ColorPulse");
                }
            }
        }

        private byte PulseRank = 5;

        private Color CalculateColor(DiagnosisRank diagnosisRank)
        {
            Color resultColor = Color.FromHex(GoodStateHEX); ;
            switch (diagnosisRank)
            {
                case DiagnosisRank.VeryGood:
                    resultColor = Color.FromHex(GoodStateHEX);
                    PulseRank = 5;
                    break;
                case DiagnosisRank.Good:
                    resultColor = Color.FromHex(GoodStateHEX);
                    PulseRank = 5;
                    break;
                case DiagnosisRank.Normal:
                    resultColor = Color.FromHex(So_SoStateHEX);
                    PulseRank = 5;
                    break;
                case DiagnosisRank.Warning:
                    resultColor = Color.FromHex(So_SoStateHEX);
                    PulseRank = 4;
                    break;
                case DiagnosisRank.Danger:
                    resultColor = Color.FromHex(BadStateHEX);
                    PulseRank = 3;
                    break;
                default:
                    break;
            }
            return resultColor;
        }

        private int widhtPulseFrame = 100;
        public int WidhtPulseFrame
        {
            get { return widhtPulseFrame; }
            set
            {
                if (widhtPulseFrame != value)
                {
                    widhtPulseFrame = value;
                    OnPropertyChanged("WidhtPulseFrame");
                }
            }
        }
        private int heightPulseFrame = 100;
        public int HeightPulseFrame
        {
            get { return heightPulseFrame; }
            set
            {
                if (heightPulseFrame != value)
                {
                    heightPulseFrame = value;
                    OnPropertyChanged("HeightPulseFrame");
                }
            }
        }
        #endregion

        //Ритмы
        #region 
        private byte RithmRank = 5;
        private string rithmDiscription = Resource.NoDeviations;
        public string RithmDiscription
        {
            get { return rithmDiscription; }
            set
            {
                if (rithmDiscription != value)
                {
                    rithmDiscription = value;
                    OnPropertyChanged("RithmDiscription");
                }
            }
        }
        private string rithmTitleDiscription = Resource.NoDeviations;
        public string RithmTitleDiscription
        {
            get { return rithmTitleDiscription; }
            set
            {
                if (rithmTitleDiscription != value)
                {
                    rithmTitleDiscription = value;
                    OnPropertyChanged("RithmTitleDiscription");
                }
            }
        }
        private Color colorRithm = Color.FromHex(GoodStateHEX);
        public Color ColorRithm
        {
            get { return colorRithm; }
            set
            {
                if (colorRithm != value)
                {
                    colorRithm = value;
                    OnPropertyChanged("ColorRithm");
                }
            }
        }
        private int widhtRithmFrame = 100;
        public int WidhtRithmFrame
        {
            get { return widhtRithmFrame; }
            set
            {
                if (widhtRithmFrame != value)
                {
                    widhtRithmFrame = value;
                    OnPropertyChanged("WidhtRithmFrame");
                }
            }
        }
        private int heightRithmFrame = 100;
        public int HeightRithmFrame
        {
            get { return heightRithmFrame; }
            set
            {
                if (heightRithmFrame != value)
                {
                    heightRithmFrame = value;
                    OnPropertyChanged("HeightRithmFrame");
                }
            }
        }

        void RithmStyleChange(object sender, RithmEventArgs e)
        {
            this.RithmDiscription = e.RithmDiscription;
            this.RithmTitleDiscription = e.RithmSmallDiscription;
            if (e.ProblemRank == 0)
            {
                this.RithmRank = 4;
                this.ColorRithm = Color.FromHex(GoodStateHEX);
            }  
            if (e.ProblemRank == 1)
            {
                this.RithmRank = 4;
                this.ColorRithm = Color.FromHex(So_SoStateHEX);
            }
            if (e.ProblemRank >= 2)
            {
                this.RithmRank = 3;
                this.ColorRithm = Color.FromHex(BadStateHEX);
            }
        }


        #endregion

        //Экстрасистолия
        #region 

        private byte ExtrasistoleRank = 5;
        private uint CountVentricularExtrasistole = 0;
        private uint CountAtrialExtrasistole = 0;

        private string smallExtrasystoleDiscription = Resource.NoDeviations;
        public string SmallExtrasystoleDiscription
        {
            get { return smallExtrasystoleDiscription; }
            set
            {
                if (smallExtrasystoleDiscription != value)
                {
                    smallExtrasystoleDiscription = value;
                    OnPropertyChanged("SmallExtrasystoleDiscription");
                }
            }
        }

        private string extrasystoleDiscription = Resource.NoDeviations;
        public string ExtrasystoleDiscription
        {
            get { return extrasystoleDiscription; }
            set
            {
                if (extrasystoleDiscription != value)
                {
                    extrasystoleDiscription = value;
                    OnPropertyChanged("ExtrasystoleDiscription");
                }
            }
        }
        private Color colorExtrasystole = Color.FromHex(GoodStateHEX);
        public Color ColorExtrasystole
        {
            get { return colorExtrasystole; }
            set
            {
                if (colorExtrasystole != value)
                {
                    colorExtrasystole = value;
                    OnPropertyChanged("ColorExtrasystole");
                }
            }
        }
        private int widhtExtrasystoleFrame = 100;
        public int WidhtExtrasystoleFrame
        {
            get { return widhtExtrasystoleFrame; }
            set
            {
                if (widhtExtrasystoleFrame != value)
                {
                    widhtExtrasystoleFrame = value;
                    OnPropertyChanged("WidhtExtrasystoleFrame");
                }
            }
        }
        private int heightExtrasystoleFrame = 100;
        public int HeightExtrasystoleFrame
        {
            get { return heightExtrasystoleFrame; }
            set
            {
                if (heightExtrasystoleFrame != value)
                {
                    heightExtrasystoleFrame = value;
                    OnPropertyChanged("HeightExtrasystoleFrame");
                }
            }
        }

        private void ExtrasistoleReaction(object sender, ExtrasystoleEventArgs e)
        {
            this.ExtrasystoleDiscription = e.ExtrasystoleDiscription;
            switch (e.ProblemRank)
            {
                case 0:
                    this.ExtrasistoleRank = 5;
                    this.SmallExtrasystoleDiscription = Resource.NoDeviations;
                    this.ColorExtrasystole = Color.FromHex(GoodStateHEX);
                    break;
                case 1:
                    this.ExtrasistoleRank = 5;
                    this.SmallExtrasystoleDiscription = Resource.MinorDeviations;
                    this.ColorExtrasystole = Color.FromHex(GoodStateHEX);
                    break;
                case 2:
                    this.ColorExtrasystole = Color.FromHex(So_SoStateHEX);
                    this.SmallExtrasystoleDiscription = Resource.AttentionMessage;
                    this.ExtrasistoleRank = 4;
                    this.ExtrasystoleDiscription += Environment.NewLine;
                    this.ExtrasystoleDiscription += Environment.NewLine;
                    this.ExtrasystoleDiscription += "Предсердных: " + e.AtrialPrematureBeatsCount.ToString();
                    this.ExtrasystoleDiscription += Environment.NewLine;
                    this.ExtrasystoleDiscription += "Желудочковых: " + e.VentricularExtrasystoleCount.ToString();
                    break;
            }
        }


        #endregion

        //ST - сегмент
        #region 
        private string stDiscription = Resource.NoDeviations;
        public string STDiscription
        {
            get { return stDiscription; }
            set
            {
                if (stDiscription != value)
                {
                    stDiscription = value;
                    OnPropertyChanged("STDiscription");
                }
            }
        }
        private Color colorST = Color.FromHex(GoodStateHEX);
        public Color ColorST
        {
            get { return colorST; }
            set
            {
                if (colorST != value)
                {
                    colorST = value;
                    OnPropertyChanged("ColorST");
                }
            }
        }
        private int widhtSTFrame = 100;
        public int WidhtSTFrame
        {
            get { return widhtSTFrame; }
            set
            {
                if (widhtSTFrame != value)
                {
                    widhtSTFrame = value;
                    OnPropertyChanged("WidhtSTFrame");
                }
            }
        }
        private int heightSTFrame = 100;
        public int HeightSTFrame
        {
            get { return heightSTFrame; }
            set
            {
                if (heightSTFrame != value)
                {
                    heightSTFrame = value;
                    OnPropertyChanged("HeightSTFrame");
                }
            }
        }
        #endregion

        //Блокады
        #region 
        private byte BlockadeRank = 5;
        private string smallBlockadesDiscription = Resource.NoDeviations;
        public string SmallBlockadesDiscription
        {
            get { return smallBlockadesDiscription; }
            set
            {
                if (smallBlockadesDiscription != value)
                {
                    smallBlockadesDiscription = value;
                    OnPropertyChanged("SmallBlockadesDiscription");
                }
            }
        }
        private string blockadesDiscription = Resource.NoDeviations;
        public string BlockadesDiscription
        {
            get { return blockadesDiscription; }
            set
            {
                if (blockadesDiscription != value)
                {
                    blockadesDiscription = value;
                    OnPropertyChanged("BlockadesDiscription");
                }
            }
        }
        private Color colorBlockades = Color.FromHex(GoodStateHEX);
        public Color ColorBlockades
        {
            get { return colorBlockades; }
            set
            {
                if (colorBlockades != value)
                {
                    colorBlockades = value;
                    OnPropertyChanged("ColorBlockades");
                }
            }
        }
        private int widhtBlockadesFrame = 100;
        public int WidhtBlockadesFrame
        {
            get { return widhtBlockadesFrame; }
            set
            {
                if (widhtBlockadesFrame != value)
                {
                    widhtBlockadesFrame = value;
                    OnPropertyChanged("WidhtBlockadesFrame");
                }
            }
        }
        private int heightBlockadesFrame = 100;
        public int HeightBlockadesFrame
        {
            get { return heightBlockadesFrame; }
            set
            {
                if (heightBlockadesFrame != value)
                {
                    heightBlockadesFrame = value;
                    OnPropertyChanged("HeightBlockadesFrame");
                }
            }
        }
        #endregion

        //ИМТ
        #region 
        private string bmiTitle = "Обработка...";
        public string BMITitle
        {
            get { return bmiTitle; }
            set
            {
                if (bmiTitle != value)
                {
                    bmiTitle = value;
                    OnPropertyChanged("BMITitle");
                }
            }
        }
        private string bmiDiscription = "";
        public string BMIDiscription
        {
            get { return bmiDiscription; }
            set
            {
                if (bmiDiscription != value)
                {
                    bmiDiscription = value;
                    OnPropertyChanged("BMIDiscription");
                }
            }
        }
        private Color colorBMI = Color.FromHex(GoodStateHEX);
        public Color ColorBMI
        {
            get { return colorBMI; }
            set
            {
                if (colorBMI != value)
                {
                    colorBMI = value;
                    OnPropertyChanged("ColorBMI");
                }
            }
        }
        private int widhtBMIFrame = 100;
        public int WidhtBMIFrame
        {
            get { return widhtBMIFrame; }
            set
            {
                if (widhtBMIFrame != value)
                {
                    widhtBMIFrame = value;
                    OnPropertyChanged("WidhtBMIFrame");
                }
            }
        }
        private int heightBMIFrame = 100;
        public int HeightBMIFrame
        {
            get { return heightBMIFrame; }
            set
            {
                if (heightBMIFrame != value)
                {
                    heightBMIFrame = value;
                    OnPropertyChanged("HeightBMIFrame");
                }
            }
        }

        void BodyMassIndexChange(object sender, BodyMassIndexEventArgs e)
        {
            this.ColorBMI = e.BodyMassIndexColor;
            this.BMITitle = e.BodyMassIndexTitleDiscription;
            if (e.BodyMassIndexRating == 2)
                this.BMIDiscription = "Отлично. У вас наблюдается " + e.BodyMassIndexDiscription;
            else
                this.BMIDiscription = "Возможные риски и патологии: " + e.BodyMassIndexDiscription;
        }

        #endregion

        //QRS
        #region 
        private string qrsDiscription = Resource.NoDeviations;
        public string QRSDiscription
        {
            get { return qrsDiscription; }
            set
            {
                if (qrsDiscription != value)
                {
                    qrsDiscription = value;
                    OnPropertyChanged("QRSDiscription");
                }
            }
        }
        private Color colorQRS = Color.FromHex(GoodStateHEX);
        public Color ColorQRS
        {
            get { return colorQRS; }
            set
            {
                if (colorQRS != value)
                {
                    colorQRS = value;
                    OnPropertyChanged("ColorQRS");
                }
            }
        }
        private int widhtQRSFrame = 100;
        public int WidhtQRSFrame
        {
            get { return widhtQRSFrame; }
            set
            {
                if (widhtQRSFrame != value)
                {
                    widhtQRSFrame = value;
                    OnPropertyChanged("WidhtQRSFrame");
                }
            }
        }
        private int heightQRSFrame = 100;
        public int HeightQRSFrame
        {
            get { return heightQRSFrame; }
            set
            {
                if (heightQRSFrame != value)
                {
                    heightQRSFrame = value;
                    OnPropertyChanged("HeightQRSFrame");
                }
            }
        }
        #endregion

        //Паузы
        #region 
        private byte PauseRank = 5;
        private string pauseDiscription = Resource.NoDeviations;
        public string PauseDiscription
        {
            get { return pauseDiscription; }
            set
            {
                if (pauseDiscription != value)
                {
                    pauseDiscription = value;
                    OnPropertyChanged("PauseDiscription");
                }
            }
        }
        private string smallPauseDiscription = Resource.NoDeviations;
        public string SmallPauseDiscription
        {
            get { return smallPauseDiscription; }
            set
            {
                if (smallPauseDiscription != value)
                {
                    smallPauseDiscription = value;
                    OnPropertyChanged("SmallPauseDiscription");
                }
            }
        }
        private Color colorPause = Color.FromHex(GoodStateHEX);
        public Color ColorPause
        {
            get { return colorPause; }
            set
            {
                if (colorPause != value)
                {
                    colorPause = value;
                    OnPropertyChanged("ColorPause");
                }
            }
        }
        private int widhtPauseFrame = 100;
        public int WidhtPauseFrame
        {
            get { return widhtPauseFrame; }
            set
            {
                if (widhtPauseFrame != value)
                {
                    widhtPauseFrame = value;
                    OnPropertyChanged("WidhtPauseFrame");
                }
            }
        }
        private int heightPauseFrame = 100;
        public int HeightPauseFrame
        {
            get { return heightPauseFrame; }
            set
            {
                if (heightPauseFrame != value)
                {
                    heightPauseFrame = value;
                    OnPropertyChanged("HeightPauseFrame");
                }
            }
        }
        #endregion

        private byte heightFramePart = 4;

        public void UdateFramesSize(double width, double height)
        {
            this.WidhtArethmiaFrame = 7 * (int)(width / 11);
            this.HeightArethmiaFrame = (int)(height / heightFramePart);
            this.WidhtPulseFrame = 4 * (int)(width / 11);
            this.HeightPulseFrame = (int)(height / heightFramePart);

            this.WidhtRithmFrame = (int)(3.5 * width / 11);
            this.HeightRithmFrame = (int)(height / heightFramePart);
            this.WidhtExtrasystoleFrame = (int)(7.5 * width / 11);
            this.HeightExtrasystoleFrame = (int)(height / heightFramePart);

            this.WidhtSTFrame = 6 * (int)(width / 11);
            this.HeightSTFrame = (int)(height / heightFramePart);

            this.WidhtBlockadesFrame = 6 * (int)(width / 11);
            this.HeightBlockadesFrame = (int)(height / heightFramePart);

            this.WidhtBMIFrame = 4 * (int)(width / 11);
            this.HeightBMIFrame = (int)(height / heightFramePart);
            this.WidhtQRSFrame = 7 * (int)(width / 11);
            this.HeightQRSFrame = (int)(height / heightFramePart);

            this.WidhtPauseFrame = 5 * (int)(width / 11);
            this.HeightPauseFrame = (int)(height / heightFramePart);
        }



        //Команды
        public ICommand ArrethmiaInfo => new Command(async () => {
            var arrethmiaDisplayAlert = new InformationDisplayAlert(DiscriptArethmia, "Аритмии", ArethmiaRank);
            await PopupNavigation.Instance.PushAsync(arrethmiaDisplayAlert);
        });
        public ICommand PulseInfo => new Command(async () => {
            string pulseInfoString = "Идёт сбор данных";
            pulseInfoString = this.PulseDiscription;
            var pulseDisplayAlert = new InformationDisplayAlert(pulseInfoString, "Пульс: " + this.PulseTitle, PulseRank);

            await PopupNavigation.Instance.PushAsync(pulseDisplayAlert);
        });
        public ICommand RithmInfo => new Command(async () => {
            var arrethmiaDisplayAlert = new InformationDisplayAlert(RithmDiscription, "Ведущий ритм", RithmRank);
            await PopupNavigation.Instance.PushAsync(arrethmiaDisplayAlert);
        });
        public ICommand ExtrasystoleInfo => new Command(async () => {
            var extrasystoleDisplayAlert = new InformationDisplayAlert(ExtrasystoleDiscription, "Экстрасистолия", ExtrasistoleRank);
            await PopupNavigation.Instance.PushAsync(extrasystoleDisplayAlert);
        });
        public ICommand STInfo => new Command(async () => {

        });
        public ICommand BlockadesInfo => new Command(async () => {
            var pauseDisplayAlert = new InformationDisplayAlert(BlockadesDiscription, "Блокады", BlockadeRank);
            await PopupNavigation.Instance.PushAsync(pauseDisplayAlert);
        });
        public ICommand BMIInfo => new Command(async () => {
            var bmiDisplayAlert = new InformationDisplayAlert(BMIDiscription, BMITitle);
            await PopupNavigation.Instance.PushAsync(bmiDisplayAlert);
        });
        public ICommand QRSInfo => new Command(async () => {

        });
        public ICommand PauseInfo => new Command(async () => {
            var pauseDisplayAlert = new InformationDisplayAlert(PauseDiscription, "Паузы", PauseRank);
            await PopupNavigation.Instance.PushAsync(pauseDisplayAlert);
        });


        public DisplayOrientation DisplayOrientation
        {
            get; private set;
        } = DisplayOrientation.VerticalOrientation;

        public void SetTrendsDisplayOrientation(double width, double height)
        {
            if (width < height)
                this.DisplayOrientation = DisplayOrientation.VerticalOrientation;
            else
                this.DisplayOrientation = DisplayOrientation.HorizontalOrientation;

            if (this.TrendsViewController != null)
                this.TrendsViewController.SetTrendsDisplayOrientation(this.DisplayOrientation);
        }

        public void SetScaterogrammDisplayOrientation(double width, double height)
        {
            if (width < height)
                this.DisplayOrientation = DisplayOrientation.VerticalOrientation;
            else
                this.DisplayOrientation = DisplayOrientation.HorizontalOrientation;

            if (this.TrendsViewController != null)
                this.TrendsViewController.SetScaterogrammDisplayOrientation(this.DisplayOrientation);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public async void Dispose()
        {
            //Запуск окна ожидания завершения работы
            CustomLoadPage customLoadPage = new CustomLoadPage("Завершение исследования", "Пожалуйста подождите");
            await PopupNavigation.Instance.PushAsync(customLoadPage);
            if (this.IsProcessValueInit)
            {
                //Освобождение модели диагностики
                if (this.ConclusionModel != null)
                {
                    this.ConclusionModel.PulseAnalyzer.DiagnosisUpdate -= this.PulseUpdate;
                    this.ConclusionModel.RithmConclusionReadyEvent -= this.RithmStyleChange;
                    this.ConclusionModel.BodyMassIndexChange -= this.BodyMassIndexChange;
                    this.ConclusionModel.HeartRatingCompiler.HeartrateRatingUpdateEvent -= HeartRatingChange;
                    this.ConclusionModel.SystemTensionIndexController.TensionIndexChangeEvent -= TensionIndexChange;
                    this.ConclusionModel.AdaptationLevel.AdaptationLevelChangeEvent -= AdaptationLevelChange;
                    this.ConclusionModel.VegetativeBalanceController.VegetativeBalanceIndexChangeEvent -= VegetativeBalanceIndexChange;
                    this.ConclusionModel.ExtrasystoleConclusionReadyEvent -= ExtrasistoleReaction;
                    this.ConclusionModel.PauseConclusionReadyEvent -= PauseReaction;
                    this.ConclusionModel.BlockadesConclusionReadyEvent -= BlockadesReaction;
                    this.ConclusionModel.ArrethmiaConclusionReadyEvent -= ArrethmiaReaction;
                    this.ConclusionModel.Dispose();
                    this.ConclusionModel = null;
                }
                //Соблюдение последовательности обязательно
                //Освобождение серверной части
                this.DataSender.Dispose();
                this.DataSender = null;
                //Освобождение части формирование данных
                this.DataHandler.Dispose();
                this.DataHandler = null;
                //Освобождение части конвертации данных
                this.DataConverter.Dispose();
                this.DataConverter = null;
                //Освобождение части получения пакетов от кардиографа
                this.PackageSequenceControler.Dispose();
                this.PackageSequenceControler = null;
            }
            this.FireFly.BluetoothConnectController.ChangeBluetoothStateEvent -= BluetoothStateChangeConnect;
            this.FireFly.BatteryHandler.BattaryStateChangeEvent -= NewBattaryState;
            this.ServerWorkerInstance.ChangeConnectionState -= ConnectionStateChange;
            this.FireFly.FireFlyInfoUpdate -= InfoCame;
            this.FireFly = null;
            this.Database = null;
            this.ServerWorkerInstance = null;
            this.FireFlyDirInfo = null;
            this.SettingsModel.IsECGProcessingRun = false;
            await PopupNavigation.Instance.RemovePageAsync(customLoadPage);
        }
    }


}
