﻿using FenixMobile.Interfaces;
using FenixMobile.Model.BluetoothPart;
using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using FenixMobile.Model.DatabasePart;
using FenixMobile.Model.ServerPart;
using FenixMobile.View.CustomDisplayAlert;
using FenixMobile.View.HeadMasterMenu;
using FenixMobile.View.HeadMasterMenu.ECGViewerPage;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace FenixMobile.ViewModel
{
    public class ModeSelectViewModel : INotifyPropertyChanged
    {
        //ServerState icon
        private readonly string BadConnectIcon = "ConnectBadState.png";
        private readonly string GoodConnectIcon = "ConnectGootState.png";
        //BluetoothState icon
        private readonly string FireFlyNotFoundIcon = "NoFireFly.png";
        private readonly string FireFlyFullCharageIcon = "FullFireFly.png";
        private readonly string FireFlyNotFullCharageIcon = "NotFullFireFly.png";
        private readonly string FireFlyMiddleCharageIcon = "MiddleFireFly.png";
        private readonly string FireFlyLowCharageIcon = "LowFireFly.png";

        ServerWorker ServerWorkerInstance;
        public INavigation Navigation { get; set; }
        FireFly FireFly;
        DatabaseWorker DatabaseWorker;

        public ModeSelectViewModel()
        {
            FireFly = FireFly.getInstance();
            ServerWorkerInstance = ServerWorker.getInstance();
            ServerStateIcon = ServerWorkerInstance.IsInternetConnection ? "ConnectGootState.png" : "ConnectBadState.png";
            ServerWorkerInstance.ChangeConnectionState += ConnectionStateChange;
            DatabaseWorker = DatabaseWorker.getInstance();
            FireFly.BluetoothConnectController.ChangeBluetoothStateEvent += BluetoothStateChangeConnect;
            FireFly.BatteryHandler.BattaryStateChangeEvent += NewBattaryState;
            this.BluetoothDeviceState = FireFly.BatteryHandler.ChargePercent > 0 ? FireFly.BatteryHandler.ChargePercent.ToString() : "--";
            this.BluetoothDeviceStateIcon = GetBatteryIcon(FireFly.BatteryHandler.ChargePercent);
        }
        string GetBatteryIcon(int chargeLevel)
        {
            if (chargeLevel > 90) return this.FireFlyFullCharageIcon;
            if (chargeLevel <= 90 && chargeLevel > 60) return this.FireFlyNotFullCharageIcon;
            if (chargeLevel <= 60 && chargeLevel > 40) return this.FireFlyMiddleCharageIcon;
            if (chargeLevel <= 40 && chargeLevel > 0) return this.FireFlyLowCharageIcon;
            return this.FireFlyNotFoundIcon;
        }

        object lockerCharageLevel = new object();
        void NewBattaryState(object sender, BatteryEventArgs e)
        {
            ushort chargeLevel = e.ChargeLevel;
            lock (lockerCharageLevel)
            {
                try
                {
                    this.BluetoothDeviceState = chargeLevel.ToString();
                    if (chargeLevel > 90) BluetoothDeviceStateIcon = this.FireFlyFullCharageIcon;
                    if (chargeLevel <= 90 && chargeLevel > 60) BluetoothDeviceStateIcon = this.FireFlyNotFullCharageIcon;
                    if (chargeLevel <= 60 && chargeLevel > 40) BluetoothDeviceStateIcon = this.FireFlyMiddleCharageIcon;
                    if (chargeLevel <= 40 && chargeLevel >= 0) BluetoothDeviceStateIcon = this.FireFlyLowCharageIcon;
                }
                catch (Exception ex)
                {
                    string exm = ex.Message;
                }
            }
        }

        object locker = new object();
        void BluetoothStateChangeConnect(object sender, EventArgs e)
        {
            BluetoothConnectServicer bluetoothConnectServicer = sender as BluetoothConnectServicer;
            if (!(bluetoothConnectServicer.IsBluetoothConnect))
            {
                lock (locker)
                {
                    this.BluetoothDeviceStateIcon = this.FireFlyNotFoundIcon;
                    this.BluetoothDeviceState = "--";
                }
            }
        }

        string bluetoothDeviceStateIcon = "NoFireFly.png";
        public string BluetoothDeviceStateIcon
        {
            get { return bluetoothDeviceStateIcon; }
            set
            {
                if (bluetoothDeviceStateIcon != value)
                {
                    bluetoothDeviceStateIcon = value;
                    OnPropertyChanged("BluetoothDeviceStateIcon");
                }
            }
        }

        string bluetoothDeviceState = "--";
        public string BluetoothDeviceState
        {
            get { return bluetoothDeviceState + "%"; }
            set
            {
                if (bluetoothDeviceState != value)
                {
                    bluetoothDeviceState = value;
                    OnPropertyChanged("BluetoothDeviceState");
                }
            }
        }

        void ConnectionStateChange(object sender, EventArgs e)
        {
            bool isConnected = (sender as ServerWorker).IsInternetConnection;
            if (isConnected)
            {
                this.ServerStateIcon = GoodConnectIcon;
            }
            else
                this.ServerStateIcon = BadConnectIcon;
        }

        string serverStateIcon = "ConnectBadState.png";
        public string ServerStateIcon
        {
            get { return serverStateIcon; }
            set
            {
                if (serverStateIcon != value)
                {
                    serverStateIcon = value;
                    OnPropertyChanged("ServerStateIcon");
                }
            }
        }

        public ICommand RunTenSecondsMode => new Command(async() => {
            if (await this.CheckCardiographStatus())
                await Navigation.PushAsync(new SimpleECGView(ECGreceiveMode.ShortECG));
        });
        public ICommand RunFiveMinutesMode => new Command(async () => {
            if (await this.CheckCardiographStatus())
                await Navigation.PushAsync(new SimpleECGView(ECGreceiveMode.MidECG));
        });
        public ICommand RunHolterMode => new Command(async () => {
            if (await this.CheckCardiographStatus())
                await Navigation.PushAsync(new MonitoringTabbedPage(Enumeration.DataTransferMode.Current, null));
        });

        private async Task<bool> CheckCardiographStatus()
        {
            if (!(FireFly.BluetoothConnectController.IsBluetoothConnect))
            {
                await PopupNavigation.Instance.PushAsync(new ErrorDisplayAlert("Кардиограф не активен! Возможно, устройство отключено."));
                return false;
            }
            if (this.DatabaseWorker.CurrentPatient == null)
            {
                await PopupNavigation.Instance.PushAsync(new ErrorDisplayAlert("Не выбран текущий пациент!"));
                return false;
            }
            if (FireFly.GetFireFlyDirInfo() == null)
            {
                await PopupNavigation.Instance.PushAsync(new ErrorDisplayAlert("Кардиограф не отвечает!"));
                return false;
            }
            if (!FireFly.GetFireFlyDirInfo().IsSD_Memory)
            {
                await PopupNavigation.Instance.PushAsync(new ErrorDisplayAlert("Ошибка! Устройство не укомплектовано долгосрочной памятью!"));
                return false;
            }
            return true;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
