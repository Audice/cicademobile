﻿using FenixMobile.Interfaces;
using FenixMobile.Model.BluetoothPart;
using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using FenixMobile.Model.BluetoothPart.DataConvert;
using FenixMobile.Model.BluetoothPart.PackageControl;
using FenixMobile.Model.DatabasePart;
using FenixMobile.Model.FilePart;
using FenixMobile.Model.ServerPart;
using FenixMobile.Model.SettingsPart;
using FenixMobile.View.CustomDisplayAlert;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FenixMobile.ViewModel
{
    public class ProcessingOldECGViewModel : INotifyPropertyChanged, IDisposable
    {
        //ServerState icon
        private readonly string BadConnectIcon = "ConnectBadState.png";
        private readonly string GoodConnectIcon = "ConnectGootState.png";
        //BluetoothState icon
        private readonly string FireFlyNotFoundIcon = "NoFireFly.png";
        private readonly string FireFlyFullCharageIcon = "FullFireFly.png";
        private readonly string FireFlyNotFullCharageIcon = "NotFullFireFly.png";
        private readonly string FireFlyMiddleCharageIcon = "MiddleFireFly.png";
        private readonly string FireFlyLowCharageIcon = "LowFireFly.png";

        public INavigation Navigation { get; set; }

        ECGreceiveMode ECGreceiveMode = ECGreceiveMode.HolterECG;

        FireFly FireFly;
        DatabaseWorker Database;
        OldPackageSequenceControler PackageSequenceControler = null;
        DataConverter DataConverter;
        ServerWorker ServerWorkerInstance;
        FileWorker FileWorker;
        SOAPPackageSender SOAPPackageSender;

        //Флаги контроля
        private RecordTable OldRecord = null;

        private bool IsProcessLoadOldRun = false;
        public bool IsProcessLoadAborted { get; set; } = false;

        SettingsModel SettingsModel;
        public ProcessingOldECGViewModel(RecordTable oldRecord)
        {
            SettingsModel = SettingsModel.getSettingsInstance();
            SettingsModel.IsECGProcessingRun = true;

            this.OldRecord = oldRecord;
            FireFly = FireFly.getInstance();
            FireFly.BluetoothConnectController.ChangeBluetoothStateEvent += BluetoothStateChangeConnect;
            FireFly.BatteryHandler.BattaryStateChangeEvent += NewBattaryState;
            FireFly.FireFlyInfoUpdate += FireFlyInfoUpdate;
            this.BluetoothDeviceState = FireFly.CharageLevel > 0 ? FireFly.CharageLevel.ToString() : "--";
            this.BluetoothDeviceStateIcon = GetBatteryIcon(FireFly.CharageLevel);

            Database = DatabaseWorker.getInstance();
            ServerWorkerInstance = ServerWorker.getInstance();
            ServerStateIcon = ServerWorkerInstance.IsInternetConnection ? "ConnectGootState.png" : "ConnectBadState.png";
            ServerWorkerInstance.ChangeConnectionState += ConnectionStateChange;


            if (this.OldRecord != null && this.OldRecord.CatalogNumber >= 0 && this.OldRecord.LastBluetoothFileNumber >= 0)
            {
                //Команда на смену директории. Если директория сменилась, приходит информация с Dir info
                FireFly.BluetoothConnectController.SetDirectory((short)OldRecord.CatalogNumber);
            }

            DependencyService.Get<ISignalsPlay>().PlayBadPulse();
        }

        void FireFlyInfoUpdate(object sender, FireFlyDirInfoEventArgs e)
        {
            FireFly.FireFlyInfoUpdate -= FireFlyInfoUpdate;
            //Как только пришла информация о директории выполняем подготовку к считыванию
            FireFlyDirInfo dirInfo = e.FireFlyDirInfo;
            if (dirInfo != null && dirInfo.IsSD_Memory && dirInfo.CurrentDirectory == this.OldRecord.CatalogNumber)
            {
                if (dirInfo.CurrentDirectory == this.OldRecord.CatalogNumber)
                    this.PackageSequenceControler = new OldPackageSequenceControler(null, FireFly, (uint)(this.OldRecord.LastBluetoothFileNumber), true);
                else
                    this.PackageSequenceControler = new OldPackageSequenceControler(null, FireFly, (uint)(this.OldRecord.LastBluetoothFileNumber), false);

                //Инициализация компонент системы для работы с файлами с SD

                this.PackageSequenceControler.ProcessStopEvent += StopProcessReadingOldFiles;
                DataConverter = new DataConverter(this.PackageSequenceControler, (uint)this.OldRecord.ChannelsCount); //Не знаю как поправить
                FileWorker = new FileWorker(DataConverter, this.OldRecord);
                this.SOAPPackageSender = new SOAPPackageSender(DataConverter, FileWorker, this.OldRecord, SOAPPackageSender.SendMode.OfflineMode);
                this.IsProcessLoadOldRun = true;
                this.PackageSequenceControler.RunSDsLoadProces();
            }
            else
            {
                //Ошибочка... Каталоги не совпадают или отсутствует флэшка
                var errorDisplayAlert = new ErrorDisplayAlert("Считывание старых файлов завершено");
                PopupNavigation.Instance.PushAsync(errorDisplayAlert);
            }
        }


        async void StopProcessReadingOldFiles(object sender, EventArgs e)
        {
            this.PackageSequenceControler.ProcessStopEvent -= StopProcessReadingOldFiles;
            this.IsProcessLoadOldRun = false;
            var informationDisplayAlert = new InformationDisplayAlert("Считывание старых файлов завершено");
            await PopupNavigation.Instance.PushAsync(informationDisplayAlert);
        }

        void NewBattaryState(object sender, BatteryEventArgs e)
        {
            ushort chargeLevel = e.ChargeLevel;

            lock (locker)
            {
                try
                {
                    this.BluetoothDeviceState = chargeLevel.ToString();
                    this.BluetoothDeviceStateIcon = GetBatteryIcon(chargeLevel);
                }
                catch (Exception ex)
                {
                    string exm = ex.Message;
                }
            }
        }

        string GetBatteryIcon(int chargeLevel)
        {
            if (chargeLevel > 90) return this.FireFlyFullCharageIcon;
            if (chargeLevel <= 90 && chargeLevel > 60) return this.FireFlyNotFullCharageIcon;
            if (chargeLevel <= 60 && chargeLevel > 40) return this.FireFlyMiddleCharageIcon;
            if (chargeLevel <= 40 && chargeLevel >= 0) return this.FireFlyLowCharageIcon;
            return this.FireFlyNotFoundIcon;
        }

        object locker = new object();
        void BluetoothStateChangeConnect(object sender, EventArgs e)
        {
            BluetoothConnectServicer bluetoothConnectServicer = sender as BluetoothConnectServicer;
            if (!(bluetoothConnectServicer.IsBluetoothConnect))
            {
                lock (locker)
                {
                    this.BluetoothDeviceStateIcon = this.FireFlyNotFoundIcon;
                    this.BluetoothDeviceState = "--";
                }
            }
        }

        string bluetoothDeviceStateIcon = "NoFireFly.png";
        public string BluetoothDeviceStateIcon
        {
            get { return bluetoothDeviceStateIcon; }
            set
            {
                if (bluetoothDeviceStateIcon != value)
                {
                    bluetoothDeviceStateIcon = value;
                    OnPropertyChanged("BluetoothDeviceStateIcon");
                }
            }
        }

        string bluetoothDeviceState = "--";
        public string BluetoothDeviceState
        {
            get { return bluetoothDeviceState + "%"; }
            set
            {
                if (bluetoothDeviceState != value)
                {
                    bluetoothDeviceState = value;
                    OnPropertyChanged("BluetoothDeviceState");
                }
            }
        }

        void ConnectionStateChange(object sender, EventArgs e)
        {
            bool isConnected = (sender as ServerWorker).IsInternetConnection;
            if (isConnected)
            {
                this.ServerStateIcon = GoodConnectIcon;
            }
            else
                this.ServerStateIcon = BadConnectIcon;
        }

        string serverStateIcon = "ConnectBadState.png";
        public string ServerStateIcon
        {
            get { return serverStateIcon; }
            set
            {
                if (serverStateIcon != value)
                {
                    serverStateIcon = value;
                    OnPropertyChanged("ServerStateIcon");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public async void Dispose()
        {
            CustomLoadPage customLoadPage = new CustomLoadPage("Завершение исследования", "Пожалуйста подождите");
            await PopupNavigation.Instance.PushAsync(customLoadPage);

            if (this.IsProcessLoadOldRun)
            {
                //Если процесс считывания не закончился, и выход происходит по кнопке "назад", то нужно закрыть данный файл...
                this.PackageSequenceControler.ProcessStopEvent -= StopProcessReadingOldFiles;
                this.PackageSequenceControler.AbortFileTransfer();
                this.FileWorker.CloseRecord();
                //Файл на сервере по логике тоже лучше закрыть
                this.SOAPPackageSender.IsAllProcessStop = true;
            }

            
            if (this.SOAPPackageSender != null)
            {
                bool result = await this.SOAPPackageSender.CloseServerFile();
                this.SOAPPackageSender.Dispose();
                this.SOAPPackageSender = null;
            }

            if (this.DataConverter != null)
            {
                this.DataConverter.Dispose();
                this.DataConverter = null;
            }
            if (this.FileWorker != null)
            {
                this.FileWorker.Dispose();
                this.FileWorker = null;
            }
            

            if (this.PackageSequenceControler != null)
            {
                this.PackageSequenceControler.Dispose();
                this.PackageSequenceControler = null;
            }

            FireFly.BluetoothConnectController.ChangeBluetoothStateEvent -= BluetoothStateChangeConnect;
            FireFly.BatteryHandler.BattaryStateChangeEvent -= NewBattaryState;
            ServerWorkerInstance.ChangeConnectionState -= ConnectionStateChange;
            SettingsModel.IsECGProcessingRun = false;
            await PopupNavigation.Instance.RemovePageAsync(customLoadPage);
        }
    }
}
