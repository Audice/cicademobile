﻿using FenixMobile.Model;
using FenixMobile.Model.DatabasePart;
using FenixMobile.Model.ServerPart;
using FenixMobile.View.CustomDisplayAlert;
using Rg.Plugins.Popup.Services;
using SoapConstruct.SoapFunction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace FenixMobile.ViewModel
{
    class RegistrationViewModel : INotifyPropertyChanged
    {
        public INavigation Navigation { get; set; }

        RegistrationModel registrationModel;
        DatabaseWorker DatabaseWorkerInstance;

        public ICommand RegistrationCommand => new Command(RunRegistration);

        public RegistrationViewModel()
        {
            registrationModel = new RegistrationModel();
            DatabaseWorkerInstance = DatabaseWorker.getInstance();
            registrationModel.UserEmail = this.DatabaseWorkerInstance.CurrentUser.Login;
            registrationModel.UserPassword = this.DatabaseWorkerInstance.CurrentUser.Password;
        }

        public string Name
        {
            get { return registrationModel.UserName; }
            set
            {
                if (registrationModel.UserName != value)
                {
                    registrationModel.UserName = value;
                    OnPropertyChanged("Name");
                }
            }
        }
        public string Surname
        {
            get { return registrationModel.UserSurname; }
            set
            {
                if (registrationModel.UserSurname != value)
                {
                    registrationModel.UserSurname = value;
                    OnPropertyChanged("Surname");
                }
            }
        }
        public string Patronomyc
        {
            get { return registrationModel.UserPatronomyc; }
            set
            {
                if (registrationModel.UserPatronomyc != value)
                {
                    registrationModel.UserPatronomyc = value;
                    OnPropertyChanged("Surname");
                }
            }
        }

        public string EMail
        {
            get { return registrationModel.UserEmail; }
            set
            {
                if (registrationModel.UserEmail != value)
                {
                    registrationModel.UserEmail = value;
                    OnPropertyChanged("EMail");
                }
            }
        }
        public string Password
        {
            get { return registrationModel.UserPassword; }
            set
            {
                if (registrationModel.UserPassword != value)
                {
                    registrationModel.UserPassword = value;
                    OnPropertyChanged("Password");
                }
            }
        }
        public string Growth
        {
            get { return registrationModel.UserGrowth; }
            set
            {
                if (registrationModel.UserGrowth != value)
                {
                    registrationModel.UserGrowth = value;
                    OnPropertyChanged("Growth");
                }
            }
        }
        public string Weight
        {
            get { return registrationModel.UserWeight; }
            set
            {
                if (registrationModel.UserWeight != value)
                {
                    registrationModel.UserWeight = value;
                    OnPropertyChanged("Weight");
                }
            }
        }
        public string Gender
        {
            get { return registrationModel.UserGender; }
            set
            {
                if (registrationModel.UserGender != value)
                {
                    registrationModel.UserGender = value;
                    OnPropertyChanged("Gender");
                }
            }
        }
        public DateTime BornDate
        {
            get { return registrationModel.UserBornDate; }
            set
            {
                if (registrationModel.UserBornDate != value)
                {
                    registrationModel.UserBornDate = value;
                    OnPropertyChanged("BornDate");
                }
            }
        }

        async void RunRegistration()
        {
            var errors = registrationModel.ValidationFilledField();
            if (errors.Count == 0)
            {
                //Запуск процесса регистрации
                //1. Получение экземпляра сервера
                ServerWorker serverWorker = ServerWorker.getInstance();
                //2. Подключение к серверу с текущеми логином и паролем
                string resultConnect = await serverWorker.Authorization(new CreateSession(registrationModel.UserEmail, registrationModel.UserPassword));
                if (resultConnect == null)
                {
                    //Проблема
                }
                //3. отправка запроса на регистрацию нового пользователя
                long patientID = await serverWorker.CreatePatient(new CreatePatient(resultConnect, this.Name + " " + this.Surname + " " + this.Patronomyc,
                    this.Gender == "Мужской" ? 1 : 0, this.BornDate, this.Growth, this.Weight));
                //4. В случае успеха - запись нового пациента в базу данных
                if (patientID >= 0 && this.DatabaseWorkerInstance.CurrentUser != null)
                {
                    if (!(this.DatabaseWorkerInstance.AddnewPatientByUserID(this.DatabaseWorkerInstance.CurrentUser.ID, patientID, this.Name, this.Surname, this.Patronomyc,
                        this.BornDate, this.Gender == "Мужской" ? 1 : 0, int.Parse(this.Growth), int.Parse(this.Weight))))
                    {
                        await PopupNavigation.Instance.PushAsync(new ErrorDisplayAlert("Проблема с регистрацией в бд. В регистрации отказано!"));
                    }
                    else
                    {
                        this.DatabaseWorkerInstance.OnPatientsDatabaseChange();
                    }
                }
                //5. Переход к интерфейсу пользования
                await Navigation.PopAsync();
            }
            else
            {
                string errorString = "В ходе регистрации были допущены следующие ошибки:" + Environment.NewLine;
                for (int i=0; i < errors.Count; i++)
                {
                    errorString += errors[i] + ";" + Environment.NewLine;
                }
                ErrorDisplayAlert errorDisplayAlert = new ErrorDisplayAlert(errorString);
                await PopupNavigation.Instance.PushAsync(errorDisplayAlert);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
