﻿using FenixMobile.Interfaces;
using FenixMobile.Model.BluetoothPart;
using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using FenixMobile.Model.DatabasePart;
using FenixMobile.Model.ServerPart;
using FenixMobile.View.HeadMasterMenu.ECGViewerPage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace FenixMobile.ViewModel
{
    public class SavedFileViewModel : INotifyPropertyChanged, IDisposable
    {

        public INavigation Navigation { get; set; }

        //ServerState icon
        private readonly string BadConnectIcon = "ConnectBadState.png";
        private readonly string GoodConnectIcon = "ConnectGootState.png";
        //BluetoothState icon
        private readonly string FireFlyNotFoundIcon = "NoFireFly.png";
        private readonly string FireFlyFullCharageIcon = "FullFireFly.png";
        private readonly string FireFlyNotFullCharageIcon = "NotFullFireFly.png";
        private readonly string FireFlyMiddleCharageIcon = "MiddleFireFly.png";
        private readonly string FireFlyLowCharageIcon = "LowFireFly.png";

        FireFly FireFly;
        DatabaseWorker DatabaseWorker;
        ServerWorker ServerWorkerInstance;



        public SavedFileViewModel()
        {
            DatabaseWorker = DatabaseWorker.getInstance();
            

            ServerWorkerInstance = ServerWorker.getInstance();
            ServerStateIcon = ServerWorkerInstance.IsInternetConnection ? "ConnectGootState.png" : "ConnectBadState.png";
            ServerWorkerInstance.ChangeConnectionState += ConnectionStateChange;

            FireFly = FireFly.getInstance();
            FireFly.BluetoothConnectController.ChangeBluetoothStateEvent += BluetoothStateChangeConnect;
            FireFly.BatteryHandler.BattaryStateChangeEvent += NewBattaryState;
            this.BluetoothDeviceState = FireFly.CharageLevel > 0 ? FireFly.CharageLevel.ToString() : "--";
            this.BluetoothDeviceStateIcon = GetBatteryIcon(FireFly.CharageLevel);

            //Инициализация списка          
            DatabaseWorker.CurrentPatientChange += CurrentPatientChange;
            DatabaseWorker.RecordListChange += CurrentPatientChange;
        }

        void NewBattaryState(object sender, BatteryEventArgs e)
        {
            ushort chargeLevel = e.ChargeLevel;

            lock (locker)
            {
                try
                {
                    this.BluetoothDeviceState = chargeLevel.ToString();
                    this.BluetoothDeviceStateIcon = GetBatteryIcon(chargeLevel);
                }
                catch (Exception ex)
                {
                    string exm = ex.Message;
                }
            }
        }

        string GetBatteryIcon(int chargeLevel)
        {
            if (chargeLevel > 90) return this.FireFlyFullCharageIcon;
            if (chargeLevel <= 90 && chargeLevel > 60) return this.FireFlyNotFullCharageIcon;
            if (chargeLevel <= 60 && chargeLevel > 40) return this.FireFlyMiddleCharageIcon;
            if (chargeLevel <= 40 && chargeLevel >= 0) return this.FireFlyLowCharageIcon;
            return this.FireFlyNotFoundIcon;
        }

        object locker = new object();
        void BluetoothStateChangeConnect(object sender, EventArgs e)
        {
            BluetoothConnectServicer bluetoothConnectServicer = sender as BluetoothConnectServicer;
            if (!(bluetoothConnectServicer.IsBluetoothConnect))
            {
                lock (locker)
                {
                    this.BluetoothDeviceStateIcon = this.FireFlyNotFoundIcon;
                    this.BluetoothDeviceState = "--";
                }
            }
        }

        string bluetoothDeviceStateIcon = "NoFireFly.png";
        public string BluetoothDeviceStateIcon
        {
            get { return bluetoothDeviceStateIcon; }
            set
            {
                if (bluetoothDeviceStateIcon != value)
                {
                    bluetoothDeviceStateIcon = value;
                    OnPropertyChanged("BluetoothDeviceStateIcon");
                }
            }
        }

        string bluetoothDeviceState = "--";
        public string BluetoothDeviceState
        {
            get { return bluetoothDeviceState + "%"; }
            set
            {
                if (bluetoothDeviceState != value)
                {
                    bluetoothDeviceState = value;
                    OnPropertyChanged("BluetoothDeviceState");
                }
            }
        }

        void ConnectionStateChange(object sender, EventArgs e)
        {
            bool isConnected = (sender as ServerWorker).IsInternetConnection;
            if (isConnected)
            {
                this.ServerStateIcon = GoodConnectIcon;
            }
            else
                this.ServerStateIcon = BadConnectIcon;
        }

        string serverStateIcon = "ConnectBadState.png";
        public string ServerStateIcon
        {
            get { return serverStateIcon; }
            set
            {
                if (serverStateIcon != value)
                {
                    serverStateIcon = value;
                    OnPropertyChanged("ServerStateIcon");
                }
            }
        }

        void InitListView()
        {
            PatientTable currentPatient = this.DatabaseWorker.CurrentPatient;
            if (currentPatient != null)
            {
                List<RecordTable> records = this.DatabaseWorker.GetRecordsByPatientID(currentPatient.ID);
                if (records != null && records.Count > 0)
                {
                    List<RecordItem> localListViewSource = new List<RecordItem>();
                    //Формируем список представления и обновляем страницу
                    for (int i = 0; i < records.Count; i++)
                    {

                        localListViewSource.Add(new RecordItem()
                        {
                            Record = records[i],
                            FileName = records[i].FileName,
                            StartDate = "Дата: " + String.Format("{0:dd-MM-yyyy}", records[i].StartRecord),
                            StartTime = "Время: " + String.Format("{0:hh:mm:ss}", records[i].StartRecord),
                            TypeIcon = GetIconByType((ECGreceiveMode)records[i].FileType)
                        });
                    }
                    this.ListViewSource = localListViewSource;
                }
                else
                {
                    //Список пуст
                    this.ListViewSource = null;
                }
            }
            else
            {
                //Не выбран профиль - пишем что не выбран пациент
                this.ListViewSource = null;
            }
        }

        string GetIconByType(ECGreceiveMode mode)
        {
            switch (mode)
            {
                case ECGreceiveMode.HolterECG: return "HolterIcon.png";
                case ECGreceiveMode.MidECG: return "FiveMinIcon.png";
                case ECGreceiveMode.ShortECG: return "TenIcon.png";
            }
            return "HolterIcon.png";
        }

        void CurrentPatientChange(object sender, EventArgs e)
        {
            InitListView();
        }

        private List<RecordItem> listViewSource = null;
        public List<RecordItem> ListViewSource
        {
            get { 
                return this.listViewSource; 
            }
            set
            {
                if (listViewSource != value)
                {
                    listViewSource = value;
                    OnPropertyChanged("ListViewSource");
                }
            }
        }


        private RecordItem recordListItem = null;
        public RecordItem RecordListItem
        {
            get { 
                return recordListItem; 
            }
            set
            {
                if (recordListItem != value)
                {
                    recordListItem = value;
                    if (value != null)
                    {
                        //Переходим на страницу визуализации
                        ViewSavedECG(value.Record);
                        recordListItem = null;
                        OnPropertyChanged("RecordListItem");
                    }
                }
            }
        }


        async void ViewSavedECG(RecordTable record)
        {
            await Navigation.PushAsync(new SavedECGView(record));
        }



        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Dispose()
        {
            ServerWorkerInstance.ChangeConnectionState -= ConnectionStateChange;
            FireFly.BluetoothConnectController.ChangeBluetoothStateEvent -= BluetoothStateChangeConnect;
            FireFly.BatteryHandler.BattaryStateChangeEvent -= NewBattaryState;      
            DatabaseWorker.CurrentPatientChange -= CurrentPatientChange;
            DatabaseWorker.RecordListChange -= CurrentPatientChange;

            DatabaseWorker = null;
            ServerWorkerInstance = null;
            FireFly = null;
        }

        public class RecordItem
        {
            public RecordTable Record { get; set; }
            public string FileName { get; set; }
            public string TypeIcon { get; set; }
            public string StartDate { get; set; }
            public string StartTime { get; set; }
        }


    }
}
