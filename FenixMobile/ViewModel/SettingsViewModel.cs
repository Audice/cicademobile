﻿using FenixMobile.Interfaces;
using FenixMobile.Model.BluetoothPart;
using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using FenixMobile.Model.DatabasePart;
using FenixMobile.Model.ServerPart;
using FenixMobile.Model.SettingsPart;
using FenixMobile.ProtectedMechanism;
using FenixMobile.View;
using FenixMobile.View.HeadMasterMenu.ECGViewerPage;
using FenixMobile.View.HeadMasterMenu.MenuItemPage;
using FenixMobile.View.Interface;
using Plugin.Connectivity.Abstractions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace FenixMobile.ViewModel
{
    public class SettingsViewModel: INotifyPropertyChanged
    {
        public INavigation Navigation { get; set; }

        //ServerState icon
        private readonly string BadConnectIcon = "ConnectBadState.png";
        private readonly string GoodConnectIcon = "ConnectGootState.png";
        //BluetoothState icon
        private readonly string FireFlyNotFoundIcon = "NoFireFly.png";
        private readonly string FireFlyFullCharageIcon = "FullFireFly.png";
        private readonly string FireFlyNotFullCharageIcon = "NotFullFireFly.png";
        private readonly string FireFlyMiddleCharageIcon = "MiddleFireFly.png";
        private readonly string FireFlyLowCharageIcon = "LowFireFly.png";

        SettingsModel SettingsModelInstance;
        ServerWorker ServerWorkerInstance;
        DatabaseWorker DatabaseWorkerInstance;
        FireFly FireFly;

        bool IsLoadOldPackageStart = false;

        public SettingsViewModel()
        {
            SettingsModelInstance = SettingsModel.getSettingsInstance();
            ServerWorkerInstance = ServerWorker.getInstance();
            DatabaseWorkerInstance = DatabaseWorker.getInstance();
            DatabaseWorkerInstance.CurrentUserPatientsCountChange += SetNewPatientList;
            ConnectionState = ServerWorkerInstance.IsInternetConnection ? "Активно" : "Неактивно";
            ServerWorkerInstance.ChangeConnectionState += ConnectionStateChange;
            SetNewPatientList(this, EventArgs.Empty);
            IsFileSaveFlage = SettingsModelInstance.GetSaveFilesFlag();

            //Подпись на несколько событий BluetoothWorker'a
            FireFly = FireFly.getInstance();
            FireFly.BluetoothConnectController.ChangeBluetoothStateEvent += BluetoothStateChangeConnect;
            FireFly.BatteryHandler.BattaryStateChangeEvent += NewBattaryState;
            this.DeviceStateConnection = FireFly.BatteryHandler.ChargePercent > 0 ? "Да" : "Нет";
            this.DeviceChargeState = FireFly.BatteryHandler.ChargePercent > 0 ? FireFly.BatteryHandler.ChargePercent.ToString() : "--";


            //Инициализация DirInfo
            DirInfoFieldInit(this.FireFly.GetFireFlyDirInfo());

            this.FireFly.FireFlyInfoUpdate += DirInfoUpdate;

            this.devicesListItem = this.DatabaseWorkerInstance.GetMACDeviceCurrentUser();
            //Доступные MAC адреса
        }

        void DirInfoFieldInit(FireFlyDirInfo info)
        {
            if (info != null)
                SelectedCountChannels = info.FilesChannels.ToString();
            else
                SelectedCountChannels = null;
        }

        void DirInfoUpdate(object sender, FireFlyDirInfoEventArgs e)
        {
            FireFlyDirInfo fireFlyDirInfo = e.FireFlyDirInfo;
            if (fireFlyDirInfo != null)
            {
                SelectedCountChannels = fireFlyDirInfo.FilesChannels.ToString();
                HasSDCard = fireFlyDirInfo.IsSD_Memory ? "Есть" : "Нет";
                SelectedFrequancy = fireFlyDirInfo.FilesSPS.ToString();
            }
            else
            {
                SelectedCountChannels = "1";
                HasSDCard = "Нет";
                SelectedFrequancy = "500";
            }
        }


        string selectedCountChannels = "1";
        public string SelectedCountChannels
        {
            get { 
                return selectedCountChannels; 
            }
            set
            {
                if (selectedCountChannels != value && value != null)
                {
                    selectedCountChannels = value;
                    if (selectedCountChannels != null)
                    {
                        this.FireFly.BluetoothConnectController.SetCountChannel(int.Parse(selectedCountChannels));
                    }
                    OnPropertyChanged("SelectedCountChannels");
                    //this.FireFly.BluetoothConnectController.GetDirInfo();
                }
            }
        }

        string selectedFrequancy = "500";
        public string SelectedFrequancy
        {
            get
            {
                return selectedFrequancy;
            }
            set
            {
                if (selectedFrequancy != value && value != null)
                {
                    selectedFrequancy = value;
                    if (selectedFrequancy != null)
                    {
                        this.FireFly.BluetoothConnectController.SetSampleFrequency(int.Parse(selectedFrequancy));
                    }
                    OnPropertyChanged("SelectedFrequancy");
                }
            }
        }

        public List<string> MACsDevices
        {
            get { return FireFly != null ? FireFly.GetDevicesList() : new List<string>(); }
            set
            {
                //Не уверен что нужно...
            }
        }

        private string devicesListItem = null;
        public string DevicesListItem
        {
            get { 
                return devicesListItem; 
            }
            set
            {
                if (devicesListItem != value && value != null)
                {
                    devicesListItem = value;
                    this.DatabaseWorkerInstance.SetDevicesMACCurrentUser(devicesListItem);
                    this.FireFly.SetNewDevice(devicesListItem);
                    OnPropertyChanged("DevicesListItem");
                }
            }
        }


        void NewBattaryState(object sender, BatteryEventArgs e)
        {
            ushort chargeLevel = e.ChargeLevel;
            
            lock (locker)
            {
                try
                {
                    this.DeviceChargeState = chargeLevel.ToString();
                    this.DeviceStateConnection = chargeLevel > 0 ? "Да" : "Нет";
                }
                catch (Exception ex)
                {
                    string exm = ex.Message;
                }
            }
        }

        string GetBatteryIcon(int chargeLevel)
        {
            if (chargeLevel > 90) return this.FireFlyFullCharageIcon;
            if (chargeLevel <= 90 && chargeLevel > 60) return this.FireFlyNotFullCharageIcon;
            if (chargeLevel <= 60 && chargeLevel > 40) return this.FireFlyMiddleCharageIcon;
            if (chargeLevel <= 40 && chargeLevel >= 0) return this.FireFlyLowCharageIcon;
            return this.FireFlyNotFoundIcon;
        }

        string deviceStateConnection = "Нет";
        public string DeviceStateConnection
        {
            get { return deviceStateConnection; }
            set
            {
                if (deviceStateConnection != value)
                {
                    deviceStateConnection = value;
                    OnPropertyChanged("DeviceStateConnection");
                }
            }
        }

        string deviceChargeState = "--";
        public string DeviceChargeState
        {
            get { 
                return deviceChargeState + "%"; 
            }
            set
            {
                if (deviceChargeState != value)
                {
                    deviceChargeState = value;
                    OnPropertyChanged("DeviceChargeState");
                }
            }
        }

        object locker = new object();
        void BluetoothStateChangeConnect(object sender, EventArgs e)
        {
            BluetoothConnectServicer bluetoothConnectServicer = sender as BluetoothConnectServicer;
            if (bluetoothConnectServicer.IsBluetoothConnect)
            {

            }
            else
            {
                lock (locker)
                {
                    this.DeviceStateConnection = "Нет";
                    this.DeviceChargeState = "--";
                }
            }
        }

        public bool IsCheckBattaryProcessRun
        {
            get; private set;
        } = false;


        void SetNewPatientList(object sender, EventArgs e)
        {
            List<PatientTable> localPatientsList = DatabaseWorkerInstance.GetListPatientByUserID(this.DatabaseWorkerInstance.CurrentUser.ID);
            this.PatienDataPickerSource = GetPatientPickerItems(localPatientsList);
        }

        private List<PatientPickerItem> GetPatientPickerItems(List<PatientTable> patientsList)
        {
            if (patientsList != null && patientsList.Count > 0)
            {
                List<PatientPickerItem> pickerItems = new List<PatientPickerItem>();
                for (int i = 0; i < patientsList.Count; i++)
                {
                    pickerItems.Add(new PatientPickerItem()
                    {
                        Patient = patientsList[i],
                        PickerPatientInfo = patientsList[i].Surname + " " + patientsList[i].Name + " " + patientsList[i].Patronymic
                    });
                }
                return pickerItems;
            }
            else
            {
                return new List<PatientPickerItem>() { new PatientPickerItem() { PickerPatientInfo = "Пациентов не найдено" } };
            }
        }

        void ConnectionStateChange(object sender, EventArgs e)
        {
            bool isConnected = (sender as ServerWorker).IsInternetConnection;
            if (isConnected)
                this.ConnectionState = "Активно";
            else
                this.ConnectionState = "Неактивно";
        }

        List<PatientPickerItem> patienDataPickerSource = null;
        public List<PatientPickerItem> PatienDataPickerSource
        {
            get 
            {
                return patienDataPickerSource;
            }
            set
            {
                if (value != null && patienDataPickerSource != null)
                {
                    if (patienDataPickerSource.Count > 0)
                    {
                        if (patienDataPickerSource[0].Patient == null)
                        {
                            patienDataPickerSource = value; ///???
                            OnPropertyChanged("PatienDataPickerSource");
                            OnPatientsListUpdate(patienDataPickerSource);
                        }
                        else
                        {
                            if (patienDataPickerSource.Count != value.Count)
                            {
                                patienDataPickerSource = value; ///???
                                OnPropertyChanged("PatienDataPickerSource");
                            }
                        }
                    }
                    else
                    {
                        patienDataPickerSource = value; ///???
                        OnPropertyChanged("PatienDataPickerSource");
                        OnPatientsListUpdate(patienDataPickerSource);
                    }
                }
                if (patienDataPickerSource == null && value != null)
                {
                    patienDataPickerSource = value; 
                    OnPropertyChanged("PatienDataPickerSource");
                    OnPatientsListUpdate(patienDataPickerSource);
                }
                if (patienDataPickerSource == null && value == null)
                {
                    List<PatientTable> localPatientsList = DatabaseWorkerInstance.GetListPatientByUserID(this.DatabaseWorkerInstance.CurrentUser.ID);
                    patienDataPickerSource = GetPatientPickerItems(localPatientsList);
                    OnPropertyChanged("PatienDataPickerSource");
                    OnPatientsListUpdate(patienDataPickerSource);
                }
            }
        }

        void OnPatientsListUpdate(List<PatientPickerItem> patientPickerItems)
        {
            if (patientPickerItems != null && patientPickerItems.Count > 0)
            {
                this.CurrentPatient = patientPickerItems[0];
            }
        }

        
        private PatientPickerItem currentPatient = null;
        public PatientPickerItem CurrentPatient
        {
            get 
            {
                return currentPatient;
            }
            set
            {
                if (value != null && value.Patient != null)
                {
                    currentPatient = value;
                    this.DatabaseWorkerInstance.SetCurrentPatient(currentPatient.Patient);
                    OnPropertyChanged("CurrentPatient");
                    OnCurrentPatientUpdate(currentPatient);
                }
            }
        }

        void OnCurrentPatientUpdate(PatientPickerItem currentPatient)
        {
            this.FullPatientName = currentPatient.PickerPatientInfo;
            this.PatientGrowth = currentPatient.Patient.Growth.ToString();
            this.PatientWeight = currentPatient.Patient.Weight.ToString();
        }

        private string fullPatientName = "";
        public string FullPatientName
        {
            get {
                PatientTable patient = this.DatabaseWorkerInstance.CurrentPatient;
                return patient != null ? "Пользователь: " + patient.Surname + " " + patient.Name + " " + patient.Patronymic : "Пользователь: ";
            }
            set
            {
                if (fullPatientName != value)
                {
                    fullPatientName = value;
                    OnPropertyChanged("FullPatientName");
                }
            }
        }

        private string patientGrowth = "";
        public string PatientGrowth
        {
            get { return this.DatabaseWorkerInstance.CurrentPatient != null ? this.DatabaseWorkerInstance.CurrentPatient.Growth.ToString() : ""; }
            set
            {
                if (patientGrowth != value)
                {
                    int outObject = -1;
                    if (int.TryParse(value, out outObject))
                    {
                        patientGrowth = value;
                        //Обновим значение роста текущего пациента
                        this.DatabaseWorkerInstance.UpdatePatientsGrowth(outObject);
                        OnPropertyChanged("PatientGrowth");
                    }
                    else
                    {
                        patientGrowth = "";
                        OnPropertyChanged("PatientGrowth");
                    }
                }
            }
        }

        private string patientWeight = "";
        public string PatientWeight
        {
            get { return this.DatabaseWorkerInstance.CurrentPatient != null ? this.DatabaseWorkerInstance.CurrentPatient.Weight.ToString() : ""; }
            set
            {
                if (patientWeight != value)
                {
                    int outObject = -1;
                    if (int.TryParse(value, out outObject))
                    {
                        patientWeight = value;
                        //Обновим значение роста текущего пациента
                        this.DatabaseWorkerInstance.UpdatePatientsWeight(outObject);
                        OnPropertyChanged("PatientWeight");
                    }
                    else
                    {
                        patientWeight = "";
                        OnPropertyChanged("PatientWeight");
                    }
                }
            }
        }

        private string hasSDCard = "Нет";
        public string HasSDCard
        {
            get { return hasSDCard; }
            set
            {
                if (hasSDCard != value)
                {
                    hasSDCard = value;
                    OnPropertyChanged("HasSDCard");
                }
            }
        }


        public string ServerURL
        {
            get { return SettingsModelInstance.ServerURL; }
            set
            {
                if (SettingsModelInstance.ServerURL != value)
                {
                    SettingsModelInstance.SetURL(value);
                    OnPropertyChanged("ServerURL");
                }
            }
        }

        string connectionState = "";
        public string ConnectionState
        {
            get { return connectionState; }
            set
            {
                if (connectionState != value)
                {
                    connectionState = value;
                    OnPropertyChanged("ConnectionState");
                }
            }
        }

        //Switch на сохранение
        private bool isFileSaveFlage = false;
        public bool IsFileSaveFlage
        {
            get { return SettingsModelInstance.GetSaveFilesFlag(); }
            set
            {
                if (isFileSaveFlage != value)
                {
                    isFileSaveFlage = value;
                    SettingsModelInstance.SetSaveFilesFlag(value);
                    OnPropertyChanged("IsFileSaveFlage");
                }
            }
        }

        //Switch на сохранение
        private string energySavingState = "Включено";
        public string EnergySavingState
        {
            get { return DependencyService.Get<IBattaryController>().GetIgnoringBatteryOptimizationsPolicy() ? "Выключено" : "Включено"; }
            set
            {
                if (energySavingState != value)
                {
                    energySavingState = value;
                    OnPropertyChanged("EnergySavingState");
                }
            }
        }

        //Команды
        public ICommand AddNewPatient => new Command(async() =>
        {
            if (!DoubleTapSaver.AllowTap) return; DoubleTapSaver.AllowTap = false;
            await Navigation.PushAsync(new Registration());
            DoubleTapSaver.ResumeTap();
        });

        public ICommand GetDirInfo => new Command(() => {
            this.FireFly.BluetoothConnectController.GetDirInfo();
        });
        public ICommand ClearDir => new Command(async() => {
            //this.FireFly.BluetoothConnectController.ClearDir();
            await Navigation.PushAsync(new TestPage());
        });

        public ICommand ChangeBattaryPolicy => new Command(() => {
            DependencyService.Get<IBattaryController>().SetIgnoringBatteryOptimizationsPolicy();
            //Лучше не придумал
            EnergySavingState = DependencyService.Get<IBattaryController>().GetIgnoringBatteryOptimizationsPolicy() ? "Выключено" : "Включено";
        });
        public ICommand UpdateBattaryPolicy => new Command(() => {
            EnergySavingState = DependencyService.Get<IBattaryController>().GetIgnoringBatteryOptimizationsPolicy() ? "Выключено" : "Включено";
        });

        public ICommand SignoutExit => new Command(() => {
            this.DatabaseWorkerInstance.ClearSessionIDByUserID(this.DatabaseWorkerInstance.CurrentUser.ID);
            DependencyService.Get<ISystemFunction>().CloseApplication();
        });
        public ICommand OpenArchive => new Command(async () => { 
            await Navigation.PushAsync(new ArchiveWorkPage());
        });

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }


        public class PatientPickerItem
        {
            public PatientTable Patient { get; set; }
            public string PickerPatientInfo { get; set; }
        }




    }
}
