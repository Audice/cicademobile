﻿using CyberHeartMobile.Classes;
using FenixMobile.Enumeration;
using FenixMobile.Interfaces;
using FenixMobile.Model.BluetoothPart;
using FenixMobile.Model.BluetoothPart.CustomEventArgsClasses;
using FenixMobile.Model.BluetoothPart.DataConvert;
using FenixMobile.Model.BluetoothPart.PackageControl;
using FenixMobile.Model.DatabasePart;
using FenixMobile.Model.DrawPart.DrawPartEventArgs;
using FenixMobile.Model.FilePart;
using FenixMobile.Model.ServerPart;
using FenixMobile.Model.SettingsPart;
using FenixMobile.View.CustomDisplayAlert;
using Rg.Plugins.Popup.Services;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TouchTracking.Forms;
using Xamarin.Forms;

namespace FenixMobile.ViewModel
{
    class SimpleECGViewModel : INotifyPropertyChanged, IDisposable
    {
        //ServerState icon
        private readonly string BadConnectIcon = "ConnectBadState.png";
        private readonly string GoodConnectIcon = "ConnectGootState.png";
        //BluetoothState icon
        private readonly string FireFlyNotFoundIcon = "NoFireFly.png";
        private readonly string FireFlyFullCharageIcon = "FullFireFly.png";
        private readonly string FireFlyNotFullCharageIcon = "NotFullFireFly.png";
        private readonly string FireFlyMiddleCharageIcon = "MiddleFireFly.png";
        private readonly string FireFlyLowCharageIcon = "LowFireFly.png";

        public INavigation Navigation { get; set; }
        public TouchEffect GeneralECGTouchEffect { get; set; }
        public TouchEffect ConcretECGTouchEffect { get; set; }

        ECGreceiveMode ECGreceiveMode = ECGreceiveMode.ShortECG;

        FireFly FireFly;
        DatabaseWorker Database;
        SkiaPaintController SkiaPaintController;
        PackageSequenceControler PackageSequenceControler;
        DataConverter DataConverter;
        ServerWorker ServerWorkerInstance;
        FileWorker FileWorker;
        SKCanvasView GeneralCanvas;
        SKCanvasView ConcretCanvas;
        SOAPPackageSender SOAPPackageSender;
        SettingsModel SettingsModel;

        //Флаги контроля
        private bool IsProcessValueInit = false;
        private bool IsProcessStarted = false;

        private bool IsInfoCame = false;
        public bool IsLoadingAborted = false;

        FireFlyDirInfo FireFlyDirInfo { get; set; } = null;

        public SimpleECGViewModel(ECGreceiveMode ecgreceiveMode, SKCanvasView generalCanvas, SKCanvasView concretCanvas)
        {
            SettingsModel = SettingsModel.getSettingsInstance();
            SettingsModel.IsECGProcessingRun = true;

            this.ConcretCanvas = concretCanvas;
            this.GeneralCanvas = generalCanvas;

            this.ECGreceiveMode = ecgreceiveMode;
            FireFly = FireFly.getInstance();
            FireFly.BluetoothConnectController.ChangeBluetoothStateEvent += BluetoothStateChangeConnect;
            FireFly.BatteryHandler.BattaryStateChangeEvent += NewBattaryState;
            this.BluetoothDeviceState = FireFly.CharageLevel > 0 ? FireFly.CharageLevel.ToString() : "--";
            this.BluetoothDeviceStateIcon = GetBatteryIcon(FireFly.CharageLevel);

            PackageSequenceControler = new PackageSequenceControler(DataTransferMode.Current, null);
            Database = DatabaseWorker.getInstance();
            ServerWorkerInstance = ServerWorker.getInstance();
            ServerStateIcon = ServerWorkerInstance.IsInternetConnection ? "ConnectGootState.png" : "ConnectBadState.png";
            ServerWorkerInstance.ChangeConnectionState += ConnectionStateChange;

            FireFly.FireFlyInfoUpdate += InfoCame;
            FireFly.BluetoothConnectController.SetDirectory((short)FireFly.GetFireFlyDirInfo().NumDirectories);

            RunParallelTask();
        }

        void InitListLeads(short numChannels)
        {
            switch (numChannels)
            {
                case 1: 
                    this.LeadsList = new List<string>() { "I" };
                    break;
                case 2:
                    this.LeadsList = new List<string>() { "I", "II", "III", "aVR", "aVL", "aVF" };
                    break;
                case 8:
                    this.LeadsList = new List<string>() { "I", "II", "III", "aVR", "aVL", "aVF", "V1", "V2", "V3", "V4", "V5", "V6" };
                    break;
                default:
                    this.LeadsList = null;
                    break;
            }
        }

        void InfoCame(object sender, FireFlyDirInfoEventArgs e)
        {
            FireFlyDirInfo = e.FireFlyDirInfo;
            this.IsInfoCame = (e.FireFlyDirInfo != null);
            InitListLeads(FireFlyDirInfo.FilesChannels);
        }


        private async void RunParallelTask()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken token = cts.Token;
            cts.CancelAfter(TimeSpan.FromSeconds(10)); //На подготовку даём 10 секунд

            CustomLoadPage customLoadPage = new CustomLoadPage("Соединение", "Получение информации от устройства");
            await PopupNavigation.Instance.PushAsync(customLoadPage);

            //Run waiter and wait 10 seconds
            try
            {
                Task taskWaiter = Task.Run(() =>
                {
                    while (!this.IsInfoCame && !this.IsLoadingAborted)
                    {
                        token.ThrowIfCancellationRequested();
                    }
                }, token);
                await Task.WhenAll(taskWaiter);
            }
            catch (OperationCanceledException ex)
            {
                this.IsInfoCame = false;
            }
            finally
            {
                if (!IsLoadingAborted)
                {
                    if (this.IsInfoCame)
                    {
                        //Запускаем процесс
                        if (this.FireFlyDirInfo != null)
                        {
                            InitStage();
                        }
                        else
                        {
                            var informationDisplayAlert = new ErrorDisplayAlert("Кардиограф не исправен.\nПерегрузите его и повторить попытку.");
                            await PopupNavigation.Instance.PushAsync(informationDisplayAlert);
                            await Navigation.PopAsync();
                            this.Dispose();
                        }
                    }
                    else
                    {
                        var informationDisplayAlert = new ErrorDisplayAlert("Кардиограф не исправен.\nПерегрузите его и повторить попытку.");
                        await PopupNavigation.Instance.PushAsync(informationDisplayAlert);
                        await Navigation.PopAsync();
                        this.Dispose();
                    }
                }

                cts.Dispose();
                

                await PopupNavigation.Instance.RemovePageAsync(customLoadPage);
            }
        }


        void InitStage()
        {
            FireFly.FireFlyInfoUpdate -= InfoCame;
            DataConverter = new DataConverter(PackageSequenceControler, (uint)this.FireFlyDirInfo.FilesChannels);
            RecordTable record = InitNewRecord(this.FireFlyDirInfo);
            FileWorker = new FileWorker(DataConverter, record);

            this.SOAPPackageSender = new SOAPPackageSender(DataConverter, FileWorker, record, SOAPPackageSender.SendMode.OnlineMode);

            SkiaPaintController = new SkiaPaintController(this.ConcretCanvas, this.GeneralCanvas, DataConverter, 
                GeneralECGTouchEffect, ConcretECGTouchEffect, this.DisplayOrientation,
                Database.CurrentPatient.Surname + " " + Database.CurrentPatient.Name[0] + ". " + Database.CurrentPatient.Patronymic[0],
                Database.CurrentPatient.BornDate.ToString("dd-MM-yyyy"), Database.CurrentPatient.Growth.ToString(), Database.CurrentPatient.Weight.ToString(),
                DateTime.Now.ToString("dd-MM-yyyy"), this.ECGreceiveMode, ViewMode.AnimationMode, this.FireFlyDirInfo.FilesSPS, (uint)this.FireFlyDirInfo.FilesChannels); //???
            SkiaPaintController.DataFilledEvent += ItsTimeToStopOK;

            IsProcessValueInit = true;

            StartESCGStream();
        }

        private RecordTable InitNewRecord(FireFlyDirInfo fireFlyDirInfo)
        {
            if (this.Database.CurrentPatient != null && this.Database.CurrentUser != null)
            {
                DateTime dateStartRecord = DateTime.Now;
                RecordTable record = new RecordTable()
                {
                    CatalogNumber = fireFlyDirInfo.CurrentDirectory, //Получаем из DirInfo
                    ChannelsCount = fireFlyDirInfo.FilesChannels, //Получаем из DirInfo
                    UserID = this.Database.CurrentUser.ID,
                    PatientID = this.Database.CurrentPatient.ID,
                    FileName = this.Database.CurrentPatient.Surname + " " + this.Database.CurrentPatient.Name[0] + ". " + this.Database.CurrentPatient.Patronymic[0] + ". "
                    + String.Format("{0:dd-MM-yyyy hh-mm-ss}", dateStartRecord),
                    FileID = -1, //Появляется при устойчивом интернет соединении и создании файла
                    FileType = (int)this.ECGreceiveMode,
                    SampleFrequency = fireFlyDirInfo.FilesSPS, //Получаем из DirInfo
                    IsFileEnd = false,
                    StartRecord = dateStartRecord,
                    FinishRecord = DateTime.MinValue,
                    LastBluetoothFileNumber = -1,
                    LastServerPackageNumber = -1,
                    LastServerPartNumber = -1,
                    MAC = this.Database.CurrentUser.MACS
                };
                return record;
            }
            return null;
        }

        public DisplayOrientation DisplayOrientation
        {
            get; private set;
        } = DisplayOrientation.VerticalOrientation;



        public void SetDisplayOrientation(double width, double height)
        {
            if (width < height)
                this.DisplayOrientation = DisplayOrientation.VerticalOrientation;
            else
                this.DisplayOrientation = DisplayOrientation.HorizontalOrientation;

            if (this.SkiaPaintController != null)
                this.SkiaPaintController.SetDisplayOrientation(this.DisplayOrientation);
        }




        void StartESCGStream()
        {
            FireFly.BluetoothConnectController.GET_ECGSend();
            IsProcessStarted = true;
        }

        void ItsTimeToStopOK(object sender, EventArgs e)
        {
            if (IsProcessStarted)
            {
                this.SkiaPaintController.IsDrawEnd = true;
                StopESCGStream();
            }
        }

        public void ECGAbort()
        {
            if (IsProcessStarted)
            {
                this.SkiaPaintController.IsDrawEnd = true;
                StopESCGStream();
            }
        }

        void StopESCGStream()
        {
            //FireFly.BluetoothConnectController.StopReadFromBluetooth();
            this.SOAPPackageSender.IsAllProcessStop = true;
            FileWorker.CloseRecord();
            this.Database.HasRecordsListChange = true;
            this.IsProcessStarted = false;
        }

        void NewBattaryState(object sender, BatteryEventArgs e)
        {
            ushort chargeLevel = e.ChargeLevel;

            lock (locker)
            {
                try
                {
                    this.BluetoothDeviceState = chargeLevel.ToString();
                    this.BluetoothDeviceStateIcon = GetBatteryIcon(chargeLevel);
                }
                catch (Exception ex)
                {
                    string exm = ex.Message;
                }
            }
        }

        string GetBatteryIcon(int chargeLevel)
        {
            if (chargeLevel > 90) return this.FireFlyFullCharageIcon;
            if (chargeLevel <= 90 && chargeLevel > 60) return this.FireFlyNotFullCharageIcon;
            if (chargeLevel <= 60 && chargeLevel > 40) return this.FireFlyMiddleCharageIcon;
            if (chargeLevel <= 40 && chargeLevel >= 0) return this.FireFlyLowCharageIcon;
            return this.FireFlyNotFoundIcon;
        }

        object locker = new object();
        void BluetoothStateChangeConnect(object sender, EventArgs e)
        {
            BluetoothConnectServicer bluetoothConnectServicer = sender as BluetoothConnectServicer;
            if (!(bluetoothConnectServicer.IsBluetoothConnect))
            {
                lock (locker)
                {
                    this.BluetoothDeviceStateIcon = this.FireFlyNotFoundIcon;
                    this.BluetoothDeviceState = "--";
                }
            }
        }


        private List<string> leadsList = null;
        public List<string> LeadsList
        {
            get
            {
                return this.leadsList;
            }
            set
            {
                if (value != null && this.leadsList != value)
                {
                    this.leadsList = value;
                    OnPropertyChanged("LeadsList");
                }
            }
        }


        string bluetoothDeviceStateIcon = "NoFireFly.png";
        public string BluetoothDeviceStateIcon
        {
            get { return bluetoothDeviceStateIcon; }
            set
            {
                if (bluetoothDeviceStateIcon != value)
                {
                    bluetoothDeviceStateIcon = value;
                    OnPropertyChanged("BluetoothDeviceStateIcon");
                }
            }
        }

        string bluetoothDeviceState = "--";
        public string BluetoothDeviceState
        {
            get { return bluetoothDeviceState + "%"; }
            set
            {
                if (bluetoothDeviceState != value)
                {
                    bluetoothDeviceState = value;
                    OnPropertyChanged("BluetoothDeviceState");
                }
            }
        }

        void ConnectionStateChange(object sender, EventArgs e)
        {
            bool isConnected = (sender as ServerWorker).IsInternetConnection;
            if (isConnected)
            {
                this.ServerStateIcon = GoodConnectIcon;
            }
            else
                this.ServerStateIcon = BadConnectIcon;
        }

        string serverStateIcon = "ConnectBadState.png";
        public string ServerStateIcon
        {
            get { return serverStateIcon; }
            set
            {
                if (serverStateIcon != value)
                {
                    serverStateIcon = value;
                    OnPropertyChanged("ServerStateIcon");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public async void Dispose()
        {
            CustomLoadPage customLoadPage = new CustomLoadPage("Завершение исследования", "Пожалуйста подождите");
            await PopupNavigation.Instance.PushAsync(customLoadPage);

            this.ECGAbort();
            if (this.IsProcessValueInit)
            {
                SkiaPaintController.Dispose();
                SkiaPaintController = null;
                this.ConcretCanvas = null;

                bool result = await this.SOAPPackageSender.CloseServerFile();
                this.SOAPPackageSender.Dispose();
                this.SOAPPackageSender = null;

                //Соблюдение последовательности обязательно
                DataConverter.Dispose();
                DataConverter = null;
                FileWorker.Dispose();
                FileWorker = null;

                PackageSequenceControler.Dispose();
            }
            FireFly.BluetoothConnectController.ChangeBluetoothStateEvent -= BluetoothStateChangeConnect;
            FireFly.BatteryHandler.BattaryStateChangeEvent -= NewBattaryState;
            ServerWorkerInstance.ChangeConnectionState -= ConnectionStateChange;
            FireFly.FireFlyInfoUpdate -= InfoCame;
            FireFly = null;
            Database = null;
            ServerWorkerInstance = null;
            this.FireFlyDirInfo = null;
            SettingsModel.IsECGProcessingRun = false;
            await PopupNavigation.Instance.RemovePageAsync(customLoadPage);
        }
    }
}
